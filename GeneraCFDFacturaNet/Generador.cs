﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;


namespace GeneraCFDFacturaNet
{
    public class Generador
    {
        public StringBuilder GeneraCFD(DataSet ds)
        {

            Int32 i = 0;
            String Caracter = "";
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("Outputmode=");
            stringBuilder.AppendLine("<Factura>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Comprobante>");
            stringBuilder.AppendLine("version=" + ds.Tables["Comprobante"].Rows[0]["version"].ToString());
            stringBuilder.AppendLine("serie=" + ds.Tables["Comprobante"].Rows[0]["serie"].ToString());
            stringBuilder.AppendLine("folio=" + ds.Tables["Comprobante"].Rows[0]["folio"].ToString());
            stringBuilder.AppendLine("fecha=" + ds.Tables["Comprobante"].Rows[0]["fecha"].ToString());
            stringBuilder.AppendLine("noAprobacion=" + ds.Tables["Comprobante"].Rows[0]["noAprobacion"].ToString());
            stringBuilder.AppendLine("anoAprobacion=" + ds.Tables["Comprobante"].Rows[0]["anoAprobacion"].ToString());
            stringBuilder.AppendLine("certificado=" + ds.Tables["Comprobante"].Rows[0]["certificado"].ToString());
            stringBuilder.AppendLine("tipoDeComprobante=" + ds.Tables["Comprobante"].Rows[0]["tipoDeComprobante"].ToString());
            stringBuilder.AppendLine("formaDePago=" + ds.Tables["Comprobante"].Rows[0]["formaDePago"].ToString());
            //stringBuilder.AppendLine("condicionesDePago=" + ds.Tables["Comprobante"].Rows[0]["condicionesDePago"].ToString() );
            stringBuilder.AppendLine("subtotal=" + ds.Tables["Comprobante"].Rows[0]["subtotal"].ToString());
            //stringBuilder.AppendLine("descuento=" + ds.Tables["Comprobante"].Rows[0]["descuento"].ToString() );
            stringBuilder.AppendLine("iva=" + ds.Tables["Comprobante"].Rows[0]["iva"].ToString());

            //if (Double.Parse(ds.Tables["Comprobante"].Rows[0]["ieps"].ToString()) > 0)
            //{
            //    stringBuilder.AppendLine("ieps=" + ds.Tables["Comprobante"].Rows[0]["ieps"].ToString());
            //}

            stringBuilder.AppendLine("total=" + ds.Tables["Comprobante"].Rows[0]["total"].ToString());
            //stringBuilder.AppendLine("sretencion=" + ds.Tables["Comprobante"].Rows[0]["retencion"].ToString() );
            //stringBuilder.AppendLine("factorRetencionIVA=" + ds.Tables["Comprobante"].Rows[0]["factorRetencionIVA"].ToString() );
            //stringBuilder.AppendLine("retencionISR=" + ds.Tables["Comprobante"].Rows[0]["retencionISR"].ToString() );
            //stringBuilder.AppendLine("factorRetencionISR=" + ds.Tables["Comprobante"].Rows[0]["factorRetencionISR"].ToString() );
            stringBuilder.AppendLine("</Comprobante>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Emisor>");
            stringBuilder.AppendLine("erfc=" + ds.Tables["Emisor"].Rows[0]["erfc"].ToString());
            stringBuilder.AppendLine("enombre=" + ds.Tables["Emisor"].Rows[0]["enombre"].ToString());
            stringBuilder.AppendLine("</Emisor>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<DomicilioFiscal>");
            stringBuilder.AppendLine("ecalle=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecalle"].ToString());
            stringBuilder.AppendLine("enoExterior=" + ds.Tables["DomicilioFiscal"].Rows[0]["enoExterior"].ToString());
            stringBuilder.AppendLine("enoInterior=" + ds.Tables["DomicilioFiscal"].Rows[0]["enoInterior"].ToString());
            stringBuilder.AppendLine("ecolonia=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecolonia"].ToString());
            stringBuilder.AppendLine("elocalidad=" + ds.Tables["DomicilioFiscal"].Rows[0]["elocalidad"].ToString());
            stringBuilder.AppendLine("ereferencia=" + ds.Tables["DomicilioFiscal"].Rows[0]["ereferencia"].ToString());
            stringBuilder.AppendLine("emunicipio=" + ds.Tables["DomicilioFiscal"].Rows[0]["emunicipio"].ToString());
            stringBuilder.AppendLine("eestado=" + ds.Tables["DomicilioFiscal"].Rows[0]["eestado"].ToString());
            stringBuilder.AppendLine("epais=" + ds.Tables["DomicilioFiscal"].Rows[0]["epais"].ToString());
            stringBuilder.AppendLine("ecodigoPostal=" + ds.Tables["DomicilioFiscal"].Rows[0]["ecodigoPostal"].ToString());
            stringBuilder.AppendLine("etel=" + ds.Tables["DomicilioFiscal"].Rows[0]["etel"].ToString());
            stringBuilder.AppendLine("eemail=" + ds.Tables["DomicilioFiscal"].Rows[0]["eemail"].ToString());
            stringBuilder.AppendLine("</DomicilioFiscal>");

            if (ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString().Length > 0)
            {
                stringBuilder.AppendLine("");
                stringBuilder.AppendLine("<ExpedidoEn>");
                stringBuilder.AppendLine("ex_calle=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_calle"].ToString());
                stringBuilder.AppendLine("ex_noExterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noExterior"].ToString());
                stringBuilder.AppendLine("ex_noInterior=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_noInterior"].ToString());
                stringBuilder.AppendLine("ex_colonia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_colonia"].ToString());
                stringBuilder.AppendLine("ex_localidad=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_localidad"].ToString());
                stringBuilder.AppendLine("ex_referencia=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_referencia"].ToString());
                stringBuilder.AppendLine("ex_municipio=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_municipio"].ToString());
                stringBuilder.AppendLine("ex_estado=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_estado"].ToString());
                stringBuilder.AppendLine("ex_pais=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_pais"].ToString());
                stringBuilder.AppendLine("ex_codigoPostal=" + ds.Tables["ExpedidoEn"].Rows[0]["ex_codigoPostal"].ToString());
                stringBuilder.AppendLine("</ExpedidoEn>");
            }

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Receptor>");
            stringBuilder.AppendLine("rfc=" + ds.Tables["Receptor"].Rows[0]["rfc"].ToString());
            stringBuilder.AppendLine("nombre=" + ds.Tables["Receptor"].Rows[0]["nombre"].ToString());
            stringBuilder.AppendLine("noCliente=" + ds.Tables["Receptor"].Rows[0]["noCliente"].ToString());
            stringBuilder.AppendLine("</Receptor>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Cliente>");
            stringBuilder.AppendLine("calle=" + ds.Tables["Cliente"].Rows[0]["calle"].ToString());
            stringBuilder.AppendLine("noExterior=" + ds.Tables["Cliente"].Rows[0]["noExterior"].ToString());
            stringBuilder.AppendLine("noInterior=" + ds.Tables["Cliente"].Rows[0]["noInterior"].ToString());
            stringBuilder.AppendLine("colonia=" + ds.Tables["Cliente"].Rows[0]["colonia"].ToString());
            stringBuilder.AppendLine("localidad=" + ds.Tables["Cliente"].Rows[0]["localidad"].ToString());
            stringBuilder.AppendLine("referencia=" + ds.Tables["Cliente"].Rows[0]["referencia"].ToString());
            stringBuilder.AppendLine("municipio=" + ds.Tables["Cliente"].Rows[0]["municipio"].ToString());
            stringBuilder.AppendLine("estado=" + ds.Tables["Cliente"].Rows[0]["estado"].ToString());
            stringBuilder.AppendLine("pais=" + ds.Tables["Cliente"].Rows[0]["pais"].ToString());
            stringBuilder.AppendLine("codigoPostal=" + ds.Tables["Cliente"].Rows[0]["codigoPostal"].ToString());
            stringBuilder.AppendLine("tel=" + ds.Tables["Cliente"].Rows[0]["tel"].ToString());
            stringBuilder.AppendLine("email=" + ds.Tables["Cliente"].Rows[0]["email"].ToString());
            stringBuilder.AppendLine("</Cliente>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Concepto>");



            foreach (DataRow e in ds.Tables["Concepto"].Rows)
            {
                if (i < 10)
                    Caracter = "0";
                else
                    Caracter = "";

                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_cantidad=" + ds.Tables["Concepto"].Rows[i]["cantidad"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_unidad=" + ds.Tables["Concepto"].Rows[i]["unidad"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_descripcion=" + ds.Tables["Concepto"].Rows[i]["descripcion"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_valorUnitario=" + ds.Tables["Concepto"].Rows[i]["valorUnitario"].ToString());
                stringBuilder.AppendLine("p" + Caracter + (i + 1) + "_importe=" + ds.Tables["Concepto"].Rows[i]["importe"].ToString());

                i += 1;
            }
            stringBuilder.AppendLine("</Concepto>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<Otros>");
            stringBuilder.AppendLine("formato=" + ds.Tables["Otros"].Rows[0]["formato"].ToString());
            stringBuilder.AppendLine("cant_letra=" + ds.Tables["Otros"].Rows[0]["cant_letra"].ToString());
            stringBuilder.AppendLine("factoriva=" + ds.Tables["Otros"].Rows[0]["factoriva"].ToString());
            stringBuilder.AppendLine("moneda=" + ds.Tables["Otros"].Rows[0]["moneda"].ToString());
            stringBuilder.AppendLine("tipoimpresion=" + ds.Tables["Otros"].Rows[0]["tipoimpresion"].ToString());
            stringBuilder.AppendLine("expedicion=" + ds.Tables["Otros"].Rows[0]["expedicion"].ToString());
            stringBuilder.AppendLine("</Otros>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("<addenda>");
            stringBuilder.AppendLine("</addenda>");

            stringBuilder.AppendLine("");
            stringBuilder.AppendLine("</Factura>");

            return stringBuilder;

        }

        public StringBuilder CancelaCFD(DataSet ds)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("Outputmode=");
            stringBuilder.AppendLine("<Factura>");
            stringBuilder.AppendLine("serie=" + ds.Tables["Factura"].Rows[0]["serie"].ToString());
            stringBuilder.AppendLine("folio=" + ds.Tables["Factura"].Rows[0]["folio"].ToString());
            stringBuilder.AppendLine("email=" + ds.Tables["Factura"].Rows[0]["email"].ToString());
            stringBuilder.AppendLine("</Factura>");

            return stringBuilder;
        }

    }
}
