﻿Public Class PantallaProcesando
    Private Sub PantallaProcesando_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me.Timer1.Enabled = False

    End Sub

    Private Sub PantallaProcesando_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Timer1.Enabled = True

    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If Me.ProgressBar1.Value = Me.ProgressBar1.Maximum Then
            Me.Close()
        Else
            Me.ProgressBar1.Value = Me.ProgressBar1.Value + 1
        End If
    End Sub
End Class