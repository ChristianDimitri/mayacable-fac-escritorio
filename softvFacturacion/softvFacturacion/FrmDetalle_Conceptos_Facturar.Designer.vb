<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmDetalle_Conceptos_Facturar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.SiQuito = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContratoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClvUnicanetDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClvTipSerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EsPaqAdicDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.EsSerDigDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.ClvServicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClvSerSeInsertaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DetalleConceptosFacturarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet
        Me.Detalle_Conceptos_FacturarTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.Detalle_Conceptos_FacturarTableAdapter
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleConceptosFacturarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SiQuito, Me.DescripcionDataGridViewTextBoxColumn, Me.ContratoDataGridViewTextBoxColumn, Me.ClvUnicanetDataGridViewTextBoxColumn, Me.ClvTipSerDataGridViewTextBoxColumn, Me.EsPaqAdicDataGridViewCheckBoxColumn, Me.EsSerDigDataGridViewCheckBoxColumn, Me.ClvServicioDataGridViewTextBoxColumn, Me.ClvSerSeInsertaDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.DetalleConceptosFacturarBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(10, 39)
        Me.DataGridView1.Name = "DataGridView1"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(628, 193)
        Me.DataGridView1.TabIndex = 0
        '
        'SiQuito
        '
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.NullValue = False
        Me.SiQuito.DefaultCellStyle = DataGridViewCellStyle10
        Me.SiQuito.HeaderText = "Seleccione"
        Me.SiQuito.Name = "SiQuito"
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "Descripcion"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Trebuchet MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DescripcionDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.Width = 450
        '
        'ContratoDataGridViewTextBoxColumn
        '
        Me.ContratoDataGridViewTextBoxColumn.DataPropertyName = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.Name = "ContratoDataGridViewTextBoxColumn"
        Me.ContratoDataGridViewTextBoxColumn.Width = 5
        '
        'ClvUnicanetDataGridViewTextBoxColumn
        '
        Me.ClvUnicanetDataGridViewTextBoxColumn.DataPropertyName = "Clv_Unicanet"
        Me.ClvUnicanetDataGridViewTextBoxColumn.HeaderText = "Clv_Unicanet"
        Me.ClvUnicanetDataGridViewTextBoxColumn.Name = "ClvUnicanetDataGridViewTextBoxColumn"
        Me.ClvUnicanetDataGridViewTextBoxColumn.Width = 5
        '
        'ClvTipSerDataGridViewTextBoxColumn
        '
        Me.ClvTipSerDataGridViewTextBoxColumn.DataPropertyName = "Clv_TipSer"
        Me.ClvTipSerDataGridViewTextBoxColumn.HeaderText = "Clv_TipSer"
        Me.ClvTipSerDataGridViewTextBoxColumn.Name = "ClvTipSerDataGridViewTextBoxColumn"
        Me.ClvTipSerDataGridViewTextBoxColumn.Width = 5
        '
        'EsPaqAdicDataGridViewCheckBoxColumn
        '
        Me.EsPaqAdicDataGridViewCheckBoxColumn.DataPropertyName = "EsPaqAdic"
        Me.EsPaqAdicDataGridViewCheckBoxColumn.HeaderText = "EsPaqAdic"
        Me.EsPaqAdicDataGridViewCheckBoxColumn.Name = "EsPaqAdicDataGridViewCheckBoxColumn"
        Me.EsPaqAdicDataGridViewCheckBoxColumn.Width = 5
        '
        'EsSerDigDataGridViewCheckBoxColumn
        '
        Me.EsSerDigDataGridViewCheckBoxColumn.DataPropertyName = "EsSerDig"
        Me.EsSerDigDataGridViewCheckBoxColumn.HeaderText = "EsSerDig"
        Me.EsSerDigDataGridViewCheckBoxColumn.Name = "EsSerDigDataGridViewCheckBoxColumn"
        Me.EsSerDigDataGridViewCheckBoxColumn.Width = 5
        '
        'ClvServicioDataGridViewTextBoxColumn
        '
        Me.ClvServicioDataGridViewTextBoxColumn.DataPropertyName = "Clv_Servicio"
        Me.ClvServicioDataGridViewTextBoxColumn.HeaderText = "Clv_Servicio"
        Me.ClvServicioDataGridViewTextBoxColumn.Name = "ClvServicioDataGridViewTextBoxColumn"
        Me.ClvServicioDataGridViewTextBoxColumn.Width = 5
        '
        'ClvSerSeInsertaDataGridViewTextBoxColumn
        '
        Me.ClvSerSeInsertaDataGridViewTextBoxColumn.DataPropertyName = "ClvSer_Se_Inserta"
        Me.ClvSerSeInsertaDataGridViewTextBoxColumn.HeaderText = "ClvSer_Se_Inserta"
        Me.ClvSerSeInsertaDataGridViewTextBoxColumn.Name = "ClvSerSeInsertaDataGridViewTextBoxColumn"
        Me.ClvSerSeInsertaDataGridViewTextBoxColumn.Width = 5
        '
        'DetalleConceptosFacturarBindingSource
        '
        Me.DetalleConceptosFacturarBindingSource.DataMember = "Detalle_Conceptos_Facturar"
        Me.DetalleConceptosFacturarBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Detalle_Conceptos_FacturarTableAdapter
        '
        Me.Detalle_Conceptos_FacturarTableAdapter.ClearBeforeFill = True
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(406, 254)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(113, 45)
        Me.Button8.TabIndex = 201
        Me.Button8.Text = "&Aceptar"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Orange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(525, 254)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(113, 45)
        Me.Button1.TabIndex = 202
        Me.Button1.Text = "&Cerrar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Trebuchet MS", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(626, 23)
        Me.Label1.TabIndex = 203
        Me.Label1.Text = "Seleccione los Paquetes que desea Cancelar"
        '
        'FrmDetalle_Conceptos_Facturar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 311)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.DataGridView1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Name = "FrmDetalle_Conceptos_Facturar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Seleccione los Servicios que desea Cancelar"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleConceptosFacturarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents DetalleConceptosFacturarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents Detalle_Conceptos_FacturarTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.Detalle_Conceptos_FacturarTableAdapter
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents SiQuito As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvUnicanetDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvTipSerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EsPaqAdicDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents EsSerDigDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents ClvServicioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvSerSeInsertaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
