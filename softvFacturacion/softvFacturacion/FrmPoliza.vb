Imports System.Data.SqlClient
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO

Public Class FrmPoliza

    Dim dia As String
    Dim mes As String
    Dim ano As String
    Dim dia2 As String
    Dim mes2 As String
    Dim ano2 As String
    Dim conceptoPoliza As String = ""

    Private Sub FrmPoliza_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.Llena_Poliza()
        If LocopPoliza = "C" Then
            Me.Consulta_Genera_PolizaDataGridView.ReadOnly = True
            Me.ToolStripContainer1.Enabled = False
   
        End If
    End Sub

    Private Sub SUMA()
        Dim DEBE As Double = 0
        Dim HABER As Double = 0
        Dim I As Integer = 0
        For I = 0 To Me.Consulta_Genera_PolizaDataGridView.RowCount - 1
            DEBE = DEBE + Me.Consulta_Genera_PolizaDataGridView.Item(4, I).Value
            HABER = HABER + Me.Consulta_Genera_PolizaDataGridView.Item(5, I).Value
        Next
        Me.TextBox1.Text = Format(CDec(DEBE), "##,##0.00")
        Me.TextBox2.Text = Format(CDec(HABER), "##,##0.00")
    End Sub

 

    Private Sub Consulta_Genera_PolizaBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Consulta_Genera_PolizaBindingNavigatorSaveItem.Click
        Dim conlidia As New SqlClient.SqlConnection(MiConexion)
        conlidia.Open()
        Me.Validate()
        Me.Consulta_Genera_PolizaBindingSource.EndEdit()
        Me.Consulta_Genera_PolizaTableAdapter.Connection = conlidia
        Me.Consulta_Genera_PolizaTableAdapter.Update(Me.DataSetEdgar3.Consulta_Genera_Poliza)
        conlidia.Close()
        modifica()
        SUMA()

    End Sub
    Private Sub modifica()
        Dim conlidia3 As New SqlClient.SqlConnection(MiConexion)
        conlidia3.Open()
        Dim comando As New SqlClient.SqlCommand
        With comando
            .CommandText = "Modificar_Descrip_Poliza"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = conlidia3
            Dim prm As New SqlParameter("@Clv_llave_Poliza", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@descrip", SqlDbType.VarChar, 400)
            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm.Value = CInt(Me.Clv_Llave_PolizaTextBox.Text)
            prm1.Value = Me.ConceptoTextBox.Text
            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            Dim i As Integer = comando.ExecuteNonQuery
        End With
        conlidia3.Close()
    End Sub
    Private Sub Llena_Poliza()
        Try
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Me.Consulta_Genera_PolizaTableAdapter.Connection = Con
            Me.Consulta_Genera_PolizaTableAdapter.Fill(Me.DataSetEdgar3.Consulta_Genera_Poliza, LocGloClv_poliza)
            Me.Dame_Tabla_PolizaTableAdapter.Connection = Con
            Me.Dame_Tabla_PolizaTableAdapter.Fill(Me.DataSetEdgar3.Dame_Tabla_Poliza, LocGloClv_poliza)
            If LocopPoliza <> "N" Then
                Dim ComandoLidia As New SqlClient.SqlCommand
                With ComandoLidia
                    .CommandText = "Dame_Clave_poliza"
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure
                    .Connection = Con
                    Dim Prm1 As New SqlParameter("@Clv_Llave_Poliza", SqlDbType.BigInt)
                    Prm1.Direction = ParameterDirection.Input
                    Prm1.Value = LocGloClv_poliza
                    .Parameters.Add(Prm1)

                    Dim Prm2 As New SqlParameter("@opc", SqlDbType.VarChar, 5)
                    Prm2.Direction = ParameterDirection.Input
                    Prm2.Value = LocopPoliza
                    .Parameters.Add(Prm2)

                    Dim Prm3 As New SqlParameter("@Clave", SqlDbType.BigInt)
                    Prm3.Direction = ParameterDirection.Output
                    Prm3.Value = 0
                    .Parameters.Add(Prm3)

                    Dim i As Integer = ComandoLidia.ExecuteNonQuery()
                    GloPoliza2 = Prm3.Value
                    Me.TextBox3.Text = GloPoliza2
                End With
            ElseIf LocopPoliza = "N" Then
                Me.TextBox3.Text = GloPoliza2
            End If
            
            Con.Close()
            SUMA()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(LocGloClv_poliza) = True Then
            FrmImprimePoliza.Show()
        End If
    End Sub

  
    Private Sub GeneraLayout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GeneraLayout.Click
        genera_archivo()

    End Sub
    Private Sub DameFechaActual()

        Dim con As New SqlConnection(MiConexion)

        con.Open()
        Dim comando As SqlClient.SqlCommand
        Dim reader As SqlDataReader
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = con
            .CommandText = "EXEC DameFechaActual " & LocGloClv_poliza
            .CommandType = CommandType.Text
            .CommandTimeout = 0
            reader = comando.ExecuteReader()
            Using reader
                While reader.Read

                    ano = CStr(reader.GetValue(2))
                    mes = CStr(reader.GetValue(1))
                    dia = CStr(reader.GetValue(0))
                    ano2 = CStr(reader.GetValue(5))
                    mes2 = CStr(reader.GetValue(4))
                    dia2 = CStr(reader.GetValue(3))


                End While
            End Using
        End With
        con.Close()

    End Sub
    Private Sub genera_archivo()
        Dim Referencia_Servicio As String = Nothing
        Try
            Dim CONE As New SqlConnection(MiConexion)
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing
            Dim fecha As String = ""
            Dim fecha2 As String = ""
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
            Dim DiaEntero As Integer = 0
            Dim DiaActual As Integer = 0
            Dim MesEntero As String = ""
            Dim contaX As Integer = 0
            Dim tabla() As String = Nothing
            Dim aux_cuenta As String = ""
            Dim cuenta_Buena As String = ""
            Dim cuenta_Aux As String = ""

            DameFechaActual()
            fecha = ano + mes + dia
            fecha2 = ano2 + mes2 + dia2
            If IsNumeric(dia2) Then
                DiaActual = CInt(dia2)
            Else
                DiaActual = 0
            End If


            If IsNumeric(mes2) = True Then
                DiaEntero = CInt(mes2)
                If DiaEntero = 1 Then
                    MesEntero = "Enero"
                End If
                If DiaEntero = 2 Then
                    MesEntero = "Febrero"
                End If
                If DiaEntero = 3 Then
                    MesEntero = "Marzo"
                End If
                If DiaEntero = 4 Then
                    MesEntero = "Abril"
                End If
                If DiaEntero = 5 Then
                    MesEntero = "Mayo"
                End If
                If DiaEntero = 6 Then
                    MesEntero = "Junio"
                End If
                If DiaEntero = 7 Then
                    MesEntero = "Julio"
                End If
                If DiaEntero = 8 Then
                    MesEntero = "Agosto"
                End If
                If DiaEntero = 9 Then
                    MesEntero = "Septiembre"
                End If
                If DiaEntero = 10 Then
                    MesEntero = "Octubre"
                End If
                If DiaEntero = 11 Then
                    MesEntero = "Noviembre"
                End If
                If DiaEntero = 12 Then
                    MesEntero = "Diciembre"
                End If
            Else
                MesEntero = 0
            End If

            If (result = DialogResult.OK) Then
                Rutatxt = Me.FolderBrowserDialog1.SelectedPath.ToString
                Nom_Archivo = Rutatxt + "\" + "POLIZA" & fecha & ".TXT"
                'Me.FolderBrowserDialog1.ShowDialog()
                Dim fileExists As Boolean
                fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                If fileExists = True Then
                    File.Delete(Nom_Archivo)
                End If

                Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                    Dim indice As String = ""

                    Dim id As String = "P"
                    Dim cuenta As String = ""
                    Dim descripcion As String = ""
                    Dim importe As String = ""
                    Dim tipo As String = ""
                    Dim espacio As String = ""
                    Dim espacio2 As String = ""
                    Dim espacio3 As String = ""
                    Dim espacio4 As String = ""
                    Dim num_poliza As String = ""
                    Dim importe_aux As String = ""
                    Dim Titulo As String = ""
                    Dim Titulo_Bueno As String = ""
                    Dim polizaa As Integer = 0
                    Dim contador As Integer = 0
                    Dim dia_aux As String = ""



                    Concepto_poliza()
                    '=== INDICE ===
                    dia_aux = Rellena_Text2((CStr(DiaActual)), 2, " ")
                    Titulo_Bueno = Mid(Trim(conceptoPoliza), 1, 100) & Microsoft.VisualBasic.Strings.Space(100 - Len(Mid(Trim(conceptoPoliza), 1, 100)))
                    num_poliza = Rellena_Text(LocGloClv_poliza, 3, "0")
                    indice = id + "  " + fecha2 + "    " + "1" + "        " + dia_aux + " " + "1" + " " + "0" + "          " + Titulo_Bueno + " " + "11" + " " + "0 0 "
                    sw.Write(indice & vbNewLine)


                    CONE.Open()
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CONE
                        .CommandText = "EXEC Muestra_Detalle_Poliza " & LocGloClv_poliza
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        reader = comando.ExecuteReader()
                        polizaa = COUNT_MUESTRA(LocGloClv_poliza)

                        Using reader
                            While reader.Read
                                If reader.GetValue(0) Is Nothing Then
                                    Exit While
                                End If
                                contador = contador + 1
                                importe_aux = CStr(Format(CDec(reader.GetValue(3)), "####0.00"))
                                cuenta_Aux = Trim(reader.GetValue(1))
                                cuenta_Buena = ""
                                aux_cuenta = ""
                                For contaX = 1 To Len(cuenta_Aux)
                                    aux_cuenta = Mid(cuenta_Aux, contaX, 1)
                                    If aux_cuenta = "-" Then
                                        aux_cuenta = ""
                                    End If
                                    cuenta_Buena = cuenta_Buena + aux_cuenta

                                Next
                                'MsgBox(cuenta_Buena)

                                cuenta = Mid(Trim(cuenta_Buena), 1, 20) & Microsoft.VisualBasic.Strings.Space(20 - Len(Mid(Trim(cuenta_Buena), 1, 20)))
                                espacio = Microsoft.VisualBasic.Strings.Space(10)
                                tipo = CStr(reader.GetValue(5))
                                'importe = Mid(Trim(reader.GetValue(3)), 1, 16) & Microsoft.VisualBasic.Strings.Space(16 - Len(Mid(Trim(reader.GetValue(3)), 16, 16)))
                                importe_aux = Rellena_Text2((importe_aux), 20, " ")
                                espacio2 = "0"
                                espacio3 = Microsoft.VisualBasic.Strings.Space(10)
                                espacio4 = Microsoft.VisualBasic.Strings.Space(18)

                                descripcion = Mid(Trim(reader.GetValue(2)), 1, 100) & Microsoft.VisualBasic.Strings.Space(100 - Len(Mid(Trim(reader.GetValue(2)), 1, 100)))
                                sw.Write("M" + "  " + cuenta + " " + espacio + "I-" + dia_aux + "       " + tipo + " " + importe_aux + " " + espacio2 + espacio3 + "0.0" + espacio4 + descripcion + " " + "   0 ")

                                If contador <= polizaa Then
                                    sw.Write(vbNewLine)
                                End If


                            End While

                        End Using
                    End With
                    CONE.Close()



                    sw.Close()
                End Using

                MsgBox("El archivo se genero en la siguiente ruta : " & Nom_Archivo)
            End If
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & Referencia_Servicio & " son Invalidos")
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Concepto_poliza()
        Dim con As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("Concepto_poliza", con)
        com.CommandType = CommandType.StoredProcedure

        Dim par1 As New SqlParameter("@CLV_POLIZA", SqlDbType.Int)
        Dim par2 As New SqlParameter("@CONCEPTO", SqlDbType.VarChar, 250)

        par1.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Output

        par1.Value = CInt(Me.Clv_Llave_PolizaTextBox.Text)

        com.Parameters.Add(par1)
        com.Parameters.Add(par2)

        Try
            con.Open()
            com.ExecuteNonQuery()
            conceptoPoliza = par2.Value.ToString
        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            con.Close()
            con.Dispose()

        End Try

    End Sub

    Function COUNT_MUESTRA(ByVal POLIZA As Long) As Integer

        Dim CON As New SqlConnection(MiConexion)
        Dim com As New SqlCommand("COUNT_MUESTRA", CON)

        com.CommandType = CommandType.StoredProcedure

        Dim par As New SqlParameter("@num_poliza", SqlDbType.BigInt)
        Dim par2 As New SqlParameter("@COUNT", SqlDbType.Int)

        par.Direction = ParameterDirection.Input
        par2.Direction = ParameterDirection.Output

        par.Value = POLIZA

        com.Parameters.Add(par)
        com.Parameters.Add(par2)

        Try
            CON.Open()
            com.ExecuteNonQuery()

            Return par2.Value

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            CON.Dispose()
            CON.Close()

        End Try

    End Function
    
End Class