﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class FrmGeneracionFF

    Dim lista As New List(Of Concepto)
    Dim concepto As Concepto
    Dim subTotal As Decimal
    Dim Serie As String
    Dim Folio As Integer


    Private Sub MuestraDatosFiscalesCFDSucursales(ByVal Op As Integer, ByVal Serie As String, ByVal Calle As String, ByVal Colonia As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim sBuilder As New StringBuilder("EXEC MuestraDatosFiscalesCFDSucursales " + Op.ToString() + ", '" + Serie + "', '" + Calle + "', '" + Colonia + "'")
        Dim dAdapter As New SqlDataAdapter(sBuilder.ToString(), conexion)
        Dim dTable As New DataTable
        Dim bSource As New BindingSource

        Try
            dAdapter.Fill(dTable)
            bSource.DataSource = dTable
            cbSerie.DataSource = bSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Public Sub GeneraFacturaCFD(ByVal Clv_Sucursal As Integer, ByVal Serie As String, ByVal SubTotal As Decimal, ByVal Iva As Decimal, ByVal Total As Decimal)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        Dim dataSet As New DataSet
        Dim generador As New GeneraCFDFacturaNet.Generador
        Dim cadena As StringBuilder
        Dim arreglo() As Byte
        Dim encoding As New System.Text.UTF8Encoding

        strSQL.Append("EXEC GeneraFacturaCFDX " + Clv_Sucursal.ToString + ", '" + Serie + "', " + SubTotal.ToString + ", " + Iva.ToString + ", " + Total.ToString)

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try

            dataAdapter.Fill(dataSet)
            dataSet.Tables(0).TableName = "Comprobante"
            dataSet.Tables(1).TableName = "Emisor"
            dataSet.Tables(2).TableName = "DomicilioFiscal"
            dataSet.Tables(3).TableName = "ExpedidoEn"
            dataSet.Tables(4).TableName = "Otros"
            dataSet.Tables(5).TableName = "addenda"

            cadena = GeneraCFD(dataSet)
            arreglo = encoding.GetBytes(cadena.ToString())

            Dim fS As New FileStream(eRutaCFD + Serie + Folio.ToString + ".txt", FileMode.Create)
            fS.Write(arreglo, 0, arreglo.Length)
            fS.Flush()
            fS.Close()
            fS.Dispose()

            Rename(eRutaCFD + Serie + Folio.ToString + ".txt", eRutaCFD + Serie + Folio.ToString + ".ff")

            NueComprobantesCFDX(Serie, Folio, GloUsuario, arreglo)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub NueComprobantesCFDX(ByVal Serie As String, ByVal Folio As Integer, ByVal Usuario As String, ByVal Archivo As Byte())
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueComprobantesCFDX", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
        par1.Direction = ParameterDirection.Input
        par1.Value = Serie
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Folio", SqlDbType.Int)
        par2.Direction = ParameterDirection.Input
        par2.Value = Folio
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Usuario", SqlDbType.VarChar, 5)
        par3.Direction = ParameterDirection.Input
        par3.Value = Usuario
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Archivo", SqlDbType.VarBinary, 8000)
        par4.Direction = ParameterDirection.Input
        par4.Value = Archivo
        comando.Parameters.Add(par4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub bnAgregar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnAgregar.Click
        If tbDescripcion.Text.Length = 0 Then
            MsgBox("Captura una descripción.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If tbImporte.Text.Length = 0 Then
            MsgBox("Capura un importe.", MsgBoxStyle.Information)
            Exit Sub
        End If

        concepto = New Concepto
        concepto.descripcion = tbDescripcion.Text
        concepto.importe = tbImporte.Text

        lista.Add(concepto)
        dgConcepto.DataSource = New List(Of Concepto)
        dgConcepto.DataSource = lista


        tbDescripcion.Clear()
        tbImporte.Clear()

    End Sub

    Private Sub bnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnEliminar.Click
        If dgConcepto.Rows.Count = 0 Then
            MsgBox("Selecciona un concepto.", MsgBoxStyle.Information)
            Exit Sub
        End If
        Dim pos As Integer
        pos = dgConcepto.CurrentRow.Index
        lista.RemoveAt(pos)

        dgConcepto.DataSource = New List(Of Concepto)
        dgConcepto.DataSource = lista
    End Sub

    Private Sub bnGenerar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnGenerar.Click

        If dgConcepto.Rows.Count = 0 Then
            MsgBox("No se ha agregado al menos un concepto.", MsgBoxStyle.Information)
            Exit Sub
        End If

        Dim x As Integer
        subTotal = 0

        For x = 0 To dgConcepto.Rows.Count - 1
            subTotal = subTotal + dgConcepto.Item(1, x).Value
        Next

        If Decimal.Parse(tbSubTotal.Text) <> subTotal Then
            MsgBox("El subtotal es diferente a la suma de los importes de los conceptos.", MsgBoxStyle.Information)
            Exit Sub
        End If

        GeneraFacturaCFD(Locclv_sucursalglo, cbSerie.Text, tbSubTotal.Text, tbIVA.Text, tbTotal.Text)

        MsgBox("Se generó una Factura Fiscal Electrónica con el nombre de " + Serie + Folio.ToString() + ".ff")



        Limpirar()

    End Sub



    Function GeneraCFD(ByVal ds As DataSet) As StringBuilder


        Dim i As Integer = 0
        Dim x As Integer = 0
        Dim Caracter As String = ""
        Dim Variable As Decimal = 0
        Serie = ""
        Folio = 0
        Dim stringBuilder As New StringBuilder


        Serie = ds.Tables("Comprobante").Rows(0)("serie").ToString()
        Folio = CInt(ds.Tables("Comprobante").Rows(0)("folio").ToString())

        stringBuilder.AppendLine("Outputmode=")
        stringBuilder.AppendLine("<Factura>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Comprobante>")
        stringBuilder.AppendLine("version=" + ds.Tables("Comprobante").Rows(0)("version").ToString())
        stringBuilder.AppendLine("serie=" + ds.Tables("Comprobante").Rows(0)("serie").ToString())
        stringBuilder.AppendLine("folio=" + ds.Tables("Comprobante").Rows(0)("folio").ToString())
        stringBuilder.AppendLine("fecha=" + ds.Tables("Comprobante").Rows(0)("fecha").ToString())
        stringBuilder.AppendLine("noAprobacion=" + ds.Tables("Comprobante").Rows(0)("noAprobacion").ToString())
        stringBuilder.AppendLine("anoAprobacion=" + ds.Tables("Comprobante").Rows(0)("anoAprobacion").ToString())
        stringBuilder.AppendLine("certificado=" + ds.Tables("Comprobante").Rows(0)("certificado").ToString())
        stringBuilder.AppendLine("tipoDeComprobante=" + ds.Tables("Comprobante").Rows(0)("tipoDeComprobante").ToString())
        stringBuilder.AppendLine("formaDePago=" + ds.Tables("Comprobante").Rows(0)("formaDePago").ToString())
        'stringBuilder.AppendLine("condicionesDePago=" + ds.Tables("Comprobante").Rows(0)("condicionesDePago").ToString() )
        stringBuilder.AppendLine("subtotal=" + ds.Tables("Comprobante").Rows(0)("subtotal").ToString())
        'stringBuilder.AppendLine("descuento=" + ds.Tables("Comprobante").Rows(0)("descuento").ToString() )
        stringBuilder.AppendLine("iva=" + ds.Tables("Comprobante").Rows(0)("iva").ToString())

        'if (Double.Parse(ds.Tables("Comprobante").Rows(0)("ieps").ToString()) > 0)
        '{
        '    stringBuilder.AppendLine("ieps=" + ds.Tables("Comprobante").Rows(0)("ieps").ToString())
        '}

        stringBuilder.AppendLine("total=" + ds.Tables("Comprobante").Rows(0)("total").ToString())
        'stringBuilder.AppendLine("sretencion=" + ds.Tables("Comprobante").Rows(0)("retencion").ToString() )
        'stringBuilder.AppendLine("factorRetencionIVA=" + ds.Tables("Comprobante").Rows(0)("factorRetencionIVA").ToString() )
        'stringBuilder.AppendLine("retencionISR=" + ds.Tables("Comprobante").Rows(0)("retencionISR").ToString() )
        'stringBuilder.AppendLine("factorRetencionISR=" + ds.Tables("Comprobante").Rows(0)("factorRetencionISR").ToString() )
        stringBuilder.AppendLine("</Comprobante>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Emisor>")
        stringBuilder.AppendLine("erfc=" + ds.Tables("Emisor").Rows(0)("erfc").ToString())
        stringBuilder.AppendLine("enombre=" + ds.Tables("Emisor").Rows(0)("enombre").ToString())
        stringBuilder.AppendLine("</Emisor>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<DomicilioFiscal>")
        stringBuilder.AppendLine("ecalle=" + ds.Tables("DomicilioFiscal").Rows(0)("ecalle").ToString())
        stringBuilder.AppendLine("enoExterior=" + ds.Tables("DomicilioFiscal").Rows(0)("enoExterior").ToString())
        stringBuilder.AppendLine("enoInterior=" + ds.Tables("DomicilioFiscal").Rows(0)("enoInterior").ToString())
        stringBuilder.AppendLine("ecolonia=" + ds.Tables("DomicilioFiscal").Rows(0)("ecolonia").ToString())
        stringBuilder.AppendLine("elocalidad=" + ds.Tables("DomicilioFiscal").Rows(0)("elocalidad").ToString())
        stringBuilder.AppendLine("ereferencia=" + ds.Tables("DomicilioFiscal").Rows(0)("ereferencia").ToString())
        stringBuilder.AppendLine("emunicipio=" + ds.Tables("DomicilioFiscal").Rows(0)("emunicipio").ToString())
        stringBuilder.AppendLine("eestado=" + ds.Tables("DomicilioFiscal").Rows(0)("eestado").ToString())
        stringBuilder.AppendLine("epais=" + ds.Tables("DomicilioFiscal").Rows(0)("epais").ToString())
        stringBuilder.AppendLine("ecodigoPostal=" + ds.Tables("DomicilioFiscal").Rows(0)("ecodigoPostal").ToString())
        stringBuilder.AppendLine("etel=" + ds.Tables("DomicilioFiscal").Rows(0)("etel").ToString())
        stringBuilder.AppendLine("eemail=" + ds.Tables("DomicilioFiscal").Rows(0)("eemail").ToString())
        stringBuilder.AppendLine("</DomicilioFiscal>")

        If (ds.Tables("ExpedidoEn").Rows(0)("ex_calle").ToString().Length > 0) Then

            stringBuilder.AppendLine("")
            stringBuilder.AppendLine("<ExpedidoEn>")
            stringBuilder.AppendLine("ex_calle=" + ds.Tables("ExpedidoEn").Rows(0)("ex_calle").ToString())
            stringBuilder.AppendLine("ex_noExterior=" + ds.Tables("ExpedidoEn").Rows(0)("ex_noExterior").ToString())
            stringBuilder.AppendLine("ex_noInterior=" + ds.Tables("ExpedidoEn").Rows(0)("ex_noInterior").ToString())
            stringBuilder.AppendLine("ex_colonia=" + ds.Tables("ExpedidoEn").Rows(0)("ex_colonia").ToString())
            stringBuilder.AppendLine("ex_localidad=" + ds.Tables("ExpedidoEn").Rows(0)("ex_localidad").ToString())
            stringBuilder.AppendLine("ex_referencia=" + ds.Tables("ExpedidoEn").Rows(0)("ex_referencia").ToString())
            stringBuilder.AppendLine("ex_municipio=" + ds.Tables("ExpedidoEn").Rows(0)("ex_municipio").ToString())
            stringBuilder.AppendLine("ex_estado=" + ds.Tables("ExpedidoEn").Rows(0)("ex_estado").ToString())
            stringBuilder.AppendLine("ex_pais=" + ds.Tables("ExpedidoEn").Rows(0)("ex_pais").ToString())
            stringBuilder.AppendLine("ex_codigoPostal=" + ds.Tables("ExpedidoEn").Rows(0)("ex_codigoPostal").ToString())
            stringBuilder.AppendLine("</ExpedidoEn>")
        End If

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Receptor>")
        stringBuilder.AppendLine("rfc=" + tbrfcR.Text)
        stringBuilder.AppendLine("nombre=" + tbNombreR.Text)
        stringBuilder.AppendLine("noCliente=")
        stringBuilder.AppendLine("</Receptor>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Cliente>")
        stringBuilder.AppendLine("calle=" + tbCalleR.Text)
        stringBuilder.AppendLine("noExterior=" + tbNoExtR.Text)
        stringBuilder.AppendLine("noInterior=" + tbNoIntR.Text)
        stringBuilder.AppendLine("colonia=" + tbColoniaR.Text)
        stringBuilder.AppendLine("localidad=" + tbLocalidadR.Text)
        stringBuilder.AppendLine("referencia=" + tbReferenciaR.Text)
        stringBuilder.AppendLine("municipio=" + tbMunicipioR.Text)
        stringBuilder.AppendLine("estado=" + tbEstadoR.Text)
        stringBuilder.AppendLine("pais=" + tbPaisR.Text)
        stringBuilder.AppendLine("codigoPostal=" + tbCodigoPostalR.Text)
        stringBuilder.AppendLine("tel=" + tbTelefonoR.Text)
        stringBuilder.AppendLine("email=" + tbEmailR.Text)
        stringBuilder.AppendLine("</Cliente>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Concepto>")

        For x = 0 To dgConcepto.Rows.Count - 1

            If (i < 10) Then
                Caracter = "0"
            Else
                Caracter = ""
            End If

            Variable = 0
            Variable = dgConcepto.Item(1, x).Value

            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_cantidad=1")
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_unidad=SERVICIO")
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_descripcion=" + dgConcepto.Item(0, x).Value.ToString)
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_valorUnitario=" + Variable.ToString())
            stringBuilder.AppendLine("p" + Caracter.ToString + (i + 1).ToString + "_importe=" + Variable.ToString())

            i += 1

        Next

        stringBuilder.AppendLine("</Concepto>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<Otros>")
        stringBuilder.AppendLine("formato=" + ds.Tables("Otros").Rows(0)("formato").ToString())
        stringBuilder.AppendLine("cant_letra=" + ds.Tables("Otros").Rows(0)("cant_letra").ToString())
        stringBuilder.AppendLine("factoriva=" + ds.Tables("Otros").Rows(0)("factoriva").ToString())
        stringBuilder.AppendLine("moneda=" + ds.Tables("Otros").Rows(0)("moneda").ToString())
        stringBuilder.AppendLine("tipoimpresion=" + ds.Tables("Otros").Rows(0)("tipoimpresion").ToString())
        stringBuilder.AppendLine("expedicion=" + ds.Tables("Otros").Rows(0)("expedicion").ToString())
        stringBuilder.AppendLine("</Otros>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("<addenda>")
        stringBuilder.AppendLine("</addenda>")

        stringBuilder.AppendLine("")
        stringBuilder.AppendLine("</Factura>")

        Return stringBuilder

    End Function

    Private Sub Limpirar()

        tbSubTotal.Clear()
        tbIVA.Clear()
        tbTotal.Clear()
        tbrfcR.Clear()
        tbNombreR.Clear()
        tbCalleR.Clear()
        tbNoExtR.Clear()
        tbNoIntR.Clear()
        tbColoniaR.Clear()
        tbLocalidadR.Clear()
        tbReferenciaR.Clear()
        tbMunicipioR.Clear()
        tbEstadoR.Clear()
        tbPaisR.Clear()
        tbCodigoPostalR.Clear()
        tbTelefonoR.Clear()
        tbEmailR.Clear()

        lista = New List(Of Concepto)
        dgConcepto.DataSource = lista

    End Sub

    Private Sub bnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnSalir.Click
        Me.Close()
    End Sub

    Private Sub FrmGeneracionFF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'colorea(Me)
        MuestraDatosFiscalesCFDSucursales(0, "", "", "")
    End Sub
End Class



Public Class Concepto

    Dim _descripcion As String
    Dim _importe As Decimal

    Public Property descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal Value As String)
            _descripcion = Value
        End Set
    End Property
    Public Property importe() As Decimal
        Get
            Return _importe
        End Get
        Set(ByVal Value As Decimal)
            _importe = Value
        End Set
    End Property

End Class