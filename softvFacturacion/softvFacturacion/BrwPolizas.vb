Imports System.Data.SqlClient

Public Class BrwPolizas

    Private Sub busca(ByVal op As Integer)
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Select Case op
            Case 0
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", "")
            Case 1
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, CLng(Me.TextBox1.Text), "", "")
            Case 2
                Me.Busca_PolizaTableAdapter.Connection = con
                Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, CStr(DateTimePicker1.Value), "")
                'Case 3
                '    Me.Busca_PolizaTableAdapter.Connection = con
                '   Me.Busca_PolizaTableAdapter.Fill(Me.ProcedimientosArnoldo2.Busca_Poliza, op, 0, "", Me.TextBox4.Text)
        End Select

    End Sub

    Private Sub BrwPolizas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If Locbndactualizapoiza = True Then
            Locbndactualizapoiza = False
            busca(0)
        End If
    End Sub



    Private Sub BrwPolizas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.StartPosition = FormStartPosition.CenterScreen
        colorea(Me)
        busca(0)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        busca(1)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busca(2)

    End Sub

    

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub



    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        If Asc(e.KeyChar) = 13 Then
            busca(2)
        End If
    End Sub

    
    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        LocopPoliza = "C"
        LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        FrmPoliza.Show()

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Dim COn As New SqlConnection(MiConexion)
        'COn.Open()
        'LocopPoliza = "N"
        'Me.DameClv_Session_ServiciosTableAdapter.Connection = COn
        'Me.DameClv_Session_ServiciosTableAdapter.Fill(Me.DataSetarnoldo.DameClv_Session_Servicios, LocClv_session)
        'COn.Close()
        'LocbndPolizaCiudad = True
        'FrmSelCiudad.Show()
        LocopPoliza = "N"
        'Dim CON As New SqlConnection(MiConexion)
        'CON.Open()
        'Dim comando As SqlClient.SqlCommand
        'comando = New SqlClient.SqlCommand
        'With comando
        '    .Connection = CON
        '    .CommandText = "DameClv_Session_Servicios "
        '    .CommandType = CommandType.StoredProcedure
        '    .CommandTimeout = 0
        '    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        '    prm.Direction = ParameterDirection.Output
        '    prm.Value = LocClv_session
        '    .Parameters.Add(prm)
        '    Dim i As Integer = comando.ExecuteNonQuery()
        '    LocClv_session = prm.Value
        'End With
        'CON.Close()
        gloClv_Session = 0
        LocbndPolizaCiudad = True
        FrmSelCiudad.Show()
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        LocopPoliza = "M"
        LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        FrmPoliza.Show()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If IsNumeric(LocGloClv_poliza) = True Then
            Dim cONe As New SqlConnection(MiConexion)
            cONe.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = cONe
                .CommandText = "EXEC BORRA_Genera_Poliza " & LocGloClv_poliza
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
            End With
            cONe.Close()
            busca(0)
        Else
            MsgBox("Seleccione la P�liza ", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Clv_calleLabel2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.Click

    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            LocGloClv_poliza = CLng(Me.Clv_calleLabel2.Text)
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(LocGloClv_poliza) = True Then
            FrmImprimePoliza.Show()
        End If
    End Sub
End Class