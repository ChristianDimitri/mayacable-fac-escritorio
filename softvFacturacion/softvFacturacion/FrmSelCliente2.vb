Imports System.Data.SqlClient
Public Class FrmSelCliente2
    Private Sub CREAARBOL()
        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next

            If IsNumeric(Me.lblContratoDatosD.Text) = True Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(Me.lblContratoDatosD.Text, Long)))
                CON.Close()
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TrVDatosCliente.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TrVDatosCliente.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 16) = "Servicio De Tele" Then
                    Me.TrVDatosCliente.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TrVDatosCliente.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TrVDatosCliente.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TrVDatosCliente.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TrVDatosCliente.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TrVDatosCliente.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TrVDatosCliente.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TrVDatosCliente.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            Me.TrVDatosCliente.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                            pasa = False
                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            Me.TrVDatosCliente.Nodes(0).ExpandAll()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub DGDatosCliente_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DGDatosCliente.DoubleClick
        If IsNumeric(Me.lblContratoDatosD.Text) = True Then
            Glocontratosel = Me.lblContratoDatosD.Text
            If bnd1conciliacion = True Then
                bnd1conciliacion = False
                bnd2conciliacion = True
            End If
            Me.Close()
        End If
    End Sub
    Private Sub busca(ByVal op As Integer)
        Dim CON As New SqlClient.SqlConnection(MiConexion)
        Dim Validacion As String = String.Empty

        If op > 0 Then
            If IsNumeric(Me.TxtContrato.Text) = True Then
                Validacion = Me.TxtContrato.Text
            Else
                Validacion = "0"
            End If
            CON.Open()
            Me.BuscaCliPorApellidosTableAdapter.Connection = CON
            Me.BuscaCliPorApellidosTableAdapter.Fill(Me.ProcedimientosArnoldo3.BuscaCliPorApellidos, CLng(Validacion), Me.TxtNombre.Text, Me.TxtApeP.Text, Me.TxtApeM.Text, Me.TxtCalle.Text, Me.TxtNumero.Text, Me.TxtCiudad.Text, Me.TxtTelefono.Text, 0)
            CON.Close()
        ElseIf op = 0 Then
            CON.Open()
            Me.BuscaCliPorApellidosTableAdapter.Connection = CON
            Me.BuscaCliPorApellidosTableAdapter.Fill(Me.ProcedimientosArnoldo3.BuscaCliPorApellidos, 0, "", "", "", "", "", "", "", 0)
            CON.Close()
        End If
    End Sub
    Private Sub FrmSelCliente2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        busca(0)
    End Sub

    Private Sub TxtContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtContrato.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TxtContrato, Asc(LCase(e.KeyChar)), "N")))
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

   

    Private Sub TxtNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtNombre.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub


    Private Sub TxtApeP_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtApeP.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub TxtApeM_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtApeM.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub TxtCalle_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCalle.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub TxtNumero_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtNumero.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub TxtCiudad_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtCiudad.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        busca(1)
    End Sub
    Private Sub Limpiar()
        Me.TxtContrato.Clear()
        Me.TxtNombre.Clear()
        Me.TxtApeM.Clear()
        Me.TxtApeP.Clear()
        Me.TxtCiudad.Clear()
        Me.TxtCalle.Clear()
        Me.TxtNumero.Clear()
        Me.TxtTelefono.Clear()
    End Sub
    Private Sub btnLimpiarB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarB.Click
        Limpiar()
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If IsNumeric(Me.lblContratoDatosD.Text) = True Then
            Glocontratosel = Me.lblContratoDatosD.Text
            If bnd1conciliacion = True Then
                bnd1conciliacion = False
                bnd2conciliacion = True
            End If
            Me.Close()
        End If
    End Sub

    Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
        Me.Close()
    End Sub

    Private Sub ckSoloInternetDatos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckSoloInternetDatos.CheckedChanged
        If Me.ckSoloInternetDatos.Checked = False Then
            Me.ckSoloInternetDatos.Enabled = False
        Else
            Me.ckSoloInternetDatos.Enabled = True
        End If
    End Sub

    Private Sub ckEsHotelDatos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckEsHotelDatos.CheckedChanged
        If Me.ckEsHotelDatos.Checked = False Then
            Me.ckEsHotelDatos.Enabled = False
        Else
            Me.ckEsHotelDatos.Enabled = True
        End If
    End Sub

 
    Private Sub lblContratoDatosD_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblContratoDatosD.TextChanged
        Try
            CREAARBOL()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

 
    Private Sub TxtTelefono_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtTelefono.KeyPress
        e.KeyChar = ChrW(ValidaKey(TxtTelefono, Asc(e.KeyChar), "N"))
        If Asc(e.KeyChar) = 13 Then
            busca(1)
        End If
    End Sub
End Class