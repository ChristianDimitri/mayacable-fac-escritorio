<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BwrEntregasParciales
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ConsecutivoLabel As System.Windows.Forms.Label
        Dim NomCajeraLabel As System.Windows.Forms.Label
        Dim NumeroCepsaLabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim NomRecibioLabel As System.Windows.Forms.Label
        Dim FechaLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.FechaLabel1 = New System.Windows.Forms.Label
        Me.BUSCAPARCIALESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet
        Me.BUSCAPARCIALESBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label
        Me.CMBConsecutivoTextBox = New System.Windows.Forms.TextBox
        Me.CMBNomCajeraTextBox = New System.Windows.Forms.TextBox
        Me.CMBNumeroCepsaTextBox = New System.Windows.Forms.TextBox
        Me.CMBImporteTextBox = New System.Windows.Forms.TextBox
        Me.CMBNomRecibioTextBox = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.TextBox5 = New System.Windows.Forms.TextBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ConsecutivoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FechaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NomCajeraDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NumeroCepsaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RecibioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NomRecibioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.Button9 = New System.Windows.Forms.Button
        Me.Button10 = New System.Windows.Forms.Button
        Me.BUSCAPARCIALESTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.BUSCAPARCIALESTableAdapter
        ConsecutivoLabel = New System.Windows.Forms.Label
        NomCajeraLabel = New System.Windows.Forms.Label
        NumeroCepsaLabel = New System.Windows.Forms.Label
        ImporteLabel = New System.Windows.Forms.Label
        NomRecibioLabel = New System.Windows.Forms.Label
        FechaLabel = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.BUSCAPARCIALESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCAPARCIALESBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ConsecutivoLabel
        '
        ConsecutivoLabel.AutoSize = True
        ConsecutivoLabel.BackColor = System.Drawing.Color.DarkOrange
        ConsecutivoLabel.ForeColor = System.Drawing.Color.White
        ConsecutivoLabel.Location = New System.Drawing.Point(-2, 46)
        ConsecutivoLabel.Name = "ConsecutivoLabel"
        ConsecutivoLabel.Size = New System.Drawing.Size(81, 13)
        ConsecutivoLabel.TabIndex = 0
        ConsecutivoLabel.Text = "Consecutivo:"
        '
        'NomCajeraLabel
        '
        NomCajeraLabel.AutoSize = True
        NomCajeraLabel.BackColor = System.Drawing.Color.DarkOrange
        NomCajeraLabel.ForeColor = System.Drawing.Color.White
        NomCajeraLabel.Location = New System.Drawing.Point(-2, 98)
        NomCajeraLabel.Name = "NomCajeraLabel"
        NomCajeraLabel.Size = New System.Drawing.Size(62, 13)
        NomCajeraLabel.TabIndex = 4
        NomCajeraLabel.Text = "Cajero(a):"
        AddHandler NomCajeraLabel.Click, AddressOf Me.NomCajeraLabel_Click
        '
        'NumeroCepsaLabel
        '
        NumeroCepsaLabel.AutoSize = True
        NumeroCepsaLabel.BackColor = System.Drawing.Color.DarkOrange
        NumeroCepsaLabel.ForeColor = System.Drawing.Color.White
        NumeroCepsaLabel.Location = New System.Drawing.Point(-2, 124)
        NumeroCepsaLabel.Name = "NumeroCepsaLabel"
        NumeroCepsaLabel.Size = New System.Drawing.Size(93, 13)
        NumeroCepsaLabel.TabIndex = 6
        NumeroCepsaLabel.Text = "Numero Cepsa:"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.BackColor = System.Drawing.Color.DarkOrange
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(-2, 150)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(53, 13)
        ImporteLabel.TabIndex = 8
        ImporteLabel.Text = "Importe:"
        '
        'NomRecibioLabel
        '
        NomRecibioLabel.AutoSize = True
        NomRecibioLabel.BackColor = System.Drawing.Color.DarkOrange
        NomRecibioLabel.ForeColor = System.Drawing.Color.White
        NomRecibioLabel.Location = New System.Drawing.Point(-2, 187)
        NomRecibioLabel.Name = "NomRecibioLabel"
        NomRecibioLabel.Size = New System.Drawing.Size(84, 13)
        NomRecibioLabel.TabIndex = 12
        NomRecibioLabel.Text = "Recibido Por:"
        '
        'FechaLabel
        '
        FechaLabel.AutoSize = True
        FechaLabel.ForeColor = System.Drawing.Color.White
        FechaLabel.Location = New System.Drawing.Point(0, 68)
        FechaLabel.Name = "FechaLabel"
        FechaLabel.Size = New System.Drawing.Size(46, 13)
        FechaLabel.TabIndex = 47
        FechaLabel.Text = "Fecha:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(806, 711)
        Me.Panel1.TabIndex = 32
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label6)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.TextBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(806, 711)
        Me.SplitContainer1.SplitterDistance = 268
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(124, 388)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 19)
        Me.Label1.TabIndex = 49
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(FechaLabel)
        Me.Panel2.Controls.Add(Me.FechaLabel1)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Controls.Add(ConsecutivoLabel)
        Me.Panel2.Controls.Add(Me.CMBConsecutivoTextBox)
        Me.Panel2.Controls.Add(NomCajeraLabel)
        Me.Panel2.Controls.Add(Me.CMBNomCajeraTextBox)
        Me.Panel2.Controls.Add(NumeroCepsaLabel)
        Me.Panel2.Controls.Add(Me.CMBNumeroCepsaTextBox)
        Me.Panel2.Controls.Add(ImporteLabel)
        Me.Panel2.Controls.Add(Me.CMBImporteTextBox)
        Me.Panel2.Controls.Add(NomRecibioLabel)
        Me.Panel2.Controls.Add(Me.CMBNomRecibioTextBox)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(3, 452)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(360, 253)
        Me.Panel2.TabIndex = 48
        '
        'FechaLabel1
        '
        Me.FechaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "Fecha", True))
        Me.FechaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.BUSCAPARCIALESBindingSource1, "Fecha", True))
        Me.FechaLabel1.ForeColor = System.Drawing.Color.White
        Me.FechaLabel1.Location = New System.Drawing.Point(58, 68)
        Me.FechaLabel1.Name = "FechaLabel1"
        Me.FechaLabel1.Size = New System.Drawing.Size(142, 13)
        Me.FechaLabel1.TabIndex = 48
        '
        'BUSCAPARCIALESBindingSource
        '
        Me.BUSCAPARCIALESBindingSource.DataMember = "BUSCAPARCIALES"
        Me.BUSCAPARCIALESBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BUSCAPARCIALESBindingSource1
        '
        Me.BUSCAPARCIALESBindingSource1.DataMember = "BUSCAPARCIALES"
        Me.BUSCAPARCIALESBindingSource1.DataSource = Me.NewsoftvDataSet
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.DarkOrange
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(11, 19)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(150, 16)
        Me.Label7.TabIndex = 47
        Me.Label7.Text = "Datos de la Entrega:"
        '
        'CMBConsecutivoTextBox
        '
        Me.CMBConsecutivoTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBConsecutivoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBConsecutivoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "Consecutivo", True))
        Me.CMBConsecutivoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBConsecutivoTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBConsecutivoTextBox.Location = New System.Drawing.Point(84, 46)
        Me.CMBConsecutivoTextBox.Name = "CMBConsecutivoTextBox"
        Me.CMBConsecutivoTextBox.ReadOnly = True
        Me.CMBConsecutivoTextBox.Size = New System.Drawing.Size(200, 13)
        Me.CMBConsecutivoTextBox.TabIndex = 1
        Me.CMBConsecutivoTextBox.TabStop = False
        '
        'CMBNomCajeraTextBox
        '
        Me.CMBNomCajeraTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNomCajeraTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNomCajeraTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "NomCajera", True))
        Me.CMBNomCajeraTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNomCajeraTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBNomCajeraTextBox.Location = New System.Drawing.Point(61, 98)
        Me.CMBNomCajeraTextBox.Name = "CMBNomCajeraTextBox"
        Me.CMBNomCajeraTextBox.ReadOnly = True
        Me.CMBNomCajeraTextBox.Size = New System.Drawing.Size(162, 13)
        Me.CMBNomCajeraTextBox.TabIndex = 5
        Me.CMBNomCajeraTextBox.TabStop = False
        '
        'CMBNumeroCepsaTextBox
        '
        Me.CMBNumeroCepsaTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNumeroCepsaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNumeroCepsaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "NumeroCepsa", True))
        Me.CMBNumeroCepsaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNumeroCepsaTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBNumeroCepsaTextBox.Location = New System.Drawing.Point(95, 124)
        Me.CMBNumeroCepsaTextBox.Name = "CMBNumeroCepsaTextBox"
        Me.CMBNumeroCepsaTextBox.ReadOnly = True
        Me.CMBNumeroCepsaTextBox.Size = New System.Drawing.Size(151, 13)
        Me.CMBNumeroCepsaTextBox.TabIndex = 7
        Me.CMBNumeroCepsaTextBox.TabStop = False
        '
        'CMBImporteTextBox
        '
        Me.CMBImporteTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBImporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBImporteTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "Importe", True))
        Me.CMBImporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBImporteTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBImporteTextBox.Location = New System.Drawing.Point(84, 147)
        Me.CMBImporteTextBox.Name = "CMBImporteTextBox"
        Me.CMBImporteTextBox.ReadOnly = True
        Me.CMBImporteTextBox.Size = New System.Drawing.Size(162, 13)
        Me.CMBImporteTextBox.TabIndex = 9
        Me.CMBImporteTextBox.TabStop = False
        '
        'CMBNomRecibioTextBox
        '
        Me.CMBNomRecibioTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNomRecibioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNomRecibioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAPARCIALESBindingSource, "NomRecibio", True))
        Me.CMBNomRecibioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNomRecibioTextBox.ForeColor = System.Drawing.Color.White
        Me.CMBNomRecibioTextBox.Location = New System.Drawing.Point(3, 203)
        Me.CMBNomRecibioTextBox.Name = "CMBNomRecibioTextBox"
        Me.CMBNomRecibioTextBox.ReadOnly = True
        Me.CMBNomRecibioTextBox.Size = New System.Drawing.Size(235, 13)
        Me.CMBNomRecibioTextBox.TabIndex = 13
        Me.CMBNomRecibioTextBox.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button5.Location = New System.Drawing.Point(16, 413)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(88, 23)
        Me.Button5.TabIndex = 9
        Me.Button5.Text = "Buscar"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(13, 371)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(50, 15)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Fecha:"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(16, 387)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(102, 20)
        Me.TextBox5.TabIndex = 8
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button4.Location = New System.Drawing.Point(16, 330)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 288)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 15)
        Me.Label5.TabIndex = 43
        Me.Label5.Text = "Cajero(a):"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(16, 307)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(159, 20)
        Me.TextBox4.TabIndex = 6
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button3.Location = New System.Drawing.Point(16, 247)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(16, 205)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(102, 15)
        Me.Label4.TabIndex = 40
        Me.Label4.Text = "Quien Recibio:"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(16, 221)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(159, 20)
        Me.TextBox3.TabIndex = 4
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkOrange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button2.Location = New System.Drawing.Point(16, 165)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(88, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Buscar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(13, 123)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(106, 15)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Numero Cepsa:"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(16, 139)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(190, 20)
        Me.TextBox2.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(177, Byte))
        Me.Button1.Location = New System.Drawing.Point(16, 86)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(13, 44)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 15)
        Me.Label2.TabIndex = 34
        Me.Label2.Text = "Referencia:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(16, 60)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(159, 20)
        Me.TextBox1.TabIndex = 0
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(-1, 13)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(241, 18)
        Me.CMBLabel1.TabIndex = 32
        Me.CMBLabel1.Text = "Buscar La Entrega Parcial Por:" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkOrange
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ConsecutivoDataGridViewTextBoxColumn, Me.FechaDataGridViewTextBoxColumn, Me.NomCajeraDataGridViewTextBoxColumn, Me.NumeroCepsaDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.RecibioDataGridViewTextBoxColumn, Me.NomRecibioDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.BUSCAPARCIALESBindingSource
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle9
        Me.DataGridView1.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowTemplate.DefaultCellStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(571, 702)
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'ConsecutivoDataGridViewTextBoxColumn
        '
        Me.ConsecutivoDataGridViewTextBoxColumn.DataPropertyName = "Consecutivo"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsecutivoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.ConsecutivoDataGridViewTextBoxColumn.HeaderText = "Consecutivo"
        Me.ConsecutivoDataGridViewTextBoxColumn.Name = "ConsecutivoDataGridViewTextBoxColumn"
        Me.ConsecutivoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FechaDataGridViewTextBoxColumn
        '
        Me.FechaDataGridViewTextBoxColumn.DataPropertyName = "Fecha"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle3
        Me.FechaDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FechaDataGridViewTextBoxColumn.Name = "FechaDataGridViewTextBoxColumn"
        Me.FechaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NomCajeraDataGridViewTextBoxColumn
        '
        Me.NomCajeraDataGridViewTextBoxColumn.DataPropertyName = "NomCajera"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomCajeraDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle4
        Me.NomCajeraDataGridViewTextBoxColumn.HeaderText = "NomCajera"
        Me.NomCajeraDataGridViewTextBoxColumn.Name = "NomCajeraDataGridViewTextBoxColumn"
        Me.NomCajeraDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NumeroCepsaDataGridViewTextBoxColumn
        '
        Me.NumeroCepsaDataGridViewTextBoxColumn.DataPropertyName = "NumeroCepsa"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumeroCepsaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.NumeroCepsaDataGridViewTextBoxColumn.HeaderText = "NumeroCepsa"
        Me.NumeroCepsaDataGridViewTextBoxColumn.Name = "NumeroCepsaDataGridViewTextBoxColumn"
        Me.NumeroCepsaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "Importe"
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "Importe"
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RecibioDataGridViewTextBoxColumn
        '
        Me.RecibioDataGridViewTextBoxColumn.DataPropertyName = "Recibio"
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecibioDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.RecibioDataGridViewTextBoxColumn.HeaderText = "Recibio"
        Me.RecibioDataGridViewTextBoxColumn.Name = "RecibioDataGridViewTextBoxColumn"
        Me.RecibioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NomRecibioDataGridViewTextBoxColumn
        '
        Me.NomRecibioDataGridViewTextBoxColumn.DataPropertyName = "NomRecibio"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NomRecibioDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.NomRecibioDataGridViewTextBoxColumn.HeaderText = "NomRecibio"
        Me.NomRecibioDataGridViewTextBoxColumn.Name = "NomRecibioDataGridViewTextBoxColumn"
        Me.NomRecibioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(843, 25)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 20
        Me.Button6.Text = "&NUEVO"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(843, 73)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(136, 36)
        Me.Button7.TabIndex = 21
        Me.Button7.Text = "&CONSULTAR"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.Location = New System.Drawing.Point(843, 124)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 36)
        Me.Button8.TabIndex = 22
        Me.Button8.Text = "&MODIFICAR"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.DarkOrange
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.Location = New System.Drawing.Point(839, 600)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(136, 36)
        Me.Button9.TabIndex = 23
        Me.Button9.Text = "&IMPRIMIR"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkOrange
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button10.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.Location = New System.Drawing.Point(839, 656)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(136, 36)
        Me.Button10.TabIndex = 24
        Me.Button10.Text = "&SALIR"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'BUSCAPARCIALESTableAdapter
        '
        Me.BUSCAPARCIALESTableAdapter.ClearBeforeFill = True
        '
        'BwrEntregasParciales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 735)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "BwrEntregasParciales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Entrega Parciales"
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.BUSCAPARCIALESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCAPARCIALESBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents BUSCAPARCIALESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAPARCIALESTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.BUSCAPARCIALESTableAdapter
    Friend WithEvents CMBConsecutivoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBNomCajeraTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBNumeroCepsaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBImporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBNomRecibioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents FechaLabel1 As System.Windows.Forms.Label
    Friend WithEvents BUSCAPARCIALESBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ConsecutivoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomCajeraDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumeroCepsaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RecibioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomRecibioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
