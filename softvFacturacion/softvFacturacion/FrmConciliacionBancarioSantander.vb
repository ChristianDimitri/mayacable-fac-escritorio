Imports System.Data.SqlClient
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO
Public Class FrmConciliacionBancarioSantander
    Dim fecha As Date = "01/01/1900"
    Dim bndborra As Boolean = False
    Private bndarchivo As Boolean = False
    Private Sub GuardaCambios()
        Dim con11 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        '[Guarda_Cambios_preprefacturas_pagolinea] (@clv_session bigint)
        Try
            con11.Open()
            With cmd
                .CommandText = "Guarda_Cambios_preprefacturas_pagolinea"
                .Connection = con11
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@clv_usuario", SqlDbType.VarChar, 20)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Input

                prm.Value = locclv_sessionconc
                prm1.Value = GloUsuario


                .Parameters.Add(prm)
                .Parameters.Add(prm1)


                Dim i As Integer = cmd.ExecuteNonQuery()
            End With
            con11.Close()
            MsgBox("Se guardo con Exito", MsgBoxStyle.Information)
            'bitsist(GloUsuario, 0, GloSistema, "Conciliación Bancaria Bancomer", "Se Guardaron Los Cambios De Los Registros", "", "Clave Session Int: " + CStr(locclv_sessionconc), SubCiudad)
            'Busca(3)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)

        End Try
    End Sub
    Private Sub borracambios()
        Dim con12 As New SqlConnection(MiConexion)
        Dim cmd1 As New SqlClient.SqlCommand()

        cmd1 = New SqlClient.SqlCommand()

        Try
            con12.Open()
            'Borra_preprefacturas_pagolinea] (@clv_session bigint)
            With cmd1
                .CommandText = "Borra_preprefacturas_pagolinea"
                .Connection = con12
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm1 As New SqlParameter("@clv_session", SqlDbType.BigInt)

                prm1.Direction = ParameterDirection.Input

                prm1.Value = locclv_sessionconc

                .Parameters.Add(prm1)

                Dim ia As Integer = cmd1.ExecuteNonQuery()

            End With
            con12.Close()
            borra_Archivo_bancomer(locclv_sessionconc)
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub checa_cambios()
        Dim con10 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        Dim bnd As Integer = Nothing
        Dim resp As Integer = Nothing
        Try
            '[Valida_si_cambio_preprefacturas_pagolinea] (@clv_session bigint,@bnd int output
            con10.Open()
            cmd = New SqlClient.SqlCommand()
            With cmd
                .CommandText = "Valida_si_cambio_preprefacturas_pagolinea"
                .Connection = con10
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                Dim prm1 As New SqlParameter("@bnd", SqlDbType.Int)

                prm.Direction = ParameterDirection.Input
                prm1.Direction = ParameterDirection.Output

                prm.Value = locclv_sessionconc
                prm1.Value = 0

                .Parameters.Add(prm)
                .Parameters.Add(prm1)


                Dim i As Integer = cmd.ExecuteNonQuery()

                bnd = prm1.Value()
            End With
            con10.Close()

            If bnd = 1 Then  'si tiene cambios
                'resp = MsgBox("Deseas Guardar Los Cambios Realizados", MsgBoxStyle.YesNo)
                'Select Case resp
                '    Case 6 'Guarda Cambios
                GuardaCambios()
                '    Case 7 'Borra cambios
                'borracambios()
                'End Select
            ElseIf bnd = 0 Then
                MsgBox("No Hay Cambios Que Guardar", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'checa_cambios()
        'borracambios()
        bndborra = True
        Me.Close()
    End Sub
    Private Sub Busca(ByVal op As Integer)
        Dim opcion_busq As Integer = 0
        Dim CON As New SqlConnection(MiConexion)

        'checa_cambios()
        'borracambios()
        
        opcion_busq = 0


        Select Case op
            Case 0
                CON.Open()
                Me.Busca_conciliacion_santaderTableAdapter.Connection = CON
                Me.Busca_conciliacion_santaderTableAdapter.Fill(Me.ProcedimientosArnoldo3.Busca_conciliacion_santader, opcion_busq, 0, "01/01/1900", "", locclv_sessionconc)
                CON.Close()
            Case 1
                If IsNumeric(Me.TextBox1.Text) = True Then
                    If CLng(Me.TextBox1.Text) > 0 Then
                        CON.Open()
                        Me.Busca_conciliacion_santaderTableAdapter.Connection = CON
                        Me.Busca_conciliacion_santaderTableAdapter.Fill(Me.ProcedimientosArnoldo3.Busca_conciliacion_santader, opcion_busq, CLng(Me.TextBox1.Text), "01/01/1900", CStr(Me.ComboBox1.SelectedValue), locclv_sessionconc)
                        CON.Close()
                    End If
                End If
            Case 2
                If fecha <> "01/01/1900" Then
                    CON.Open()
                    Me.Busca_conciliacion_santaderTableAdapter.Connection = CON
                    Me.Busca_conciliacion_santaderTableAdapter.Fill(Me.ProcedimientosArnoldo3.Busca_conciliacion_santader, opcion_busq, 0, fecha, CStr(Me.ComboBox1.SelectedValue), locclv_sessionconc)
                    CON.Close()
                End If
            Case 3
                CON.Open()
                Me.Busca_conciliacion_santaderTableAdapter.Connection = CON
                Me.Busca_conciliacion_santaderTableAdapter.Fill(Me.ProcedimientosArnoldo3.Busca_conciliacion_santader, opcion_busq, 0, "01/01/1900", CStr(Me.ComboBox1.SelectedValue), locclv_sessionconc)
                CON.Close()
            Case 4
                CON.Open()
                Me.Busca_conciliacion_santaderTableAdapter.Connection = CON
                Me.Busca_conciliacion_santaderTableAdapter.Fill(Me.ProcedimientosArnoldo3.Busca_conciliacion_santader, 1, 0, "01/01/1900", "", locclv_sessionconc)
                CON.Close()
        End Select

    End Sub

    Private Sub FrmConciliacionBancarioSantander_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bnd2conciliacion = True Then
            bnd2conciliacion = False
            Me.TextBox1.Text = Glocontratosel
        End If
    End Sub

    Private Sub FrmConciliacionBancarioSantander_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        borracambios()
        bndborra = True
    End Sub


    Private Sub FrmConciliacionBancarioSantander_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim con8 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand


        con8.Open()
        Me.Muestra_Status_ConciliacionTableAdapter.Connection = con8
        Me.Muestra_Status_ConciliacionTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Status_Conciliacion, 0)
        con8.Close()

        con8.Open()
        cmd = New SqlClient.SqlCommand()
        With cmd
            .CommandText = "Dame_clv_session_conciliacion"
            .Connection = con8
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)

            prm.Direction = ParameterDirection.Output

            prm.Value = 0

            .Parameters.Add(prm)

            Dim i As Integer = cmd.ExecuteNonQuery()

            locclv_sessionconc = prm.Value

        End With
        con8.Close()



        Me.DateTimePicker1.MaxDate = Today()
        If Me.ComboBox1.SelectedValue = "P" Then
            Button5.Visible = True
        Else
            Button5.Visible = False
        End If
        Busca(0)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'Busca(1)
        Glocontratosel = 0
        bnd1conciliacion = True
        FrmSelCliente.Show()
    End Sub



    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr(ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "N"))
        'If Asc(e.KeyChar) = 13 Then
        '    Busca(1)
        'End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        Busca(1)
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        fecha = Me.DateTimePicker1.Text
        Me.TextBox1.Clear()
        Busca(2)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If bndborra = False Then
            Me.TextBox1.Clear()
            Busca(3)
            If Me.ComboBox1.SelectedValue = "P" Then
                Button5.Visible = True
            Else
                Button5.Visible = False
            End If
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub

    Private Sub Busca_conciliacion_santaderDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Busca_conciliacion_santaderDataGridView.CellContentClick
        'MsgBox(e.ColumnIndex.ToString, MsgBoxStyle.Information)
        Dim op As Boolean = False
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand
        If e.ColumnIndex = 7 Then
            If Me.Busca_conciliacion_santaderDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString() = "False" Then
                Me.Busca_conciliacion_santaderDataGridView.Rows(e.RowIndex).Cells(7).Value = True
            ElseIf Me.Busca_conciliacion_santaderDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString() = "True" Then
                Me.Busca_conciliacion_santaderDataGridView.Rows(e.RowIndex).Cells(7).Value = False
            End If
            op = Me.Busca_conciliacion_santaderDataGridView.Rows(e.RowIndex).Cells(7).Value


            'Inserta al precambios
            If IsNumeric(Me.Clv_sessionTextBox.Text) = True Then
                con.Open()
                cmd = New SqlClient.SqlCommand
                With cmd
                    .CommandText = "Inserta_preprefacturas_pagolinea"
                    .Connection = con
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    '@clv_session bigint,@clv_session1 bigint,@contrato bigint,@autorizado bit

                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    Dim prm1 As New SqlParameter("@clv_session1", SqlDbType.BigInt)
                    Dim prm2 As New SqlParameter("@contrato", SqlDbType.BigInt)
                    Dim prm3 As New SqlParameter("@autorizado", SqlDbType.Bit)

                    prm.Direction = ParameterDirection.Input
                    prm1.Direction = ParameterDirection.Input
                    prm2.Direction = ParameterDirection.Input
                    prm3.Direction = ParameterDirection.Input

                    prm.Value = locclv_sessionconc
                    prm1.Value = CLng(Me.Clv_sessionTextBox.Text)
                    prm2.Value = CLng(Me.ClienteTextBox.Text)
                    prm3.Value = op

                    .Parameters.Add(prm)
                    .Parameters.Add(prm1)
                    .Parameters.Add(prm2)
                    .Parameters.Add(prm3)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                End With
                con.Close()

                'bitsist(GloUsuario, CLng(Me.ClienteTextBox.Text), GloSistema, "Conciliación Bancaria Bancomer", "Se Cambio Un Registro Con La Clave Session: " + Me.Clv_sessionTextBox.Text, "", "Clave Session Int: " + locclv_sessionconc, SubCiudad)
           
            End If
        ElseIf e.ColumnIndex = 8 Then
            
            If Me.StatusTextBox.Text = "I" Then
                locclv_sessionconcproc = CLng(Me.Clv_sessionTextBox.Text)
                GloContrato = CLng(Me.ClienteTextBox.Text)
                FrmVisorBancomer.Show()
            Else
                MsgBox("El Detalle Solo Se Puede Ver Para Los Intentos", MsgBoxStyle.Information)
            End If
            'MsgBox("Ver Detalle", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim contlineas As Integer = Nothing
        Dim x As Integer = Nothing
        Dim linea As String = ""
        Dim y As Integer = Nothing
        Dim fecha As String = ""
        Dim cliente As String = ""
        Dim clv_session As String = ""
        Dim importe As String = ""
        Try
            Me.OpenFileDialog1.FileName = ""
            Me.OpenFileDialog1.Filter = "Archivo Resultados *.des|*.des"
            Me.OpenFileDialog1.ShowDialog()

            If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                MsgBox("No se Selecciono el Archivo", MsgBoxStyle.Information)
            Else
                'Borra Tabla del archivo porque es un archivo nuevo
                borra_Archivo_bancomer(locclv_sessionconc)


                Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                Dim archivo3 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)

                While archivo2.Peek <> -1
                    archivo2.ReadLine()
                    contlineas += 1
                End While
                archivo2.Close()

                'MsgBox(contlineas.ToString, MsgBoxStyle.Information)

                Dim vector(0 To contlineas) As String
                vector(contlineas) = (0)
                ReDim vector(contlineas)
                For x = 0 To contlineas
                    fecha = ""
                    cliente = ""
                    clv_session = ""
                    importe = ""
                    vector(x) = archivo3.ReadLine()
                    linea = vector(x)
                    If x <> contlineas Then
                        For y = 0 To 26
                            'String.Concat(fecha, linea(y))
                            If Len(fecha) > 0 Then
                                fecha = fecha + linea(y)
                            Else
                                fecha = linea(y)
                            End If
                        Next
                        For y = 27 To 58
                            If Len(cliente) > 0 Then
                                cliente = cliente + linea(y)
                            Else
                                cliente = linea(y)
                            End If
                        Next
                        For y = 123 To 153
                            If Len(clv_session) > 0 Then
                                clv_session = clv_session + linea(y)
                            Else
                                clv_session = linea(y)
                            End If
                        Next
                        For y = 183 To 204
                            If Len(importe) > 0 Then
                                importe = importe + linea(y)
                            Else
                                importe = linea(y)
                            End If
                        Next

                        Inserta_archivo_bancomer(locclv_sessionconc, CLng(RTrim(LTrim(clv_session))), CLng(LTrim(RTrim(importe))), RTrim(LTrim(fecha)))

                    End If
                Next
                MsgBox("Se Cargo El Archivo Con Exito", MsgBoxStyle.Information)
                bitsist(GloUsuario, 0, GloSistema, "Conciliación Bancaria Bancomer", "Se Cargo Un Archivo De Conciliación De Bancomer", "", "Nombre Del Archivo: " + Me.OpenFileDialog1.FileName, SubCiudad)

                bndarchivo = True
                Busca(4)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Inserta_archivo_bancomer(ByVal clv_sessionint As Long, ByVal clv_sessionbanc As Long, ByVal Importe As Double, ByVal Fecha_pago As String)
        Dim con15 As New SqlConnection(MiConexion)
        Dim cmd7 As New SqlClient.SqlCommand()
        cmd7 = New SqlClient.SqlCommand()
        '                    Inserta_Archivo_Bancomer()
        '@clv_session bigint,@clv_sessionbanc bigint,@Importe decimal(18,2),@fecha_pago datetime)
        con15.Open()
        With cmd7
            .CommandText = "Inserta_Archivo_Bancomer"
            .Connection = con15
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure

            Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
            Dim prm1 As New SqlParameter("@clv_sessionbanc", SqlDbType.BigInt)
            Dim prm2 As New SqlParameter("@Importe", SqlDbType.Money)
            Dim prm3 As New SqlParameter("@fecha_pago", SqlDbType.DateTime)

            prm.Direction = ParameterDirection.Input
            prm1.Direction = ParameterDirection.Input
            prm2.Direction = ParameterDirection.Input
            prm3.Direction = ParameterDirection.Input

            prm.Value = clv_sessionint
            prm1.Value = clv_sessionbanc
            prm2.Value = Importe
            prm3.Value = Fecha_pago

            .Parameters.Add(prm)
            .Parameters.Add(prm1)
            .Parameters.Add(prm2)
            .Parameters.Add(prm3)
            Dim ex As Integer = cmd7.ExecuteNonQuery()
        End With
        con15.Close()
    End Sub
    Private Sub borra_Archivo_bancomer(ByVal clv_session As Long)
        Dim con16 As New SqlConnection(MiConexion)
        Dim cmd8 As New SqlClient.SqlCommand()
        If bndarchivo = True Then
            bndarchivo = False
            con16.Open()
            cmd8 = New SqlClient.SqlCommand()

            With cmd8
                .CommandText = "Borra_Archivo_Bancomer"
                .Connection = con16
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)

                prm.Direction = ParameterDirection.Input

                prm.Value = locclv_sessionconc

                .Parameters.Add(prm)

                Dim ex1 As Integer = cmd8.ExecuteNonQuery()
            End With
            con16.Close()
        End If
    End Sub

    

    Private Sub CMBPanel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles CMBPanel1.Paint

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If IsNumeric(Me.Clv_facturaTextBox.Text) = True Then
            If CLng(Me.Clv_facturaTextBox.Text) > 0 Then
                LiTipo = 2
                GloClv_Factura = CLng(Me.Clv_facturaTextBox.Text)
                FrmImprimir.Show()
            Else
                MsgBox(" No Se Puede Imprimir No Tiene Una Factura Grabada", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("No Se Puede Imprimir la Factura", MsgBoxStyle.Information)
        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'GuardaCambios()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        FrmSelFechaGral.Show()
    End Sub

    Private Sub GuardarToolStripButton_AvailableChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton.AvailableChanged

    End Sub

    Private Sub GuardarToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GuardarToolStripButton.Click
        'GuardaCambios()
        checa_cambios()
    End Sub

    Private Sub BorPreFacturas_PagoLinea(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorPreFacturas_PagoLinea", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Msj", SqlDbType.VarChar, 150)
        parametro2.Direction = ParameterDirection.Output
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            If parametro2.Value.ToString.Length > 0 Then
                MsgBox(parametro2.Value.ToString, MsgBoxStyle.Information)
            Else
                MsgBox("Se Eliminó con Éxito", MsgBoxStyle.Information)
                Busca(3)
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try
    End Sub


    Private Sub tsbEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsbEliminar.Click
        BorPreFacturas_PagoLinea(Clv_sessionTextBox.Text)
    End Sub
End Class