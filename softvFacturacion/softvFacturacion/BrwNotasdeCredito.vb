Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient

Public Class BrwNotasdeCredito
    Private customersByCityReport As ReportDocument
    Dim motivo_Cancelacion As Integer
    Dim Clv_Factura As Integer = 0

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        OPCION = "N"
        'If IdSistema <> "VA" Then
        '    bnd = 1
        'End If
        If IdSistema <> "LO" Then FrmNotasdeCredito.Show() Else FrmNotasdeCreditoLogitel.Show()

    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If DataGridView1.RowCount = 0 Then
            MessageBox.Show("Selecciona una Devoluci�n.")
            Exit Sub
        End If
        status = Me.DataGridView1.SelectedCells(5).Value
        If Me.ClienteLabel1.Text.Trim.Length > 0 Then
            OPCION = "M"
            gloClvNota = CInt(Me.Clv_FacturaLabel1.Text)
            bnd = 3
            If IdSistema <> "LO" Then FrmNotasdeCredito.Show() Else FrmNotasdeCreditoLogitel.Show()
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click

        If DataGridView1.RowCount = 0 Then
            MessageBox.Show("Selecciona una Devoluci�n.")
            Exit Sub
        End If

        If Me.ClienteLabel1.Text.Trim.Length > 0 Then
            gloClvNota = CInt(Me.Clv_FacturaLabel1.Text)
            OPCION = "C"
            bnd = 3
            If IdSistema <> "LO" Then FrmNotasdeCredito.Show() Else FrmNotasdeCreditoLogitel.Show()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Try
            If DataGridView1.RowCount = 0 Then
                MessageBox.Show("Selecciona una Devoluci�n.")
                Exit Sub
            End If
            'If Me.DataGridView1.SelectedCells(5).Value = "Cancelada" And IdSistema <> "VA" Then
            '    MsgBox("La Nota Ha Sido Cancelada con Anterioridad ", MsgBoxStyle.Information, "Atenci�n")
            '    Exit Sub
            'ElseIf Me.DataGridView1.SelectedCells(5).Value = "Cancelada" And IdSistema = "VA" Then
            '    MsgBox("La Devoluci�n en Efectivo Ha Sido Cancelada con Anterioridad ", MsgBoxStyle.Information, "Atenci�n")
            '    Exit Sub
            'ElseIf Me.DataGridView1.SelectedCells(3).Value <> Me.DataGridView1.SelectedCells(4).Value And IdSistema <> "VA" Then
            '    MsgBox("La Nota de Cr�dito No Puede Ser Cancelada", MsgBoxStyle.Information, "Atenci�n")
            '    Exit Sub
            'ElseIf Me.DataGridView1.SelectedCells(3).Value <> Me.DataGridView1.SelectedCells(4).Value And IdSistema = "VA" Then
            '    MsgBox("La Devoluci�n en Efectivo No Puede Ser Cancelada", MsgBoxStyle.Information, "Atenci�n")
            '    Exit Sub
            'End If

            If Me.DataGridView1.SelectedCells(5).Value = "Cancelada" Then
                MsgBox("La Devoluci�n en Efectivo Ha Sido Cancelada con Anterioridad ", MsgBoxStyle.Information, "Atenci�n")
                Exit Sub
            ElseIf Me.DataGridView1.SelectedCells(3).Value <> Me.DataGridView1.SelectedCells(4).Value Then
                MsgBox("La Devoluci�n en Efectivo No Puede Ser Cancelada", MsgBoxStyle.Information, "Atenci�n")
                Exit Sub
            End If

            Dim resp As MsgBoxResult = MsgBoxResult.Cancel

            'If IdSistema <> "VA" Then
            '    resp = MsgBox(" � Estas Seguro que Deseas Cancelar la Nota de Cr�dito " + Me.Clv_FacturaLabel1.Text + "  ?", MsgBoxStyle.YesNoCancel)
            'ElseIf IdSistema = "VA" Then
            resp = MsgBox(" � Estas Seguro que Deseas Cancelar la Devoluci�n en Efectivo " + Me.Clv_FacturaLabel1.Text + "  ?", MsgBoxStyle.YesNoCancel)
            'End If

            If resp = MsgBoxResult.Yes Then
                FrmCancela_NotaCredito.Show()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Busca(ByVal Opcion As Integer)
        Try
            Dim con As New SqlConnection(MiConexion)
            con.Open()
            Me.BUSCANOTASDECREDITOTableAdapter.Connection = con
            If Opcion = 0 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, Opcion, 0, "01/01/1900", 0, 0, "")
            ElseIf Opcion = 1 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, Opcion, Me.SERIETextBox.Text, "01/01/1900", 0, 0, "")
            ElseIf Opcion = 2 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, Opcion, 0, Me.FECHATextBox.Text, 0, 0, "")
            ElseIf Opcion = 3 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, Opcion, 0, "01/01/1900", Me.TextBox1.Text, 0, "")
            ElseIf Opcion = 4 Then
                Me.BUSCANOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.BUSCANOTASDECREDITO, Opcion, 0, "01/01/1900", 0, Me.ComboBox1.SelectedValue, "")
            End If
            con.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'If Me.SERIETextBox.Text.Trim.Length = 0 And IdSistema <> "VA" Then
        '    MsgBox(" � Se Debe Capturar una Nota de Cr�dito ! ", MsgBoxStyle.Information)
        'ElseIf Me.SERIETextBox.Text.Trim.Length = 0 And IdSistema = "VA" Then
        '    MsgBox(" � Se Debe Capturar una Devoluci�n en Efectivo ! ", MsgBoxStyle.Information)
        'Else
        '    Busca(1)
        '    Me.SERIETextBox.Clear()
        'End If


        If Me.SERIETextBox.Text.Trim.Length = 0 Then
            MsgBox("�Se debe capturar una Devoluci�n en Efectivo!", MsgBoxStyle.Information)
        Else
            Busca(1)
            Me.SERIETextBox.Clear()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsDate(Me.FECHATextBox.Text) = True Then
            Busca(2)
        End If
    End Sub

    Public Sub Cancelacion(ByVal glomotivocan)
        Dim con2 As New SqlClient.SqlConnection(MiConexion)
        con2.Open()
        Me.Cancela_NotaCreditoTableAdapter.Connection = con2
        Me.Cancela_NotaCreditoTableAdapter.Fill(Me.DataSetLydia.Cancela_NotaCredito, Me.Clv_FacturaLabel1.Text, motivo_Cancelacion)
        con2.Close()
        Me.ComboBox1.Text = ""
        glomotivocan = 0

        'If IdSistema = "VA" Then
        MsgBox("La Devoluci�n en Efectivo fue cancelada con �xito.", MsgBoxStyle.Information)
        'Else
        '    MsgBox("La Nota de Cr�dito fue Cancelada con �xito", MsgBoxStyle.Information)
        'End If


        'FacturaFiscalCFD---------------------------------------------------------------------
        'Generaci�n de FF
        DameFacturaNotaDeCredito(Clv_FacturaLabel1.Text)
        facturaFiscalCFD = False
        facturaFiscalCFD = ChecaSiEsFacturaFiscal("C", Clv_Factura)
        If facturaFiscalCFD = True Then
            DameSerieFolio(2, Clv_Factura)
            CancelaFacturaCFD("C", Clv_Factura, eSerie, eFolio, ClienteLabel1.Text, "")
        End If
        '--------------------------------------------------------------------------------------

        Busca(0)
    End Sub

    Private Sub BrwNotasdeCredito_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If refrescar = True Then
            Busca(0)
            refrescar = False
        End If
    End Sub


    Private Sub BrwNotasdeCredito_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)

        'If IdSistema = "VA" Then
        Me.CMBLabel5.Text = "Devoluci�n en Efectivo"
        Me.CMBLabel1.Text = "Buscar Devoluci�n por :"
        Me.Label3.Text = "Clave de Devoluci�n"
        Me.SerieLabel.Text = "Clave de Devoluci�n"
        Me.Label8.Text = "Datos de la Devoluci�n en Efectivo"
        Me.Button6.Text = "Reimprimir Devoluci�n en Efectivo"
        Me.Button2.Text = "Cancelar Devoluci�n en Efectivo"
        Me.Text = "Cat�logo de Devoluciones en Efectivo"
        Me.DataGridView1.Columns(0).HeaderText = "Clave de la Devoluci�n en Efectivo"
        Me.DetalleNOTASDECREDITODataGridView.Visible = False
        'End If

        Dim con4 As New SqlClient.SqlConnection(MiConexion)
        con4.Open()
        Me.MUESTRASUCURSALES2TableAdapter.Connection = con4
        Me.MUESTRASUCURSALES2TableAdapter.Fill(Me.DataSetLydia.MUESTRASUCURSALES2, 0)
        con4.Close()
        Me.ComboBox1.Text = ""
        Busca(0)
    End Sub

    Private Sub SERIETextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles SERIETextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
            Me.SERIETextBox.Clear()
        End If
    End Sub

    Private Sub FECHATextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles FECHATextBox.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
            Me.FECHATextBox.Clear()
        End If
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        If Me.TextBox1.Text.Trim.Length = 0 Then
            MsgBox("�Se debe capturar un Contrato!", MsgBoxStyle.Information)
        Else
            Busca(3)
            Me.TextBox1.Clear()
        End If
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        If Me.ComboBox1.Text = "" Then
            MsgBox("�Se debe seleccionar una Sucursal!", MsgBoxStyle.Information)
        Else
            Busca(4)
            Me.ComboBox1.Text = ""
        End If
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        If Me.ClienteLabel1.Text.Trim.Length > 0 Then
            gloClvNota = CInt(Me.Clv_FacturaLabel1.Text)
            OPCION = "C"
            bnd = 3
            If IdSistema <> "LO" Then FrmNotasdeCredito.Show() Else FrmNotasdeCreditoLogitel.Show()
        End If
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If DataGridView1.RowCount = 0 Then
            MessageBox.Show("Selecciona una Devoluci�n.")
            Exit Sub
        End If
        gloClvNota = Me.Clv_FacturaLabel1.Text
        LocBndNotasReporteTick = True
        locoprepnotas = 0
        FrmImprimirRepGral.Show()
        'ConfigureCrystalReports(gloClvNota)
    End Sub
    Private Sub ConfigureCrystalReports(ByVal Clv_Factura As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim ba As Boolean
        Dim busfac As New NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
        Dim bfac As New NewsoftvDataSet2.BusFacFiscalDataTable
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        'If GloImprimeTickets = False Then
        ' reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteNotasdeCredito.rpt"

        'busfac.Connection = CON
        'busfac.Fill(bfac, Clv_Factura, identi)
        'If IdSistema = "SA" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasTvRey.rpt"
        '    ba = True
        'ElseIf IdSistema = "TO" And facnormal = True And identi > 0 Then
        '    reportPath = RutaReportes + "\ReporteCajasCabSta.rpt"
        '    ba = True

        'Else
        '    reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        'SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        ' End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)
        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, gloClvNota)
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, "0")
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, "0")
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, "0")
        'If GloImprimeTickets = True Then
        If ba = False Then
            customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
            customersByCityReport.DataDefinition.FormulaFields("Copia").Text = "'Copia'"
        End If

        'If (IdSistema = "TO" Or IdSistema = "SA") Then 'And facnormal = True And identi > 0 
        '    customersByCityReport.PrintOptions.PrinterName = impresorafiscal
        'Else

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
        ' End If

        customersByCityReport.PrintToPrinter(1, True, 1, 1)
        CON.Close()
        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub
    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub DataGridView1_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.SelectionChanged
        Dim cone As New SqlConnection(MiConexion)
        If Me.Clv_FacturaLabel1.Text <> "" Then
            cone.Open()
            Me.DetalleNOTASDECREDITOTableAdapter.Connection = cone
            Me.DetalleNOTASDECREDITOTableAdapter.Fill(Me.DataSetLydia.DetalleNOTASDECREDITO, Me.Clv_FacturaLabel1.Text)
            cone.Close()
        End If
    End Sub

    Private Sub DameFacturaNotaDeCredito(ByVal Clv_NotaDeCredito As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameFacturaNotaDeCredito", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@CLV_NOTADECREDITO", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_NotaDeCredito
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@CLV_FACTURA", SqlDbType.Int)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            Clv_Factura = 0
            Clv_Factura = par2.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
End Class