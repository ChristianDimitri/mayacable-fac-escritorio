<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCortesdeFacturas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Clave = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ReporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CatalogodeReportesFacturaBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet
        Me.ComboBox2 = New System.Windows.Forms.ComboBox
        Me.MUESTRAOPREPORTESFACTURABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo
        Me.MUESTRAOPREPORTESFACTURABindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ORSTATUS = New System.Windows.Forms.CheckBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.CrystalReportViewer1 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.dameSerDELCliTableAdapter
        Me.CatalogodeReportesFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CatalogodeReportes_FacturaTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.CatalogodeReportes_FacturaTableAdapter
        Me.CatalogodeReportesFacturaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAOP_REPORTES_FACTURATableAdapter
        Me.Borra_Reporte_cortesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Borra_Reporte_cortesTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Borra_Reporte_cortesTableAdapter
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogodeReportesFacturaBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAOPREPORTESFACTURABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAOPREPORTESFACTURABindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogodeReportesFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CatalogodeReportesFacturaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Borra_Reporte_cortesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.CrystalReportViewer1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1016, 741)
        Me.SplitContainer1.SplitterDistance = 337
        Me.SplitContainer1.TabIndex = 0
        Me.SplitContainer1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.DataGridView1)
        Me.Panel2.Location = New System.Drawing.Point(15, 68)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(305, 386)
        Me.Panel2.TabIndex = 3
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.DarkKhaki
        Me.DataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Olive
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.DarkKhaki
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Clave, Me.ReporteDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.CatalogodeReportesFacturaBindingSource2
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.Location = New System.Drawing.Point(-41, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(414, 407)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'Clave
        '
        Me.Clave.DataPropertyName = "Clave"
        Me.Clave.HeaderText = "Clave"
        Me.Clave.Name = "Clave"
        Me.Clave.ReadOnly = True
        Me.Clave.Visible = False
        '
        'ReporteDataGridViewTextBoxColumn
        '
        Me.ReporteDataGridViewTextBoxColumn.DataPropertyName = "Reporte"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.ReporteDataGridViewTextBoxColumn.HeaderText = "Catalogo de Cortes"
        Me.ReporteDataGridViewTextBoxColumn.Name = "ReporteDataGridViewTextBoxColumn"
        Me.ReporteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ReporteDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.ReporteDataGridViewTextBoxColumn.Width = 310
        '
        'CatalogodeReportesFacturaBindingSource2
        '
        Me.CatalogodeReportesFacturaBindingSource2.DataMember = "CatalogodeReportes_Factura"
        Me.CatalogodeReportesFacturaBindingSource2.DataSource = Me.NewsoftvDataSet
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.MUESTRAOPREPORTESFACTURABindingSource, "Clv_Vendedor", True))
        Me.ComboBox2.DataSource = Me.MUESTRAOPREPORTESFACTURABindingSource1
        Me.ComboBox2.DisplayMember = "Nombre"
        Me.ComboBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.ForeColor = System.Drawing.Color.Red
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(15, 30)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(302, 24)
        Me.ComboBox2.TabIndex = 0
        Me.ComboBox2.ValueMember = "Clv_Vendedor"
        '
        'MUESTRAOPREPORTESFACTURABindingSource
        '
        Me.MUESTRAOPREPORTESFACTURABindingSource.DataMember = "MUESTRAOP_REPORTES_FACTURA"
        Me.MUESTRAOPREPORTESFACTURABindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MUESTRAOPREPORTESFACTURABindingSource1
        '
        Me.MUESTRAOPREPORTESFACTURABindingSource1.DataMember = "MUESTRAOP_REPORTES_FACTURA"
        Me.MUESTRAOPREPORTESFACTURABindingSource1.DataSource = Me.Procedimientos_arnoldo
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(9, 9)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(256, 18)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Seleccione la opción del reporte:"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkKhaki
        Me.Panel1.Controls.Add(Me.ORSTATUS)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(15, 506)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(302, 92)
        Me.Panel1.TabIndex = 2
        '
        'ORSTATUS
        '
        Me.ORSTATUS.AutoSize = True
        Me.ORSTATUS.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.ORSTATUS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ORSTATUS.Location = New System.Drawing.Point(12, 48)
        Me.ORSTATUS.Name = "ORSTATUS"
        Me.ORSTATUS.Size = New System.Drawing.Size(76, 19)
        Me.ORSTATUS.TabIndex = 2
        Me.ORSTATUS.Text = "STATUS"
        Me.ORSTATUS.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.DarkGoldenrod
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(302, 26)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Ordenar Reporte por :"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Olive
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(27, 690)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(273, 39)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'CrystalReportViewer1
        '
        Me.CrystalReportViewer1.ActiveViewIndex = -1
        Me.CrystalReportViewer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrystalReportViewer1.DisplayGroupTree = False
        Me.CrystalReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrystalReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.CrystalReportViewer1.Name = "CrystalReportViewer1"
        Me.CrystalReportViewer1.SelectionFormula = ""
        Me.CrystalReportViewer1.Size = New System.Drawing.Size(675, 741)
        Me.CrystalReportViewer1.TabIndex = 0
        Me.CrystalReportViewer1.TabStop = False
        Me.CrystalReportViewer1.ViewTimeSelectionFormula = ""
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataMember = "dameSerDELCli"
        Me.DameSerDELCliBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'CatalogodeReportesFacturaBindingSource
        '
        Me.CatalogodeReportesFacturaBindingSource.DataMember = "CatalogodeReportes_Factura"
        Me.CatalogodeReportesFacturaBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'CatalogodeReportes_FacturaTableAdapter
        '
        Me.CatalogodeReportes_FacturaTableAdapter.ClearBeforeFill = True
        '
        'CatalogodeReportesFacturaBindingSource1
        '
        Me.CatalogodeReportesFacturaBindingSource1.DataMember = "CatalogodeReportes_Factura"
        Me.CatalogodeReportesFacturaBindingSource1.DataSource = Me.NewsoftvDataSet
        '
        'MUESTRAOP_REPORTES_FACTURATableAdapter
        '
        Me.MUESTRAOP_REPORTES_FACTURATableAdapter.ClearBeforeFill = True
        '
        'Borra_Reporte_cortesBindingSource
        '
        Me.Borra_Reporte_cortesBindingSource.DataMember = "Borra_Reporte_cortes"
        Me.Borra_Reporte_cortesBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Borra_Reporte_cortesTableAdapter
        '
        Me.Borra_Reporte_cortesTableAdapter.ClearBeforeFill = True
        '
        'FrmCortesdeFacturas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "FrmCortesdeFacturas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reportes de Cortes de Factura"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogodeReportesFacturaBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAOPREPORTESFACTURABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAOPREPORTESFACTURABindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogodeReportesFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CatalogodeReportesFacturaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Borra_Reporte_cortesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents CrystalReportViewer1 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents CatalogodeReportesFacturaBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents CatalogodeReportesFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CatalogodeReportes_FacturaTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.CatalogodeReportes_FacturaTableAdapter
    Friend WithEvents CatalogodeReportesFacturaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAOPREPORTESFACTURABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents MUESTRAOP_REPORTES_FACTURATableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAOP_REPORTES_FACTURATableAdapter
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRAOPREPORTESFACTURABindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents ORSTATUS As System.Windows.Forms.CheckBox
    Friend WithEvents Clave As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Borra_Reporte_cortesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Borra_Reporte_cortesTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Borra_Reporte_cortesTableAdapter
End Class
