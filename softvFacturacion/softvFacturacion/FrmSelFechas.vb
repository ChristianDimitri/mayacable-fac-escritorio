Imports System.Data.SqlClient
Public Class FrmSelFechas

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub


    Private Sub FrmSelFechas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        If LocBanderaRep1 = 0 Or LocBanderaRep1 = 1 Then
            Select Case LocBanderaRep1
                Case 0
                    Me.CMBLabel6.Text = "Usuario que Cancelo:"
                    Me.Text = "Seleccion Fechas Listado Facturas Canceladas"
                Case 1
                    Me.CMBLabel6.Text = "Usuario que Reimprimio:"
                    Me.Text = "Seleccion Fechas Listado Facturas Reimpresas"
            End Select
            CON.Open()
            Me.Muestra_Usuarios_Que_cancelaron_ImprimieronTableAdapter.Connection = CON
            Me.Muestra_Usuarios_Que_cancelaron_ImprimieronTableAdapter.Fill(Me.Procedimientos_arnoldo.Muestra_Usuarios_Que_cancelaron_Imprimieron, LocBanderaRep1)
            CON.Close()
            Me.CheckBox1.Visible = False
            Me.CMBLabel5.Visible = False
        ElseIf LocBanderaRep1 = 2 Then
            Me.ComboBox2.Visible = True
            LocResumenBon = False
            Me.ComboBox1.Visible = False
            Me.CMBLabel6.Text = "Supervisor Que Autorizo la Bonificación:"
            Me.Text = "Seleccion Fechas Bonificaciones"
            CON.Open()
            Me.Muestra_Usuarios_Que_BonificaronTableAdapter.Connection = CON
            Me.Muestra_Usuarios_Que_BonificaronTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Usuarios_Que_Bonificaron, LocBanderaRep1)
            CON.Close()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        LocFecha1 = Me.DateTimePicker1.Text
        LocFecha2 = Me.DateTimePicker2.Text
        Select Case LocBanderaRep1
            Case 0
                Locclv_usuario = Me.ComboBox1.SelectedValue
                LocNombreusuario = Me.ComboBox1.Text
                LocBndrepfac1 = True
            Case 1
                Locclv_usuario = Me.ComboBox1.SelectedValue
                LocNombreusuario = Me.ComboBox1.Text
                LocBndrepfac1 = True
            Case 2
                Locclv_usuario = Me.ComboBox2.SelectedValue
                LocNombreusuario = Me.ComboBox2.Text
                LocBndBon = True
        End Select
        'MsgBox(Locclv_usuario)
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LocResumenBon = True
        Else
            LocResumenBon = False
        End If
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub
End Class