Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports System.Data.SqlClient
Imports System.Text

Public Class BWRFACTURAGLOBAL
    Dim Letra2 As String = Nothing
    Dim Importe As String = Nothing
    Private eCont As Integer = 1
    Private eRes As Integer = 0
    Private customersByCityReport As ReportDocument
    Dim SubTotal, Ieps, Iva, Total As Double

    Private Sub busqueda(ByVal op As Integer)
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Select Case op
                Case 0
                    Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, Me.TextBox1.Text, Me.TextBox4.Text, "01/01/1900", "", op, bec_tipo)
                Case 1
                    Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", Me.TextBox2.Text, "", op, bec_tipo)
                Case 2
                    Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", Me.TextBox3.Text, op, bec_tipo)
                Case 3
                    Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                Case 4
                    Me.BuscaFacturasGlobalesTableAdapter.Connection = CON
                    Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales, "", "", "01/01/1900", "", op, bec_tipo)
                    'Me.BuscaFacturasGlobalesTableAdapter.Fill(Me.DataSetLydia.BuscaFacturasGlobales
            End Select
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub BWRFACTURAGLOBAL_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If bec_bandera = 1 Then
            bec_bandera = 0
            busqueda(4)
        End If
    End Sub
    Private Sub CantidadaLetra()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameCantidadALetraTableAdapter.Connection = CON
            Me.DameCantidadALetraTableAdapter.Fill(Me.NewsoftvDataSet2.DameCantidadALetra, CDec(Importe), 0, Letra2)
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BWRFACTURAGLOBAL_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        'TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.DameFechadelServidorHora' Puede moverla o quitarla seg�n sea necesario.
        Me.DameFechadelServidorHoraTableAdapter.Connection = CON
        Me.DameFechadelServidorHoraTableAdapter.Fill(Me.NewsoftvDataSet2.DameFechadelServidorHora)
        ''TODO: esta l�nea de c�digo carga datos en la tabla 'NewsoftvDataSet2.MUESTRATIPOFACTGLO' Puede moverla o quitarla seg�n sea necesario.
        Me.MUESTRATIPOFACTGLOTableAdapter.Connection = CON
        Me.MUESTRATIPOFACTGLOTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRATIPOFACTGLO)
        CON.Close()
        bec_tipo = Me.ComboBox2.SelectedValue
        busqueda(4)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        busqueda(0)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        'Me.TextBox2.Text = String.Format("dd/mm/yyyy")
        busqueda(1)
    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 And Me.TextBox4.Text = "" Then
            MsgBox("Selecciona Primero la Factura")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub
    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        If (Asc(e.KeyChar) = 13) And Me.TextBox1.Text = "" Then
            MsgBox("Selecciona Primero la Serie")
        ElseIf (Asc(e.KeyChar) = 13) Then
            busqueda(0)
        End If
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(2)
        End If
    End Sub
    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            busqueda(1)
        End If
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Gloop = "N"
        LiTipo = 4
        bnd = 1
        CAPTURAFACTURAGLOBAL.Show()
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        If Me.CMBCanceladaTextBox.Text = 1 Then
            MsgBox("La Factura ya ha sido Cancelada con Anterioridad")
        Else 'If Me.DateTimePicker1.Text = Me.CMBFechaTextBox.Text Then
            Dim resp As MsgBoxResult = MsgBoxResult.Cancel
            resp = MsgBox("� Esta Seguro de que Desea Cancelar la Factura Global con Serie: " + Me.CMBSerieTextBox.Text + " y Folio:" + Me.FacturaLabel1.Text + " ?", MsgBoxStyle.YesNo)
            If resp = MsgBoxResult.Yes Then
                bec_bandera = 1
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.CANCELAFACTGLOBALTableAdapter.Connection = CON
                Me.CANCELAFACTGLOBALTableAdapter.Fill(Me.NewsoftvDataSet2.CANCELAFACTGLOBAL, Me.CMBIdFacturaTextBox.Text, " hola")
                CON.Close()

                'FacturaFiscalCFD---------------------------------------------------------------------
                'Generaci�n de FF
                facturaFiscalCFD = False
                facturaFiscalCFD = ChecaSiEsFacturaFiscal("G", 0)
                If facturaFiscalCFD = True Then
                    CancelaFacturaCFD("G", Me.CMBIdFacturaTextBox.Text, CMBSerieTextBox.Text, FacturaLabel1.Text, 0, "")
                End If
                '--------------------------------------------------------------------------------------

                MsgBox("La Factura ha sido Cancelada")
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        busqueda(2)
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        bec_tipo = Me.ComboBox2.SelectedValue
        busqueda(3)
    End Sub




    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Serie As String = Nothing
        Dim fecha As String = Nothing
        Dim Cajera As String = Nothing
        Dim Factura As String = Nothing

        Serie = Me.CMBSerieTextBox.Text
        Factura = Me.FacturaLabel1.Text
        fecha = Me.CMBFechaTextBox.Text

        Cajera = Me.CMBCajeraTextBox.Text
        Importe = Me.CMBImporteTextBox.Text
        CantidadaLetra()
        If LocImpresoraTickets = "" Then
            MsgBox("No Se Ha Asigando Una Impresora de Tickets A Esta Sucursal", MsgBoxStyle.Information)
        ElseIf LocImpresoraTickets <> "" Then
            'Me.ConfigureCrystalReportefacturaGlobal(Letra2, Importe, Serie, fecha, Cajera, Factura)
            DameImporteFacturaGlobal(Today, Today, 0, " ", CMBIdFacturaTextBox.Text, 2)
            ConfigureCrystalReportefacturaGlobal(Letra2, Importe, SubTotal, Iva, Ieps, Serie, fecha, Cajera, Factura)
        End If

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        If Me.FacturaLabel1.Text <> "" Then
            LocSerieglo = Me.CMBSerieTextBox.Text '.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value
            LocFacturaGlo = Me.FacturaLabel1.Text 'CInt(Me.BuscaFacturasGlobalesDataGridView.SelectedCells(1).Value)
            LocFechaGlo = Me.CMBFechaTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(3).Value
            LocCajeroGlo = Me.CMBCajeraTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(6).Value
            LocImporteGlo = Me.CMBImporteTextBox.Text 'Me.BuscaFacturasGlobalesDataGridView.SelectedCells(2).Value
            'Me.Dame_clv_sucursalTableAdapter.Fill(Me.Procedimientos_arnoldo.Dame_clv_sucursal, Me.BuscaFacturasGlobalesDataGridView.SelectedCells(0).Value, Locclv_sucursalglo)
            Locclv_sucursalglo = Me.Label6.Text
            LocFechaGloFinal = Me.Label8.Text
            Gloop = "C"
            CAPTURAFACTURAGLOBAL.Show()
        Else
            MsgBox("No Se Ha Seleccionado Alguna Factura para Mostrar", MsgBoxStyle.Information)
        End If
    End Sub


    Private Sub DameImporteFacturaGlobal(ByVal Fecha As DateTime, ByVal FechaFin As DateTime, ByVal SelSucursal As Integer, ByVal Tipo As Char, ByVal IDFactura As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DameImporteFacturaGlobal ")
        strSQL.Append("'" & CStr(Fecha) & "', ")
        strSQL.Append("'" & CStr(FechaFin) & "', ")
        strSQL.Append(CStr(SelSucursal) & ", ")
        strSQL.Append("'" & Tipo & "', ")
        strSQL.Append(CStr(IDFactura) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try

            dataAdapter.Fill(dataTable)
            Importe = Format(CDec(dataTable.Rows(0)(0).ToString()), "#####0.00")
            SubTotal = Format(CDec(dataTable.Rows(0)(1).ToString()), "#####0.00")
            Iva = Format(CDec(dataTable.Rows(0)(2).ToString()), "#####0.00")
            Ieps = Format(CDec(dataTable.Rows(0)(3).ToString()), "#####0.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Function Usp_DameDetallaFacturaGlobal(ByVal prmclvfactura As Long) As DataSet
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@IdFactura", SqlDbType.BigInt, prmclvfactura)
        Dim listaTablas As New List(Of String)
        listaTablas.Add("Usp_DameDetallaFacturaGlobal")
        Usp_DameDetallaFacturaGlobal = BaseII.ConsultaDS("Usp_DameDetallaFacturaGlobal", listaTablas)

    End Function
    Private Sub ConfigureCrystalReportefacturaGlobal(ByVal Letra2 As String, ByVal Importe As String, ByVal SubTotal As String, ByVal Iva As String, ByVal Ieps As String, ByVal Serie2 As String, ByVal Fecha2 As String, ByVal Cajera2 As String, ByVal Factura2 As String)
        Try
            Dim ds As New DataSet
            customersByCityReport = New ReportDocument
            Dim connectionInfo As New ConnectionInfo
            connectionInfo.ServerName = GloServerName
            connectionInfo.DatabaseName = GloDatabaseName
            connectionInfo.UserID = GloUserID
            connectionInfo.Password = GloPassword

            Dim cliente2 As String = "P�blico en General"
            Dim concepto2 As String = "Ingreso por Pago de Servicios"
            Dim reportPath As String = Nothing

            'Select Case Locclv_empresa
            '    Case "AG"
            reportPath = RutaReportes + "\ReporteFacturaGlobalGiga.rpt"
            '    Case "TO"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobal.rpt"
            '    Case "SA"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            '    Case "VA"
            '        reportPath = RutaReportes + "\ReporteFacturaGlobalTvRey.rpt"
            'End Select
            customersByCityReport.Load(reportPath)
            ds.Clear()
            ds.Tables.Clear()
            ds = Usp_DameDetallaFacturaGlobal(CInt(Me.CMBIdFacturaTextBox.Text))

            SetDBReport(ds, customersByCityReport)

            If Locclv_empresa = "SA" Or Locclv_empresa = "TO" Or Locclv_empresa = "AG" Or Locclv_empresa = "VA" Then

                customersByCityReport.DataDefinition.FormulaFields("Letra").Text = "'" & Letra2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cliente").Text = "'" & cliente2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Concepto").Text = "'" & concepto2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Serie").Text = "'" & Serie2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Fecha").Text = "'" & Fecha2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Cajera").Text = "'" & Cajera2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("ImporteServicio").Text = "'" & Format(CDec(SubTotal.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Factura").Text = "'" & Factura2 & "'"
                customersByCityReport.DataDefinition.FormulaFields("Direccion").Text = "'" & GloDireccionEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("RFC").Text = "'" & GloRfcEmpresa & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ciudad").Text = "'" & GloCiudadEmpresa & "'"

                customersByCityReport.DataDefinition.FormulaFields("SubTotal").Text = "'" & Format(CDec(SubTotal), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Iva").Text = "'" & Format(CDec(Iva.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Ieps").Text = "'" & Format(CDec(Ieps.ToString()), "##,##0.00") & "'"
                customersByCityReport.DataDefinition.FormulaFields("Total").Text = "'" & Format(CDec(Importe.ToString()), "##,##0.00") & "'"

            End If


            ''Select Case Locclv_empresa
            ''    Case "AG"
            ''customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets
            'impresorafiscal = "ticket
            'customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "TO"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "SA"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''    Case "VA"
            ''        customersByCityReport.PrintOptions.PrinterName = impresorafiscal
            ''End Select

            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una Factura Global. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont + 1) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura Global " + CStr(eCont + 2) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            'eCont = 1
            'eRes = 0
            'Do
            '    If (MsgBox("Se va a imprimir una copia de la Factura Global anterior. �Est� lista la impresora?", MsgBoxStyle.YesNo)) = 6 Then
            '        customersByCityReport.PrintToPrinter(1, True, 1, 1)
            '        eRes = MsgBox("La Impresi�n de la Factura " + CStr(eCont + 3) + "/4, �Fu� Correcta?", MsgBoxStyle.YesNo, "Atenci�n")
            '        '6=Yes;7=No
            '        If eRes = 6 Then eCont = eCont + 1
            '    Else
            '        Exit Sub
            '    End If
            'Loop While eCont <= 1
            ''customersByCityReport.PrintToPrinter(1, True, 1, 1)
            'customersByCityReport = Nothing

            FrmImprimir.CrystalReportViewer1.ReportSource = customersByCityReport
            LiTipo = 0
            FrmImprimir.Show()

        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
   
   
End Class
