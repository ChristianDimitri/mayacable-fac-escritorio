<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCAMDO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Clv_CiudadLabel As System.Windows.Forms.Label
        Dim TELEFONOLabel As System.Windows.Forms.Label
        Dim Clv_ColoniaLabel As System.Windows.Forms.Label
        Dim ENTRECALLESLabel As System.Windows.Forms.Label
        Dim Clv_CalleLabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim SectorLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormCAMDO))
        Me.TELEFONOTextBox = New System.Windows.Forms.TextBox()
        Me.CONCAMDOFACBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.Clv_ColoniaComboBox = New System.Windows.Forms.ComboBox()
        Me.DAMECOLONIA_CALLEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ConCAMDOTMPBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEric3 = New softvFacturacion.DataSetEric3()
        Me.ENTRECALLESTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CiudadComboBox = New System.Windows.Forms.ComboBox()
        Me.MuestraCVECOLCIUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NUMEROTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CalleComboBox = New System.Windows.Forms.ComboBox()
        Me.MUESTRACALLESBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.Clv_CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cmbTap = New System.Windows.Forms.ComboBox()
        Me.SectorComboBox = New System.Windows.Forms.ComboBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Clv_ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_CalleTextBox = New System.Windows.Forms.TextBox()
        Me.Clv_OrdenTextBox = New System.Windows.Forms.TextBox()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.CONCAMDOFACTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.CONCAMDOFACTableAdapter()
        Me.CONCAMDOFACBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.BindingNavigatorDeleteItem = New System.Windows.Forms.ToolStripButton()
        Me.CONCAMDOFACBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.MUESTRACALLESTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRACALLESTableAdapter()
        Me.DAMECOLONIA_CALLETableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DAMECOLONIA_CALLETableAdapter()
        Me.MuestraCVECOLCIUTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.MuestraCVECOLCIUTableAdapter()
        Me.ConCAMDOTMPTableAdapter = New softvFacturacion.DataSetEric3TableAdapters.ConCAMDOTMPTableAdapter()
        Me.ContratoTextBox1 = New System.Windows.Forms.TextBox()
        Me.Clv_SessionTextBox = New System.Windows.Forms.TextBox()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSet1 = New System.Data.DataSet()
        Me.DataTable1 = New System.Data.DataTable()
        Me.DataColumn1 = New System.Data.DataColumn()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Clv_CiudadLabel = New System.Windows.Forms.Label()
        TELEFONOLabel = New System.Windows.Forms.Label()
        Clv_ColoniaLabel = New System.Windows.Forms.Label()
        ENTRECALLESLabel = New System.Windows.Forms.Label()
        Clv_CalleLabel = New System.Windows.Forms.Label()
        NUMEROLabel = New System.Windows.Forms.Label()
        SectorLabel = New System.Windows.Forms.Label()
        CType(Me.CONCAMDOFACBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ConCAMDOTMPBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.CONCAMDOFACBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CONCAMDOFACBindingNavigator.SuspendLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Clv_CiudadLabel
        '
        Clv_CiudadLabel.AutoSize = True
        Clv_CiudadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CiudadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CiudadLabel.Location = New System.Drawing.Point(79, 256)
        Clv_CiudadLabel.Name = "Clv_CiudadLabel"
        Clv_CiudadLabel.Size = New System.Drawing.Size(60, 15)
        Clv_CiudadLabel.TabIndex = 20
        Clv_CiudadLabel.Text = "Ciudad :"
        Clv_CiudadLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TELEFONOLabel
        '
        TELEFONOLabel.AutoSize = True
        TELEFONOLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TELEFONOLabel.ForeColor = System.Drawing.Color.LightSlateGray
        TELEFONOLabel.Location = New System.Drawing.Point(68, 221)
        TELEFONOLabel.Name = "TELEFONOLabel"
        TELEFONOLabel.Size = New System.Drawing.Size(71, 15)
        TELEFONOLabel.TabIndex = 16
        TELEFONOLabel.Text = "Teléfono :"
        TELEFONOLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Clv_ColoniaLabel
        '
        Clv_ColoniaLabel.AutoSize = True
        Clv_ColoniaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_ColoniaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_ColoniaLabel.Location = New System.Drawing.Point(75, 146)
        Clv_ColoniaLabel.Name = "Clv_ColoniaLabel"
        Clv_ColoniaLabel.Size = New System.Drawing.Size(64, 15)
        Clv_ColoniaLabel.TabIndex = 14
        Clv_ColoniaLabel.Text = "Colonia :"
        Clv_ColoniaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ENTRECALLESLabel
        '
        ENTRECALLESLabel.AutoSize = True
        ENTRECALLESLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ENTRECALLESLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ENTRECALLESLabel.Location = New System.Drawing.Point(46, 108)
        ENTRECALLESLabel.Name = "ENTRECALLESLabel"
        ENTRECALLESLabel.Size = New System.Drawing.Size(93, 15)
        ENTRECALLESLabel.TabIndex = 12
        ENTRECALLESLabel.Text = "Entre Calles :"
        ENTRECALLESLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Clv_CalleLabel
        '
        Clv_CalleLabel.AutoSize = True
        Clv_CalleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_CalleLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Clv_CalleLabel.Location = New System.Drawing.Point(91, 35)
        Clv_CalleLabel.Name = "Clv_CalleLabel"
        Clv_CalleLabel.Size = New System.Drawing.Size(48, 15)
        Clv_CalleLabel.TabIndex = 8
        Clv_CalleLabel.Text = "Calle :"
        Clv_CalleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.LightSlateGray
        NUMEROLabel.Location = New System.Drawing.Point(73, 72)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(66, 15)
        NUMEROLabel.TabIndex = 10
        NUMEROLabel.Text = "Numero :"
        NUMEROLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'SectorLabel
        '
        SectorLabel.AutoSize = True
        SectorLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SectorLabel.ForeColor = System.Drawing.Color.LightSlateGray
        SectorLabel.Location = New System.Drawing.Point(83, 185)
        SectorLabel.Name = "SectorLabel"
        SectorLabel.Size = New System.Drawing.Size(56, 15)
        SectorLabel.TabIndex = 21
        SectorLabel.Text = "Sector :"
        SectorLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TELEFONOTextBox
        '
        Me.TELEFONOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TELEFONOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOFACBindingSource, "TELEFONO", True))
        Me.TELEFONOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TELEFONOTextBox.ForeColor = System.Drawing.Color.Black
        Me.TELEFONOTextBox.Location = New System.Drawing.Point(145, 215)
        Me.TELEFONOTextBox.Name = "TELEFONOTextBox"
        Me.TELEFONOTextBox.Size = New System.Drawing.Size(203, 21)
        Me.TELEFONOTextBox.TabIndex = 4
        '
        'CONCAMDOFACBindingSource
        '
        Me.CONCAMDOFACBindingSource.DataMember = "CONCAMDOFAC"
        Me.CONCAMDOFACBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_ColoniaComboBox
        '
        Me.Clv_ColoniaComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCAMDOFACBindingSource, "Clv_Colonia", True))
        Me.Clv_ColoniaComboBox.DataSource = Me.DAMECOLONIA_CALLEBindingSource
        Me.Clv_ColoniaComboBox.DisplayMember = "COLONIA"
        Me.Clv_ColoniaComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_ColoniaComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_ColoniaComboBox.FormattingEnabled = True
        Me.Clv_ColoniaComboBox.Location = New System.Drawing.Point(145, 138)
        Me.Clv_ColoniaComboBox.Name = "Clv_ColoniaComboBox"
        Me.Clv_ColoniaComboBox.Size = New System.Drawing.Size(338, 23)
        Me.Clv_ColoniaComboBox.TabIndex = 3
        Me.Clv_ColoniaComboBox.ValueMember = "CLV_COLONIA"
        '
        'DAMECOLONIA_CALLEBindingSource
        '
        Me.DAMECOLONIA_CALLEBindingSource.DataMember = "DAMECOLONIA_CALLE"
        Me.DAMECOLONIA_CALLEBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'ConCAMDOTMPBindingSource
        '
        Me.ConCAMDOTMPBindingSource.DataMember = "ConCAMDOTMP"
        Me.ConCAMDOTMPBindingSource.DataSource = Me.DataSetEric3
        '
        'DataSetEric3
        '
        Me.DataSetEric3.DataSetName = "DataSetEric3"
        Me.DataSetEric3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ENTRECALLESTextBox
        '
        Me.ENTRECALLESTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.ENTRECALLESTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOFACBindingSource, "ENTRECALLES", True))
        Me.ENTRECALLESTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ENTRECALLESTextBox.ForeColor = System.Drawing.Color.Black
        Me.ENTRECALLESTextBox.Location = New System.Drawing.Point(145, 102)
        Me.ENTRECALLESTextBox.Name = "ENTRECALLESTextBox"
        Me.ENTRECALLESTextBox.Size = New System.Drawing.Size(338, 21)
        Me.ENTRECALLESTextBox.TabIndex = 2
        '
        'Clv_CiudadComboBox
        '
        Me.Clv_CiudadComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCAMDOFACBindingSource, "Clv_Ciudad", True))
        Me.Clv_CiudadComboBox.DataSource = Me.MuestraCVECOLCIUBindingSource
        Me.Clv_CiudadComboBox.DisplayMember = "Nombre"
        Me.Clv_CiudadComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CiudadComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_CiudadComboBox.FormattingEnabled = True
        Me.Clv_CiudadComboBox.Location = New System.Drawing.Point(145, 248)
        Me.Clv_CiudadComboBox.Name = "Clv_CiudadComboBox"
        Me.Clv_CiudadComboBox.Size = New System.Drawing.Size(237, 23)
        Me.Clv_CiudadComboBox.TabIndex = 5
        Me.Clv_CiudadComboBox.ValueMember = "Clv_Ciudad"
        '
        'MuestraCVECOLCIUBindingSource
        '
        Me.MuestraCVECOLCIUBindingSource.DataMember = "MuestraCVECOLCIU"
        Me.MuestraCVECOLCIUBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'NUMEROTextBox
        '
        Me.NUMEROTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NUMEROTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOFACBindingSource, "NUMERO", True))
        Me.NUMEROTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROTextBox.ForeColor = System.Drawing.Color.Black
        Me.NUMEROTextBox.Location = New System.Drawing.Point(145, 66)
        Me.NUMEROTextBox.Name = "NUMEROTextBox"
        Me.NUMEROTextBox.Size = New System.Drawing.Size(203, 21)
        Me.NUMEROTextBox.TabIndex = 1
        '
        'Clv_CalleComboBox
        '
        Me.Clv_CalleComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.CONCAMDOFACBindingSource, "Clv_Calle", True))
        Me.Clv_CalleComboBox.DataSource = Me.MUESTRACALLESBindingSource
        Me.Clv_CalleComboBox.DisplayMember = "NOMBRE"
        Me.Clv_CalleComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_CalleComboBox.ForeColor = System.Drawing.Color.Black
        Me.Clv_CalleComboBox.FormattingEnabled = True
        Me.Clv_CalleComboBox.Location = New System.Drawing.Point(145, 27)
        Me.Clv_CalleComboBox.Name = "Clv_CalleComboBox"
        Me.Clv_CalleComboBox.Size = New System.Drawing.Size(444, 23)
        Me.Clv_CalleComboBox.TabIndex = 0
        Me.Clv_CalleComboBox.ValueMember = "CLV_CALLE"
        '
        'MUESTRACALLESBindingSource
        '
        Me.MUESTRACALLESBindingSource.DataMember = "MUESTRACALLES"
        Me.MUESTRACALLESBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clv_CiudadTextBox
        '
        Me.Clv_CiudadTextBox.Location = New System.Drawing.Point(277, 0)
        Me.Clv_CiudadTextBox.Name = "Clv_CiudadTextBox"
        Me.Clv_CiudadTextBox.ReadOnly = True
        Me.Clv_CiudadTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_CiudadTextBox.TabIndex = 25
        Me.Clv_CiudadTextBox.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.cmbTap)
        Me.Panel1.Controls.Add(Me.SectorComboBox)
        Me.Panel1.Controls.Add(SectorLabel)
        Me.Panel1.Controls.Add(Me.Clv_CiudadComboBox)
        Me.Panel1.Controls.Add(Clv_CiudadLabel)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.TELEFONOTextBox)
        Me.Panel1.Controls.Add(TELEFONOLabel)
        Me.Panel1.Controls.Add(Me.Clv_ColoniaComboBox)
        Me.Panel1.Controls.Add(Clv_ColoniaLabel)
        Me.Panel1.Controls.Add(Me.ENTRECALLESTextBox)
        Me.Panel1.Controls.Add(ENTRECALLESLabel)
        Me.Panel1.Controls.Add(Me.NUMEROTextBox)
        Me.Panel1.Controls.Add(Clv_CalleLabel)
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Me.Clv_CalleComboBox)
        Me.Panel1.Location = New System.Drawing.Point(0, 31)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(603, 299)
        Me.Panel1.TabIndex = 0
        Me.Panel1.TabStop = True
        '
        'cmbTap
        '
        Me.cmbTap.DisplayMember = "CLAVE"
        Me.cmbTap.Location = New System.Drawing.Point(12, 3)
        Me.cmbTap.Name = "cmbTap"
        Me.cmbTap.Size = New System.Drawing.Size(25, 21)
        Me.cmbTap.TabIndex = 23
        Me.cmbTap.ValueMember = "IdTap"
        Me.cmbTap.Visible = False
        '
        'SectorComboBox
        '
        Me.SectorComboBox.DisplayMember = "DESCRIPCION"
        Me.SectorComboBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SectorComboBox.ForeColor = System.Drawing.Color.Black
        Me.SectorComboBox.FormattingEnabled = True
        Me.SectorComboBox.Location = New System.Drawing.Point(145, 177)
        Me.SectorComboBox.Name = "SectorComboBox"
        Me.SectorComboBox.Size = New System.Drawing.Size(338, 23)
        Me.SectorComboBox.TabIndex = 22
        Me.SectorComboBox.ValueMember = "CLV_SECTOR"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(456, 254)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 33)
        Me.Button5.TabIndex = 7
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Clv_ColoniaTextBox
        '
        Me.Clv_ColoniaTextBox.Location = New System.Drawing.Point(261, 0)
        Me.Clv_ColoniaTextBox.Name = "Clv_ColoniaTextBox"
        Me.Clv_ColoniaTextBox.ReadOnly = True
        Me.Clv_ColoniaTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_ColoniaTextBox.TabIndex = 24
        Me.Clv_ColoniaTextBox.TabStop = False
        '
        'Clv_CalleTextBox
        '
        Me.Clv_CalleTextBox.Location = New System.Drawing.Point(293, 0)
        Me.Clv_CalleTextBox.Name = "Clv_CalleTextBox"
        Me.Clv_CalleTextBox.ReadOnly = True
        Me.Clv_CalleTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_CalleTextBox.TabIndex = 23
        Me.Clv_CalleTextBox.TabStop = False
        '
        'Clv_OrdenTextBox
        '
        Me.Clv_OrdenTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOFACBindingSource, "Clv_Sesion", True))
        Me.Clv_OrdenTextBox.Location = New System.Drawing.Point(325, 0)
        Me.Clv_OrdenTextBox.Name = "Clv_OrdenTextBox"
        Me.Clv_OrdenTextBox.ReadOnly = True
        Me.Clv_OrdenTextBox.Size = New System.Drawing.Size(10, 20)
        Me.Clv_OrdenTextBox.TabIndex = 31
        Me.Clv_OrdenTextBox.TabStop = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.CONCAMDOFACBindingSource, "CONTRATO", True))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(309, 0)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.ReadOnly = True
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(10, 20)
        Me.CONTRATOTextBox.TabIndex = 33
        Me.CONTRATOTextBox.TabStop = False
        '
        'CONCAMDOFACTableAdapter
        '
        Me.CONCAMDOFACTableAdapter.ClearBeforeFill = True
        '
        'CONCAMDOFACBindingNavigator
        '
        Me.CONCAMDOFACBindingNavigator.AddNewItem = Nothing
        Me.CONCAMDOFACBindingNavigator.BindingSource = Me.CONCAMDOFACBindingSource
        Me.CONCAMDOFACBindingNavigator.CountItem = Nothing
        Me.CONCAMDOFACBindingNavigator.DeleteItem = Me.BindingNavigatorDeleteItem
        Me.CONCAMDOFACBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorDeleteItem, Me.CONCAMDOFACBindingNavigatorSaveItem})
        Me.CONCAMDOFACBindingNavigator.Location = New System.Drawing.Point(0, 0)
        Me.CONCAMDOFACBindingNavigator.MoveFirstItem = Nothing
        Me.CONCAMDOFACBindingNavigator.MoveLastItem = Nothing
        Me.CONCAMDOFACBindingNavigator.MoveNextItem = Nothing
        Me.CONCAMDOFACBindingNavigator.MovePreviousItem = Nothing
        Me.CONCAMDOFACBindingNavigator.Name = "CONCAMDOFACBindingNavigator"
        Me.CONCAMDOFACBindingNavigator.PositionItem = Nothing
        Me.CONCAMDOFACBindingNavigator.Size = New System.Drawing.Size(606, 25)
        Me.CONCAMDOFACBindingNavigator.TabIndex = 6
        Me.CONCAMDOFACBindingNavigator.TabStop = True
        Me.CONCAMDOFACBindingNavigator.Text = "BindingNavigator1"
        '
        'BindingNavigatorDeleteItem
        '
        Me.BindingNavigatorDeleteItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.BindingNavigatorDeleteItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BindingNavigatorDeleteItem.Image = CType(resources.GetObject("BindingNavigatorDeleteItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorDeleteItem.Name = "BindingNavigatorDeleteItem"
        Me.BindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorDeleteItem.Size = New System.Drawing.Size(95, 22)
        Me.BindingNavigatorDeleteItem.Text = "&CANCELAR"
        '
        'CONCAMDOFACBindingNavigatorSaveItem
        '
        Me.CONCAMDOFACBindingNavigatorSaveItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.CONCAMDOFACBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONCAMDOFACBindingNavigatorSaveItem.Image = CType(resources.GetObject("CONCAMDOFACBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.CONCAMDOFACBindingNavigatorSaveItem.Name = "CONCAMDOFACBindingNavigatorSaveItem"
        Me.CONCAMDOFACBindingNavigatorSaveItem.Size = New System.Drawing.Size(87, 22)
        Me.CONCAMDOFACBindingNavigatorSaveItem.Text = "&ACEPTAR"
        '
        'MUESTRACALLESTableAdapter
        '
        Me.MUESTRACALLESTableAdapter.ClearBeforeFill = True
        '
        'DAMECOLONIA_CALLETableAdapter
        '
        Me.DAMECOLONIA_CALLETableAdapter.ClearBeforeFill = True
        '
        'MuestraCVECOLCIUTableAdapter
        '
        Me.MuestraCVECOLCIUTableAdapter.ClearBeforeFill = True
        '
        'ConCAMDOTMPTableAdapter
        '
        Me.ConCAMDOTMPTableAdapter.ClearBeforeFill = True
        '
        'ContratoTextBox1
        '
        Me.ContratoTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCAMDOTMPBindingSource, "Contrato", True))
        Me.ContratoTextBox1.Location = New System.Drawing.Point(145, 5)
        Me.ContratoTextBox1.Name = "ContratoTextBox1"
        Me.ContratoTextBox1.Size = New System.Drawing.Size(100, 20)
        Me.ContratoTextBox1.TabIndex = 35
        '
        'Clv_SessionTextBox
        '
        Me.Clv_SessionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ConCAMDOTMPBindingSource, "Clv_Session", True))
        Me.Clv_SessionTextBox.Location = New System.Drawing.Point(44, 5)
        Me.Clv_SessionTextBox.Name = "Clv_SessionTextBox"
        Me.Clv_SessionTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Clv_SessionTextBox.TabIndex = 34
        '
        'BindingSource1
        '
        '
        'DataSet1
        '
        Me.DataSet1.DataSetName = "NewDataSet"
        Me.DataSet1.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1})
        Me.DataTable1.TableName = "Table1"
        '
        'DataColumn1
        '
        Me.DataColumn1.ColumnName = "Column1"
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'FormCAMDO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(606, 330)
        Me.Controls.Add(Me.CONCAMDOFACBindingNavigator)
        Me.Controls.Add(Me.ContratoTextBox1)
        Me.Controls.Add(Me.Clv_SessionTextBox)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Clv_OrdenTextBox)
        Me.Controls.Add(Me.CONTRATOTextBox)
        Me.Controls.Add(Me.Clv_CalleTextBox)
        Me.Controls.Add(Me.Clv_ColoniaTextBox)
        Me.Controls.Add(Me.Clv_CiudadTextBox)
        Me.Name = "FormCAMDO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capture el Nuevo Domicilio"
        CType(Me.CONCAMDOFACBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECOLONIA_CALLEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ConCAMDOTMPBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCVECOLCIUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACALLESBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.CONCAMDOFACBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CONCAMDOFACBindingNavigator.ResumeLayout(False)
        Me.CONCAMDOFACBindingNavigator.PerformLayout()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TELEFONOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_ColoniaComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ENTRECALLESTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CiudadComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NUMEROTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Clv_CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Clv_ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_CalleTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Clv_OrdenTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents CONCAMDOFACBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CONCAMDOFACTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.CONCAMDOFACTableAdapter
    Friend WithEvents CONCAMDOFACBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorDeleteItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents CONCAMDOFACBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents MUESTRACALLESBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACALLESTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.MUESTRACALLESTableAdapter
    Friend WithEvents DAMECOLONIA_CALLEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECOLONIA_CALLETableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DAMECOLONIA_CALLETableAdapter
    Friend WithEvents MuestraCVECOLCIUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCVECOLCIUTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.MuestraCVECOLCIUTableAdapter
    Friend WithEvents DataSetEric3 As softvFacturacion.DataSetEric3
    Friend WithEvents ConCAMDOTMPBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ConCAMDOTMPTableAdapter As softvFacturacion.DataSetEric3TableAdapters.ConCAMDOTMPTableAdapter
    Friend WithEvents ContratoTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Clv_SessionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents SectorComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTap As System.Windows.Forms.ComboBox
    Friend WithEvents BindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DataSet1 As System.Data.DataSet
    Friend WithEvents DataTable1 As System.Data.DataTable
    Friend WithEvents DataColumn1 As System.Data.DataColumn
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
End Class
