<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmNotasdeCreditoLogitel
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBContratoLabel As System.Windows.Forms.Label
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim Fecha_deGeneracionLabel As System.Windows.Forms.Label
        Dim Usuario_CapturaLabel As System.Windows.Forms.Label
        Dim Fecha_CaducidadLabel As System.Windows.Forms.Label
        Dim CMBMontoLabel As System.Windows.Forms.Label
        Dim CMBStatusLabel As System.Windows.Forms.Label
        Dim ObservacionesLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim ServicioLabel As System.Windows.Forms.Label
        Dim ESHOTELLabel1 As System.Windows.Forms.Label
        Dim SOLOINTERNETLabel1 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Clv_sucursalLabel = New System.Windows.Forms.Label()
        Me.Usuario_AutorizoLabel = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CMBClv_NotadeCreditoLabel = New System.Windows.Forms.Label()
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ComboBox8 = New System.Windows.Forms.ComboBox()
        Me.Consulta_NotaCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRACAJAS2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox7 = New System.Windows.Forms.ComboBox()
        Me.MUESTRASUCURSALES2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox5 = New System.Windows.Forms.ComboBox()
        Me.MUESTRACAJERASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MUESTRAUSUARIOSINBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Fecha_deGeneracionDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Fecha_CaducidadDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ObservacionesTextBox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox3 = New System.Windows.Forms.ComboBox()
        Me.DAME_FACTURASDECLIENTEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Detalle_NotasdeCreditoDataGridView = New System.Windows.Forms.DataGridView()
        Me.SecobraDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvdetalleDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvservicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Detalle_NotasdeCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.MontoTextBox = New System.Windows.Forms.TextBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.StatusNotadeCreditoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Clv_NotadeCreditoTextBox = New System.Windows.Forms.TextBox()
        Me.ContratoTextBox = New System.Windows.Forms.TextBox()
        Me.MUESTRAUSUARIOSin2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDOR_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRASUCURSALES2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.StatusNotadeCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.StatusNotadeCreditoTableAdapter()
        Me.DAMEFECHADELSERVIDOR_2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter()
        Me.MUESTRAUSUARIOSINTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRAUSUARIOSINTableAdapter()
        Me.MUESTRAUSUARIOSin2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRAUSUARIOSin2TableAdapter()
        Me.ComboBox6 = New System.Windows.Forms.ComboBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.DetalleNOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.MUESTRACAJERASTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRACAJERASTableAdapter()
        Me.MUESTRACAJAS2TableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRACAJAS2TableAdapter()
        Me.Consulta_NotaCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Consulta_NotaCreditoTableAdapter()
        Me.Detalle_NotasdeCreditoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Detalle_NotasdeCreditoTableAdapter()
        Me.REDLabel3 = New System.Windows.Forms.Label()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.DAME_FACTURASDECLIENTETableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DAME_FACTURASDECLIENTETableAdapter()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Muestra_Tipo_NotaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Muestra_Tipo_NotaTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.Muestra_Tipo_NotaTableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.ESHOTELCheckBox = New System.Windows.Forms.CheckBox()
        Me.BUSCLIPORCONTRATOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SOLOINTERNETCheckBox = New System.Windows.Forms.CheckBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.NOMBRELabel1 = New System.Windows.Forms.Label()
        Me.CALLELabel1 = New System.Windows.Forms.Label()
        Me.COLONIALabel1 = New System.Windows.Forms.Label()
        Me.NUMEROLabel1 = New System.Windows.Forms.Label()
        Me.CIUDADLabel1 = New System.Windows.Forms.Label()
        Me.BUSCLIPORCONTRATOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BUSCLIPORCONTRATOTableAdapter()
        Me.DameSerDELCliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameSerDELCliTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.dameSerDELCliTableAdapter()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.MuestraDetalleNota_por_ConceptoDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MuestraDetalleNota_por_ConceptoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ComboBox9 = New System.Windows.Forms.ComboBox()
        Me.MuestraTipSerPrincipal_SERBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox10 = New System.Windows.Forms.ComboBox()
        Me.MuestraServicios_por_TipoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraTipSerPrincipal_SERTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MuestraTipSerPrincipal_SERTableAdapter()
        Me.MuestraServicios_por_TipoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MuestraServicios_por_TipoTableAdapter()
        Me.MuestraDetalleNota_por_ConceptoTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MuestraDetalleNota_por_ConceptoTableAdapter()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.CMBLabelMonto = New System.Windows.Forms.Label()
        Me.RadioButtonSi = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.RadioButtonNo = New System.Windows.Forms.RadioButton()
        CMBContratoLabel = New System.Windows.Forms.Label()
        FacturaLabel = New System.Windows.Forms.Label()
        Fecha_deGeneracionLabel = New System.Windows.Forms.Label()
        Usuario_CapturaLabel = New System.Windows.Forms.Label()
        Fecha_CaducidadLabel = New System.Windows.Forms.Label()
        CMBMontoLabel = New System.Windows.Forms.Label()
        CMBStatusLabel = New System.Windows.Forms.Label()
        ObservacionesLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        ServicioLabel = New System.Windows.Forms.Label()
        ESHOTELLabel1 = New System.Windows.Forms.Label()
        SOLOINTERNETLabel1 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACAJAS2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRACAJERASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOSINBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAME_FACTURASDECLIENTEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.Detalle_NotasdeCreditoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Detalle_NotasdeCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatusNotadeCreditoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAUSUARIOSin2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Muestra_Tipo_NotaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        CType(Me.MuestraDetalleNota_por_ConceptoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraDetalleNota_por_ConceptoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraTipSerPrincipal_SERBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraServicios_por_TipoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'CMBContratoLabel
        '
        CMBContratoLabel.AutoSize = True
        CMBContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBContratoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBContratoLabel.Location = New System.Drawing.Point(59, 37)
        CMBContratoLabel.Name = "CMBContratoLabel"
        CMBContratoLabel.Size = New System.Drawing.Size(65, 15)
        CMBContratoLabel.TabIndex = 2
        CMBContratoLabel.Text = "Contrato:"
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        FacturaLabel.Location = New System.Drawing.Point(108, 8)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(196, 15)
        FacturaLabel.TabIndex = 4
        FacturaLabel.Text = "Factura de Estado de Cuenta:"
        '
        'Fecha_deGeneracionLabel
        '
        Fecha_deGeneracionLabel.AutoSize = True
        Fecha_deGeneracionLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_deGeneracionLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_deGeneracionLabel.Location = New System.Drawing.Point(152, 90)
        Fecha_deGeneracionLabel.Name = "Fecha_deGeneracionLabel"
        Fecha_deGeneracionLabel.Size = New System.Drawing.Size(152, 15)
        Fecha_deGeneracionLabel.TabIndex = 6
        Fecha_deGeneracionLabel.Text = "Fecha de Generacion :"
        '
        'Usuario_CapturaLabel
        '
        Usuario_CapturaLabel.AutoSize = True
        Usuario_CapturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Usuario_CapturaLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Usuario_CapturaLabel.Location = New System.Drawing.Point(247, 63)
        Usuario_CapturaLabel.Name = "Usuario_CapturaLabel"
        Usuario_CapturaLabel.Size = New System.Drawing.Size(57, 15)
        Usuario_CapturaLabel.TabIndex = 10
        Usuario_CapturaLabel.Text = "Cajero :"
        '
        'Fecha_CaducidadLabel
        '
        Fecha_CaducidadLabel.AutoSize = True
        Fecha_CaducidadLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Fecha_CaducidadLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Fecha_CaducidadLabel.Location = New System.Drawing.Point(178, 114)
        Fecha_CaducidadLabel.Name = "Fecha_CaducidadLabel"
        Fecha_CaducidadLabel.Size = New System.Drawing.Size(126, 15)
        Fecha_CaducidadLabel.TabIndex = 14
        Fecha_CaducidadLabel.Text = "Fecha Caducidad :"
        '
        'CMBMontoLabel
        '
        CMBMontoLabel.AutoSize = True
        CMBMontoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBMontoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBMontoLabel.Location = New System.Drawing.Point(129, 432)
        CMBMontoLabel.Name = "CMBMontoLabel"
        CMBMontoLabel.Size = New System.Drawing.Size(187, 15)
        CMBMontoLabel.TabIndex = 16
        CMBMontoLabel.Text = "Monto de Nota de Crédito: $"
        '
        'CMBStatusLabel
        '
        CMBStatusLabel.AutoSize = True
        CMBStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBStatusLabel.ForeColor = System.Drawing.Color.LightSlateGray
        CMBStatusLabel.Location = New System.Drawing.Point(743, 16)
        CMBStatusLabel.Name = "CMBStatusLabel"
        CMBStatusLabel.Size = New System.Drawing.Size(61, 18)
        CMBStatusLabel.TabIndex = 18
        CMBStatusLabel.Text = "Status:"
        '
        'ObservacionesLabel
        '
        ObservacionesLabel.AutoSize = True
        ObservacionesLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ObservacionesLabel.ForeColor = System.Drawing.Color.LightSlateGray
        ObservacionesLabel.Location = New System.Drawing.Point(541, 63)
        ObservacionesLabel.Name = "ObservacionesLabel"
        ObservacionesLabel.Size = New System.Drawing.Size(109, 15)
        ObservacionesLabel.TabIndex = 20
        ObservacionesLabel.Text = "Observaciones :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.LightSlateGray
        Label5.Location = New System.Drawing.Point(260, 36)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(44, 15)
        Label5.TabIndex = 35
        Label5.Text = "Caja :"
        '
        'ServicioLabel
        '
        ServicioLabel.AutoSize = True
        ServicioLabel.Location = New System.Drawing.Point(562, 13)
        ServicioLabel.Name = "ServicioLabel"
        ServicioLabel.Size = New System.Drawing.Size(151, 16)
        ServicioLabel.TabIndex = 18
        ServicioLabel.Text = "Servicio Asignados :"
        '
        'ESHOTELLabel1
        '
        ESHOTELLabel1.AutoSize = True
        ESHOTELLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ESHOTELLabel1.ForeColor = System.Drawing.Color.White
        ESHOTELLabel1.Location = New System.Drawing.Point(431, 87)
        ESHOTELLabel1.Name = "ESHOTELLabel1"
        ESHOTELLabel1.Size = New System.Drawing.Size(67, 15)
        ESHOTELLabel1.TabIndex = 17
        ESHOTELLabel1.Text = "Es hotel :"
        '
        'SOLOINTERNETLabel1
        '
        SOLOINTERNETLabel1.AutoSize = True
        SOLOINTERNETLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SOLOINTERNETLabel1.ForeColor = System.Drawing.Color.White
        SOLOINTERNETLabel1.Location = New System.Drawing.Point(301, 87)
        SOLOINTERNETLabel1.Name = "SOLOINTERNETLabel1"
        SOLOINTERNETLabel1.Size = New System.Drawing.Size(97, 15)
        SOLOINTERNETLabel1.TabIndex = 16
        SOLOINTERNETLabel1.Text = "Solo Internet :"
        SOLOINTERNETLabel1.Visible = False
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label6.ForeColor = System.Drawing.Color.LightSlateGray
        Label6.Location = New System.Drawing.Point(148, 8)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(118, 15)
        Label6.TabIndex = 39
        Label6.Text = "Tipo de Servicio :"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label7.ForeColor = System.Drawing.Color.LightSlateGray
        Label7.Location = New System.Drawing.Point(491, 8)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(66, 15)
        Label7.TabIndex = 37
        Label7.Text = "Servicio :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.LightSlateGray
        Label14.Location = New System.Drawing.Point(816, 8)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(56, 15)
        Label14.TabIndex = 37
        Label14.Text = "Precio :"
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBLabel1.Location = New System.Drawing.Point(256, 459)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(60, 15)
        Me.CMBLabel1.TabIndex = 28
        Me.CMBLabel1.Text = "Saldo: $"
        '
        'Clv_sucursalLabel
        '
        Me.Clv_sucursalLabel.AutoSize = True
        Me.Clv_sucursalLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_sucursalLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Clv_sucursalLabel.Location = New System.Drawing.Point(484, 9)
        Me.Clv_sucursalLabel.Name = "Clv_sucursalLabel"
        Me.Clv_sucursalLabel.Size = New System.Drawing.Size(272, 15)
        Me.Clv_sucursalLabel.TabIndex = 8
        Me.Clv_sucursalLabel.Text = "Sucursal que Realiza la Nota de Crédito :"
        '
        'Usuario_AutorizoLabel
        '
        Me.Usuario_AutorizoLabel.AutoSize = True
        Me.Usuario_AutorizoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Usuario_AutorizoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Usuario_AutorizoLabel.Location = New System.Drawing.Point(506, 36)
        Me.Usuario_AutorizoLabel.Name = "Usuario_AutorizoLabel"
        Me.Usuario_AutorizoLabel.Size = New System.Drawing.Size(250, 15)
        Me.Usuario_AutorizoLabel.TabIndex = 12
        Me.Usuario_AutorizoLabel.Text = "Usuario que Realiza Nota de Crédito :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label4.Location = New System.Drawing.Point(5, 9)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(299, 15)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "Sucursal Donde se Aplica la Nota de Crédito :"
        '
        'CMBClv_NotadeCreditoLabel
        '
        Me.CMBClv_NotadeCreditoLabel.AutoSize = True
        Me.CMBClv_NotadeCreditoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBClv_NotadeCreditoLabel.ForeColor = System.Drawing.Color.LightSlateGray
        Me.CMBClv_NotadeCreditoLabel.Location = New System.Drawing.Point(13, 13)
        Me.CMBClv_NotadeCreditoLabel.Name = "CMBClv_NotadeCreditoLabel"
        Me.CMBClv_NotadeCreditoLabel.Size = New System.Drawing.Size(111, 15)
        Me.CMBClv_NotadeCreditoLabel.TabIndex = 0
        Me.CMBClv_NotadeCreditoLabel.Text = "Nota de Crédito:"
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ComboBox8)
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Me.ComboBox7)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ComboBox5)
        Me.Panel1.Controls.Add(Me.ComboBox4)
        Me.Panel1.Controls.Add(Me.ComboBox1)
        Me.Panel1.Controls.Add(Fecha_deGeneracionLabel)
        Me.Panel1.Controls.Add(Me.Fecha_deGeneracionDateTimePicker)
        Me.Panel1.Controls.Add(Me.Clv_sucursalLabel)
        Me.Panel1.Controls.Add(Usuario_CapturaLabel)
        Me.Panel1.Controls.Add(Me.Usuario_AutorizoLabel)
        Me.Panel1.Controls.Add(Fecha_CaducidadLabel)
        Me.Panel1.Controls.Add(Me.Fecha_CaducidadDateTimePicker)
        Me.Panel1.Controls.Add(ObservacionesLabel)
        Me.Panel1.Controls.Add(Me.ObservacionesTextBox)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(12, 212)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(975, 139)
        Me.Panel1.TabIndex = 1
        '
        'ComboBox8
        '
        Me.ComboBox8.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "caja", True))
        Me.ComboBox8.DataSource = Me.MUESTRACAJAS2BindingSource
        Me.ComboBox8.DisplayMember = "NOMBRE"
        Me.ComboBox8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox8.FormattingEnabled = True
        Me.ComboBox8.Location = New System.Drawing.Point(308, 33)
        Me.ComboBox8.Name = "ComboBox8"
        Me.ComboBox8.Size = New System.Drawing.Size(172, 23)
        Me.ComboBox8.TabIndex = 36
        Me.ComboBox8.ValueMember = "CLV_USUARIO"
        '
        'Consulta_NotaCreditoBindingSource
        '
        Me.Consulta_NotaCreditoBindingSource.DataMember = "Consulta_NotaCredito"
        Me.Consulta_NotaCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'MUESTRACAJAS2BindingSource
        '
        Me.MUESTRACAJAS2BindingSource.DataMember = "MUESTRACAJAS2"
        Me.MUESTRACAJAS2BindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox7
        '
        Me.ComboBox7.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "clv_suc_aplica", True))
        Me.ComboBox7.DataSource = Me.MUESTRASUCURSALES2BindingSource
        Me.ComboBox7.DisplayMember = "NOMBRE"
        Me.ComboBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox7.FormattingEnabled = True
        Me.ComboBox7.Location = New System.Drawing.Point(307, 6)
        Me.ComboBox7.Name = "ComboBox7"
        Me.ComboBox7.Size = New System.Drawing.Size(173, 23)
        Me.ComboBox7.TabIndex = 34
        Me.ComboBox7.ValueMember = "CLV_SUCURSAL"
        '
        'MUESTRASUCURSALES2BindingSource
        '
        Me.MUESTRASUCURSALES2BindingSource.DataMember = "MUESTRASUCURSALES2"
        Me.MUESTRASUCURSALES2BindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox5
        '
        Me.ComboBox5.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "Usuario_Captura", True))
        Me.ComboBox5.DataSource = Me.MUESTRACAJERASBindingSource
        Me.ComboBox5.DisplayMember = "Nombre"
        Me.ComboBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox5.FormattingEnabled = True
        Me.ComboBox5.Location = New System.Drawing.Point(308, 60)
        Me.ComboBox5.Name = "ComboBox5"
        Me.ComboBox5.Size = New System.Drawing.Size(172, 23)
        Me.ComboBox5.TabIndex = 27
        Me.ComboBox5.ValueMember = "Clv_Usuario"
        '
        'MUESTRACAJERASBindingSource
        '
        Me.MUESTRACAJERASBindingSource.DataMember = "MUESTRACAJERAS"
        Me.MUESTRACAJERASBindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox4
        '
        Me.ComboBox4.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "Usuario_Autorizo", True))
        Me.ComboBox4.DataSource = Me.MUESTRAUSUARIOSINBindingSource
        Me.ComboBox4.DisplayMember = "Clv_Usuario"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(758, 33)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(193, 23)
        Me.ComboBox4.TabIndex = 26
        Me.ComboBox4.ValueMember = "clave"
        '
        'MUESTRAUSUARIOSINBindingSource
        '
        Me.MUESTRAUSUARIOSINBindingSource.DataMember = "MUESTRAUSUARIOSIN"
        Me.MUESTRAUSUARIOSINBindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "clv_sucursal", True))
        Me.ComboBox1.DataSource = Me.MUESTRASUCURSALES2BindingSource
        Me.ComboBox1.DisplayMember = "NOMBRE"
        Me.ComboBox1.Enabled = False
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(758, 6)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(193, 23)
        Me.ComboBox1.TabIndex = 22
        Me.ComboBox1.ValueMember = "CLV_SUCURSAL"
        '
        'Fecha_deGeneracionDateTimePicker
        '
        Me.Fecha_deGeneracionDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_deGeneracionDateTimePicker.CustomFormat = "aa-mm-dd"
        Me.Fecha_deGeneracionDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Fecha_deGeneracion", True))
        Me.Fecha_deGeneracionDateTimePicker.Enabled = False
        Me.Fecha_deGeneracionDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_deGeneracionDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_deGeneracionDateTimePicker.Location = New System.Drawing.Point(307, 87)
        Me.Fecha_deGeneracionDateTimePicker.Name = "Fecha_deGeneracionDateTimePicker"
        Me.Fecha_deGeneracionDateTimePicker.Size = New System.Drawing.Size(173, 21)
        Me.Fecha_deGeneracionDateTimePicker.TabIndex = 7
        '
        'Fecha_CaducidadDateTimePicker
        '
        Me.Fecha_CaducidadDateTimePicker.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_CaducidadDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Fecha_Caducidad", True))
        Me.Fecha_CaducidadDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Fecha_CaducidadDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Fecha_CaducidadDateTimePicker.Location = New System.Drawing.Point(307, 111)
        Me.Fecha_CaducidadDateTimePicker.Name = "Fecha_CaducidadDateTimePicker"
        Me.Fecha_CaducidadDateTimePicker.Size = New System.Drawing.Size(173, 21)
        Me.Fecha_CaducidadDateTimePicker.TabIndex = 15
        '
        'ObservacionesTextBox
        '
        Me.ObservacionesTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Observaciones", True))
        Me.ObservacionesTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ObservacionesTextBox.Location = New System.Drawing.Point(654, 62)
        Me.ObservacionesTextBox.Multiline = True
        Me.ObservacionesTextBox.Name = "ObservacionesTextBox"
        Me.ObservacionesTextBox.Size = New System.Drawing.Size(297, 74)
        Me.ObservacionesTextBox.TabIndex = 21
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.LightSlateGray
        Me.Label3.Location = New System.Drawing.Point(163, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(141, 15)
        Me.Label3.TabIndex = 37
        Me.Label3.Text = "Detalle de la Factura"
        '
        'ComboBox3
        '
        Me.ComboBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Column1", True))
        Me.ComboBox3.DataSource = Me.DAME_FACTURASDECLIENTEBindingSource
        Me.ComboBox3.DisplayMember = "FACTURAS"
        Me.ComboBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox3.FormattingEnabled = True
        Me.ComboBox3.Location = New System.Drawing.Point(307, 0)
        Me.ComboBox3.Name = "ComboBox3"
        Me.ComboBox3.Size = New System.Drawing.Size(173, 23)
        Me.ComboBox3.TabIndex = 25
        Me.ComboBox3.ValueMember = "CLV_FACTURA"
        '
        'DAME_FACTURASDECLIENTEBindingSource
        '
        Me.DAME_FACTURASDECLIENTEBindingSource.DataMember = "DAME_FACTURASDECLIENTE"
        Me.DAME_FACTURASDECLIENTEBindingSource.DataSource = Me.DataSetLydia
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Detalle_NotasdeCreditoDataGridView)
        Me.Panel2.Location = New System.Drawing.Point(307, 32)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(645, 182)
        Me.Panel2.TabIndex = 30
        '
        'Detalle_NotasdeCreditoDataGridView
        '
        Me.Detalle_NotasdeCreditoDataGridView.AllowUserToAddRows = False
        Me.Detalle_NotasdeCreditoDataGridView.AllowUserToDeleteRows = False
        Me.Detalle_NotasdeCreditoDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Detalle_NotasdeCreditoDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Detalle_NotasdeCreditoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SecobraDataGridViewCheckBoxColumn, Me.DescripcionDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.ClvdetalleDataGridViewTextBoxColumn, Me.ClvservicioDataGridViewTextBoxColumn})
        Me.Detalle_NotasdeCreditoDataGridView.DataSource = Me.Detalle_NotasdeCreditoBindingSource
        Me.Detalle_NotasdeCreditoDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Detalle_NotasdeCreditoDataGridView.Location = New System.Drawing.Point(0, 0)
        Me.Detalle_NotasdeCreditoDataGridView.Name = "Detalle_NotasdeCreditoDataGridView"
        Me.Detalle_NotasdeCreditoDataGridView.ReadOnly = True
        Me.Detalle_NotasdeCreditoDataGridView.Size = New System.Drawing.Size(645, 182)
        Me.Detalle_NotasdeCreditoDataGridView.TabIndex = 0
        '
        'SecobraDataGridViewCheckBoxColumn
        '
        Me.SecobraDataGridViewCheckBoxColumn.DataPropertyName = "se_cobra"
        Me.SecobraDataGridViewCheckBoxColumn.HeaderText = "COBRAR"
        Me.SecobraDataGridViewCheckBoxColumn.Name = "SecobraDataGridViewCheckBoxColumn"
        Me.SecobraDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "DESCRIPCION"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        Me.DescripcionDataGridViewTextBoxColumn.Width = 400
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "importe"
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "IMPORTE"
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClvdetalleDataGridViewTextBoxColumn
        '
        Me.ClvdetalleDataGridViewTextBoxColumn.DataPropertyName = "clv_detalle"
        Me.ClvdetalleDataGridViewTextBoxColumn.HeaderText = "clv_detalle"
        Me.ClvdetalleDataGridViewTextBoxColumn.Name = "ClvdetalleDataGridViewTextBoxColumn"
        Me.ClvdetalleDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvdetalleDataGridViewTextBoxColumn.Visible = False
        '
        'ClvservicioDataGridViewTextBoxColumn
        '
        Me.ClvservicioDataGridViewTextBoxColumn.DataPropertyName = "clv_servicio"
        Me.ClvservicioDataGridViewTextBoxColumn.HeaderText = "clv_servicio"
        Me.ClvservicioDataGridViewTextBoxColumn.Name = "ClvservicioDataGridViewTextBoxColumn"
        Me.ClvservicioDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvservicioDataGridViewTextBoxColumn.Visible = False
        '
        'Detalle_NotasdeCreditoBindingSource
        '
        Me.Detalle_NotasdeCreditoBindingSource.DataMember = "Detalle_NotasdeCredito"
        Me.Detalle_NotasdeCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.White
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "saldo", True))
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(319, 455)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(170, 21)
        Me.TextBox1.TabIndex = 29
        '
        'MontoTextBox
        '
        Me.MontoTextBox.BackColor = System.Drawing.Color.White
        Me.MontoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Monto", True))
        Me.MontoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MontoTextBox.Location = New System.Drawing.Point(319, 426)
        Me.MontoTextBox.Name = "MontoTextBox"
        Me.MontoTextBox.Size = New System.Drawing.Size(170, 21)
        Me.MontoTextBox.TabIndex = 17
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel3.Controls.Add(Me.Button5)
        Me.Panel3.Controls.Add(Me.Label2)
        Me.Panel3.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.Panel3.Location = New System.Drawing.Point(22, 400)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(34, 33)
        Me.Panel3.TabIndex = 38
        Me.Panel3.Visible = False
        '
        'Button5
        '
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.Location = New System.Drawing.Point(543, 15)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(37, 20)
        Me.Button5.TabIndex = 40
        Me.Button5.Text = "X"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(129, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(339, 18)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Pagos Realizados con esta Nota de Crédito"
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DetalleNOTASDECREDITODataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.DetalleNOTASDECREDITODataGridView.ColumnHeadersHeight = 40
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn1})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(77, 60)
        Me.DetalleNOTASDECREDITODataGridView.MultiSelect = False
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.ReadOnly = True
        Me.DetalleNOTASDECREDITODataGridView.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(445, 195)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 29
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Column1"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn3.HeaderText = "Factura"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "monto"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Monto"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "FECHA_degeneracion"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha de Generación"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Folio Nota de Crédito"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox2
        '
        Me.ComboBox2.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "Status", True))
        Me.ComboBox2.DataSource = Me.StatusNotadeCreditoBindingSource
        Me.ComboBox2.DisplayMember = "Status"
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(816, 13)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(164, 26)
        Me.ComboBox2.TabIndex = 23
        Me.ComboBox2.ValueMember = "Clv_Status"
        '
        'StatusNotadeCreditoBindingSource
        '
        Me.StatusNotadeCreditoBindingSource.DataMember = "StatusNotadeCredito"
        Me.StatusNotadeCreditoBindingSource.DataSource = Me.DataSetLydia
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(240, 36)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(37, 20)
        Me.Button2.TabIndex = 24
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Clv_NotadeCreditoTextBox
        '
        Me.Clv_NotadeCreditoTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_NotadeCreditoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Clv_NotadeCredito", True))
        Me.Clv_NotadeCreditoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_NotadeCreditoTextBox.Location = New System.Drawing.Point(130, 10)
        Me.Clv_NotadeCreditoTextBox.Name = "Clv_NotadeCreditoTextBox"
        Me.Clv_NotadeCreditoTextBox.ReadOnly = True
        Me.Clv_NotadeCreditoTextBox.Size = New System.Drawing.Size(110, 20)
        Me.Clv_NotadeCreditoTextBox.TabIndex = 50
        Me.Clv_NotadeCreditoTextBox.Text = "0"
        '
        'ContratoTextBox
        '
        Me.ContratoTextBox.CausesValidation = False
        Me.ContratoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Contrato", True))
        Me.ContratoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ContratoTextBox.Location = New System.Drawing.Point(130, 36)
        Me.ContratoTextBox.Name = "ContratoTextBox"
        Me.ContratoTextBox.Size = New System.Drawing.Size(110, 20)
        Me.ContratoTextBox.TabIndex = 1
        '
        'MUESTRAUSUARIOSin2BindingSource
        '
        Me.MUESTRAUSUARIOSin2BindingSource.DataMember = "MUESTRAUSUARIOSin2"
        Me.MUESTRAUSUARIOSin2BindingSource.DataSource = Me.DataSetLydia
        '
        'DAMEFECHADELSERVIDOR_2BindingSource
        '
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataMember = "DAMEFECHADELSERVIDOR_2"
        Me.DAMEFECHADELSERVIDOR_2BindingSource.DataSource = Me.DataSetLydia
        '
        'MUESTRASUCURSALES2TableAdapter
        '
        Me.MUESTRASUCURSALES2TableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(809, 488)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(171, 34)
        Me.Button1.TabIndex = 27
        Me.Button1.Text = "&SALIR"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'StatusNotadeCreditoTableAdapter
        '
        Me.StatusNotadeCreditoTableAdapter.ClearBeforeFill = True
        '
        'DAMEFECHADELSERVIDOR_2TableAdapter
        '
        Me.DAMEFECHADELSERVIDOR_2TableAdapter.ClearBeforeFill = True
        '
        'MUESTRAUSUARIOSINTableAdapter
        '
        Me.MUESTRAUSUARIOSINTableAdapter.ClearBeforeFill = True
        '
        'MUESTRAUSUARIOSin2TableAdapter
        '
        Me.MUESTRAUSUARIOSin2TableAdapter.ClearBeforeFill = True
        '
        'ComboBox6
        '
        Me.ComboBox6.DataSource = Me.DAME_FACTURASDECLIENTEBindingSource
        Me.ComboBox6.DisplayMember = "Monto"
        Me.ComboBox6.FormattingEnabled = True
        Me.ComboBox6.Location = New System.Drawing.Point(242, 766)
        Me.ComboBox6.Name = "ComboBox6"
        Me.ComboBox6.Size = New System.Drawing.Size(53, 21)
        Me.ComboBox6.TabIndex = 28
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DAMEFECHADELSERVIDOR_2BindingSource, "FECHA", True))
        Me.DateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DateTimePicker1.Location = New System.Drawing.Point(161, 767)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(75, 20)
        Me.DateTimePicker1.TabIndex = 29
        '
        'Button3
        '
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(608, 488)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(171, 34)
        Me.Button3.TabIndex = 31
        Me.Button3.Text = "&GUARDAR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'TextBox2
        '
        Me.TextBox2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Detalle_NotasdeCreditoBindingSource, "clv_detalle", True))
        Me.TextBox2.Location = New System.Drawing.Point(3, 767)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(34, 20)
        Me.TextBox2.TabIndex = 32
        '
        'TextBox3
        '
        Me.TextBox3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Detalle_NotasdeCreditoBindingSource, "clv_servicio", True))
        Me.TextBox3.Location = New System.Drawing.Point(96, 767)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(59, 20)
        Me.TextBox3.TabIndex = 33
        '
        'TextBox4
        '
        Me.TextBox4.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "factura", True))
        Me.TextBox4.Location = New System.Drawing.Point(301, 768)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(51, 20)
        Me.TextBox4.TabIndex = 34
        '
        'MUESTRACAJERASTableAdapter
        '
        Me.MUESTRACAJERASTableAdapter.ClearBeforeFill = True
        '
        'MUESTRACAJAS2TableAdapter
        '
        Me.MUESTRACAJAS2TableAdapter.ClearBeforeFill = True
        '
        'Consulta_NotaCreditoTableAdapter
        '
        Me.Consulta_NotaCreditoTableAdapter.ClearBeforeFill = True
        '
        'Detalle_NotasdeCreditoTableAdapter
        '
        Me.Detalle_NotasdeCreditoTableAdapter.ClearBeforeFill = True
        '
        'REDLabel3
        '
        Me.REDLabel3.AutoSize = True
        Me.REDLabel3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.REDLabel3.Location = New System.Drawing.Point(354, 15)
        Me.REDLabel3.Name = "REDLabel3"
        Me.REDLabel3.Size = New System.Drawing.Size(111, 20)
        Me.REDLabel3.TabIndex = 35
        Me.REDLabel3.Text = "Tipo de Nota"
        '
        'TextBox5
        '
        Me.TextBox5.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "factura", True))
        Me.TextBox5.Location = New System.Drawing.Point(43, 767)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(47, 20)
        Me.TextBox5.TabIndex = 37
        '
        'DAME_FACTURASDECLIENTETableAdapter
        '
        Me.DAME_FACTURASDECLIENTETableAdapter.ClearBeforeFill = True
        '
        'TextBox6
        '
        Me.TextBox6.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Usuario_Captura", True))
        Me.TextBox6.Location = New System.Drawing.Point(358, 768)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(46, 20)
        Me.TextBox6.TabIndex = 38
        '
        'Button4
        '
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.Location = New System.Drawing.Point(9, 489)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(511, 34)
        Me.Button4.TabIndex = 39
        Me.Button4.Text = "&PAGOS REALIZADOS CON LA NOTA DE CREDITO"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Muestra_Tipo_NotaBindingSource
        '
        Me.Muestra_Tipo_NotaBindingSource.DataMember = "Muestra_Tipo_Nota"
        Me.Muestra_Tipo_NotaBindingSource.DataSource = Me.DataSetLydia
        '
        'Muestra_Tipo_NotaTableAdapter
        '
        Me.Muestra_Tipo_NotaTableAdapter.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.GroupBox1.Controls.Add(Me.TreeView1)
        Me.GroupBox1.Controls.Add(ServicioLabel)
        Me.GroupBox1.Controls.Add(ESHOTELLabel1)
        Me.GroupBox1.Controls.Add(Me.ESHOTELCheckBox)
        Me.GroupBox1.Controls.Add(SOLOINTERNETLabel1)
        Me.GroupBox1.Controls.Add(Me.SOLOINTERNETCheckBox)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.NOMBRELabel1)
        Me.GroupBox1.Controls.Add(Me.CALLELabel1)
        Me.GroupBox1.Controls.Add(Me.COLONIALabel1)
        Me.GroupBox1.Controls.Add(Me.NUMEROLabel1)
        Me.GroupBox1.Controls.Add(Me.CIUDADLabel1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.White
        Me.GroupBox1.Location = New System.Drawing.Point(12, 70)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(975, 136)
        Me.GroupBox1.TabIndex = 173
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos del Cliente"
        '
        'TreeView1
        '
        Me.TreeView1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TreeView1.Location = New System.Drawing.Point(565, 32)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(382, 79)
        Me.TreeView1.TabIndex = 19
        Me.TreeView1.TabStop = False
        '
        'ESHOTELCheckBox
        '
        Me.ESHOTELCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOBindingSource, "ESHOTEL", True))
        Me.ESHOTELCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ESHOTELCheckBox.Location = New System.Drawing.Point(504, 81)
        Me.ESHOTELCheckBox.Name = "ESHOTELCheckBox"
        Me.ESHOTELCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.ESHOTELCheckBox.TabIndex = 18
        Me.ESHOTELCheckBox.TabStop = False
        '
        'BUSCLIPORCONTRATOBindingSource
        '
        Me.BUSCLIPORCONTRATOBindingSource.DataMember = "BUSCLIPORCONTRATO"
        Me.BUSCLIPORCONTRATOBindingSource.DataSource = Me.DataSetLydia
        '
        'SOLOINTERNETCheckBox
        '
        Me.SOLOINTERNETCheckBox.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.BUSCLIPORCONTRATOBindingSource, "SOLOINTERNET", True))
        Me.SOLOINTERNETCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.SOLOINTERNETCheckBox.Location = New System.Drawing.Point(404, 81)
        Me.SOLOINTERNETCheckBox.Name = "SOLOINTERNETCheckBox"
        Me.SOLOINTERNETCheckBox.Size = New System.Drawing.Size(21, 24)
        Me.SOLOINTERNETCheckBox.TabIndex = 17
        Me.SOLOINTERNETCheckBox.TabStop = False
        Me.SOLOINTERNETCheckBox.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(379, 55)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(31, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Col. :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(300, 55)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(20, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "# :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(10, 55)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(58, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Dirección :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(11, 32)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(50, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Nombre :"
        '
        'NOMBRELabel1
        '
        Me.NOMBRELabel1.AutoEllipsis = True
        Me.NOMBRELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NOMBRELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "NOMBRE", True))
        Me.NOMBRELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRELabel1.Location = New System.Drawing.Point(70, 32)
        Me.NOMBRELabel1.Name = "NOMBRELabel1"
        Me.NOMBRELabel1.Size = New System.Drawing.Size(467, 23)
        Me.NOMBRELabel1.TabIndex = 3
        '
        'CALLELabel1
        '
        Me.CALLELabel1.AutoEllipsis = True
        Me.CALLELabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "CALLE", True))
        Me.CALLELabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CALLELabel1.Location = New System.Drawing.Point(70, 55)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(224, 23)
        Me.CALLELabel1.TabIndex = 5
        '
        'COLONIALabel1
        '
        Me.COLONIALabel1.AutoEllipsis = True
        Me.COLONIALabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.COLONIALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "COLONIA", True))
        Me.COLONIALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.COLONIALabel1.Location = New System.Drawing.Point(416, 55)
        Me.COLONIALabel1.Name = "COLONIALabel1"
        Me.COLONIALabel1.Size = New System.Drawing.Size(121, 23)
        Me.COLONIALabel1.TabIndex = 7
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.AutoEllipsis = True
        Me.NUMEROLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCLIPORCONTRATOBindingSource, "NUMERO", True))
        Me.NUMEROLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(326, 55)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(47, 23)
        Me.NUMEROLabel1.TabIndex = 9
        '
        'CIUDADLabel1
        '
        Me.CIUDADLabel1.AutoEllipsis = True
        Me.CIUDADLabel1.BackColor = System.Drawing.Color.DarkOrange
        Me.CIUDADLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CIUDADLabel1.Location = New System.Drawing.Point(70, 79)
        Me.CIUDADLabel1.Name = "CIUDADLabel1"
        Me.CIUDADLabel1.Size = New System.Drawing.Size(224, 23)
        Me.CIUDADLabel1.TabIndex = 11
        '
        'BUSCLIPORCONTRATOTableAdapter
        '
        Me.BUSCLIPORCONTRATOTableAdapter.ClearBeforeFill = True
        '
        'DameSerDELCliBindingSource
        '
        Me.DameSerDELCliBindingSource.DataSource = Me.DataSetLydia
        Me.DameSerDELCliBindingSource.Position = 0
        '
        'DameSerDELCliTableAdapter
        '
        Me.DameSerDELCliTableAdapter.ClearBeforeFill = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.ComboBox3)
        Me.Panel4.Controls.Add(Me.Label3)
        Me.Panel4.Controls.Add(FacturaLabel)
        Me.Panel4.Controls.Add(Me.Panel2)
        Me.Panel4.Location = New System.Drawing.Point(12, 354)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(975, 31)
        Me.Panel4.TabIndex = 174
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.MuestraDetalleNota_por_ConceptoDataGridView)
        Me.Panel5.Controls.Add(Label14)
        Me.Panel5.Controls.Add(Me.TextBox9)
        Me.Panel5.Controls.Add(Me.Button7)
        Me.Panel5.Controls.Add(Me.Button6)
        Me.Panel5.Controls.Add(Me.ComboBox9)
        Me.Panel5.Controls.Add(Label6)
        Me.Panel5.Controls.Add(Label7)
        Me.Panel5.Controls.Add(Me.ComboBox10)
        Me.Panel5.Location = New System.Drawing.Point(62, 400)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(33, 33)
        Me.Panel5.TabIndex = 175
        '
        'MuestraDetalleNota_por_ConceptoDataGridView
        '
        Me.MuestraDetalleNota_por_ConceptoDataGridView.AllowUserToAddRows = False
        Me.MuestraDetalleNota_por_ConceptoDataGridView.AllowUserToDeleteRows = False
        Me.MuestraDetalleNota_por_ConceptoDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraDetalleNota_por_ConceptoDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.MuestraDetalleNota_por_ConceptoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn11})
        Me.MuestraDetalleNota_por_ConceptoDataGridView.DataSource = Me.MuestraDetalleNota_por_ConceptoBindingSource
        Me.MuestraDetalleNota_por_ConceptoDataGridView.Location = New System.Drawing.Point(82, 88)
        Me.MuestraDetalleNota_por_ConceptoDataGridView.Name = "MuestraDetalleNota_por_ConceptoDataGridView"
        Me.MuestraDetalleNota_por_ConceptoDataGridView.ReadOnly = True
        Me.MuestraDetalleNota_por_ConceptoDataGridView.Size = New System.Drawing.Size(874, 145)
        Me.MuestraDetalleNota_por_ConceptoDataGridView.TabIndex = 177
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "clv_txt"
        Me.DataGridViewTextBoxColumn10.HeaderText = "CLAVE"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "concepto"
        Me.DataGridViewTextBoxColumn8.HeaderText = "TIPO DE SERVICIO"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 200
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "descripcion"
        Me.DataGridViewTextBoxColumn9.HeaderText = "DESCRIPCION DEL SERVICIO"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 430
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "precio"
        Me.DataGridViewTextBoxColumn11.HeaderText = "PRECIO"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        '
        'MuestraDetalleNota_por_ConceptoBindingSource
        '
        Me.MuestraDetalleNota_por_ConceptoBindingSource.DataMember = "MuestraDetalleNota_por_Concepto"
        Me.MuestraDetalleNota_por_ConceptoBindingSource.DataSource = Me.DataSetLydia
        '
        'TextBox9
        '
        Me.TextBox9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Consulta_NotaCreditoBindingSource, "Observaciones", True))
        Me.TextBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox9.Location = New System.Drawing.Point(781, 31)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(150, 20)
        Me.TextBox9.TabIndex = 38
        '
        'Button7
        '
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.Location = New System.Drawing.Point(216, 60)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(110, 23)
        Me.Button7.TabIndex = 177
        Me.Button7.Text = "&ELIMINAR"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.Location = New System.Drawing.Point(82, 60)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(110, 23)
        Me.Button6.TabIndex = 176
        Me.Button6.Text = "&AGREGAR"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'ComboBox9
        '
        Me.ComboBox9.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "caja", True))
        Me.ComboBox9.DataSource = Me.MuestraTipSerPrincipal_SERBindingSource
        Me.ComboBox9.DisplayMember = "Concepto"
        Me.ComboBox9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox9.FormattingEnabled = True
        Me.ComboBox9.Location = New System.Drawing.Point(82, 29)
        Me.ComboBox9.Name = "ComboBox9"
        Me.ComboBox9.Size = New System.Drawing.Size(234, 23)
        Me.ComboBox9.TabIndex = 40
        Me.ComboBox9.ValueMember = "Clv_TipSerPrincipal"
        '
        'MuestraTipSerPrincipal_SERBindingSource
        '
        Me.MuestraTipSerPrincipal_SERBindingSource.DataMember = "MuestraTipSerPrincipal_SER"
        Me.MuestraTipSerPrincipal_SERBindingSource.DataSource = Me.DataSetLydia
        '
        'ComboBox10
        '
        Me.ComboBox10.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.Consulta_NotaCreditoBindingSource, "Usuario_Captura", True))
        Me.ComboBox10.DataSource = Me.MuestraServicios_por_TipoBindingSource
        Me.ComboBox10.DisplayMember = "Descripcion"
        Me.ComboBox10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox10.FormattingEnabled = True
        Me.ComboBox10.Location = New System.Drawing.Point(361, 29)
        Me.ComboBox10.Name = "ComboBox10"
        Me.ComboBox10.Size = New System.Drawing.Size(367, 23)
        Me.ComboBox10.TabIndex = 38
        Me.ComboBox10.ValueMember = "Clv_Servicio"
        '
        'MuestraServicios_por_TipoBindingSource
        '
        Me.MuestraServicios_por_TipoBindingSource.DataMember = "MuestraServicios_por_Tipo"
        Me.MuestraServicios_por_TipoBindingSource.DataSource = Me.DataSetLydia
        '
        'MuestraTipSerPrincipal_SERTableAdapter
        '
        Me.MuestraTipSerPrincipal_SERTableAdapter.ClearBeforeFill = True
        '
        'MuestraServicios_por_TipoTableAdapter
        '
        Me.MuestraServicios_por_TipoTableAdapter.ClearBeforeFill = True
        '
        'MuestraDetalleNota_por_ConceptoTableAdapter
        '
        Me.MuestraDetalleNota_por_ConceptoTableAdapter.ClearBeforeFill = True
        '
        'TextBox10
        '
        Me.TextBox10.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraDetalleNota_por_ConceptoBindingSource, "clv_detalle", True))
        Me.TextBox10.Location = New System.Drawing.Point(460, 768)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(149, 20)
        Me.TextBox10.TabIndex = 177
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(319, 386)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(173, 21)
        Me.TextBox7.TabIndex = 178
        '
        'CMBLabelMonto
        '
        Me.CMBLabelMonto.AutoSize = True
        Me.CMBLabelMonto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabelMonto.Location = New System.Drawing.Point(116, 389)
        Me.CMBLabelMonto.Name = "CMBLabelMonto"
        Me.CMBLabelMonto.Size = New System.Drawing.Size(200, 15)
        Me.CMBLabelMonto.TabIndex = 179
        Me.CMBLabelMonto.Text = "Monto de Estado de Cuenta: $"
        '
        'RadioButtonSi
        '
        Me.RadioButtonSi.AutoSize = True
        Me.RadioButtonSi.Checked = True
        Me.RadioButtonSi.Location = New System.Drawing.Point(39, 21)
        Me.RadioButtonSi.Name = "RadioButtonSi"
        Me.RadioButtonSi.Size = New System.Drawing.Size(38, 19)
        Me.RadioButtonSi.TabIndex = 180
        Me.RadioButtonSi.TabStop = True
        Me.RadioButtonSi.Text = "Sí"
        Me.RadioButtonSi.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButtonNo)
        Me.GroupBox2.Controls.Add(Me.RadioButtonSi)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(591, 400)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(199, 52)
        Me.GroupBox2.TabIndex = 181
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "¿Aplicar Nota de Crédito?"
        '
        'RadioButtonNo
        '
        Me.RadioButtonNo.AutoSize = True
        Me.RadioButtonNo.Location = New System.Drawing.Point(117, 21)
        Me.RadioButtonNo.Name = "RadioButtonNo"
        Me.RadioButtonNo.Size = New System.Drawing.Size(43, 19)
        Me.RadioButtonNo.TabIndex = 181
        Me.RadioButtonNo.Text = "No"
        Me.RadioButtonNo.UseVisualStyleBackColor = True
        '
        'FrmNotasdeCreditoLogitel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(1016, 537)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.CMBLabelMonto)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.TextBox10)
        Me.Controls.Add(Me.MontoTextBox)
        Me.Controls.Add(CMBMontoLabel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.REDLabel3)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.ComboBox6)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ContratoTextBox)
        Me.Controls.Add(CMBContratoLabel)
        Me.Controls.Add(Me.Clv_NotadeCreditoTextBox)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.CMBClv_NotadeCreditoLabel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(CMBStatusLabel)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Panel4)
        Me.MaximizeBox = False
        Me.Name = "FrmNotasdeCreditoLogitel"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Notas de Crédito"
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.Consulta_NotaCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACAJAS2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRASUCURSALES2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRACAJERASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOSINBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAME_FACTURASDECLIENTEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.Detalle_NotasdeCreditoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Detalle_NotasdeCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatusNotadeCreditoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAUSUARIOSin2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMEFECHADELSERVIDOR_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Muestra_Tipo_NotaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.BUSCLIPORCONTRATOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameSerDELCliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.MuestraDetalleNota_por_ConceptoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraDetalleNota_por_ConceptoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraTipSerPrincipal_SERBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraServicios_por_TipoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Clv_NotadeCreditoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ContratoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Fecha_deGeneracionDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Fecha_CaducidadDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents MontoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObservacionesTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRASUCURSALES2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRASUCURSALES2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRASUCURSALES2TableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents StatusNotadeCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents StatusNotadeCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.StatusNotadeCreditoTableAdapter
    Friend WithEvents DAMEFECHADELSERVIDOR_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDOR_2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DAMEFECHADELSERVIDOR_2TableAdapter
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents ComboBox3 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRAUSUARIOSINBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOSINTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRAUSUARIOSINTableAdapter
    Friend WithEvents ComboBox5 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRAUSUARIOSin2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAUSUARIOSin2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRAUSUARIOSin2TableAdapter
    Friend WithEvents ComboBox6 As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox7 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox8 As System.Windows.Forms.ComboBox
    Friend WithEvents MUESTRACAJERASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACAJERASTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRACAJERASTableAdapter
    Friend WithEvents MUESTRACAJAS2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRACAJAS2TableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRACAJAS2TableAdapter
    Friend WithEvents Consulta_NotaCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_NotaCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Consulta_NotaCreditoTableAdapter
    Friend WithEvents Detalle_NotasdeCreditoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Detalle_NotasdeCreditoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Detalle_NotasdeCreditoTableAdapter
    Friend WithEvents REDLabel3 As System.Windows.Forms.Label
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DAME_FACTURASDECLIENTEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAME_FACTURASDECLIENTETableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DAME_FACTURASDECLIENTETableAdapter
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Muestra_Tipo_NotaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_Tipo_NotaTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.Muestra_Tipo_NotaTableAdapter
    Friend WithEvents Detalle_NotasdeCreditoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents SecobraDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvdetalleDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvservicioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents ESHOTELCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents SOLOINTERNETCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NOMBRELabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents COLONIALabel1 As System.Windows.Forms.Label
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CIUDADLabel1 As System.Windows.Forms.Label
    Friend WithEvents BUSCLIPORCONTRATOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCLIPORCONTRATOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BUSCLIPORCONTRATOTableAdapter
    Friend WithEvents DameSerDELCliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameSerDELCliTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.dameSerDELCliTableAdapter
    Friend WithEvents CMBClv_NotadeCreditoLabel As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ComboBox9 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox10 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clv_sucursalLabel As System.Windows.Forms.Label
    Friend WithEvents Usuario_AutorizoLabel As System.Windows.Forms.Label
    Friend WithEvents MuestraTipSerPrincipal_SERBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraTipSerPrincipal_SERTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MuestraTipSerPrincipal_SERTableAdapter
    Friend WithEvents MuestraServicios_por_TipoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraServicios_por_TipoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MuestraServicios_por_TipoTableAdapter
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents MuestraDetalleNota_por_ConceptoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraDetalleNota_por_ConceptoTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MuestraDetalleNota_por_ConceptoTableAdapter
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents MuestraDetalleNota_por_ConceptoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TextBox7 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabelMonto As System.Windows.Forms.Label
    Friend WithEvents RadioButtonSi As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButtonNo As System.Windows.Forms.RadioButton
End Class
