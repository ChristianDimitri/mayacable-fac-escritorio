<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class LoginForm1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NombreLabel As System.Windows.Forms.Label
        Dim DireccionLabel As System.Windows.Forms.Label
        Dim ColoniaLabel As System.Windows.Forms.Label
        Dim CiudadLabel As System.Windows.Forms.Label
        Dim RfcLabel As System.Windows.Forms.Label
        Dim TELefonosLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm1))
        Me.NewsoftvDataSet = New softvFacturacion.NewsoftvDataSet()
        Me.VerAccesoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAccesoTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.VerAccesoTableAdapter()
        Me.DAMECAJA_SUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECAJA_SUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSetTableAdapters.DAMECAJA_SUCURSALTableAdapter()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.OK = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MUESTRAIMAGENBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.UsernameTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Cancel = New System.Windows.Forms.Button()
        Me.PasswordTextBox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MuestraEmpresaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MUESTRAIMAGENTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAIMAGENTableAdapter()
        Me.Muestra_EmpresaTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Muestra_EmpresaTableAdapter()
        Me.CiudadTextBox = New System.Windows.Forms.TextBox()
        Me.DameDatosGeneralesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.DameDatosGenerales_2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DireccionTextBox = New System.Windows.Forms.TextBox()
        Me.ColoniaTextBox = New System.Windows.Forms.TextBox()
        Me.CiudadTextBox1 = New System.Windows.Forms.TextBox()
        Me.RfcTextBox = New System.Windows.Forms.TextBox()
        Me.TELefonosTextBox = New System.Windows.Forms.TextBox()
        Me.NewsoftvDataSet2 = New softvFacturacion.NewsoftvDataSet2()
        Me.DameRutaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameRutaTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameRutaTableAdapter()
        Me.DAME_INF_SUCURSALBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAME_INF_SUCURSALTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DAME_INF_SUCURSALTableAdapter()
        Me.DameIdBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameIdTableAdapter = New softvFacturacion.NewsoftvDataSet2TableAdapters.DameIdTableAdapter()
        Me.Dame_clv_sucursalBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_sucursalTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_sucursalTableAdapter()
        Me.Dame_clv_EmpresaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dame_clv_EmpresaTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_EmpresaTableAdapter()
        Me.DameDatosGeneralesTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DameDatosGeneralesTableAdapter()
        Me.DameDatosGenerales_2TableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.DameDatosGenerales_2TableAdapter()
        Me.VerAccesoAdminBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAccesoAdminTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.VerAccesoAdminTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Selecciona_impresoraticketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_impresoraticketsTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        NombreLabel = New System.Windows.Forms.Label()
        DireccionLabel = New System.Windows.Forms.Label()
        ColoniaLabel = New System.Windows.Forms.Label()
        CiudadLabel = New System.Windows.Forms.Label()
        RfcLabel = New System.Windows.Forms.Label()
        TELefonosLabel = New System.Windows.Forms.Label()
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAccesoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECAJA_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraEmpresaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameDatosGenerales_2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameRutaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAME_INF_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameIdBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_sucursalBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dame_clv_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAccesoAdminBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Location = New System.Drawing.Point(187, 258)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(60, 15)
        NombreLabel.TabIndex = 21
        NombreLabel.Text = "nombre:"
        '
        'DireccionLabel
        '
        DireccionLabel.AutoSize = True
        DireccionLabel.ForeColor = System.Drawing.Color.White
        DireccionLabel.Location = New System.Drawing.Point(381, 268)
        DireccionLabel.Name = "DireccionLabel"
        DireccionLabel.Size = New System.Drawing.Size(70, 15)
        DireccionLabel.TabIndex = 22
        DireccionLabel.Text = "direccion:"
        '
        'ColoniaLabel
        '
        ColoniaLabel.AutoSize = True
        ColoniaLabel.ForeColor = System.Drawing.Color.White
        ColoniaLabel.Location = New System.Drawing.Point(110, 261)
        ColoniaLabel.Name = "ColoniaLabel"
        ColoniaLabel.Size = New System.Drawing.Size(58, 15)
        ColoniaLabel.TabIndex = 23
        ColoniaLabel.Text = "colonia:"
        '
        'CiudadLabel
        '
        CiudadLabel.AutoSize = True
        CiudadLabel.ForeColor = System.Drawing.Color.White
        CiudadLabel.Location = New System.Drawing.Point(375, 248)
        CiudadLabel.Name = "CiudadLabel"
        CiudadLabel.Size = New System.Drawing.Size(54, 15)
        CiudadLabel.TabIndex = 24
        CiudadLabel.Text = "ciudad:"
        '
        'RfcLabel
        '
        RfcLabel.AutoSize = True
        RfcLabel.ForeColor = System.Drawing.Color.White
        RfcLabel.Location = New System.Drawing.Point(342, 248)
        RfcLabel.Name = "RfcLabel"
        RfcLabel.Size = New System.Drawing.Size(27, 15)
        RfcLabel.TabIndex = 25
        RfcLabel.Text = "rfc:"
        '
        'TELefonosLabel
        '
        TELefonosLabel.AutoSize = True
        TELefonosLabel.ForeColor = System.Drawing.Color.White
        TELefonosLabel.Location = New System.Drawing.Point(443, 248)
        TELefonosLabel.Name = "TELefonosLabel"
        TELefonosLabel.Size = New System.Drawing.Size(79, 15)
        TELefonosLabel.TabIndex = 26
        TELefonosLabel.Text = "TELefonos:"
        '
        'NewsoftvDataSet
        '
        Me.NewsoftvDataSet.DataSetName = "NewsoftvDataSet"
        Me.NewsoftvDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'VerAccesoBindingSource
        '
        Me.VerAccesoBindingSource.DataMember = "VerAcceso"
        Me.VerAccesoBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'VerAccesoTableAdapter
        '
        Me.VerAccesoTableAdapter.ClearBeforeFill = True
        '
        'DAMECAJA_SUCURSALBindingSource
        '
        Me.DAMECAJA_SUCURSALBindingSource.DataMember = "DAMECAJA_SUCURSAL"
        Me.DAMECAJA_SUCURSALBindingSource.DataSource = Me.NewsoftvDataSet
        '
        'DAMECAJA_SUCURSALTableAdapter
        '
        Me.DAMECAJA_SUCURSALTableAdapter.ClearBeforeFill = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(314, 46)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(391, 80)
        Me.PictureBox2.TabIndex = 13
        Me.PictureBox2.TabStop = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.DarkOrange
        Me.OK.BackgroundImage = CType(resources.GetObject("OK.BackgroundImage"), System.Drawing.Image)
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.ForeColor = System.Drawing.Color.White
        Me.OK.Location = New System.Drawing.Point(354, 207)
        Me.OK.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(135, 28)
        Me.OK.TabIndex = 2
        Me.OK.Text = "&Aceptar"
        Me.OK.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.PictureBox1.DataBindings.Add(New System.Windows.Forms.Binding("Image", Me.MUESTRAIMAGENBindingSource, "IMAGEN", True))
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(59, 46)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(245, 198)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'MUESTRAIMAGENBindingSource
        '
        Me.MUESTRAIMAGENBindingSource.DataMember = "MUESTRAIMAGEN"
        Me.MUESTRAIMAGENBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.PictureBox3.Location = New System.Drawing.Point(314, 134)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(391, 65)
        Me.PictureBox3.TabIndex = 14
        Me.PictureBox3.TabStop = False
        '
        'UsernameTextBox
        '
        Me.UsernameTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.UsernameTextBox.Location = New System.Drawing.Point(440, 55)
        Me.UsernameTextBox.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.UsernameTextBox.Name = "UsernameTextBox"
        Me.UsernameTextBox.Size = New System.Drawing.Size(164, 21)
        Me.UsernameTextBox.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label4.Location = New System.Drawing.Point(376, 58)
        Me.Label4.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Login :"
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.DarkOrange
        Me.Cancel.BackgroundImage = CType(resources.GetObject("Cancel.BackgroundImage"), System.Drawing.Image)
        Me.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.ForeColor = System.Drawing.Color.White
        Me.Cancel.Location = New System.Drawing.Point(559, 207)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(135, 28)
        Me.Cancel.TabIndex = 3
        Me.Cancel.Text = "&Cancelar"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'PasswordTextBox
        '
        Me.PasswordTextBox.Location = New System.Drawing.Point(440, 87)
        Me.PasswordTextBox.Margin = New System.Windows.Forms.Padding(5, 4, 5, 4)
        Me.PasswordTextBox.Name = "PasswordTextBox"
        Me.PasswordTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTextBox.Size = New System.Drawing.Size(164, 21)
        Me.PasswordTextBox.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label2.Location = New System.Drawing.Point(342, 90)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "Pasaporte :"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraEmpresaBindingSource, "nombre", True))
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label3.Location = New System.Drawing.Point(314, 136)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(391, 36)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Sistemas Administrativos para Televisión Restringida."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'MuestraEmpresaBindingSource
        '
        Me.MuestraEmpresaBindingSource.DataMember = "Muestra_Empresa"
        Me.MuestraEmpresaBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'MUESTRAIMAGENTableAdapter
        '
        Me.MUESTRAIMAGENTableAdapter.ClearBeforeFill = True
        '
        'Muestra_EmpresaTableAdapter
        '
        Me.Muestra_EmpresaTableAdapter.ClearBeforeFill = True
        '
        'CiudadTextBox
        '
        Me.CiudadTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGeneralesBindingSource, "Ciudad", True))
        Me.CiudadTextBox.Location = New System.Drawing.Point(578, 211)
        Me.CiudadTextBox.Name = "CiudadTextBox"
        Me.CiudadTextBox.ReadOnly = True
        Me.CiudadTextBox.Size = New System.Drawing.Size(100, 21)
        Me.CiudadTextBox.TabIndex = 21
        Me.CiudadTextBox.TabStop = False
        '
        'DameDatosGeneralesBindingSource
        '
        Me.DameDatosGeneralesBindingSource.DataMember = "DameDatosGenerales"
        Me.DameDatosGeneralesBindingSource.DataSource = Me.DataSetEdgar
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "nombre", True))
        Me.NombreTextBox.Location = New System.Drawing.Point(249, 273)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.Size = New System.Drawing.Size(100, 14)
        Me.NombreTextBox.TabIndex = 22
        Me.NombreTextBox.TabStop = False
        '
        'DameDatosGenerales_2BindingSource
        '
        Me.DameDatosGenerales_2BindingSource.DataMember = "DameDatosGenerales_2"
        Me.DameDatosGenerales_2BindingSource.DataSource = Me.DataSetEdgar
        '
        'DireccionTextBox
        '
        Me.DireccionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DireccionTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "direccion", True))
        Me.DireccionTextBox.Location = New System.Drawing.Point(440, 286)
        Me.DireccionTextBox.Name = "DireccionTextBox"
        Me.DireccionTextBox.Size = New System.Drawing.Size(100, 14)
        Me.DireccionTextBox.TabIndex = 23
        Me.DireccionTextBox.TabStop = False
        '
        'ColoniaTextBox
        '
        Me.ColoniaTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.ColoniaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "colonia", True))
        Me.ColoniaTextBox.Location = New System.Drawing.Point(174, 258)
        Me.ColoniaTextBox.Name = "ColoniaTextBox"
        Me.ColoniaTextBox.Size = New System.Drawing.Size(100, 14)
        Me.ColoniaTextBox.TabIndex = 24
        Me.ColoniaTextBox.TabStop = False
        '
        'CiudadTextBox1
        '
        Me.CiudadTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CiudadTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "ciudad", True))
        Me.CiudadTextBox1.Location = New System.Drawing.Point(528, 248)
        Me.CiudadTextBox1.Name = "CiudadTextBox1"
        Me.CiudadTextBox1.Size = New System.Drawing.Size(100, 14)
        Me.CiudadTextBox1.TabIndex = 25
        Me.CiudadTextBox1.TabStop = False
        '
        'RfcTextBox
        '
        Me.RfcTextBox.BackColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.RfcTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.RfcTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "rfc", True))
        Me.RfcTextBox.Location = New System.Drawing.Point(329, 242)
        Me.RfcTextBox.Name = "RfcTextBox"
        Me.RfcTextBox.Size = New System.Drawing.Size(100, 14)
        Me.RfcTextBox.TabIndex = 26
        Me.RfcTextBox.TabStop = False
        '
        'TELefonosTextBox
        '
        Me.TELefonosTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.TELefonosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DameDatosGenerales_2BindingSource, "TELefonos", True))
        Me.TELefonosTextBox.Location = New System.Drawing.Point(546, 273)
        Me.TELefonosTextBox.Name = "TELefonosTextBox"
        Me.TELefonosTextBox.Size = New System.Drawing.Size(100, 14)
        Me.TELefonosTextBox.TabIndex = 27
        Me.TELefonosTextBox.TabStop = False
        '
        'NewsoftvDataSet2
        '
        Me.NewsoftvDataSet2.DataSetName = "NewsoftvDataSet2"
        Me.NewsoftvDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameRutaBindingSource
        '
        Me.DameRutaBindingSource.DataMember = "DameRuta"
        Me.DameRutaBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameRutaTableAdapter
        '
        Me.DameRutaTableAdapter.ClearBeforeFill = True
        '
        'DAME_INF_SUCURSALBindingSource
        '
        Me.DAME_INF_SUCURSALBindingSource.DataMember = "DAME_INF_SUCURSAL"
        Me.DAME_INF_SUCURSALBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DAME_INF_SUCURSALTableAdapter
        '
        Me.DAME_INF_SUCURSALTableAdapter.ClearBeforeFill = True
        '
        'DameIdBindingSource
        '
        Me.DameIdBindingSource.DataMember = "DameId"
        Me.DameIdBindingSource.DataSource = Me.NewsoftvDataSet2
        '
        'DameIdTableAdapter
        '
        Me.DameIdTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_sucursalBindingSource
        '
        Me.Dame_clv_sucursalBindingSource.DataMember = "Dame_clv_sucursal"
        Me.Dame_clv_sucursalBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Dame_clv_sucursalTableAdapter
        '
        Me.Dame_clv_sucursalTableAdapter.ClearBeforeFill = True
        '
        'Dame_clv_EmpresaBindingSource
        '
        Me.Dame_clv_EmpresaBindingSource.DataMember = "Dame_clv_Empresa"
        Me.Dame_clv_EmpresaBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Dame_clv_EmpresaTableAdapter
        '
        Me.Dame_clv_EmpresaTableAdapter.ClearBeforeFill = True
        '
        'DameDatosGeneralesTableAdapter
        '
        Me.DameDatosGeneralesTableAdapter.ClearBeforeFill = True
        '
        'DameDatosGenerales_2TableAdapter
        '
        Me.DameDatosGenerales_2TableAdapter.ClearBeforeFill = True
        '
        'VerAccesoAdminBindingSource
        '
        Me.VerAccesoAdminBindingSource.DataMember = "VerAccesoAdmin"
        Me.VerAccesoAdminBindingSource.DataSource = Me.DataSetEdgar
        '
        'VerAccesoAdminTableAdapter
        '
        Me.VerAccesoAdminTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label1.Location = New System.Drawing.Point(314, 162)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(391, 24)
        Me.Label1.TabIndex = 28
        Me.Label1.Text = "SofTV Facturación"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Selecciona_impresoraticketsBindingSource
        '
        Me.Selecciona_impresoraticketsBindingSource.DataMember = "Selecciona_impresoratickets"
        Me.Selecciona_impresoraticketsBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Selecciona_impresoraticketsTableAdapter
        '
        Me.Selecciona_impresoraticketsTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'LoginForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(771, 272)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(TELefonosLabel)
        Me.Controls.Add(Me.TELefonosTextBox)
        Me.Controls.Add(RfcLabel)
        Me.Controls.Add(Me.RfcTextBox)
        Me.Controls.Add(CiudadLabel)
        Me.Controls.Add(Me.CiudadTextBox1)
        Me.Controls.Add(ColoniaLabel)
        Me.Controls.Add(Me.ColoniaTextBox)
        Me.Controls.Add(DireccionLabel)
        Me.Controls.Add(Me.DireccionTextBox)
        Me.Controls.Add(NombreLabel)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.CiudadTextBox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PasswordTextBox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.UsernameTextBox)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.PictureBox2)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "LoginForm1"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso al Sistema"
        CType(Me.NewsoftvDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAccesoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECAJA_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRAIMAGENBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraEmpresaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDatosGeneralesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameDatosGenerales_2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameRutaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAME_INF_SUCURSALBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameIdBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_sucursalBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dame_clv_EmpresaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAccesoAdminBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents NewsoftvDataSet As softvFacturacion.NewsoftvDataSet
    Friend WithEvents VerAccesoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAccesoTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.VerAccesoTableAdapter
    Friend WithEvents DAMECAJA_SUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECAJA_SUCURSALTableAdapter As softvFacturacion.NewsoftvDataSetTableAdapters.DAMECAJA_SUCURSALTableAdapter
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents UsernameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents PasswordTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents MUESTRAIMAGENBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRAIMAGENTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.MUESTRAIMAGENTableAdapter
    Friend WithEvents MuestraEmpresaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Muestra_EmpresaTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Muestra_EmpresaTableAdapter
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents DameDatosGeneralesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGeneralesTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DameDatosGeneralesTableAdapter
    Friend WithEvents CiudadTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DameDatosGenerales_2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameDatosGenerales_2TableAdapter As softvFacturacion.DataSetEdgarTableAdapters.DameDatosGenerales_2TableAdapter
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DireccionTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ColoniaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents CiudadTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents RfcTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TELefonosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NewsoftvDataSet2 As softvFacturacion.NewsoftvDataSet2
    Friend WithEvents DameRutaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameRutaTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameRutaTableAdapter
    Friend WithEvents DAME_INF_SUCURSALBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAME_INF_SUCURSALTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DAME_INF_SUCURSALTableAdapter
    Friend WithEvents DameIdBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameIdTableAdapter As softvFacturacion.NewsoftvDataSet2TableAdapters.DameIdTableAdapter
    Friend WithEvents Dame_clv_sucursalBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_sucursalTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_sucursalTableAdapter
    Friend WithEvents Dame_clv_EmpresaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_clv_EmpresaTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Dame_clv_EmpresaTableAdapter
    Friend WithEvents VerAccesoAdminBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAccesoAdminTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.VerAccesoAdminTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Selecciona_impresoraticketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_impresoraticketsTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter

End Class
