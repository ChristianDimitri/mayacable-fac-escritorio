Imports System.Net.NetworkInformation
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO
Imports System
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic
Imports System.Text


Public Class FrmListadoPreliminar
    Dim Archivo As String
    Dim space As String
    Dim intro As String
    Dim indice As Integer
    Dim indice2 As Integer
    Dim Fecha As String
    Dim CantidadTotal As String
    Dim NoClientes As String
    Dim Contrato As String
    Dim NoCuenta As String
    Dim Cantidadcte As String
    Dim Ruta As String
    Private Clave_archivo_bancomer As Long = Nothing
    Private customersByCityReport As ReportDocument
    Dim proceso As Integer = 0
    Dim locerrorsantander As Integer = 0
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Close()
    End Sub

    Private Sub FrmListadoPreliminar_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If BndPasaBancos = True Then
            BndPasaBancos = False
            GeneraListadoPreliminar()
        End If

        If GloActPeriodo = 1 Then
            GloActPeriodo = 0
            Me.Label4.Text = GloClv_Periodo_Txt
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.ConPreliminarBancosTableAdapter.Connection = CON
            Me.ConPreliminarBancosTableAdapter.Fill(Me.NewsoftvDataSet.ConPreliminarBancos, GloClv_Periodo_Num, 0)
            CON.Close()
            GloBndControl = True
        End If
    End Sub

    Private Sub FrmListadoPreliminar_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloBndControl = False
    End Sub

    Private Sub FrmListadoPreliminar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientos_arnoldo.Procesa_Arhivo_santader' Puede moverla o quitarla seg�n sea necesario.
        'Me.Procesa_Arhivo_santaderTableAdapter.Fill(Me.Procedimientos_arnoldo.Procesa_Arhivo_santader)
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientos_arnoldo.Borra_Tablas_Archivos' Puede moverla o quitarla seg�n sea necesario.
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Borra_Tablas_ArchivosTableAdapter.Connection = CON
        Me.Borra_Tablas_ArchivosTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Tablas_Archivos)
        CON.Close()
        'Me.MUESTRAPERIODOS_SeleccionarTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRAPERIODOS_Seleccionar, 0)
        GloActPeriodo = 0
        GloClv_Periodo_Txt = " Primer Periodo "
        GloClv_Periodo_Num = 1
        FrmSelPeriodo.Show()
        Dim CON2 As New SqlConnection(MiConexion)
        CON2.Open()
        Me.DamedatosUsuarioTableAdapter.Connection = CON2
        Me.DamedatosUsuarioTableAdapter.Fill(Me.NewsoftvDataSet.DamedatosUsuario, GloUsuario)
        Me.DAMENOMBRESUCURSALTableAdapter.Connection = CON2
        Me.DAMENOMBRESUCURSALTableAdapter.Fill(Me.NewsoftvDataSet.DAMENOMBRESUCURSAL, GloSucursal)
        Me.DameDatosGeneralesTableAdapter.Connection = CON2
        Me.DameDatosGeneralesTableAdapter.Fill(Me.NewsoftvDataSet.DameDatosGenerales)
        CON2.Close()
        Me.LblNomCaja.Text = GlonOMCaja
        Me.LblVersion.Text = My.Application.Info.Version.ToString
        GloBndControl = True
    End Sub

    Public Sub GeneraDocumentoTxt()
        Dim ReferenciaCliente As String = Nothing
        Try
            'GloProcesa = 3
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Txt As String = Nothing
            Dim GLOBND As Boolean = True
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameGeneralesBancosTableAdapter.Connection = CON
            Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "PR")
            CON.Close()
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()

            'Me.FolderBrowserDialog1.ShowDialog()


            If (result = DialogResult.OK) Then
                Rutatxt = Me.FolderBrowserDialog1.SelectedPath.ToString
                If IsNumeric(Me.ImporteTextBox.Text) = False Then Me.ImporteTextBox.Text = 0
                imp1 = CStr(Mid(Me.ImporteTextBox.Text, 1, Len(Me.ImporteTextBox.Text) - 2))
                Nom_Archivo = Rutatxt + "\" + "SCAENT" & Mid(Me.EmisoraTextBox.Text, 1, 4) & "D" & Mid(Me.ANOTextBox.Text, 3, 2) & Me.MESTextBox.Text & Me.DIATextBox.Text & "ER" & Mid(Me.ConsecutivoTextBox.Text, 2, 2) & ".ftp"

                Dim fileExists As Boolean
                fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                If fileExists = True Then
                    File.Delete(Nom_Archivo)
                End If
                Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                    Encabezado = Me.DIATextBox.Text & Me.MESTextBox.Text & Me.ANOTextBox.Text & Me.HORATextBox.Text & Me.MITextBox.Text & "00" & Microsoft.VisualBasic.Strings.Space(6 - Len(Me.ContadorTextBox.Text)) & Me.ContadorTextBox.Text & Microsoft.VisualBasic.Strings.Space(16 - Len(imp1)) & imp1 & Microsoft.VisualBasic.Strings.Space(31)

                    sw.WriteLine(Encabezado)
                    Dim FilaRow As DataRow
                    'Me.CONSULTACNRTableAdapter.Fill(Me.DataSetLidia.CONSULTACNR)
                    Dim NumeroAfiliacion As String = Nothing
                    Dim ClaveBanco As String = Nothing

                    Dim NumeroTarjeta As String = Nothing
                    Dim StDetalle As String = Nothing
                    Dim StMonto As String = Nothing
                    Dim CON3 As New SqlConnection(MiConexion)
                    CON3.Open()
                    Me.DameGeneralesBancos_Total_DetalleTableAdapter.Connection = CON3
                    Me.DameGeneralesBancos_Total_DetalleTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos_Total_Detalle, Me.Clv_SessionBancosTextBox.Text)
                    For Each FilaRow In Me.NewsoftvDataSet2.DameGeneralesBancos_Total_Detalle.Rows
                        If FilaRow("Cliente".ToString()) Is Nothing Then
                            Exit For

                        End If
                        NumeroAfiliacion = Microsoft.VisualBasic.Strings.Space(7 - Len(Trim(Me.EmisoraTextBox.Text))) & Trim(Me.EmisoraTextBox.Text)
                        ClaveBanco = Microsoft.VisualBasic.Strings.Space(2 - Len(Trim(Me.SucursalTextBox.Text))) & Trim(Me.SucursalTextBox.Text)
                        ReferenciaCliente = Trim(FilaRow("Cliente".ToString())) & Microsoft.VisualBasic.Strings.Space(23 - Len(Trim(FilaRow("Cliente".ToString()))))
                        NumeroTarjeta = Trim(FilaRow("Cuenta_Banco".ToString())) & Microsoft.VisualBasic.Strings.Space(19 - Len(Trim(FilaRow("Cuenta_Banco".ToString()))))
                        'StMonto = Space(14 - Len(FilaRow("Importe".ToString()))) & FilaRow("Importe".ToString())
                        StMonto = Microsoft.VisualBasic.Strings.Space(14 - Len(Trim(FilaRow("Importe".ToString())))) & Trim(FilaRow("Importe".ToString()))
                        StDetalle = NumeroAfiliacion & ClaveBanco & ReferenciaCliente & NumeroTarjeta & StMonto & "00"
                        sw.WriteLine(StDetalle)
                    Next
                    con3.Close()

                    '    Txt = "save"
                    '    sw.Write(Txt)
                    sw.Close()
                End Using
                MsgBox("El archivo se genero en la siguiente ruta : " & Nom_Archivo)
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & ReferenciaCliente & " son Invalidos")

        End Try

    End Sub
    Private Function Dame_archivo_bancomer() As String
        Dim Con1 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Dim Nombre_Archivo As String
        Con1.Open()
        With cmd
            .CommandText = "Dame_archivo_bancomer"
            .Connection = Con1
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            '@Clave bigint output,@NomArchivo varchar(20) output
            Dim prm1 As New SqlParameter("@Clave", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Output
            prm1.Value = 0
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@NomArchivo", SqlDbType.VarChar, 20)
            prm2.Direction = ParameterDirection.Output
            prm2.Value = 0
            .Parameters.Add(prm2)

            Dim i As Integer = .ExecuteNonQuery()

            Clave_archivo_bancomer = prm1.Value
            Nombre_Archivo = prm2.Value
        End With
        Con1.Close()
        Return Nombre_Archivo
    End Function
    Public Sub Guarda_archivo_bancomer(ByVal Clv_archivo As Long)
        Dim Con2 As New SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand()
        Try
            Con2.Open()
            cmd2 = New SqlClient.SqlCommand()
            With cmd2
                .CommandText = "Guarda_archivo_bancomer"
                .Connection = Con2
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clave", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_archivo
                .Parameters.Add(prm)

                Dim a As Integer = .ExecuteNonQuery()
            End With
            Con2.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub Guarda_Inserta_Datos_H(ByVal No_Proceso As Long, ByVal Lcontrato As Long, ByVal no_tarjeta As String, ByVal Monto_cte As Double, ByVal Rechazado As String)
        Dim Con2 As New SqlConnection(MiConexion)
        Dim cmd2 As New SqlClient.SqlCommand()
        '--@No_Proceso bigint,@contrato bigint ,@no_tarjeta varchar(80),@Monto_cte decimal(18,2),@Rechazado varchar(50)
        Try
            Con2.Open()
            cmd2 = New SqlClient.SqlCommand()
            With cmd2
                .CommandText = "Inserta_Datos_H"
                .Connection = Con2
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@No_Proceso", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = No_Proceso
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@contrato", SqlDbType.BigInt)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = Lcontrato
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@no_tarjeta", SqlDbType.VarChar, 80)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = no_tarjeta
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Monto_cte", SqlDbType.Money)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = Monto_cte
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Rechazado", SqlDbType.VarChar, 50)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = Rechazado
                .Parameters.Add(prm4)

                Dim a As Integer = .ExecuteNonQuery()
            End With
            Con2.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub




    Public Sub GeneraDocumentotxt_Bancomer()
        Dim CON As New SqlClient.SqlConnection(MiConexion)
        'dim sw as New 
        Dim cmd As New SqlCommand()
        'Dim sw As StreamWriter
        Dim Nom_Archivo As String = Nothing
        Dim Nom_Archivo2 As String = Nothing
        Dim Encabezado As String = Nothing
        Dim DetalleArchivo As String = Nothing
        Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()
        Dim Rutatxt As String = Nothing

        'Variables Archivo
        Dim Afiliacion As String = Nothing
        Dim NomComercio As String = Nothing
        Dim Cuenta_Banco As String = Nothing
        Dim Importe As Double = New Double()
        Dim sMonoto As String = Nothing
        Dim Clv_id As Long = New Long()
        Dim MontoTotal As Double = New Double()
        Dim TotalTransacciones As Integer = Nothing

        Try
            'Me.FolderBrowserDialog1.ShowDialog()
            If (result = DialogResult.OK) Then
                Rutatxt = Me.FolderBrowserDialog1.SelectedPath.ToString
                Nom_Archivo2 = Dame_archivo_bancomer()
                Nom_Archivo = Rutatxt + "\" + Nom_Archivo2

                Dim fileExists As Boolean
                fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                If fileExists = True Then
                    File.Delete(Nom_Archivo)
                End If
                Using sw As StreamWriter = File.CreateText(Nom_Archivo)

                    '�	Registro de encabezado

                    Encabezado = "HWEBFT2.00ENT"
                    Dim datos1 As New StringBuilder
                    datos1.Append(Encabezado)

                    cmd = New SqlClient.SqlCommand()
                    CON.Open()
                    With cmd
                        .CommandText = "Consulta_Grales_Prosa_bancomer"
                        .Connection = CON
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure

                        Dim prm1 As New SqlParameter("@op", SqlDbType.Int)
                        prm1.Direction = ParameterDirection.Input
                        prm1.Value = 0
                        .Parameters.Add(prm1)

                        Dim lector As SqlDataReader = .ExecuteReader()

                        While lector.Read()
                            Afiliacion = lector(0).ToString()
                            NomComercio = lector(1).ToString()
                        End While
                    End With
                    CON.Close()

                    If Len(Afiliacion) = 0 Or Afiliacion = Nothing Then
                        MsgBox("El N�mero De Afiliaci�n No Se Ha Capturado En Generales Del Sistema", MsgBoxStyle.Information)
                        Exit Sub
                    End If
                    If Len(NomComercio) = 0 Or NomComercio = Nothing Then
                        MsgBox("El Nombre Del Comercio No Se Ha Capturado En Generales Del Sistema", MsgBoxStyle.Information)
                        Exit Sub
                    End If


                    datos1.Append(Afiliacion)
                    datos1.Append(NomComercio)
                    datos1.Append("BANCOMER")
                    datos1.Append(DateTime.Today.ToString("MMddyyyy"))
                    datos1.Insert(datos1.ToString.Length, " ", 18)
                    datos1.Append(".")
                    sw.WriteLine(datos1.ToString())


                    '�	Registros de detalle
                    cmd = New SqlClient.SqlCommand()
                    CON.Open()
                    With cmd
                        'Cuenta_Banco varchar(50),importe decimal(18,2),smonto varchar(50),Clv_Id bigint
                        'DameGeneralesBancos_Total_Detalle_Bancomer](@Clv_SessionBancos bigint)
                        .CommandText = "DameGeneralesBancos_Total_Detalle_Bancomer"
                        .Connection = CON
                        .CommandTimeout = 0
                        .CommandType = CommandType.StoredProcedure

                        Dim prm As New SqlParameter("@Clv_sessionBancos", SqlDbType.BigInt)
                        prm.Direction = ParameterDirection.Input
                        prm.Value = CLng(Me.Clv_SessionBancosTextBox.Text)
                        .Parameters.Add(prm)

                        Dim lector As SqlDataReader = .ExecuteReader()
                        While lector.Read()
                            datos1.Remove(0, datos1.Length)

                            Cuenta_Banco = RTrim(LTrim(lector.GetValue(0).ToString))
                            Importe = CDbl(lector.GetValue(1))
                            sMonoto = RTrim(LTrim(lector.GetValue(2).ToString()))
                            Clv_id = CLng(lector.GetValue(3))

                            If MontoTotal = Nothing Then
                                MontoTotal = Importe
                            Else
                                MontoTotal = MontoTotal + Importe
                            End If

                            'DetalleArchivo 
                            If Cuenta_Banco.ToString.Length > 0 And sMonoto.Length > 0 And Clv_id.ToString.Length > 0 Then
                                datos1.Append("D5")
                                datos1.Append(Cuenta_Banco)
                                If Cuenta_Banco.Length <= 16 Then
                                    datos1.Insert(datos1.Length, "0", 16 - Cuenta_Banco.Length)
                                End If
                                If sMonoto.Length <= 12 Then
                                    datos1.Insert(datos1.Length, "0", 12 - sMonoto.Length)
                                End If
                                datos1.Append(sMonoto)
                                datos1.Append("484")
                                If Clv_id.ToString.Length <= 19 Then
                                    datos1.Insert(datos1.Length, "0", 19 - Clv_id.ToString.Length)
                                End If

                                datos1.Append(Clv_id.ToString)
                                datos1.Append("000")
                                datos1.Insert(datos1.Length, " ", 9)
                                datos1.Append(".")
                                If datos1.ToString.Length > 0 Then
                                    sw.WriteLine(datos1.ToString)
                                End If
                                TotalTransacciones += 1
                                Cuenta_Banco = ""
                                Importe = 0
                                sMonoto = ""
                                Clv_id = 0
                            End If
                        End While
                    End With
                    CON.Close()

                    '�	Registro final de Totales
                    datos1.Remove(0, datos1.Length)
                    datos1.Append("T")
                    If Len(TotalTransacciones.ToString) <= 6 Then
                        datos1.Insert(datos1.Length, "0", 6 - TotalTransacciones.ToString.Length)
                    End If
                    datos1.Append(TotalTransacciones.ToString)
                    If (MontoTotal.ToString.Length - 1) <= 15 Then
                        datos1.Insert(datos1.Length, "0", 15 - (MontoTotal.ToString.Length - 1))
                    End If
                    datos1.Append(MontoTotal.ToString)
                    datos1.Replace(".", "", datos1.Length - MontoTotal.ToString.Length, MontoTotal.ToString.Length)
                    datos1.Insert(datos1.Length, "0", 42)
                    datos1.Append(".")
                    sw.WriteLine(datos1.ToString)

                    sw.Close()
                End Using
                Guarda_archivo_bancomer(Clave_archivo_bancomer)

                MsgBox("El archivo se gener� en la siguiente ruta : " & Nom_Archivo)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
            'System.Windows.Forms.MessageBox.Show(ex.StackTrace().ToString())
        End Try
    End Sub
    Public Sub GeneraDocumentoTxt_Santander()
        Dim Referencia_Servicio As String = Nothing
        Try
            'GloProcesa = 3
            Dim CONE As New SqlConnection(MiConexion)
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Txt As String = Nothing
            Dim GLOBND As Boolean = True
            Dim Emisora As String = Nothing
            Dim ImporteTotal As String = Nothing
            Dim Leyenda_tmp As String = Nothing
            Dim Tot_Reg As String = Nothing
            Dim Ano As String = Nothing
            Dim Mes As String = Nothing
            Dim DIA As String = Nothing
            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Me.DameGeneralesBancosTableAdapter.Connection = CON
            'Me.DameGeneralesBancosTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos, "SA")
            'CON.Close()
            '
            Dim Cont As Integer
            Cont = 0
            CONE.Open()
            Dim comando As SqlClient.SqlCommand
            Dim reader As SqlDataReader
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC DameGeneralesBancos SA"
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        Emisora = CStr(reader.GetValue(2))
                        Ano = CStr(reader.GetValue(5))
                        Mes = CStr(reader.GetValue(6))
                        DIA = CStr(reader.GetValue(7))
                        'Try

                        'Catch
                        'MsgBox("No se ha Podido Mandar el Correo a " & CType(Email, String) & " .", , "Error")
                        'End Try
                    End While
                End Using
            End With
            CONE.Close()
            '
            CONE.Open()
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CONE
                .CommandText = "EXEC DameGeneralesBancos_Total_Santader " & Me.Clv_SessionBancosTextBox.Text
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                reader = comando.ExecuteReader()
                Using reader
                    While reader.Read
                        ImporteTotal = reader.GetValue(0)
                        Tot_Reg = reader.GetValue(1)
                    End While
                End Using
            End With
            CONE.Close()
            '
            Dim Nom_Archivo As String = Nothing
            Dim Encabezado As String = Nothing
            Dim imp1 As String = Nothing
            Dim Rutatxt As String = Nothing
            Dim result As DialogResult = FolderBrowserDialog1.ShowDialog()

            'Me.FolderBrowserDialog1.ShowDialog()


            If (result = DialogResult.OK) Then
                Rutatxt = Me.FolderBrowserDialog1.SelectedPath.ToString
                Nom_Archivo = Rutatxt + "\" + "SE" & Mid(Emisora, 1, 5) & Ano & Mes & DIA & ".TXT"

                Dim fileExists As Boolean
                fileExists = My.Computer.FileSystem.FileExists(Nom_Archivo)
                If fileExists = True Then
                    File.Delete(Nom_Archivo)
                End If
                Using sw As StreamWriter = File.CreateText(Nom_Archivo)
                    'Encabezado = Me.DIATextBox.Text & Me.MESTextBox.Text & Me.ANOTextBox.Text & Me.HORATextBox.Text & Me.MITextBox.Text & "00" & Microsoft.VisualBasic.Strings.Space(6 - Len(Me.ContadorTextBox.Text)) & Me.ContadorTextBox.Text & Microsoft.VisualBasic.Strings.Space(16 - Len(imp1)) & imp1 & Microsoft.VisualBasic.Strings.Space(31)
                    'GloEmpresa = "Gigacable de Aguascalientes S.A. de C.V."
                    Encabezado = "01" & "0000001" & "30" & "003" & "E" & "2" & "0000001" & Ano & Mes & DIA & "01" & "000000000000000000000000000" & GloEmpresa & Microsoft.VisualBasic.Strings.Space(40 - Len(GloEmpresa)) & GloRfcEmpresa & Microsoft.VisualBasic.Strings.Space(18 - Len(GloRfcEmpresa)) & "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
                    sw.Write(Encabezado & vbNewLine)
                    Dim Tipo_Registro As String = "02"
                    Dim FinPagina As String = Nothing
                    Dim Contador As Long = 2
                    Dim Numero_Secuencia As String = Nothing
                    Dim NumeroAfiliacion As String = Nothing
                    Dim ClaveBanco As String = Nothing
                    Dim ReferenciaCliente As String = Nothing
                    Dim Codigo_Operacion As String = "30"
                    Dim Codigo_de_la_Divisa As String = "01"
                    Dim Tipo_Operacion As String = "51"
                    Dim Relleno_19 As String = "0"
                    Dim Relleno_31 As String = "0"
                    Dim Relleno_38 As String = "0"
                    Dim FECHA_VENCIMIENTO As String = Nothing
                    FECHA_VENCIMIENTO = Ano & Mes & DIA
                    Dim NumeroTarjeta As String = Nothing
                    Dim Banco_Receptor As String = Nothing
                    Dim Nombre_Cuenta As String = Nothing
                    Dim Nombre_Contrato As String = Nothing
                    Dim StDetalle As String = Nothing

                    Dim Referencia_Numerica As String
                    Dim Importe_Sin_Puntos As String = Nothing
                    Dim Tipo_Cuenta As String = "03"
                    Dim Referencia_Leyenda As String = "Pago del Servicio de Television por Cable"
                    Dim StIva As String = Nothing
                    Dim StMonto As String = Nothing
                    'Dim CON3 As New SqlConnection(MiConexion)
                    'CON3.Open()
                    'Me.DameGeneralesBancos_Total_DetalleTableAdapter.Connection = CON3
                    'Me.DameGeneralesBancos_Total_DetalleTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos_Total_Detalle, Me.Clv_SessionBancosTextBox.Text)

                    '
                    CONE.Open()
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CONE
                        .CommandText = "EXEC DameGeneralesBancos_Total_Detalle_Santader " & Me.Clv_SessionBancosTextBox.Text
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        reader = comando.ExecuteReader()
                        Using reader
                            While reader.Read
                                If reader.GetValue(0) Is Nothing Then
                                    Exit While
                                End If
                                'ImporteTotal = reader.GetValue("Importe")
                                'Tot_Reg = reader.GetValue("Contador")
                                Referencia_Servicio = Trim(reader.GetValue(6)) & Microsoft.VisualBasic.Strings.Space(40 - Len(Trim(reader.GetValue(6))))
                                Numero_Secuencia = Rellena_Text(CStr(Contador), 7, "0")
                                Importe_Sin_Puntos = Rellena_Text(Trim(reader.GetValue(3)), 15, "0")
                                Relleno_19 = Rellena_Text(Relleno_19, 32, "0")
                                Banco_Receptor = Trim(reader.GetValue(4))
                                Nombre_Cuenta = Mid(Trim(reader.GetValue(5)), 1, 40) & Microsoft.VisualBasic.Strings.Space(40 - Len(Mid(Trim(reader.GetValue(5)), 1, 40)))
                                Nombre_Contrato = Mid(Trim(reader.GetValue(7)), 1, 40) & Microsoft.VisualBasic.Strings.Space(40 - Len(Mid(Trim(reader.GetValue(7)), 1, 40)))
                                StIva = Rellena_Text(Trim(reader.GetValue(8)), 15, "0")
                                Referencia_Numerica = Rellena_Text(Trim(CStr(reader.GetValue(9))), 7, "0")
                                Leyenda_tmp = "Pago del Servicio Television por Cable"
                                Referencia_Leyenda = Leyenda_tmp & Microsoft.VisualBasic.Strings.Space(40 - Microsoft.VisualBasic.Strings.Len(Trim(Leyenda_tmp)))
                                Relleno_31 = Rellena_Text(Relleno_31, 23, "0")
                                'NumeroAfiliacion = Trim(Me.EmisoraTextBox.Text)
                                'ClaveBanco = Microsoft.VisualBasic.Strings.Space(2 - Len(Trim(Me.SucursalTextBox.Text))) & Trim(Me.SucursalTextBox.Text)
                                'ReferenciaCliente = Trim(reader.GetValue("Cliente")) & Microsoft.VisualBasic.Strings.Space(23 - Len(Trim(reader.GetValue("Cliente"))))
                                NumeroTarjeta = Rellena_Text(CStr(Trim(reader.GetValue(1))), 20, "0")
                                'StMonto = Space(14 - Len(FilaRow("Importe".ToString()))) & FilaRow("Importe".ToString())
                                StMonto = Trim(reader.GetValue(2))
                                StDetalle = "02" & Numero_Secuencia & "30" & "01" & Importe_Sin_Puntos & Relleno_19 & "51" & Ano & Mes & DIA & Banco_Receptor & "03" & NumeroTarjeta & Nombre_Cuenta & Referencia_Servicio & Nombre_Contrato & StIva & Referencia_Numerica & Referencia_Leyenda & Relleno_31
                                Contador = Contador + 1
                                sw.Write(StDetalle & vbNewLine)
                            End While
                        End Using
                    End With
                    CONE.Close()
                    '
                    Numero_Secuencia = Rellena_Text(CStr(Contador), 7, "0")
                    Tot_Reg = Rellena_Text(CStr(Tot_Reg), 7, "0")
                    ImporteTotal = Rellena_Text(CStr(ImporteTotal), 18, "0")
                    Relleno_38 = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
                    FinPagina = "09" & Numero_Secuencia & "30" & "0000001" & Tot_Reg & ImporteTotal

                    sw.Write(FinPagina & Relleno_38 & vbNewLine)

                    sw.Close()
                End Using

                MsgBox("El archivo se genero en la siguiente ruta : " & Nom_Archivo)
            End If
        Catch ex As System.Exception
            'System.Windows.Forms.MessageBox.Show("Los Datos Bancarios de este Contrato : " & Referencia_Servicio & " son Invalidos")
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ConPreliminarBancosDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConPreliminarBancosDataGridView.CellClick

    End Sub

    Private Sub ConPreliminarBancosDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles ConPreliminarBancosDataGridView.CellContentClick



    End Sub

    Private Sub DameDetalle()
        Try
            If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
                If Me.Clv_SessionBancosTextBox.Text > 0 Then
                    GloClv_SessionBancos = Clv_SessionBancosTextBox.Text
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.CONDETFACTURASBANCOSTableAdapter.Connection = CON
                    Me.CONDETFACTURASBANCOSTableAdapter.Fill(Me.NewsoftvDataSet.CONDETFACTURASBANCOS, Me.Clv_SessionBancosTextBox.Text, 0)
                    CON.Close()
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_SessionBancosTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CONDETFACTURASBANCOSBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONDETFACTURASBANCOSBindingNavigatorSaveItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.Validate()
        Me.CONDETFACTURASBANCOSBindingSource.EndEdit()
        Me.CONDETFACTURASBANCOSTableAdapter.Connection = CON
        Me.CONDETFACTURASBANCOSTableAdapter.Update(Me.NewsoftvDataSet.CONDETFACTURASBANCOS)
        If IsNumeric(Clv_SessionBancosTextBox1.Text) = True Then
            Me.DameGeneralesBancos_TotalTableAdapter.Connection = CON
            Me.DameGeneralesBancos_TotalTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos_Total, Clv_SessionBancosTextBox1.Text)
        End If
        CON.Close()
    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton1.Click
        If IsNumeric(GloClv_SessionBancos) = True Then
            GloReporte = 1
            My.Forms.FrmImprimirRepGral.Show()
        End If
    End Sub

    Private Sub Clv_SessionBancosTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionBancosTextBox.TextChanged
        DameDetalle()
        Me.Button5.Enabled = True
        Me.Button6.Enabled = True
        Me.Button7.Enabled = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmSelTipoCuenta.Show()
        'FrmSelFecha_PasaBancos.Show()
        'GeneraListadoPreliminar()
    End Sub

    Private Sub GeneraListadoPreliminar()
        Try

            'Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            'Dim LocClv_SessionBancos As Long = 0
            'Me.GeneraBancosTableAdapter.Connection = CON
            'Me.GeneraBancosTableAdapter.Fill(Me.NewsoftvDataSet.GeneraBancos, GloClv_Periodo_Num, IdSistema, 999, 0, GloCajera, GloSucursal, GloCaja, LocClv_SessionBancos)
            'If LocClv_SessionBancos > 0 Then
            '    Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
            '    Me.ConPreliminarBancosTableAdapter.Connection = CON
            '    Me.ConPreliminarBancosTableAdapter.Fill(Me.NewsoftvDataSet.ConPreliminarBancos, GloClv_Periodo_Num, 0)
            '    Me.Clv_SessionBancosTextBox.Text = LocClv_SessionBancos
            'Else
            '    MsgBox("No hay Clientes que adeuden el mes Actual")
            'End If
            'CON.Close()
            '@Clv_Periodo, @IdSistema, @Clv_Banco, @Op, @Cajera, @Sucursal, @Caja, @Clv_SessionBancos
            'GloClv_Periodo_Num, IdSistema, 999, 0, GloCajera, GloSucursal, GloCaja, LocClv_SessionBancos)

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim LocClv_SessionBancos As Long = 0
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "GeneraBancos "
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                Dim prm As New SqlParameter("@Clv_Periodo", SqlDbType.Int)
                prm.Direction = ParameterDirection.Input
                prm.Value = GloClv_Periodo_Num
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@IdSistema", SqlDbType.VarChar)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = IdSistema
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Clv_Banco", SqlDbType.Int)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = 999
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@Op", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Input
                prm3.Value = opcionCuenta
                .Parameters.Add(prm3)

                Dim prm4 As New SqlParameter("@Cajera", SqlDbType.VarChar)
                prm4.Direction = ParameterDirection.Input
                prm4.Value = GloCajera
                .Parameters.Add(prm4)

                Dim prm5 As New SqlParameter("@Sucursal", SqlDbType.Int)
                prm5.Direction = ParameterDirection.Input
                prm5.Value = GloSucursal
                .Parameters.Add(prm5)

                Dim prm6 As New SqlParameter("@Caja", SqlDbType.Int)
                prm6.Direction = ParameterDirection.Input
                prm6.Value = GloCaja
                .Parameters.Add(prm6)

                Dim prm7 As New SqlParameter("@Clv_SessionBancos", SqlDbType.BigInt)
                prm7.Direction = ParameterDirection.Output
                prm7.Value = 0
                .Parameters.Add(prm7)
                Dim i As Integer = comando.ExecuteNonQuery()
                LocClv_SessionBancos = prm7.Value
                CON.Close()

                If LocClv_SessionBancos > 0 Then
                    CON.Open()
                    Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
                    Me.ConPreliminarBancosTableAdapter.Connection = CON
                    Me.ConPreliminarBancosTableAdapter.Fill(Me.NewsoftvDataSet.ConPreliminarBancos, GloClv_Periodo_Num, 0)
                    CON.Close()
                    Me.Clv_SessionBancosTextBox.Text = LocClv_SessionBancos
                    bitsist(GloUsuario, 0, GloSistema, Me.Name, Me.Button1.Text, "Se Genero Listado Preliminar del Periodo: " + CStr(GloClv_Periodo_Num), "Cajera: " + CStr(GloCajera) + " en la Caja:" + CStr(GloCaja) + " Sucursal: " + CStr(GloSucursal), LocClv_Ciudad)
                Else
                    MsgBox("No hay Clientes que adeuden el mes Actual")
                End If
            End With

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CANCELA_LISTADO_PRELIMINAR()
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim MSG As String = Nothing
            Me.CANCELALISTADOPRELIMINARTableAdapter.Connection = CON
            Me.CANCELALISTADOPRELIMINARTableAdapter.Fill(Me.NewsoftvDataSet.CANCELALISTADOPRELIMINAR, GloClv_SessionBancos, MSG)
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Cancelo Listado Preliminar", "Con numero de Session Bancos: " + CStr(GloClv_SessionBancos), LocClv_Ciudad)
            MsgBox(MSG)
            If MSG = " Listado Preliminar Cancelado con �xito " Then
                Me.ConPreliminarBancosTableAdapter.Connection = CON
                Me.ConPreliminarBancosTableAdapter.Fill(Me.NewsoftvDataSet.ConPreliminarBancos, GloClv_Periodo_Num, 0)
                DameDetalle()
            End If
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'MsgBox(Me.Proceso_CanceladoTextBox.Text)
        If Me.Proceso_CanceladoTextBox.Text <> "" Then
            If Me.Proceso_CanceladoTextBox.Text = 1 Then
                MsgBox("El N�mero de Proceso : " & Me.Clv_SessionBancosTextBox.Text & " ya fue cancelado", MsgBoxStyle.Information)
                Exit Sub
            End If
            If Me.RealizadoCheckBox.Checked = True Then
                MsgBox("El N�mero de Proceso : " & Me.Clv_SessionBancosTextBox.Text & " ya fue afectado por lo cual no se puede cancelar ", MsgBoxStyle.Information)
                Exit Sub
            End If
            If IsNumeric(GloClv_SessionBancos) = True Then
                CANCELA_LISTADO_PRELIMINAR()
            End If
        Else
            MsgBox("Por el momento no existen clientes con cargo automatico", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub ConPreliminarBancosDataGridView_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ConPreliminarBancosDataGridView.SelectionChanged

    End Sub

    Private Sub Proceso_CanceladoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Proceso_CanceladoTextBox.TextChanged
        If Me.Proceso_CanceladoTextBox.Text = 1 Then
            Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = False
        Else
            If Me.RealizadoCheckBox.Checked = False Then
                Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
                Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
                Me.ToolStripButton4.Enabled = True
                Me.ToolStripButton1.Enabled = True
                Me.ToolStripButton2.Enabled = True
                Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Enabled = True
                Me.ToolStripButton3.Enabled = True
            End If
        End If
    End Sub

    Private Sub RealizadoCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RealizadoCheckBox.CheckedChanged
        If Me.RealizadoCheckBox.Checked = True Then
            Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
            Me.ToolStripButton4.Enabled = True
            Me.ToolStripButton1.Enabled = False
            Me.ToolStripButton2.Enabled = False
            Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Enabled = False
            Me.ToolStripButton3.Enabled = False
        Else
            If Me.Proceso_CanceladoTextBox.Text = 0 Then
                Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
                Me.CONDETFACTURASBANCOSBindingNavigator.Enabled = True
                Me.ToolStripButton4.Enabled = True
                Me.ToolStripButton1.Enabled = True
                Me.ToolStripButton2.Enabled = True
                Me.CONDETFACTURASBANCOSBindingNavigatorSaveItem.Enabled = True
                Me.ToolStripButton3.Enabled = True
            End If
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim MSG As String = Nothing
        Dim NUMERRO As Integer = 0
        If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
            If Me.RealizadoCheckBox.Checked = False And Me.Proceso_CanceladoTextBox.Text = 0 Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.ValidaAfectacionBancosTableAdapter.Connection = CON
                Me.ValidaAfectacionBancosTableAdapter.Fill(Me.NewsoftvDataSet.ValidaAfectacionBancos, Me.Clv_SessionBancosTextBox.Text, 0, MSG, NUMERRO)
                CON.Close()
                If NUMERRO = 0 Then
                    GRABAFACTURASBANCOS()
                Else
                    MsgBox(MSG)
                End If
            Else
                If Me.RealizadoCheckBox.Checked = True Then MsgBox("El N�mero de Proceso : " & Me.Clv_SessionBancosTextBox.Text & " ya fue Afectado ", MsgBoxStyle.Information)
                If Me.Proceso_CanceladoTextBox.Text = 1 Then MsgBox("El N�mero de Proceso : " & Me.Clv_SessionBancosTextBox.Text & " esta Cancelado por lo cual no se puede generar el proceso de Afectaci�n ", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Seleccione el Proceso que desea Afectar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub GRABAFACTURASBANCOS()
        Dim TMPClv_SessionBancos As Long = 0
        Try
            Dim CON As New SqlConnection(MiConexion)

            TMPClv_SessionBancos = Me.Clv_SessionBancosTextBox.Text

            'Me.GUARDAFACTURASBANCOSTableAdapter.Connection = CON
            'Me.GUARDAFACTURASBANCOSTableAdapter.Fill(Me.NewsoftvDataSet.GUARDAFACTURASBANCOS, New System.Nullable(Of Long)(CType(Me.Clv_SessionBancosTextBox.Text, Long)), GloCajera, New System.Nullable(Of Integer)(CType(GloSucursal, Integer)), New System.Nullable(Of Integer)(CType(GloCaja, Integer)))
            ''
            CON.Open()
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "EXEC GUARDAFACTURASBANCOS " & Me.Clv_SessionBancosTextBox.Text & "," & GloCajera & "," & GloSucursal & "," & GloCaja
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                .ExecuteReader()
            End With
            CON.Close()
            ''
            CON.Open()
            Me.ConPreliminarBancosTableAdapter.Connection = CON
            Me.ConPreliminarBancosTableAdapter.Fill(Me.NewsoftvDataSet.ConPreliminarBancos, GloClv_Periodo_Num, 0)
            CON.Close()
            Me.Clv_SessionBancosTextBox.Text = TMPClv_SessionBancos
            bitsist(GloUsuario, 0, GloSistema, Me.Name, Me.Button2.Text, "Proceso de Afectacion del Periodo " + CStr(GloClv_Periodo_Num), "", LocClv_Ciudad)
            MsgBox("Proceso de Afectaci�n Finalizado con �xito", MsgBoxStyle.Information)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    

    'Private Sub CONDETFACTURASBANCOSDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    If IsNumeric(Me.Clv_IdTextBox.Text) = True Then
    '        If Me.Clv_FacturaTextBox.Text > 0 Then
    '            GloClv_Factura = Me.Clv_FacturaTextBox.Text
    '            FrmImprimir.Show()
    '        ElseIf Me.Clv_SessionTextBox.Text > 0 Then
    '            Glo_Clv_SessionVer = Me.Clv_SessionTextBox.Text
    '            Glocontratosel2 = Me.ContratoTextBox.Text
    '            Glo_BndErrorVer = 0
    '            Glo_MsgVer = ""
    '            FrmVerDetalleCobro.Show()
    '        End If
    '    End If

    'End Sub

    'Private Sub CONDETFACTURASBANCOSDataGridView_CellStateChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellStateChangedEventArgs)

    'End Sub

    'Private Sub CONDETFACTURASBANCOSDataGridView_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
    '    'If GloBndControl = False Then Exit Sub
    '    Try

    '        If e.ColumnIndex = 7 Then
    '            If IsNumeric(Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString) = True Then
    '                If Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString = 1 Then
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = True
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = True
    '                Else
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = False
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
    '                End If
    '            Else
    '                Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = False
    '                Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
    '            End If
    '        ElseIf e.ColumnIndex = 6 Then
    '            If IsNumeric(Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).Value.ToString) = True Then
    '                If Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).Value.ToString = 1 Then
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = True
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
    '                Else
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = False
    '                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
    '                End If
    '            Else
    '                Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = False
    '                Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
    '            End If
    '        End If
    '    Catch ex As Exception
    '        Exit Sub
    '    End Try
    'End Sub

    'Private Sub CONDETFACTURASBANCOSDataGridView_RowStateChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowStateChangedEventArgs)
    'If e.Row.Index >= 0 Then
    '    If IsNumeric(e.Row.Cells(6).Value.ToString()) = False Then Exit Sub
    '    If IsNumeric(e.Row.Cells(7).Value.ToString()) = False Then Exit Sub
    '    If e.Row.Cells(6).Value.ToString() = 1 Then
    '        e.Row.Cells(6).ReadOnly = True
    '        e.Row.Cells(7).ReadOnly = True
    '        e.Row.Cells(8).ReadOnly = True
    '    ElseIf e.Row.Cells(7).Value.ToString() = 1 Then
    '        e.Row.Cells(6).ReadOnly = True
    '        e.Row.Cells(7).ReadOnly = True
    '        e.Row.Cells(8).ReadOnly = True
    '    End If
    '    'If Me.CONDETFACTURASBANCOSDataGridView.Rows(e.Row.Selected).Cells(6).Value.ToString() = 1 Then

    '    'End If
    'End If
    'End Sub

    Private Sub Button5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Clv_SessionBancosTextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionBancosTextBox1.TextChanged
        If IsNumeric(Clv_SessionBancosTextBox1.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameGeneralesBancos_TotalTableAdapter.Connection = CON
            Me.DameGeneralesBancos_TotalTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos_Total, Clv_SessionBancosTextBox1.Text)
            CON.Close()
        End If
    End Sub

    Private Sub ToolStripButton2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton2.Click
        MsgBox("Primero se Generara el Archivo de Prosa", MsgBoxStyle.Information)
        GeneraDocumentoTxt()
        MsgBox("Ahora se Generara el Archivo de Santander Serfin", MsgBoxStyle.Information)
        GeneraDocumentoTxt_Santander()
        MsgBox("Ahora se Generara el Archivo de Bancomer", MsgBoxStyle.Information)
        GeneraDocumentotxt_Bancomer()
        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Generaron los Archivos de Prosa y de Santander Serfin", "", LocClv_Ciudad)
    End Sub

    Private Sub DIALabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ToolStripButton3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton3.Click
        'Me.CONDETFACTURASBANCOSBindingSource.CancelEdit()
        DameDetalle()
        If IsNumeric(Clv_SessionBancosTextBox1.Text) = True Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.DameGeneralesBancos_TotalTableAdapter.Connection = CON
            Me.DameGeneralesBancos_TotalTableAdapter.Fill(Me.NewsoftvDataSet2.DameGeneralesBancos_Total, Clv_SessionBancosTextBox1.Text)
            CON.Close()
        End If
        '
    End Sub

    Private Sub ToolStripButton4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripButton4.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.DameRangoFacturasTableAdapter.Connection = CON
        Me.DameRangoFacturasTableAdapter.Fill(Me.NewsoftvDataSet2.DameRangoFacturas, Me.Clv_SessionBancosTextBox.Text)
        CON.Close()
        If Me.INIText.Text = "0" And Me.FinText.Text = "0" Then
            MsgBox("No se Puede Imprimir pues no hay Facturas Generadas", MsgBoxStyle.Information)
        End If
        RangoFacturasIni = Me.INIText.Text
        RangoFacturasFin = Me.FinText.Text
        If LocImpresoraTickets = "" Then
            MsgBox("No se ha asigando una impresora de tickets a esta sucursal", MsgBoxStyle.Information)
        Else
            ConfigureCrystalReports(RangoFacturasIni, RangoFacturasFin)
            MsgBox("Su Factura se ha Impreso con �xito", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub ConfigureCrystalReports(ByVal rangoini As Long, ByVal rangofin As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)


        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, "0")
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, CStr(rangoini))
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, CStr(rangofin))
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, 1)

        ' If GloImprimeTickets = True Then
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        ' End If

        customersByCityReport.PrintToPrinter(1, True, 0, 0)

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub



    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)

        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub Button5_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try


            Dim i As Integer = 0
            Dim cont As Integer = 0
            Dim temporal As String = Nothing
            Dim mes As String = Nothing
            Dim a�o As String = Nothing
            Dim var As Integer
            Dim dia As String = Nothing
            Dim error1 As Integer = 0
            Dim errorglobal As Integer = 0
            Dim msj As String = Nothing
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.Borra_Tablas_ArchivosTableAdapter.Connection = CON
            Me.Borra_Tablas_ArchivosTableAdapter.Fill(Me.Procedimientos_arnoldo.Borra_Tablas_Archivos)
            Me.Valida_Proceso_CanceladoTableAdapter.Connection = CON
            Me.Valida_Proceso_CanceladoTableAdapter.Fill(Me.Procedimientos_arnoldo.Valida_Proceso_Cancelado, CInt(Me.Clv_SessionBancosTextBox.Text), error1)
            CON.Close()

            If error1 = 0 Then

                CantidadTotal = ""
                NoClientes = ""
                Fecha = ""
                Me.OpenFileDialog1.FileName = ""
                Me.OpenFileDialog1.Filter = "Archivo Resultados *.res|*.res"
                Me.OpenFileDialog1.ShowDialog()

                If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                    MsgBox("No se Selecciono el Archivo")
                Else

                    Archivo = My.Computer.FileSystem.ReadAllText(Me.OpenFileDialog1.FileName)
                    Ruta = Me.OpenFileDialog1.FileName
                    space = ChrW((Keys.Space))

                    indice = Archivo.IndexOfAny(space, 0)



                    '===================Obtener la Fecha====================
                    For i = 0 To indice - 7
                        Fecha = String.Concat(Fecha, Archivo(i))
                    Next
                    '=======================================================

                    '==========Obtener No de Clientes Procesados============


                    While Archivo(indice) = space
                        indice = indice + 1
                    End While

                    indice2 = Archivo.IndexOfAny(space, indice)

                    For i = indice To indice2
                        NoClientes = String.Concat(NoClientes, Archivo(i))
                    Next
                    '======================================================
                    '=========Obtener el Importe Total del Archivo=========

                    While Archivo(indice2) = space
                        indice2 = indice2 + 1
                    End While

                    indice = Archivo.IndexOfAny(".", indice2)

                    For i = indice2 To indice + 2
                        CantidadTotal = String.Concat(CantidadTotal, Archivo(i))
                    Next
                    '======================================================

                    '======Proceso de datos de los Clientes================    

                    While (cont < CInt(NoClientes))
                        var = 0
                        Contrato = ""
                        NoCuenta = ""
                        Cantidadcte = ""



                        intro = ChrW((Keys.Enter))

                        indice = Archivo.IndexOfAny(intro, indice2)
                        indice += 2

                        While (var < 9)
                            temporal = String.Concat(temporal, Archivo(indice))
                            indice += 1
                            var += 1
                        End While

                        indice2 = Archivo.IndexOfAny(space, indice)

                        For i = indice To indice2
                            Contrato = String.Concat(Contrato, Archivo(i))
                        Next

                        '====Quito espacios==========
                        While Archivo(indice2) = space
                            indice2 = indice2 + 1
                        End While

                        indice = Archivo.IndexOfAny(space, indice2)

                        For i = indice2 To indice
                            NoCuenta = String.Concat(NoCuenta, Archivo(i))
                        Next

                        '========Quito espacios =============
                        While Archivo(indice) = space
                            indice = indice + 1
                        End While

                        indice2 = Archivo.IndexOfAny(".", indice)

                        For i = indice To indice2 + 4
                            Cantidadcte = String.Concat(Cantidadcte, Archivo(i))
                        Next
                        Dim j As Integer = 0
                        Dim rechazado As String = Nothing

                        For j = indice2 + 11 To indice2 + 12
                            rechazado = String.Concat(rechazado, Archivo(j))
                        Next
                        cont += 1


                        '===Procedimiento Para Introducir Los Datos de Cada Cliente===============
                        '--Dim CON2 As New SqlConnection(MiConexion)
                        '--CON2.Open()
                        '--Me.Inserta_Datos_HTableAdapter.Connection = CON2
                        '--Me.Inserta_Datos_HTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Datos_H, CInt(Me.Clv_SessionBancosTextBox.Text), CInt(Contrato), NoCuenta, CDec(Cantidadcte))
                        '--CON2.Close()
                        Guarda_Inserta_Datos_H(CLng(Me.Clv_SessionBancosTextBox.Text), CLng(Contrato), NoCuenta, CDec(Cantidadcte), rechazado)

                    End While


                    '==========conversion de Fecha =========
                    For i = 0 To 1
                        dia = String.Concat(dia, Fecha(i))
                    Next
                    For i = 2 To 3
                        mes = String.Concat(mes, Fecha(i))
                    Next
                    For i = 4 To 7
                        a�o = String.Concat(a�o, Fecha(i))
                    Next

                    Fecha = dia + "/" + mes + "/" + a�o


                    '========Procedimiento para saber si el archivo Procede o no================
                    Dim CON3 As New SqlConnection(MiConexion)
                    CON3.Open()
                    Me.Checa_Archivo_ResProsaTableAdapter.Connection = CON3
                    Me.Checa_Archivo_ResProsaTableAdapter.Fill(Me.Procedimientos_arnoldo.Checa_Archivo_ResProsa, CInt(Me.Clv_SessionBancosTextBox.Text), errorglobal)
                    CON3.Close()
                    If errorglobal = 1 Then
                        MsgBox("El Archivo No Procede ya que algunos clientes no corresponden algunos datos", MsgBoxStyle.Information)
                    ElseIf errorglobal = 0 Then
                        '==============Procedimiento Para Indtoducir los Datos Generales de la Transaccion =============
                        Dim CON4 As New SqlConnection(MiConexion)
                        CON4.Open()
                        Me.Inserta_Datos_Archivo_PTableAdapter.Connection = CON4
                        Me.Inserta_Datos_Archivo_PTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_Datos_Archivo_P, CInt(Me.Clv_SessionBancosTextBox.Text), Ruta, CInt(NoClientes), CDec(CantidadTotal), Fecha)
                        CON4.Close()
                        bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Se Genero Archivo Prosa el dia: " + Fecha + " Con N�mero de Proceso de: " + Me.Clv_SessionBancosTextBox.Text, "Archivo Generado por un Total de: " + CStr(CantidadTotal) + "Con un Total de Clientes de: " + CStr(NoClientes), LocClv_Ciudad)
                        MsgBox("Archivo Procesado Exitosamente", MsgBoxStyle.Information)
                        DameDetalle()
                    End If
                End If
            Else
                msj = "El n�mero de Proceso: " + Me.Clv_SessionBancosTextBox.Text + " Esta cancelado por lo cu�l no se puede generar el proceso de Afectaci�n del Archivo de Resultados PROSA"
                MsgBox(msj, MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try


    End Sub




    Private Sub Dame_PreRecibo_oFactura()
        Try
            If IsNumeric(Me.Clv_IdTextBox.Text) = True Then
                Dim CON2 As New SqlConnection(MiConexion)
                CON2.Open()
                Me.Dame_PreRecibo_oFacturaTableAdapter.Connection = CON2
                Me.Dame_PreRecibo_oFacturaTableAdapter.Fill(Me.DataSetEdgar.Dame_PreRecibo_oFactura, Me.Clv_IdTextBox.Text)
                CON2.Close()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_IdTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_IdTextBox.TextChanged
        If IsNumeric(Me.Clv_IdTextBox.Text) = True Then
            Me.Dame_PreRecibo_oFactura()
        End If
    End Sub

    Private Sub Panel3_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel3.Paint

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try

            Dim CON As New SqlConnection(MiConexion)

            CON.Open()
            Dim prueba As String = Nothing
            'Dim locerrorsantander As Integer = 0
            'Dim proceso As Integer = 0
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim z As Integer = 0
            Dim clv_aceptado As String = Nothing
            Dim clv_id As String = Nothing
            Dim contrato As String = Nothing


            Me.OpenFileDialog1.FileName = ""
            Me.OpenFileDialog1.Filter = "Archivo Resultados *.txt|*.txt"
            Me.OpenFileDialog1.ShowDialog()


            If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                MsgBox("No se Selecciono el Archivo")
            Else
                Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                Dim archivo3 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                While archivo2.Peek() <> -1
                    archivo2.ReadLine()
                    y += 1
                End While
                archivo2.Close()

                ' MsgBox(y.ToString)


                Dim vector(0 To y) As String
                vector(y) = (0)
                ReDim vector(y)
                For x = 0 To y
                    vector(x) = archivo3.ReadLine()
                    prueba = vector(x)
                    If x > 0 And x <> y Then

                        clv_aceptado = prueba(277) + prueba(278)
                        clv_id = prueba(230) + prueba(231) + prueba(232) + prueba(233) + prueba(234) + prueba(235) + prueba(236)

                        For z = 135 To 174
                            If Len(contrato) > 0 Then
                                contrato = contrato + prueba(z)
                            Else
                                contrato = prueba(z)
                            End If
                        Next

                        '=========================Inserta Datos del Cliente==========================
                        If IsNumeric(contrato) = True And IsNumeric(clv_id) = True And IsNumeric(clv_aceptado) = True Then
                            Me.Inserta_ProcesoArchivoDebitoTableAdapter.Connection = CON
                            Me.Inserta_ProcesoArchivoDebitoTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_ProcesoArchivoDebito, CLng(contrato), CLng(clv_id), CLng(clv_aceptado), Me.OpenFileDialog1.FileName)
                        End If
                        contrato = ""
                    End If
                Next
                archivo3.Close()
            End If

            '========Afecto Clientes que Pasaron el el Archivo========00
            If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
                proceso = CLng(Me.Clv_SessionBancosTextBox.Text)
                'Me.Procesa_Arhivo_santaderTableAdapter.Connection = CON
                'Me.Procesa_Arhivo_santaderTableAdapter.Fill(Me.Procedimientos_arnoldo.Procesa_Arhivo_santader, proceso, locerrorsantander)
                'CON.Close()
                Procesa_Arhivo_santader(proceso)

                If locerrorsantander = 0 Then
                    bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Afectar Archivos Resultados Santander: " + Fecha + " Con Numero de Proceso de: " + CStr(proceso), "Archivo Generado por un Total de: " + CStr(CantidadTotal) + "Con un Total de Clientes de: " + CStr(NoClientes), LocClv_Ciudad)
                    MsgBox("Archivo Procesado Exitosamente", MsgBoxStyle.Information)
                    DameDetalle()
                ElseIf locerrorsantander = 1 Then
                    MsgBox("El N�mero De Proceso No Corresponde Al Archivo Seleccionado", MsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub Procesa_Arhivo_santader(ByVal PROCESO As Long)
        Dim CON20 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            CON20.Open()
            With cmd
                .CommandText = "Procesa_Arhivo_santader"
                .Connection = CON20
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_proceso", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = PROCESO
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Output
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
                locerrorsantander = prm1.Value
            End With
            CON20.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    'Private Sub CONDETFACTURASBANCOSDataGridView_CellContentClick_1(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONDETFACTURASBANCOSDataGridView.CellContentClick

    'End Sub

    Private Sub CONDETFACTURASBANCOSDataGridView_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONDETFACTURASBANCOSDataGridView.CellContentDoubleClick
        If IsNumeric(Me.Clv_IdTextBox.Text) = True Then
            If Me.Clv_FacturaTextBox.Text > 0 Then
                GloClv_Factura = Me.Clv_FacturaTextBox.Text
                FrmImprimir.Show()
            ElseIf Me.Clv_SessionTextBox.Text > 0 Then
                Glo_Clv_SessionVer = Me.Clv_SessionTextBox.Text
                Glocontratosel2 = Me.ContratoTextBox.Text
                Glo_BndErrorVer = 0
                Glo_MsgVer = ""
                FrmVerDetalleCobro.Show()
            End If
        End If

    End Sub

    Private Sub CONDETFACTURASBANCOSDataGridView_CellValueChanged1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONDETFACTURASBANCOSDataGridView.CellValueChanged
        'If GloBndControl = False Then Exit Sub
        Try

            If e.ColumnIndex = 7 Then
                If IsNumeric(Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString) = True Then
                    If Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).Value.ToString = 1 Then
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = True
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = True
                    Else
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = False
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
                    End If
                Else
                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).ReadOnly = False
                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
                End If
            ElseIf e.ColumnIndex = 6 Then
                If IsNumeric(Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).Value.ToString) = True Then
                    If Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(6).Value.ToString = 1 Then
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = True
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
                    Else
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = False
                        Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
                    End If
                Else
                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(7).ReadOnly = False
                    Me.CONDETFACTURASBANCOSDataGridView.Rows(e.RowIndex).Cells(8).ReadOnly = False
                End If
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub CONDETFACTURASBANCOSDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONDETFACTURASBANCOSDataGridView.CellContentClick

    End Sub

    Private Sub Clv_SessionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTextBox.TextChanged

    End Sub
    Private Sub Inserta_Proceso_archivo_bancomer(ByVal Clv_id As Long, ByVal clv_aceptacion As Integer)
        Dim CON20 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            CON20.Open()
            With cmd
                .CommandText = "Inserta_Proceso_archivo_bancomer"
                .Connection = CON20
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@Clv_id", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = Clv_id
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@clv_aceptacion", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = clv_aceptacion
                .Parameters.Add(prm1)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            CON20.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub
    Private Function Procesa_archivo_bancomer(ByVal clv_sessionBancos As Long) As Integer
        Dim error1 As Integer
        Dim con10 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        cmd = New SqlClient.SqlCommand()
        con10.Open()
        With cmd
            .CommandText = "Procesa_archivo_bancomer"
            .Connection = con10
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            '@clv_sessionBancos bigint,@error int output
            Dim prm As New SqlParameter("@clv_sessionBancos", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = clv_sessionBancos
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@error", SqlDbType.Int)
            prm1.Direction = ParameterDirection.Output
            prm1.Value = 0
            .Parameters.Add(prm1)

            Dim i As Integer = .ExecuteNonQuery()

            error1 = prm1.Value
        End With
        con10.Close()
        Return error1
    End Function
    Public Sub Borra_tabla_afectacion_bancomer(ByVal clv_proceso As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()
        Try
            cmd = New SqlClient.SqlCommand()
            con.Open()
            With cmd
                .CommandText = "Borra_tabla_afectacion_bancomer"
                .Connection = con
                .CommandTimeout = 0
                .CommandType = CommandType.StoredProcedure

                Dim prm As New SqlParameter("@clv_proceso", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = clv_proceso
                .Parameters.Add(prm)

                Dim i As Integer = .ExecuteNonQuery()
            End With
            con.Close()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Try

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Dim prueba As String = Nothing
            Dim locerrorsantander As Integer = 0
            Dim proceso As Integer = 0
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim z As Integer = 0
            Dim clv_aceptado As String = Nothing
            Dim clv_id As String = Nothing
            Dim contrato As String = Nothing
            Dim checaarchivo As String = Nothing


            Me.OpenFileDialog1.FileName = ""
            'Me.OpenFileDialog1.Filter = "Archivo Resultados Bancomer"
            Me.OpenFileDialog1.ShowDialog()


            If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                MsgBox("No se Selecciono el Archivo")
            Else
                Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                Dim archivo3 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                While archivo2.Peek() <> -1
                    archivo2.ReadLine()
                    y += 1
                End While
                archivo2.Close()
                y = y - 1

                ' MsgBox(y.ToString)


                Dim vector(0 To y) As String
                vector(y) = (0)
                ReDim vector(y)
                For x = 0 To y
                    vector(x) = archivo3.ReadLine()
                    prueba = vector(x)
                    If x > 0 And x <> y Then
                        clv_aceptado = prueba.Substring(54, 1)
                        clv_id = prueba.Substring(33, 19)

                        'MsgBox(clv_aceptado + "-" + clv_id, MsgBoxStyle.Information)

                        '=========================Inserta Datos del Cliente==========================
                        If IsNumeric(clv_id) = True And IsNumeric(clv_aceptado) = True Then
                            Inserta_Proceso_archivo_bancomer(CLng(clv_id), CInt(clv_aceptado))
                        End If
                        '    contrato = ""
                    ElseIf x = 0 Then
                        checaarchivo = prueba.Substring(10, 3)
                        Fecha = prueba.Substring(38, 8)
                        If checaarchivo <> "RSP" Then
                            MsgBox("El Archivo Seleccionado No Es De Respuesta", MsgBoxStyle.Information)
                            Exit Sub
                        End If
                    ElseIf x = y Then
                        NoClientes = prueba.Substring(1, 6)
                        CantidadTotal = prueba.Substring(7, 15)
                    End If
                Next
                archivo3.Close()
            End If

            '========Afecto Clientes que Pasaron el el Archivo========00
            If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
                proceso = CLng(Me.Clv_SessionBancosTextBox.Text)
                locerrorsantander = Procesa_archivo_bancomer(proceso)
                If locerrorsantander = 0 Then
                    'Fecha = DateTime.Today.ToString("dd/MM/yyy")
                    bitsist(GloUsuario, 0, GloSistema, Me.Name, "", "Afectar Archivos Resultados Bancomer: " + Fecha + " Con Numero de Proceso de: " + CStr(proceso), "Archivo Generado por un Total de: " + CStr(CLng(CantidadTotal)) + " Con un Total de Clientes de: " + CStr(CLng(NoClientes)), LocClv_Ciudad)
                    MsgBox("Archivo Procesado Exitosamente", MsgBoxStyle.Information)
                    Borra_tabla_afectacion_bancomer(CLng(Me.Clv_SessionBancosTextBox.Text))
                    DameDetalle()
                ElseIf locerrorsantander = 1 Then
                    MsgBox("El N�mero De Proceso No Corresponde Al Archivo Seleccionado", MsgBoxStyle.Information)
                End If
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub



End Class
