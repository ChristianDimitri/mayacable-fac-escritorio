''Imports System
Imports System.IO
Imports System.Text
Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class FrmSeleccionaCiudad

    Dim fecha As Date

    Private Sub llena_combo()
        Dim a As Integer = 0
        Dim b As Integer = 0
        Dim c As Integer = 0
        Dim d As Integer = 0
        Dim retString As String = Nothing
        Dim ciudad As String = Nothing
        Dim IP As String = Nothing
        Dim BD As String = Nothing
        Dim Login As String = Nothing
        Dim Pass As String = Nothing

        Dim path As String = "C:\Windows\SETUP_001.TXT"

        'Dim path As String = "\\Servidor-CMTS\Exes\Softv\SETUP_001.TXT"
        'eRutaCFD = "\\Tequis\Exes\NewSoftvFacturacion\FacturaNetCFD\"
        'rutaBanner = "\\tequis\Exes\Facturacion\Banner\"

        Me.ComboBox1.Items.Clear()
        Me.ComboBox2.Items.Clear()
        Me.ComboBox3.Items.Clear()
        Me.ComboBox4.Items.Clear()
        Me.ComboBox5.Items.Clear()
        ' Open the file to read from.
        Dim sr As StreamReader = File.OpenText(path)
        Do While sr.Peek() >= 0
            retString = DesEncriptaME(sr.ReadLine())
            If Len(retString) > 0 Then
                a = InStr(1, Trim(retString), "|", vbTextCompare)
                If a > 0 Then
                    b = InStr(a + 1, Trim(retString), "|", vbTextCompare)
                    c = InStr(b + 1, Trim(retString), "|", vbTextCompare)
                    d = InStr(c + 1, Trim(retString), "|", vbTextCompare)
                    ciudad = Mid(Trim(retString), 1, a - 1)
                    IP = Mid(Trim(retString), a + 1, (b - a) - 1)
                    BD = Mid(Trim(retString), b + 1, (c - b) - 1)
                    Login = Mid(Trim(retString), c + 1, (d - c) - 1)
                    Pass = Mid(Trim(retString), d + 1, Len(Trim(Trim(retString))))
                    Me.ComboBox1.Items.Add(ciudad)
                    Me.ComboBox2.Items.Add(IP)
                    Me.ComboBox3.Items.Add(BD)
                    Me.ComboBox4.Items.Add(Login)
                    Me.ComboBox5.Items.Add(Pass)

                End If
            End If
        Loop
        sr.Close()
        If Me.ComboBox1.Items.Count = 1 Then
            Me.ComboBox1.SelectedIndex = 0
            Me.ComboBox2.SelectedIndex = 0
            Me.ComboBox3.SelectedIndex = 0
            Me.ComboBox4.SelectedIndex = 0
            Me.ComboBox5.SelectedIndex = 0
        End If
    End Sub


    Private Sub FrmSeleccionaCiudad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        VALIDASetup()
        llena_combo()
    End Sub

    Private Sub VALIDASetup()
        'MiConexion = "Data Source=Servidor-CMTS; Initial Catalog= NewSoftv; Persist Security Info=True;User ID= SA; Password=0601x-2L; Connect Timeout=0"
        'MiConexion = "Data Source=ferrer-pc; Initial Catalog= NewSoftv; Persist Security Info=True;User ID= SA; Password=06011975; Connect Timeout=0"
        'Dim Mensaje As String = ""
        'Dim Elimina As Boolean = False
        'BaseII.limpiaParametros()
        'BaseII.CreateMyParameter("@MENSAJE", ParameterDirection.Output, SqlDbType.VarChar, 150)
        'BaseII.CreateMyParameter("@ELIMINA", ParameterDirection.Output, SqlDbType.Bit)
        'BaseII.ProcedimientoOutPut("VALIDASetup")
        'Mensaje = BaseII.dicoPar("@MENSAJE").ToString
        'Elimina = Boolean.Parse(BaseII.dicoPar("@ELIMINA").ToString)

        'MiConexion = ""

        'If Mensaje.Length > 0 Then
        '    MessageBox.Show(Mensaje)
        'End If

        'If Elimina = True Then
        '    File.Delete("\\Servidor-CMTS\Exes\Softv\SETUP_001.TXT")
        '    'File.Delete("\\Tequis\Exes\Softv\SETUP_001.TXT")
        'End If

    End Sub

    Private Sub ComboBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ComboBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Entrar()
        End If
    End Sub

    Private Sub ComboBox1_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles ComboBox1.LostFocus
        If Me.ComboBox1.SelectedIndex = -1 Then
            Me.ComboBox1.Text = ""
            Me.ComboBox2.Text = ""
            Me.ComboBox3.Text = ""
            Me.ComboBox4.Text = ""
            Me.ComboBox5.Text = ""
        End If
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.ComboBox2.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox3.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox4.SelectedIndex = Me.ComboBox1.SelectedIndex
        Me.ComboBox5.SelectedIndex = Me.ComboBox1.SelectedIndex
    End Sub

    Private Sub Entrar()
        If Me.ComboBox1.SelectedIndex <> -1 Then

            GloServerName = Me.ComboBox2.Text
            GloDatabaseName = Me.ComboBox3.Text
            GloUserID = Me.ComboBox4.Text
            GloPassword = Me.ComboBox5.Text

            'GloServerName = "Oswaldo-pc"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "06011975"

            'GloServerName = "192.168.50.245\LAB2,1633"
            'GloDatabaseName = "NewSoftv"
            'GloUserID = "sa"
            'GloPassword = "0601x-2L"

            GloServerName = "Richard2-pc"
            GloDatabaseName = "NewSoftv_Mayacable"
            GloUserID = "sa"
            GloPassword = "06011975"

            MiConexion = "Data Source=" & GloServerName & ";Initial Catalog=" & GloDatabaseName & " ;User ID=" & GloUserID & ";Password=" & GloPassword & ";Connection Timeout=0" & ""
            GloIp = GloServerName
            LoginForm1.Show()
            Me.Close()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Entrar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        End
        Me.Close()
    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub



End Class
