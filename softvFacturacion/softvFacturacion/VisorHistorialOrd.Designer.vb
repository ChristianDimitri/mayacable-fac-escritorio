<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VisorHistorialOrd
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim NombreLabel As System.Windows.Forms.Label
        Dim NUMEROLabel As System.Windows.Forms.Label
        Dim CALLELabel As System.Windows.Forms.Label
        Dim ContratoLabel As System.Windows.Forms.Label
        Dim Clv_calleLabel1 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label4 = New System.Windows.Forms.Label
        Me.Clv_calleLabel2 = New System.Windows.Forms.Label
        Me.BUSCAORDSERBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.LydiaDataSet2 = New softvFacturacion.LydiaDataSet2
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ClvOrdenDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ContratoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.STATUSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NombreDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CALLEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NUMERODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ClvTipSerDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Button6 = New System.Windows.Forms.Button
        Me.CMBNombreTextBox = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.NUMEROLabel1 = New System.Windows.Forms.Label
        Me.CALLELabel1 = New System.Windows.Forms.Label
        Me.ContratoLabel1 = New System.Windows.Forms.Label
        Me.CMBLabel1 = New System.Windows.Forms.Label
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.BUSCAORDSERTableAdapter = New softvFacturacion.LydiaDataSet2TableAdapters.BUSCAORDSERTableAdapter
        NombreLabel = New System.Windows.Forms.Label
        NUMEROLabel = New System.Windows.Forms.Label
        CALLELabel = New System.Windows.Forms.Label
        ContratoLabel = New System.Windows.Forms.Label
        Clv_calleLabel1 = New System.Windows.Forms.Label
        CType(Me.BUSCAORDSERBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LydiaDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'NombreLabel
        '
        NombreLabel.AutoSize = True
        NombreLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NombreLabel.ForeColor = System.Drawing.Color.White
        NombreLabel.Location = New System.Drawing.Point(7, 110)
        NombreLabel.Name = "NombreLabel"
        NombreLabel.Size = New System.Drawing.Size(66, 15)
        NombreLabel.TabIndex = 3
        NombreLabel.Text = "Nombre :"
        '
        'NUMEROLabel
        '
        NUMEROLabel.AutoSize = True
        NUMEROLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NUMEROLabel.ForeColor = System.Drawing.Color.White
        NUMEROLabel.Location = New System.Drawing.Point(3, 236)
        NUMEROLabel.Name = "NUMEROLabel"
        NUMEROLabel.Size = New System.Drawing.Size(70, 15)
        NUMEROLabel.TabIndex = 9
        NUMEROLabel.Text = "Numero  :"
        '
        'CALLELabel
        '
        CALLELabel.AutoSize = True
        CALLELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CALLELabel.ForeColor = System.Drawing.Color.White
        CALLELabel.Location = New System.Drawing.Point(24, 164)
        CALLELabel.Name = "CALLELabel"
        CALLELabel.Size = New System.Drawing.Size(48, 15)
        CALLELabel.TabIndex = 7
        CALLELabel.Text = "Calle :"
        '
        'ContratoLabel
        '
        ContratoLabel.AutoSize = True
        ContratoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ContratoLabel.ForeColor = System.Drawing.Color.White
        ContratoLabel.Location = New System.Drawing.Point(3, 69)
        ContratoLabel.Name = "ContratoLabel"
        ContratoLabel.Size = New System.Drawing.Size(69, 15)
        ContratoLabel.TabIndex = 6
        ContratoLabel.Text = "Contrato :"
        '
        'Clv_calleLabel1
        '
        Clv_calleLabel1.AutoSize = True
        Clv_calleLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Clv_calleLabel1.ForeColor = System.Drawing.Color.White
        Clv_calleLabel1.Location = New System.Drawing.Point(7, 35)
        Clv_calleLabel1.Name = "Clv_calleLabel1"
        Clv_calleLabel1.Size = New System.Drawing.Size(66, 15)
        Clv_calleLabel1.TabIndex = 1
        Clv_calleLabel1.Text = "# Orden :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(234, 18)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Datos de la Orden de Servicio"
        '
        'Clv_calleLabel2
        '
        Me.Clv_calleLabel2.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Clv_Orden", True))
        Me.Clv_calleLabel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_calleLabel2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Clv_calleLabel2.Location = New System.Drawing.Point(88, 35)
        Me.Clv_calleLabel2.Name = "Clv_calleLabel2"
        Me.Clv_calleLabel2.Size = New System.Drawing.Size(100, 23)
        Me.Clv_calleLabel2.TabIndex = 2
        '
        'BUSCAORDSERBindingSource1
        '
        Me.BUSCAORDSERBindingSource1.DataMember = "BUSCAORDSER"
        Me.BUSCAORDSERBindingSource1.DataSource = Me.LydiaDataSet2
        '
        'LydiaDataSet2
        '
        Me.LydiaDataSet2.DataSetName = "LydiaDataSet2"
        Me.LydiaDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClvOrdenDataGridViewTextBoxColumn, Me.ContratoDataGridViewTextBoxColumn, Me.STATUSDataGridViewTextBoxColumn, Me.NombreDataGridViewTextBoxColumn, Me.CALLEDataGridViewTextBoxColumn, Me.NUMERODataGridViewTextBoxColumn, Me.ClvTipSerDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.BUSCAORDSERBindingSource1
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(554, 729)
        Me.DataGridView1.TabIndex = 1
        Me.DataGridView1.TabStop = False
        '
        'ClvOrdenDataGridViewTextBoxColumn
        '
        Me.ClvOrdenDataGridViewTextBoxColumn.DataPropertyName = "Clv_Orden"
        Me.ClvOrdenDataGridViewTextBoxColumn.HeaderText = "Clave de la Orden"
        Me.ClvOrdenDataGridViewTextBoxColumn.Name = "ClvOrdenDataGridViewTextBoxColumn"
        Me.ClvOrdenDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ContratoDataGridViewTextBoxColumn
        '
        Me.ContratoDataGridViewTextBoxColumn.DataPropertyName = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.ContratoDataGridViewTextBoxColumn.Name = "ContratoDataGridViewTextBoxColumn"
        Me.ContratoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'STATUSDataGridViewTextBoxColumn
        '
        Me.STATUSDataGridViewTextBoxColumn.DataPropertyName = "STATUS"
        Me.STATUSDataGridViewTextBoxColumn.HeaderText = "Status"
        Me.STATUSDataGridViewTextBoxColumn.Name = "STATUSDataGridViewTextBoxColumn"
        Me.STATUSDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NombreDataGridViewTextBoxColumn
        '
        Me.NombreDataGridViewTextBoxColumn.DataPropertyName = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NombreDataGridViewTextBoxColumn.Name = "NombreDataGridViewTextBoxColumn"
        Me.NombreDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CALLEDataGridViewTextBoxColumn
        '
        Me.CALLEDataGridViewTextBoxColumn.DataPropertyName = "CALLE"
        Me.CALLEDataGridViewTextBoxColumn.HeaderText = "Calle"
        Me.CALLEDataGridViewTextBoxColumn.Name = "CALLEDataGridViewTextBoxColumn"
        Me.CALLEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NUMERODataGridViewTextBoxColumn
        '
        Me.NUMERODataGridViewTextBoxColumn.DataPropertyName = "NUMERO"
        Me.NUMERODataGridViewTextBoxColumn.HeaderText = "Número"
        Me.NUMERODataGridViewTextBoxColumn.Name = "NUMERODataGridViewTextBoxColumn"
        Me.NUMERODataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClvTipSerDataGridViewTextBoxColumn
        '
        Me.ClvTipSerDataGridViewTextBoxColumn.DataPropertyName = "Clv_TipSer"
        Me.ClvTipSerDataGridViewTextBoxColumn.HeaderText = "Tipo de Servicio"
        Me.ClvTipSerDataGridViewTextBoxColumn.Name = "ClvTipSerDataGridViewTextBoxColumn"
        Me.ClvTipSerDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(868, 610)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 36)
        Me.Button6.TabIndex = 32
        Me.Button6.Text = "&IMPRIMIR"
        Me.Button6.UseVisualStyleBackColor = False
        Me.Button6.Visible = False
        '
        'CMBNombreTextBox
        '
        Me.CMBNombreTextBox.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNombreTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Nombre", True))
        Me.CMBNombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBNombreTextBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CMBNombreTextBox.Location = New System.Drawing.Point(91, 110)
        Me.CMBNombreTextBox.Multiline = True
        Me.CMBNombreTextBox.Name = "CMBNombreTextBox"
        Me.CMBNombreTextBox.ReadOnly = True
        Me.CMBNombreTextBox.Size = New System.Drawing.Size(171, 46)
        Me.CMBNombreTextBox.TabIndex = 6
        Me.CMBNombreTextBox.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton3)
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(11, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(257, 172)
        Me.GroupBox1.TabIndex = 10
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ver Por Status"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.ForeColor = System.Drawing.Color.Black
        Me.RadioButton3.Location = New System.Drawing.Point(22, 125)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(103, 22)
        Me.RadioButton3.TabIndex = 12
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "Con Visita"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.ForeColor = System.Drawing.Color.Black
        Me.RadioButton2.Location = New System.Drawing.Point(22, 76)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(109, 22)
        Me.RadioButton2.TabIndex = 11
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Ejecutadas"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.ForeColor = System.Drawing.Color.Black
        Me.RadioButton1.Location = New System.Drawing.Point(22, 31)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(109, 22)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Pendientes"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 6)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 729)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 34
        Me.SplitContainer1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(NUMEROLabel)
        Me.Panel1.Controls.Add(Me.NUMEROLabel1)
        Me.Panel1.Controls.Add(CALLELabel)
        Me.Panel1.Controls.Add(Me.CALLELabel1)
        Me.Panel1.Controls.Add(ContratoLabel)
        Me.Panel1.Controls.Add(Me.ContratoLabel1)
        Me.Panel1.Controls.Add(Me.CMBNombreTextBox)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(NombreLabel)
        Me.Panel1.Controls.Add(Clv_calleLabel1)
        Me.Panel1.Controls.Add(Me.Clv_calleLabel2)
        Me.Panel1.Location = New System.Drawing.Point(6, 326)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(269, 281)
        Me.Panel1.TabIndex = 8
        '
        'NUMEROLabel1
        '
        Me.NUMEROLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "NUMERO", True))
        Me.NUMEROLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.NUMEROLabel1.Location = New System.Drawing.Point(88, 238)
        Me.NUMEROLabel1.Name = "NUMEROLabel1"
        Me.NUMEROLabel1.Size = New System.Drawing.Size(100, 23)
        Me.NUMEROLabel1.TabIndex = 10
        '
        'CALLELabel1
        '
        Me.CALLELabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "CALLE", True))
        Me.CALLELabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.CALLELabel1.Location = New System.Drawing.Point(91, 166)
        Me.CALLELabel1.Name = "CALLELabel1"
        Me.CALLELabel1.Size = New System.Drawing.Size(171, 23)
        Me.CALLELabel1.TabIndex = 8
        '
        'ContratoLabel1
        '
        Me.ContratoLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAORDSERBindingSource1, "Contrato", True))
        Me.ContratoLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ContratoLabel1.Location = New System.Drawing.Point(88, 71)
        Me.ContratoLabel1.Name = "ContratoLabel1"
        Me.ContratoLabel1.Size = New System.Drawing.Size(100, 23)
        Me.ContratoLabel1.TabIndex = 7
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(9, 12)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(221, 16)
        Me.CMBLabel1.TabIndex = 1
        Me.CMBLabel1.Text = "Buscar Orden de Servicio Por :"
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(868, 664)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 33
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.Orange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(868, 6)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 30
        Me.Button3.Text = "&CONSULTA"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'BUSCAORDSERTableAdapter
        '
        Me.BUSCAORDSERTableAdapter.ClearBeforeFill = True
        '
        'VisorHistorialOrd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1016, 741)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button5)
        Me.MaximizeBox = False
        Me.Name = "VisorHistorialOrd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Visor de Historial de Ordenes de Servicio"
        CType(Me.BUSCAORDSERBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LydiaDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Clv_calleLabel2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBNombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents NUMEROLabel1 As System.Windows.Forms.Label
    Friend WithEvents CALLELabel1 As System.Windows.Forms.Label
    Friend WithEvents ContratoLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents LydiaDataSet2 As softvFacturacion.LydiaDataSet2
    Friend WithEvents BUSCAORDSERBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAORDSERTableAdapter As softvFacturacion.LydiaDataSet2TableAdapters.BUSCAORDSERTableAdapter
    Friend WithEvents ClvOrdenDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ContratoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents STATUSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CALLEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NUMERODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvTipSerDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
