Imports System.Data.SqlClient
Public Class FrmSelTipCargoAuto

    Private Sub FrmSelTipCargoAuto_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Me.Text = "Seleccion Tipo De Cargos Automaticos"
        Dim con1 As New SqlClient.SqlConnection(MiConexion)
        Llena_tabla_tmp(locclv_sessioncargosauto)
        con1.Open()
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter.Connection = con1
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsulta, locclv_sessioncargosauto)
        con1.Close()
    End Sub
    Private Sub Llena_tabla_tmp(ByVal clv_session As Long)
        Dim con As New SqlConnection(MiConexion)
        Dim cmd As New SqlCommand()

        con.Open()
        cmd = New SqlClient.SqlCommand()
        With cmd
            .CommandText = "Muestra_Seleccion_TipoCuenta_CargosAuto_tmpnuevo"
            .CommandTimeout = 0
            .CommandType = CommandType.StoredProcedure
            .Connection = con

            Dim prm As New SqlParameter("@clv_session ", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = clv_session
            .Parameters.Add(prm)

            Dim i As Integer = cmd.ExecuteNonQuery()
        End With
        con.Close()
    End Sub
    Private Sub Inserta_uno(ByVal clv_session As Long, ByVal tipocargo As String, ByVal op As Integer)
        Dim con2 As New SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()

        Select Case op
            Case 0
                cmd = New SqlClient.SqlCommand()
                con2.Open()
                With cmd
                    .CommandText = "Insertauno_Seleccion_TipoCuenta_CargosAuto"
                    .Connection = con2
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    '@clv_session bigint, @Tipo_cuenta varchar(50)

                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = clv_session
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@Tipo_cuenta", SqlDbType.VarChar, 50)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = tipocargo
                    .Parameters.Add(prm1)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                End With
                con2.Close()
            Case 1
                cmd = New SqlClient.SqlCommand()
                con2.Open()
                With cmd
                    .CommandText = "Insertauno_Seleccion_TipoCuenta_CargosAuto_tmp"
                    .Connection = con2
                    .CommandTimeout = 0
                    .CommandType = CommandType.StoredProcedure

                    '@clv_session bigint, @Tipo_cuenta varchar(50)

                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = clv_session
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@Tipo_cuenta", SqlDbType.VarChar, 50)
                    prm1.Direction = ParameterDirection.Input
                    prm1.Value = tipocargo
                    .Parameters.Add(prm1)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                End With
                con2.Close()
        End Select
    End Sub
    Private Sub Inserta_todo(ByVal clv_session As Long, ByVal op As Integer)
        Dim con3 As New SqlClient.SqlConnection(MiConexion)
        Dim cmd As New SqlClient.SqlCommand()

        Select Case op
            Case 0
                cmd = New SqlClient.SqlCommand()
                con3.Open()

                With cmd
                    .CommandText = "InsertaTOdos_Seleccion_TipoCuenta_CargosAuto"
                    .CommandTimeout = 0
                    .Connection = con3
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = clv_session
                    .Parameters.Add(prm)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                End With
                con3.Close()

            Case 1

                cmd = New SqlClient.SqlCommand()
                con3.Open()

                With cmd
                    .CommandText = "InsertaTOdos_Seleccion_TipoCuenta_CargosAuto_tmp"
                    .CommandTimeout = 0
                    .Connection = con3
                    .CommandType = CommandType.StoredProcedure

                    Dim prm As New SqlParameter("@clv_session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = clv_session
                    .Parameters.Add(prm)

                    Dim i As Integer = cmd.ExecuteNonQuery()

                End With
                con3.Close()

        End Select
    End Sub
    Private Sub consulta(ByVal clv_session)
        Dim con4 As New SqlClient.SqlConnection(MiConexion)
        con4.Open()
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter.Connection = con4
        Me.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsultaTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Seleccion_TipoCuenta_CargosAuto_tmpconsulta, clv_session)
        Me.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter.Connection = con4
        Me.Muestra_Seleccion_TipoCuenta_CargosAutoconsultaTableAdapter.Fill(Me.ProcedimientosArnoldo3.Muestra_Seleccion_TipoCuenta_CargosAutoconsulta, clv_session)
        con4.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Inserta_uno(locclv_sessioncargosauto, CStr(Me.ListBox1.SelectedValue), 0)
        consulta(locclv_sessioncargosauto)
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Inserta_uno(locclv_sessioncargosauto, CStr(Me.ListBox2.SelectedValue), 1)
        consulta(locclv_sessioncargosauto)
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Me.Inserta_todo(locclv_sessioncargosauto, 0)
        consulta(locclv_sessioncargosauto)
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Inserta_todo(locclv_sessioncargosauto, 1)
        consulta(locclv_sessioncargosauto)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        bndcancelareportcargos = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmSelFechasReporteCargos.Show()
        Me.Close()
    End Sub
End Class