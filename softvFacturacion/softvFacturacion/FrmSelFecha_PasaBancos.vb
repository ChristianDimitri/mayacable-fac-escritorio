Imports System.Data.SqlClient
Public Class FrmSelFecha_PasaBancos

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        BndPasaBancos = False
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        FechaPasaBancos = Me.DateTimePicker1.Value
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "EXEC GUARDAPASABANCOS" & "'" & FechaPasaBancos & "'"
            .CommandType = CommandType.Text
            .CommandTimeout = 0
            .ExecuteReader()
        End With
        CON.Close()

        BndPasaBancos = True
        Me.Close()
    End Sub

    Private Sub FrmSelFecha_PasaBancos_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
    End Sub
End Class