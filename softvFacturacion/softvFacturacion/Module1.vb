Imports System.Data.SqlClient
Imports System.Text
Imports GeneraCFDFacturaNet
Imports System.IO
Imports CrystalDecisions.CrystalReports.Engine


Module Module1


    Public BonificacionCajera As Boolean = False
    'EDGAR
    '***********************
    Public MiConexion As String = Nothing
    Public locPregunta As String = ""
    Public MsjUspHaz_Pregunta As String = ""
    Public LcjUspHaz_Pregunta As Integer = 0
    '***********************
    'Eric Variables
    Public eClvMotCan As Integer = 0
    Public eBndMensAde As Boolean = False
    Public bnd1pardep1 As Integer = 0
    Public bnd1pardep2 As Integer = 0
    Public bnd2pardep As Boolean = False
    Public bnd2pardep1 As Boolean = False
    Public locclvsessionpardep As Long = 0
    Public Loccontratopardep As Long = 0
    Public locbndborracabiosdep As Boolean = False
    'Edgar me Va servir para SAber si es un Estado de cuenta o un Cobro Normal
    Public GloOpCobro As Integer = 0
    Public GloCEXTTE As Boolean = False
    Public GloComproEquipo As Boolean = False
    Public GloAbonoACuenta As Boolean = False
    Public LocBndrelingporconceptos As Boolean = False
    Public eBotonGuardar As Boolean
    Public LocBndNotasReporteTick As Boolean = False
    Public locoprepnotas As Integer = 0
    Public Repetido As Boolean
    Public eFechaInicial, eFechaFinal As Date
    Public eFechaTitulo As String
    Public eCveFactura As Long
    Public eEntra As Boolean
    Public eReImprimirF As Integer = 0
    Public eEntraReImprimir As Boolean
    Public eMotivoBonificacion As String = Nothing
    Public eLoginCajera As String = Nothing
    Public ePassCajera As String = Nothing
    Public eConModDesglose As Boolean = False
    Public eCveCajera As String = Nothing
    Public eCorresponde As Boolean = False
    Public eClv_Progra As Long = 0
    Public eClv_Txt As String = 0
    Public eFechaServidor As Date
    Public eRes As Integer = 0
    Public eMsj As String = String.Empty
    Public eBndPPE As Boolean = False
    Public eAccesoAdmin As Boolean = False
    Public eClv_Session As Long = 0
    Public eContrato As Long = 0
    Public eBndContrato As Boolean = False
    Public eBndContratoEq As Boolean = False
    Public eBndFactura As Boolean = False
    Public eClv_Factura As Long = 0
    Public eFactura As String = String.Empty
    Public eNota As Boolean = False
    Public ePeriodo1 As Integer = 0
    Public ePeriodo2 As Integer = 0
    Public eMes As Integer = 0
    Public eAnio As Integer = 0
    Public ePago1 As Integer = 0
    Public ePago2 As Integer = 0
    Public eTitulo As String = String.Empty
    Public rutaBanner As String = String.Empty
    '------------------------------
    Public eOpRep As Integer = 0
    Public eClvTipSer As Integer = 0
    Public eConcepto As String = String.Empty
    Public eOp As Integer = 0
    Public eOperador As String = String.Empty
    Public eMonto As Decimal = 0
    Public eNumero As String = String.Empty
    Public eFechaIni As Date
    Public eFechaFin As Date
    Public eCancelacionRE As Boolean = False
    Public eC As Boolean = False
    Public eI As Boolean = False
    Public eD As Boolean = False
    Public eS As Boolean = False
    Public eB As Boolean = False
    Public eF As Boolean = False
    Public eSerie As String = Nothing
    Public eFolio As Long = 0
    Public eRutaCFD As String = Nothing
    Public facturaFiscalCFD As Boolean = False

    '--------------------------------

    'Public GloClv_Detalle As Long = 0
    '**************************************
    Public RangoFacturasIni As Integer
    Public RangoFacturasFin As Integer
    Public RutaReportes As String
    Public OPCION As Char
    Public BLOQUEA As Boolean = False
    Public band As Boolean = False
    Public locnomsupervisor As String = ""
    Public bec_tipo As String = Nothing
    Public bec_impIva As Decimal
    Public bec_serie As String
    Public bec_letra As String
    Public bec_factura As String
    Public bec_fecha As String
    Public bec_importe As String
    Public bec_bandera As Integer = 0
    Public bec_consecutivo As Integer
    Public GLOMENUS As Integer
    Public glomenu As String
    Public GloBnd As Boolean
    Public GloAdelantados As Integer
    Public gloClv_Session As Long = 0
    Public gloClv_Servicio As Long = 0
    Public gloClv_llave As Long = 0
    Public gloClv_UnicaNet As Long = 0
    Public gloClave As Long = 0
    Public GloContrato As Long = 0
    Public IdSistema As String = " "
    Public GloBndExt As Boolean = False
    Public GloExt As Integer = 0
    Public GloClv_Txt As String = Nothing
    Public GloCajera As String = "SISTE"
    Public GLONOMCAJERAARQUEO As String = Nothing
    Public GloSucursal As Integer = 0
    Public GloCaja As Integer = 0
    Public GlonOMCaja As String
    Public GloClv_Factura As Long = 0
    Public GloIpMaquina As String = Nothing
    Public GloHostName As String = Nothing
    Public GloUsuario As String = Nothing
    Public op As String
    Public Fecha_ini As String
    Public Fecha_Fin As String
    Public NomCajera As String
    Public NomCaja As String
    Public NomSucursal As String
    Public BanderaReporte As Boolean = False
    Public ExtraT As String
    Public Resumen As Boolean = False
    Public Resumen1 As Integer
    Public Resumen2 As Integer
    Public Resumen3 As Boolean = False
    Public GloReporte As Integer = 0
    Public GloClv_SessionBancos As Long = 0
    Public GloTitulo As String = Nothing
    Public GloSubTitulo As String = Nothing
    Public GloBndControl As Boolean = False
    Public GloTipo As String = "C"
    Public GloServerName As String = Nothing
    Public GloDatabaseName As String = Nothing
    Public GloUserID As String = Nothing
    Public GloPassword As String = Nothing
    Public SelCajaResumen As Integer
    Public FlagCaja As Boolean = False
    Public Consecutivo As Long = 0
    Public Suma As Double = 0
    Public GloConsecutivo As Long
    Public loctitulo As String = Nothing
    Public gloClv_Detalle As Long = 0
    Public GloDes_Ser As String = Nothing
    Public locband_pant As Integer = 0
    Public GloNomSucursal As String
    Public GloOpFacturas As Integer = 0
    Public GLOIMPTOTAL As Double = 0
    Public GLOSIPAGO As Integer = 0
    Public GloTipoUsuario As Integer
    Public glolec As Integer
    Public gloescr As Integer
    Public gloctr As Integer
    Public GloBonif As Integer = 0
    Public GloImprimeTickets As Boolean = False
    Public LocNomEmpresa As String = Nothing
    '--Variables Generales de Facturacion
    Public GloEmpresa As String = Nothing
    Public GloDireccionEmpresa As String = Nothing
    Public GloColonia_CpEmpresa As String = Nothing
    Public GloCiudadEmpresa As String = Nothing
    Public GloRfcEmpresa As String = Nothing
    Public GloTelefonoEmpresa As String = Nothing
    '--Variables para el tipo de Pago
    Public GLOCLV_NOTA As Long = 0
    Public GLONOTA As Double = 0
    Public GLOEFECTIVO As Double = 0
    Public GLOCHEQUE As Double = 0
    Public GLOCLV_BANCOCHEQUE As Integer = 0
    Public NUMEROCHEQUE As String = Nothing
    Public GLOTARJETA As Double = 0
    Public GLOCLV_BANCOTARJETA As Integer = 0
    Public NUMEROTARJETA As String = Nothing
    Public TARJETAAUTORIZACION As String = Nothing
    Public GLOTOTALEFECTIVO As Double = 0
    Public GLOCAMBIO As Double = 0
    Public Glocontratosel As Long = 0
    Public Glocontratosel2 As Long = 0
    Public GLOTRANSFERENCIA As Double = 0
    Public GLOBANCO As Integer = 0
    Public GLONUMEROTRANSFERENCIA As String = ""
    Public GLOAUTORIZACION As String = ""
    ' Fin varirables para el tipo de Pago

    Public Glo_Clv_SessionVer As Long = 0
    Public Glo_BndErrorVer As Integer = 0
    Public Glo_MsgVer As String = Nothing

    Public GloOpRepGral As String = Nothing
    Public LocEfectivo As Decimal = 0
    Public LocTarjeta As Decimal = 0
    Public LocCheque As Decimal = 0
    Public LocAuto As Decimal = 0
    Public LocParciales As Decimal = 0
    Public LocDesglose As Decimal = 0
    Public LocTotTransferencia As Decimal = 0
    Public LocTotTarjetaDebito As Decimal = 0
    Public LocTransferencia As Decimal = 0
    Public LocClv_session As Integer = 0
    Public Gloop As String = Nothing
    Public Glo_Apli_Pnt_Ade As Boolean = False

    Public GloClv_Periodo_Num As Integer = 1
    Public GloClv_Periodo_Txt As String = " Primer Periodo "
    Public GloActPeriodo As Integer = 0
    Public facnormal As Boolean
    Public facticket As Boolean
    Public impresorafiscal As String
    Public Locclv_sucursalglo As String = Nothing
    Public Tipo As String = Nothing
    Public LocSerieglo As String = Nothing
    Public LocFacturaGlo As Integer = 0
    Public LocFechaGlo As Date
    Public LocImporteGlo As Double = 0
    Public LocCajeroGlo As String = Nothing
    Public Locclv_empresa As String = Nothing

    ''VARIABLES PARA ESPECIFICACIONES---------
    Public ColorBut As Integer
    Public ColorLetraBut As Integer
    Public ColorMenu As Integer
    Public ColorMenuLetra As Integer
    Public ColorBwr As Integer
    Public ColorBwrLetra As Integer
    Public ColorGrid As Integer
    Public ColorForm As Integer
    Public ColorLetraForm As Integer
    Public ColorLabel As Integer
    Public ColorLetraLabel As Integer
    Public bytesImg() As Byte
    Public bytesImg2() As Byte
    Public num, num2 As Integer
    Public bfac As softvFacturacion.NewsoftvDataSet2.BusFacFiscalDataTable
    Public busfac As NewsoftvDataSet2TableAdapters.BusFacFiscalTableAdapter
    Public horaini As String
    Public horafin As String
    Public bndcontt As Boolean
    Public res As Integer
    Public LocImpresoraTickets As String = Nothing
    Public LocFecha1 As String = Nothing
    Public LocFecha2 As String = Nothing
    Public LocResumenBon As Boolean = False
    Public LocBndBon As Boolean = False
    Public locBndBon1 As Boolean = False
    Public LocSupBon As String = Nothing
    Public LocLoginUsuario As String = Nothing
    Public LocBanderaRep1 As Integer = 0
    Public LocBndrepfac1 As Boolean = False
    Public Locclv_usuario As String = Nothing
    Public LocNombreusuario As String = Nothing
    Public Factura_inicial As Integer
    Public Factura_final As Integer
    Public Unico As Boolean = True
    Public LocCiudades As String = Nothing
    Public gloMotivoCan As Integer
    Public gloClvNota As Integer
    Public GloPoliza2 As Long
    Public glotipoNota As Short
    Public glotipoFacGlo As Short
    Public SubCiudad As String
    Public bnd As Integer = 0
    Public LiTipo As Integer = 0
    Public GloSistema As String = "Facturaci�n"
    Public LiContrato As Long
    Public LocFechaGloFinal As String
    Public Glo_tipSer As Integer
    'variables de Datos
    Public locclv_id As String
    Public LocBdd As String
    Public LocClv_Ciudad As String
    Public LocNomciudad As String
    Public LIFACTURA As Long
    Public refrescar As Boolean = False
    Public status As String
    Public LocbndNotas As Boolean = False
    Public LocActiva As Boolean = False
    Public LocSaldada As Boolean = False
    Public LocCancelada As Boolean = False
    Public LocUsuariosNotas As String = Nothing
    Public LocClientesPagosAdelantados As Boolean = False
    'Proceso de Bancos
    Public BndPasaBancos As Boolean = False
    Public FechaPasaBancos As String = Nothing
    'Fin Proceso de Bancos
    'proceso de Oxxo
    Public GloClv_Recibo As Long = 0
    'Fin Proceso de Oxxo
    'Polizas
    Public Locbndactualizapoiza As Boolean = True
    Public LocopPoliza As String = "C"
    Public LocGloClv_poliza As Long = 0
    Public LocbndPolizaCiudad As Boolean = False
    'Polizas
    'Reporte_Desgloce_Moneda
    Public GloBnd_Des_Men As Boolean = False
    'Clave Session para Facturacion
    Public GloTelClv_Session As Long = 0
    Public GloTiene_Cancelaciones As Boolean = False

    '
    Public GloPagorecibido As Double = 0
    'Reporte_Desgloce_Moneda Contratacion
    Public GloBnd_Des_Cont As Boolean = False
    Public Locbndcortedet As Boolean = False

    Public Locbndrepnotas As Integer = 0
    Public locbndrepnotas2 As Boolean = False
    Public Locclv_sucursalnotas As Integer = 0
    Public Verifica As Integer
    Public BndAlerta As Boolean
    Public LocbndDesPagos As Boolean = False
    Public guardabitacora As softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
    Public guardabitacorabuena As softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter

    Public opcionCuenta As Integer = 1
    Public ContratoCAMDO As Integer = 0

    Public Sub bitsist(ByVal usuario As String, ByVal contrato As Long, ByVal sistema As String, ByVal pantalla As String, ByVal control As String, ByVal valorant As String, ByVal valornuevo As String, ByVal clv_ciudad As String)
        Dim COn85 As New SqlConnection(MiConexion)
        COn85.Open()
        guardabitacora = New softvFacturacion.DataSetLydia.Inserta_MovSistDataTable
        guardabitacorabuena = New softvFacturacion.DataSetLydiaTableAdapters.Inserta_MovSistTableAdapter
        COn85.Close()
        If valorant <> valornuevo Then
            COn85.Open()
            guardabitacorabuena.Connection = COn85
            guardabitacorabuena.Fill(guardabitacora, usuario, contrato, sistema, pantalla, control, valorant, valornuevo, clv_ciudad)
            COn85.Close()
        End If
    End Sub


    Public Sub RecorrerEstructuraMenu(ByVal oMenu As MenuStrip)
        Dim menu As ToolStripMenuItem
        For Each oOpcionMenu As ToolStripMenuItem In oMenu.Items
            menu = New ToolStripMenuItem
            menu = oOpcionMenu
            menu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
            menu = Nothing
            If oOpcionMenu.DropDownItems.Count > 0 Then
                RecorrerSubmenu(oOpcionMenu.DropDownItems, "----")
            End If
        Next
    End Sub

    Public Sub RecorrerSubmenu(ByVal oSubmenuItems As ToolStripItemCollection, ByVal sGuiones As String)
        Dim submenu As ToolStripItem
        For Each oSubitem As ToolStripItem In oSubmenuItems
            If oSubitem.GetType Is GetType(ToolStripMenuItem) Then
                submenu = New ToolStripMenuItem
                submenu = oSubitem
                submenu.ForeColor = System.Drawing.Color.FromArgb(ColorMenuLetra)
                submenu = Nothing
                If CType(oSubitem, ToolStripMenuItem).DropDownItems.Count > 0 Then
                    RecorrerSubmenu(CType(oSubitem, ToolStripMenuItem).DropDownItems, sGuiones & "----")
                End If
            End If
        Next
    End Sub
    Public Sub bwrpanel(ByVal panel2 As Panel)
        Dim data As DataGridView
        Dim label As Label
        Dim boton As Button
        Dim split As SplitContainer
        Dim panel3 As Panel
        Dim text As TextBox
        Dim GROUP As GroupBox
        Dim var As String
        If panel2.BackColor <> Color.WhiteSmoke Then
            panel2.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
            ' panel2.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
        End If
        For Each ctr As Control In panel2.Controls
            If ctr.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                split = New SplitContainer
                split = ctr
                bwrpanel(split.Panel1)
                bwrpanel(split.Panel2)
            End If
            If ctr.GetType Is GetType(CrystalDecisions.Windows.Forms.CrystalReportViewer) Then
                ctr.BackColor = Color.WhiteSmoke
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                data = New DataGridView
                data = ctr
                data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                data = Nothing
            End If
            var = Mid(ctr.Name, 1, 3)
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctr.BackColor <> Color.WhiteSmoke And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" And var <> "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Black
                label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                label = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                label = New Label
                label = ctr
                label.ForeColor = Color.Red
                label.BackColor = Color.Yellow
                label = Nothing
            End If

            If ctr.GetType Is GetType(System.Windows.Forms.TextBox) And var = "CMB" Then
                text = New TextBox
                text = ctr
                text.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                text.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                text = Nothing
            End If


            If ctr.GetType Is GetType(System.Windows.Forms.Button) Then
                boton = New Button
                boton = ctr
                boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                boton = Nothing
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                panel3 = New Panel
                panel3 = ctr
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                GROUP = New GroupBox
                GROUP = ctr
                GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                bwrgroup(GROUP)
            End If
            If ctr.GetType Is GetType(System.Windows.Forms.Panel) And var <> "CMB" Then
                bwrpanel(ctr)
            End If
        Next
    End Sub
    Public Sub bwrgroup(ByVal grup As GroupBox)
        Dim panel3 As Panel
        Dim label As Label
        Dim var As String
        For Each ctm As Control In grup.Controls
            var = Mid(ctm.Name, 1, 3)
            If ctm.GetType Is GetType(System.Windows.Forms.Panel) And ctm.BackColor <> Color.WhiteSmoke Then
                panel3 = New Panel
                panel3 = ctm
                panel3.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                bwrpanel(panel3)
            End If
            If ctm.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" And ctm.BackColor <> Color.WhiteSmoke Then
                label = New Label
                label = ctm
                label.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                label.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                label = Nothing
            End If
        Next
    End Sub

    Public Sub colorea(ByVal formulario As Form)
        Dim boton As Button
        Dim panel As Panel
        Dim label As Label
        Dim split As SplitContainer
        Dim var As String
        Dim data As DataGridView
        Dim Menupal As MenuStrip
        Dim GROUP As GroupBox

        For Each ctl As Control In formulario.Controls
            var = Mid(ctl.Name, 1, 3)

            If var <> "CNO" Then

                If ctl.GetType Is GetType(System.Windows.Forms.Button) Then
                    boton = New Button
                    boton = ctl
                    boton.BackColor = System.Drawing.Color.FromArgb(ColorBut)
                    boton.ForeColor = System.Drawing.Color.FromArgb(ColorLetraBut)
                    boton = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.MenuStrip) Then
                    Menupal = New MenuStrip
                    Menupal = ctl
                    Menupal.BackColor = System.Drawing.Color.FromArgb(ColorMenu)
                    RecorrerEstructuraMenu(Menupal)
                    Menupal = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    split = New SplitContainer
                    split = ctl
                    bwrpanel(split.Panel1)
                    bwrpanel(split.Panel2)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor <> Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    panel.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    bwrpanel(panel)
                    panel = Nothing

                ElseIf ctl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    GROUP = New GroupBox
                    GROUP = ctl
                    GROUP.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    bwrgroup(GROUP)
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And ctl.BackColor = Color.WhiteSmoke Then
                    panel = New Panel
                    panel = ctl
                    bwrpanel(panel)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Panel) And var = "CMB" Then
                    panel = New Panel
                    panel = ctl
                    panel.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    panel = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var <> "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = System.Drawing.Color.FromArgb(ColorLetraLabel)
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "RED" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Red
                    label.BackColor = Color.WhiteSmoke
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.Label) And var = "CMB" Then
                    label = New Label
                    label = ctl
                    label.ForeColor = Color.Black
                    label.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                    label = Nothing
                ElseIf ctl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    data = New DataGridView
                    data = ctl
                    data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorBwr)
                    data.ColumnHeadersDefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(ColorBwrLetra)
                    If ctl.Name = "SumaDetalleDataGridView" Then
                        data.ColumnHeadersDefaultCellStyle.BackColor = System.Drawing.Color.FromArgb(ColorForm)
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorForm)
                    Else
                        data.BackgroundColor = System.Drawing.Color.FromArgb(ColorGrid)
                    End If
                    data = Nothing
                End If
            End If
        Next
        formulario.BackColor = System.Drawing.Color.FromArgb(ColorForm)
        formulario.ForeColor = System.Drawing.Color.FromArgb(ColorLetraForm)
    End Sub

    Public Function ValidaKey(ByVal ctl As Object, ByRef nChar As Integer, ByVal Tipo As String) As Integer
        'Solo Enteros
        If Tipo = "N" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'Con Enteros y Decimales
        ElseIf Tipo = "L" Then
            If (nChar >= 48 And nChar <= 57) Or nChar = 8 Or nChar = 13 Or nChar = 46 Then
                ValidaKey = nChar
            Else
                ValidaKey = 0
            End If
            'String
        ElseIf Tipo = "S" Then
            nChar = Asc(LCase(Chr(nChar)))
            If ctl.SelectionStart = 0 Then
                ' This is the first character, so change to uppercase.
                nChar = Asc(UCase(Chr(nChar)))
            Else
                ' If the previous character is a space, capitalize
                ' the current character.
                'If Mid(ctl, ctl.SelectionStart, 1) = Space(1) Then
                'MsgBox(Mid(ctl.ToString, ctl.SelectionStart, 1))
                'MsgBox(Mid(ctl.text, ctl.SelectionStart, 1))
                If Mid(ctl.text, ctl.SelectionStart, 1) = Space(1) Then
                    nChar = Asc(UCase(Chr(nChar)))
                End If
            End If
            ValidaKey = nChar
        ElseIf Tipo = "M" Then
            nChar = Asc(UCase(Chr(nChar)))
            ValidaKey = nChar
            'ElseIf Tipo = "D" Then
            ' If KeyAscii <> 64 Then
            '     a = Right(Txt, 1)
            '     L = Len(Txt)
            '     If a = " " Or L = 0 Or a = "." Then
            '         ValidaKey = (Asc(UCase(Chr(KeyAscii))))
            '     Else
            '         ValidaKey = (Asc(LCase(Chr(KeyAscii))))
            '     End If
            '  End If
        End If
    End Function

    Function DesEncriptaME(ByVal Pass As String) As String
        Dim Clave As String, i As Integer, Pass2 As String
        Dim CAR As String, Codigo As String
        Dim j As Integer

        Clave = "%��T@#$A_"
        Pass2 = ""
        j = 1
        For i = 1 To Len(Pass) Step 2
            CAR = Mid(Pass, i, 2)
            Codigo = Mid(Clave, ((j - 1) Mod Len(Clave)) + 1, 1)
            Pass2 = Pass2 & Chr(Asc(Codigo) Xor Val("&h" + CAR))
            j = j + 1
        Next i
        DesEncriptaME = Pass2
    End Function


    Function Rellena_Text(ByVal Valor As String, ByVal Longitud As Integer, ByVal Relleno As String) As String
        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Contador = Len(Valor)
        Rellena_Text = ""
        Total = Longitud - Contador
        Dim i As Integer
        i = 1
        For i = 1 To Total
            Rellena_Text = Rellena_Text + Relleno
            'i = i + 1
        Next i
        Rellena_Text = Rellena_Text + Valor
    End Function


    Function ChecaSiEsFacturaFiscal(ByVal Tipo As String, ByVal Clv_Factura As Long) As Boolean
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaSiEsFacturaFiscal", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par0 As New SqlParameter("@Tipo", SqlDbType.VarChar)
        par0.Direction = ParameterDirection.Input
        par0.Value = Tipo
        comando.Parameters.Add(par0)

        Dim par1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Factura
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Bnd", SqlDbType.Bit)
        par2.Direction = ParameterDirection.Output
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox("ChecaSiEsFacturaFiscal " + ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Close()
        End Try

        Return Boolean.Parse(par2.Value.ToString())

    End Function

    Public Sub GeneraFacturaCFD(ByVal Tipo As String, ByVal Clv_Factura As Long, ByVal Serie As String, ByVal Folio As Long, ByVal Contrato As Long, ByVal ProcedeDe As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()

        Dim dataSet As New DataSet
        Dim generador As New Generador
        Dim cadena As StringBuilder
        Dim arreglo() As Byte
        Dim encoding As New System.Text.UTF8Encoding
        Dim notaCredito As String = ""

        If Tipo = "N" Then strSQL.Append("EXEC GeneraFacturaCFD " + Clv_Factura.ToString())
        If Tipo = "G" Then strSQL.Append("EXEC GeneraFacturaCFDGlobal " + Clv_Factura.ToString())
        If Tipo = "C" Then strSQL.Append("EXEC GeneraFacturaCFDCredito " + Clv_Factura.ToString())



        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try

            dataAdapter.Fill(dataSet)
            dataSet.Tables(0).TableName = "Comprobante"
            dataSet.Tables(1).TableName = "Emisor"
            dataSet.Tables(2).TableName = "DomicilioFiscal"
            dataSet.Tables(3).TableName = "ExpedidoEn"
            dataSet.Tables(4).TableName = "Receptor"
            dataSet.Tables(5).TableName = "Cliente"
            dataSet.Tables(6).TableName = "Concepto"
            dataSet.Tables(7).TableName = "Otros"
            dataSet.Tables(8).TableName = "addenda"

            cadena = generador.GeneraCFD(dataSet)
            arreglo = encoding.GetBytes(cadena.ToString())

            If Tipo = "C" Then notaCredito = "nc"

            Dim fS As New FileStream(eRutaCFD + notaCredito + Serie + Folio.ToString() + ".txt", FileMode.Create)
            fS.Write(arreglo, 0, arreglo.Length)
            fS.Flush()
            fS.Close()
            fS.Dispose()

            Rename(eRutaCFD + notaCredito + Serie + Folio.ToString() + ".txt", eRutaCFD + notaCredito + Serie + Folio.ToString() + ".ff")

            NueComprobantesCFD(Clv_Factura, Tipo, Serie, Folio, dataSet.Tables("Comprobante").Rows(0)("serie").ToString(), dataSet.Tables("Comprobante").Rows(0)("folio").ToString(), arreglo)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Public Sub CancelaFacturaCFD(ByVal Tipo As String, ByVal Clv_Factura As Long, ByVal Serie As String, ByVal Folio As Long, ByVal Contrato As Long, ByVal ProcedeDe As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()

        Dim dataSet As New DataSet
        Dim generador As New Generador
        Dim cadena As StringBuilder
        Dim arreglo() As Byte
        Dim encoding As New System.Text.UTF8Encoding
        Dim notaCredito As String = ""

        If Tipo = "N" Then strSQL.Append("EXEC CancelaFacturaCFD " + Clv_Factura.ToString())
        If Tipo = "G" Then strSQL.Append("EXEC CancelaFacturaCFDGlobal " + Clv_Factura.ToString())
        If Tipo = "C" Then strSQL.Append("EXEC CancelaFacturaCFDCredito " + Clv_Factura.ToString())

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)

        Try

            dataAdapter.Fill(dataSet)
            dataSet.Tables(0).TableName = "Factura"

            cadena = generador.CancelaCFD(dataSet)
            arreglo = encoding.GetBytes(cadena.ToString())

            If Tipo = "C" Then notaCredito = "nc"

            Dim fS As New FileStream(eRutaCFD + notaCredito + Serie + Folio.ToString() + ".txc", FileMode.Create)
            fS.Write(arreglo, 0, arreglo.Length)
            fS.Flush()
            fS.Close()
            fS.Dispose()

            Rename(eRutaCFD + notaCredito + Serie + Folio.ToString() + ".txc", eRutaCFD + notaCredito + Serie + Folio.ToString() + ".ffc")

            ModComprobantesCFD(Clv_Factura, Tipo)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Public Sub DameSerieFolio(ByVal Op As Integer, ByVal Clv_Factura As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("DameSerieFolio", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0
        Dim reader As SqlDataReader

        Dim par1 As New SqlParameter("@Op", SqlDbType.Int)
        par1.Direction = ParameterDirection.Input
        par1.Value = Op
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par2.Direction = ParameterDirection.Input
        par2.Value = Clv_Factura
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            reader = comando.ExecuteReader

            While (reader.Read())
                eSerie = reader(0).ToString()
                eFolio = Integer.Parse(reader(1).ToString())
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub NueComprobantesCFD(ByVal Clv_Factura As Long, ByVal Tipo As String, ByVal Serie As String, ByVal Folio As Integer, ByVal SerieSAT As String, ByVal FolioSAT As Integer, ByVal Archivo() As Byte)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("NueComprobantesCFD", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Factura
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        par2.Direction = ParameterDirection.Input
        par2.Value = Tipo
        comando.Parameters.Add(par2)

        Dim par3 As New SqlParameter("@Serie", SqlDbType.VarChar, 50)
        par3.Direction = ParameterDirection.Input
        par3.Value = Serie
        comando.Parameters.Add(par3)

        Dim par4 As New SqlParameter("@Folio", SqlDbType.Int)
        par4.Direction = ParameterDirection.Input
        par4.Value = Folio
        comando.Parameters.Add(par4)

        Dim par5 As New SqlParameter("@SerieSAT", SqlDbType.VarChar, 50)
        par5.Direction = ParameterDirection.Input
        par5.Value = SerieSAT
        comando.Parameters.Add(par5)

        Dim par6 As New SqlParameter("@FolioSAT", SqlDbType.Int)
        par6.Direction = ParameterDirection.Input
        par6.Value = FolioSAT
        comando.Parameters.Add(par6)

        Dim par7 As New SqlParameter("@Archivo", SqlDbType.VarBinary, 8000)
        par7.Direction = ParameterDirection.Input
        par7.Value = Archivo
        comando.Parameters.Add(par7)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub

    Private Sub ModComprobantesCFD(ByVal Clv_Factura As Long, ByVal Tipo As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ModComprobantesCFD", conexion)
        comando.CommandType = CommandType.StoredProcedure
        comando.CommandTimeout = 0

        Dim par1 As New SqlParameter("@Clv_Factura", SqlDbType.BigInt)
        par1.Direction = ParameterDirection.Input
        par1.Value = Clv_Factura
        comando.Parameters.Add(par1)

        Dim par2 As New SqlParameter("@Tipo", SqlDbType.VarChar, 1)
        par2.Direction = ParameterDirection.Input
        par2.Value = Tipo
        comando.Parameters.Add(par2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub


    Public Sub SetDBReport(ByVal ds As DataSet, ByVal myReportDocument As ReportDocument)
        Try
            Dim myTables As Tables = myReportDocument.Database.Tables
            Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
            For Each myTable In myTables
                myTable.SetDataSource(ds)
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Public Function GeneraCodeBar128(ByVal CodigoOxxo As [String]) As [Byte]()
        Dim barcode As New PictureBox()
        Dim w As Int32 = Convert.ToInt32(200)
        Dim h As Int32 = Convert.ToInt32(75)
        Dim imgCodigoOxxo As [Byte]()
        Dim imgBites As [Byte]()
        Dim b As New BarcodeLib.Barcode()

        Dim aling As BarcodeLib.AlignmentPositions = BarcodeLib.AlignmentPositions.CENTER
        aling = BarcodeLib.AlignmentPositions.CENTER

        Dim type As BarcodeLib.TYPE = BarcodeLib.TYPE.UNSPECIFIED
        type = BarcodeLib.TYPE.CODE128

        If type <> BarcodeLib.TYPE.UNSPECIFIED Then
            b.IncludeLabel = True
            b.Alignment = aling
            b.RotateFlipType = RotateFlipType.RotateNoneFlipNone
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER
            barcode.Image = b.Encode(type, CodigoOxxo, Color.Black, Color.White, w, h)
        End If

        barcode.Width = barcode.Image.Width
        barcode.Height = barcode.Image.Height

        imgBites = ImageToByte2(barcode.Image)
        imgCodigoOxxo = imgBites

        Return imgCodigoOxxo
    End Function

    Public Function ImageToByte2(ByVal img As Image) As [Byte]()
        Dim byteArray As [Byte]()
        Using mStream As New MemoryStream()
            img.Save(mStream, System.Drawing.Imaging.ImageFormat.Jpeg)
            mStream.Close()
            byteArray = mStream.ToArray()
        End Using
        Return byteArray
    End Function

    Function Rellena_Text2(ByVal Valor As String, ByVal Longitud As Integer, ByVal Relleno As String) As String
        Dim Contador As Integer = 0
        Dim Total As Integer = 0
        Contador = Len(Valor)
        Rellena_Text2 = ""
        Total = Longitud - Contador
        Dim i As Integer
        i = 1
        For i = 1 To Total
            Rellena_Text2 = Rellena_Text2 + Relleno
            'i = i + 1
        Next i
        Rellena_Text2 = Valor + Rellena_Text2
    End Function

    Public Function UspHaz_Pregunta(ByVal oContrato As Long, ByVal oOp As Integer) As Integer
        UspHaz_Pregunta = 0
        Dim CON80 As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand()
        Try
            CMD = New SqlCommand()
            CON80.Open()
            With CMD
                .CommandText = "UspHaz_Pregunta"
                .CommandType = CommandType.StoredProcedure
                .Connection = CON80
                .CommandTimeout = 0

                Dim prm As New SqlParameter("@Contrato", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Input
                prm.Value = oContrato
                .Parameters.Add(prm)

                Dim prm1 As New SqlParameter("@Op", SqlDbType.Int)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = oOp
                .Parameters.Add(prm1)

                Dim prm2 As New SqlParameter("@Pregunta", SqlDbType.VarChar, 800)
                prm2.Direction = ParameterDirection.Output
                prm2.Value = ""
                .Parameters.Add(prm2)

                Dim prm3 As New SqlParameter("@OpBtn", SqlDbType.Int)
                prm3.Direction = ParameterDirection.Output
                prm3.Value = ""
                .Parameters.Add(prm3)

                Dim i As Integer = .ExecuteNonQuery()
                MsjUspHaz_Pregunta = prm2.Value
                UspHaz_Pregunta = prm3.Value
            End With

        Catch ex As Exception

        Finally
            CON80.Close()
        End Try
    End Function

End Module

