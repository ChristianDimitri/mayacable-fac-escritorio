Imports System.Data.SqlClient

Public Class FrmDESGLOSEMONEDA
    Private eRes As Integer = 0
    Private eMsg As String = String.Empty
    Private eTipo As String = String.Empty
    Dim BND1 As Boolean = False

    Private Sub FrmDESGLOSEMONEDA_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        'Me.RecibioTextBox.Enabled = False
        If op = "N" Then
            Me.CONDESGLOSEMONEDABindingSource1.AddNew()
            Me.BindingNavigatorDeleteItem.Enabled = False
            Me.Label3.Text = "Nuevo Desglose de Moneda"
            Me.TextBox1.Text = GloUsuario
            ' Me.MUESTRAUSUARIOS2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRAUSUARIOS2, 1)
        ElseIf op = "C" Then
            Me.TextBox1.Text = GloUsuario
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.CONDESGLOSEMONEDATableAdapter.Connection = CON
            Me.CONDESGLOSEMONEDATableAdapter.Fill(Me.NewsoftvDataSet1.CONDESGLOSEMONEDA, Consecutivo)
            Me.Consulta_Desglose_DolarTableAdapter.Connection = CON
            Me.Consulta_Desglose_DolarTableAdapter.Fill(Me.Procedimientos_arnoldo.Consulta_Desglose_Dolar, Consecutivo)
            CON.Close()

            CONDESGLOSEMONEDA(Consecutivo)
            Me.BindingNavigatorDeleteItem.Enabled = True
            Me.Panel1.Enabled = False
            Me.Label3.Text = "Consulta Desglose Moneda"
            Me.B1000TextBox.TabStop = False
            Me.B100TextBox.TabStop = False
            Me.B200TextBox.TabStop = False
            Me.B20TextBox.TabStop = False
            Me.B500TextBox.TabStop = False
            Me.B50TextBox.TabStop = False
            Me.ChequesTextBox.TabStop = False
            Me.TarjetaTextBox.TabStop = False
            Me.TarjetaDebitoTextBox.TabStop = False
            Me.M005TextBox.TabStop = False
            Me.M010TextBox.TabStop = False
            Me.M020TextBox.TabStop = False
            Me.M050TextBox.TabStop = False
            Me.M100TextBox.TabStop = False
            Me.M10TextBox.TabStop = False
            Me.M1TextBox.TabStop = False
            Me.M20TextBox.TabStop = False
            Me.M2TextBox.TabStop = False
            Me.M5TextBox.TabStop = False
            Me.CONDESGLOSEMONEDABindingNavigator.TabStop = False

        ElseIf op = "M" Then
            Me.TextBox1.Text = GloUsuario
            Dim CON2 As New SqlConnection(MiConexion)
            CON2.Open()
            Me.CONDESGLOSEMONEDATableAdapter.Connection = CON2
            Me.CONDESGLOSEMONEDATableAdapter.Fill(Me.NewsoftvDataSet1.CONDESGLOSEMONEDA, Consecutivo)
            Me.Consulta_Desglose_DolarTableAdapter.Connection = CON2
            Me.Consulta_Desglose_DolarTableAdapter.Fill(Me.Procedimientos_arnoldo.Consulta_Desglose_Dolar, Consecutivo)
            Me.BindingNavigatorDeleteItem.Enabled = True
            Me.Label3.Text = "Modificaciones De Un Desglose Moneda"
            Me.MUESTRAUSUARIOS2TableAdapter.Connection = CON2
            Me.MUESTRAUSUARIOS2TableAdapter.Fill(Me.NewsoftvDataSet1.MUESTRAUSUARIOS2, 1)
            CON2.Close()
            CONDESGLOSEMONEDA(Consecutivo)
        End If

        If IdSistema = "LO" Then
            Me.LabelTipo.Visible = True
            If op = "M" Or op = "C" Then
                If Me.ReferenciaTextBox.Text.Length > 0 Then
                    Me.TextBoxTipo.Text = Me.ReferenciaTextBox.Text
                    Me.TextBoxTipo.Visible = True
                End If
            Else
                ChecaDesgloseMoneda(GloUsuario)
            End If
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
    Public Sub Suma_Detalle2()
        If IsNumeric(Me.TextBox3.Text) = True Or IsNumeric(Me.TextBox5.Text) = True Then
            If IsNumeric(Me.TextBox3.Text) = True And IsNumeric(Me.TextBox5.Text) = True Then
                Me.ImporteTextBox.Text = CDbl(Me.TextBox5.Text) + CDbl(Me.TextBox3.Text)
            ElseIf IsNumeric(Me.TextBox3.Text) = True Then
                Me.ImporteTextBox.Text = CDbl(Me.TextBox3.Text)
            ElseIf IsNumeric(Me.TextBox5.Text) = True Then
                Me.ImporteTextBox.Text = CDbl(Me.TextBox5.Text)
            End If
        End If
    End Sub

    Private Sub Suma_Detalle()
        Dim b1000 As Double = 0
        Dim b500 As Double = 0
        Dim b200 As Double = 0
        Dim b100 As Double = 0
        Dim b20 As Double = 0
        Dim b50 As Double = 0
        Dim M100 As Double = 0
        Dim M50 As Double = 0
        Dim M20 As Double = 0
        Dim M10 As Double = 0
        Dim M5 As Double = 0
        Dim M2 As Double = 0
        Dim M1 As Double = 0
        Dim M050 As Double = 0
        Dim M020 As Double = 0
        Dim M010 As Double = 0
        Dim M005 As Double = 0
        Dim Tarjeta As Double = 0
        Dim Cheque As Double = 0
        Dim Transferencia As Double = 0
        Dim TarjetaDebito As Double = 0

        Try

            If IsNumeric(Me.B1000TextBox.Text) = True Then
                b1000 = Me.B1000TextBox.Text * 1000
            End If
            If IsNumeric(Me.B500TextBox.Text) = True Then
                b500 = Me.B500TextBox.Text * 500
            End If
            If IsNumeric(Me.B200TextBox.Text) = True Then
                b200 = Me.B200TextBox.Text * 200
            End If
            If IsNumeric(Me.B100TextBox.Text) = True Then
                b100 = Me.B100TextBox.Text * 100
            End If
            If IsNumeric(Me.B20TextBox.Text) = True Then
                b20 = Me.B20TextBox.Text * 20
            End If
            If IsNumeric(Me.B50TextBox.Text) = True Then
                b50 = Me.B50TextBox.Text * 50
            End If
            If IsNumeric(Me.M100TextBox.Text) = True Then
                M100 = Me.M100TextBox.Text * 100
            End If
            If IsNumeric(Me.M50TextBox.Text) = True Then
                M50 = Me.M50TextBox.Text * 50
            End If
            If IsNumeric(Me.M20TextBox.Text) = True Then
                M20 = Me.M20TextBox.Text * 20
            End If
            If IsNumeric(Me.M10TextBox.Text) = True Then
                M10 = Me.M10TextBox.Text * 10
            End If
            If IsNumeric(Me.M5TextBox.Text) = True Then
                M5 = Me.M5TextBox.Text * 5
            End If
            If IsNumeric(Me.M2TextBox.Text) = True Then
                M2 = Me.M2TextBox.Text * 2
            End If
            If IsNumeric(Me.M1TextBox.Text) = True Then
                M1 = Me.M1TextBox.Text * 1
            End If
            If IsNumeric(Me.M050TextBox.Text) = True Then
                M050 = Me.M050TextBox.Text * 0.5
            End If
            If IsNumeric(Me.M020TextBox.Text) = True Then
                M020 = Me.M020TextBox.Text * 0.2
            End If
            If IsNumeric(Me.M010TextBox.Text) = True Then
                M010 = Me.M010TextBox.Text * 0.1
            End If
            If IsNumeric(Me.M005TextBox.Text) = True Then
                M005 = Me.M005TextBox.Text * 0.05
            End If
            If IsNumeric(Me.ChequesTextBox.Text) = True Then
                Cheque = Me.ChequesTextBox.Text
            End If
            If IsNumeric(Me.TarjetaTextBox.Text) = True Then
                Tarjeta = Me.TarjetaTextBox.Text    ''tarjeta de credito
            End If
            If IsNumeric(Me.TransTextbox.Text) = True Then
                Transferencia = Me.TransTextbox.Text
            End If
            If IsNumeric(Me.TarjetaDebitoTextBox.Text) = True Then
                TarjetaDebito = Me.TarjetaDebitoTextBox.Text    ''tarjeta de debito
            End If

            Suma = b1000 + b500 + b200 + b100 + b50 + b20 + M100 + M50 + M20 + M10 + M5 + M2 + M1 + M050 + M020 + M010 + M005 + Tarjeta + Cheque + Transferencia + TarjetaDebito
            Me.ImporteTextBox.Text = Suma
            Me.TextBox5.Text = Suma
            SUMATOTALBILLETES()
            SUMATOTALMONEDAS()
        Catch
            MsgBox("Problema de Desbordamiento, Demaciado Dinero", , "Advertencia")

        End Try


    End Sub

    Private Sub B1000TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B1000TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B1000TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub



    Private Sub B1000TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B1000TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B1000TextBox.Text) = True Then Me.TextBoxB1000.Text = B1000TextBox.Text * 1000 Else Me.TextBoxB1000.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub B500TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B500TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B500TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B500TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B500TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B500TextBox.Text) = True Then Me.TextBoxB500.Text = B500TextBox.Text * 500 Else Me.TextBoxB500.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub B200TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B200TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B200TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B200TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B200TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B200TextBox.Text) = True Then Me.TextBoxB200.Text = B200TextBox.Text * 200 Else Me.TextBoxB200.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub B100TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B100TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B100TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B100TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B100TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B100TextBox.Text) = True Then Me.TextBoxB100.Text = B100TextBox.Text * 100 Else Me.TextBoxB100.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub B50TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B50TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B50TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B50TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B50TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B50TextBox.Text) = True Then Me.TextBoxB50.Text = B50TextBox.Text * 50 Else Me.TextBoxB50.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub B20TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles B20TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub B20TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B20TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(B20TextBox.Text) = True Then Me.TextBoxB20.Text = B20TextBox.Text * 20 Else Me.TextBoxB20.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M100TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M100TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M100TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M100TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M100TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M100TextBox.Text) = True Then Me.TextBoxM100.Text = M100TextBox.Text * 100 Else Me.TextBoxM100.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M50TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M50TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B50TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M50TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M50TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M50TextBox.Text) = True Then Me.TextBoxM50.Text = M50TextBox.Text * 50 Else Me.TextBoxM50.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M20TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M20TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.B20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M20TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M20TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M20TextBox.Text) = True Then Me.TextBoxM20.Text = M20TextBox.Text * 20 Else Me.TextBoxM20.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M10TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M10TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M10TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M10TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M10TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M10TextBox.Text) = True Then Me.TextBoxM10.Text = M10TextBox.Text * 10 Else Me.TextBoxM10.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M5TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M5TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M5TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M5TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M5TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M5TextBox.Text) = True Then Me.TextBoxM5.Text = M5TextBox.Text * 5 Else Me.TextBoxM5.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M2TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M2TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M2TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M2TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M2TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M2TextBox.Text) = True Then Me.TextBoxM2.Text = M2TextBox.Text * 2 Else Me.TextBoxM2.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M1TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M1TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M1TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M1TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M1TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M1TextBox.Text) = True Then Me.TextBoxM1.Text = M1TextBox.Text * 1 Else Me.TextBoxM1.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M050TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M050TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M050TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M050TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M050TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M050TextBox.Text) = True Then Me.TextBoxM050.Text = M050TextBox.Text * 0.5 Else Me.TextBoxM050.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M020TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M020TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M20TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub



    Private Sub M020TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M020TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M020TextBox.Text) = True Then Me.TextBoxM020.Text = M020TextBox.Text * 0.2 Else Me.TextBoxM020.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M010TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M010TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M010TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M010TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M010TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M010TextBox.Text) = True Then Me.TextBoxM010.Text = M010TextBox.Text * 0.1 Else Me.TextBoxM010.Text = 0
            Suma_Detalle()
        End If
    End Sub

    Private Sub M005TextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles M005TextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.M005TextBox, Asc(LCase(e.KeyChar)), "N")))
    End Sub

    Private Sub M005TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M005TextBox.TextChanged
        If BND1 = False Then
            If IsNumeric(M005TextBox.Text) = True Then Me.TextBoxM005.Text = M005TextBox.Text * 0.005 Else Me.TextBoxM005.Text = 0
            Suma_Detalle()
        End If
    End Sub


    Private Sub ChequesTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles ChequesTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.ChequesTextBox, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub ChequesTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ChequesTextBox.TextChanged
        If BND1 = False Then
            Suma_Detalle()
        End If

    End Sub

    Private Sub TarjetaTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TarjetaTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TarjetaTextBox, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub TarjetaTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarjetaTextBox.TextChanged
        If BND1 = False Then
            Suma_Detalle()
        End If
    End Sub

    Private Sub TarjetaDebitoTextBox_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TarjetaDebitoTextBox.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TarjetaDebitoTextBox, Asc(LCase(e.KeyChar)), "M")))
    End Sub

    Private Sub TarjetaDebitoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TarjetaDebitoTextBox.TextChanged
        If BND1 = False Then
            Suma_Detalle()
        End If
    End Sub

    Private Sub BindingNavigatorDeleteItem_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BindingNavigatorDeleteItem.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.CONDESGLOSEMONEDATableAdapter.Connection = CON
        Me.CONDESGLOSEMONEDATableAdapter.Delete(Consecutivo)
        Me.CONDESGLOSEMONEDATableAdapter.Connection = CON
        Me.CONDESGLOSEMONEDATableAdapter.Update(Me.NewsoftvDataSet1.CONDESGLOSEMONEDA)
        CON.Close()
        GloBnd = True
        bitsist(GloUsuario, 0, "Facturacion ", Me.Name, "", "Se Borro Desglose de Moneda ", "A la cajera:" + Me.TextBox1.Text + " Con un Importe de: " + Me.ImporteTextBox.Text, LocClv_Ciudad)
        MsgBox("Los Datos fueron borrados Satisfactoriamente", MsgBoxStyle.Information, "Borrar")
        Me.Close()
    End Sub


    Private Sub CONDESGLOSEMONEDABindingNavigatorSaveItem_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CONDESGLOSEMONEDABindingNavigatorSaveItem.Click
        Try
            If Me.TextBox1.Text = "" Then
                MsgBox("Selecciona Primero un(a) Cajero(a)", MsgBoxStyle.Information, "Alerta")
                Exit Sub
            End If
            If Me.ReferenciaTextBox.Text.Length = 0 And IdSistema = "LO" Then
                MsgBox("Selecciona un tipo de Desglose.", MsgBoxStyle.Exclamation)
                Exit Sub
            End If
            BND1 = True

            If IsNumeric(Me.B1000TextBox.Text) = False Then
                Me.B1000TextBox.Text = 0
            End If
            If IsNumeric(Me.B500TextBox.Text) = False Then
                Me.B500TextBox.Text = 0
            End If
            If IsNumeric(Me.B200TextBox.Text) = False Then
                Me.B200TextBox.Text = 0
            End If
            If IsNumeric(Me.B100TextBox.Text) = False Then
                Me.B100TextBox.Text = 0
            End If
            If IsNumeric(Me.B20TextBox.Text) = False Then
                Me.B20TextBox.Text = 0
            End If
            If IsNumeric(Me.B50TextBox.Text) = False Then
                Me.B50TextBox.Text = 0
            End If
            If IsNumeric(Me.M100TextBox.Text) = False Then
                Me.M100TextBox.Text = 0
            End If
            If IsNumeric(Me.M50TextBox.Text) = False Then
                Me.M50TextBox.Text = 0
            End If
            If IsNumeric(Me.M20TextBox.Text) = False Then
                Me.M20TextBox.Text = 0
            End If
            If IsNumeric(Me.M10TextBox.Text) = False Then
                Me.M10TextBox.Text = 0
            End If
            If IsNumeric(Me.M5TextBox.Text) = False Then
                Me.M5TextBox.Text = 0
            End If
            If IsNumeric(Me.M2TextBox.Text) = False Then
                Me.M2TextBox.Text = 0
            End If
            If IsNumeric(Me.M1TextBox.Text) = False Then
                Me.M1TextBox.Text = 0
            End If
            If IsNumeric(Me.M050TextBox.Text) = False Then
                Me.M050TextBox.Text = 0
            End If
            If IsNumeric(Me.M020TextBox.Text) = False Then
                Me.M020TextBox.Text = 0
            End If
            If IsNumeric(Me.M010TextBox.Text) = False Then
                Me.M010TextBox.Text = 0
            End If
            If IsNumeric(Me.M005TextBox.Text) = False Then
                Me.M005TextBox.Text = 0
            End If
            If IsNumeric(Me.ChequesTextBox.Text) = False Then
                Me.ChequesTextBox.Text = 0
            End If
            If IsNumeric(Me.TarjetaTextBox.Text) = False Then
                Me.TarjetaTextBox.Text = 0
            End If
            If IsNumeric(Me.TarjetaDebitoTextBox.Text) = False Then
                Me.TarjetaDebitoTextBox.Text = 0
            End If
            If IsNumeric(Me.TransTextbox.Text) = False Then
                Me.TransTextbox.Text = 0
            End If
            If IsNumeric(Me.TextBox4.Text) = False Then
                Me.TextBox4.Text = 0
            End If
            If IsNumeric(Me.TextBox2.Text) = False Then
                Me.TextBox2.Text = 0
            End If
            If IsNumeric(Me.TextBox3.Text) = False Then
                Me.TextBox3.Text = 0
            End If

            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            Me.Validate()

            'Me.CONDESGLOSEMONEDABindingSource1.EndEdit()
            'Me.CONDESGLOSEMONEDATableAdapter.Connection = CON
            'Me.CONDESGLOSEMONEDATableAdapter.Update(Me.NewsoftvDataSet1.CONDESGLOSEMONEDA)
            'GloConsecutivo = CInt(Me.ConsecutivoTextBox.Text)
            If op = "N" Then
                NUEVODESGLOSEMONEDA(Me.FechaDateTimePicker.Value, Me.TextBox1.Text, Me.ImporteTextBox.Text, Me.B1000TextBox.Text, Me.B500TextBox.Text, _
                              Me.B200TextBox.Text, Me.B100TextBox.Text, Me.B50TextBox.Text, Me.B20TextBox.Text, Me.M100TextBox.Text, _
                              Me.M50TextBox.Text, Me.M20TextBox.Text, Me.M10TextBox.Text, Me.M5TextBox.Text, Me.M2TextBox.Text, M1TextBox.Text, _
                              Me.M050TextBox.Text, Me.M020TextBox.Text, Me.M010TextBox.Text, Me.M005TextBox.Text, Me.ChequesTextBox.Text, _
                              Me.TarjetaTextBox.Text, String.Empty, Me.TransTextbox.Text, Me.TarjetaDebitoTextBox.Text)
                Me.ConsecutivoTextBox.Text = GloConsecutivo
                Consecutivo = GloConsecutivo
            ElseIf op = "M" Then
                MODIFICADESGLOSEMONEDA(Me.FechaDateTimePicker.Value, Me.TextBox1.Text, Me.ImporteTextBox.Text, Me.B1000TextBox.Text, Me.B500TextBox.Text, _
                              Me.B200TextBox.Text, Me.B100TextBox.Text, Me.B50TextBox.Text, Me.B20TextBox.Text, Me.M100TextBox.Text, _
                              Me.M50TextBox.Text, Me.M20TextBox.Text, Me.M10TextBox.Text, Me.M5TextBox.Text, Me.M2TextBox.Text, M1TextBox.Text, _
                              Me.M050TextBox.Text, Me.M020TextBox.Text, Me.M010TextBox.Text, Me.M005TextBox.Text, Me.ChequesTextBox.Text, _
                              Me.TarjetaTextBox.Text, Consecutivo, "", Me.TransTextbox.Text, Me.TarjetaDebitoTextBox.Text)
                Me.ConsecutivoTextBox.Text = Consecutivo
                GloConsecutivo = Consecutivo
            End If

            Me.Inserta_REl_dolarTableAdapter.Connection = CON
            Me.Inserta_REl_dolarTableAdapter.Fill(Me.Procedimientos_arnoldo.Inserta_REl_dolar, Me.ConsecutivoTextBox.Text, CDbl(Me.TextBox4.Text), CDbl(Me.TextBox2.Text), CDbl(Me.TextBox3.Text))
            CON.Close()
            GloBnd = True
            bitsist(GloUsuario, 0, "Facturacion ", Me.Name, "", "Se Genero Desglose de Moneda", "A la cajera:" + Me.TextBox1.Text + " Con un Importe de: " + Me.ImporteTextBox.Text, SubCiudad)
            MsgBox("Los Cambios Fueron Guardados", MsgBoxStyle.Information, "Guardar")
            GloReporte = 3
            My.Forms.FrmImprimirRepGral.Show()
            Me.Close()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub TextBox4_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox4.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox4, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox2, Asc(LCase(e.KeyChar)), "L")))
    End Sub

    Private Sub TextBox3_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox3.TextChanged
        If BND1 = False Then
            Suma_Detalle2()
        End If
    End Sub

    Private Sub TextBox4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TextBox4.TextChanged
        If IsNumeric(Me.TextBox4.Text) = True And IsNumeric(Me.TextBox2.Text) = True Then
            Me.TextBox3.Text = CDbl(Me.TextBox4.Text) * CDbl(Me.TextBox2.Text)
        End If
    End Sub

    Private Sub TextBox2_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox2.TextChanged
        If IsNumeric(Me.TextBox4.Text) = True And IsNumeric(Me.TextBox2.Text) = True Then
            Me.TextBox3.Text = CDbl(Me.TextBox4.Text) * CDbl(Me.TextBox2.Text)
        End If
    End Sub

    Private Sub TextBox5_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox5.TextChanged
        If BND1 = False Then
            Suma_Detalle()
            Suma_Detalle2()
        End If
    End Sub

    Private Sub ChecaDesgloseMoneda(ByVal Clv_Usuario As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("ChecaDesgloseMoneda", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("Clv_Usuario", SqlDbType.VarChar, 10)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Usuario
        comando.Parameters.Add(parametro)

        Dim parametro1 As New SqlParameter("@Tipo", SqlDbType.VarChar, 50)
        parametro1.Direction = ParameterDirection.Output
        parametro1.Value = String.Empty
        comando.Parameters.Add(parametro1)

        Dim parametro2 As New SqlParameter("@Res", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Output
        parametro2.Value = 0
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Msg", SqlDbType.VarChar, 150)
        parametro3.Direction = ParameterDirection.Output
        parametro3.Value = String.Empty
        comando.Parameters.Add(parametro3)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            eTipo = parametro1.Value.ToString
            eRes = CInt(parametro2.Value.ToString)
            eMsg = parametro3.Value.ToString
            conexion.Close()

            If eRes = 1 Then
                Me.TextBoxTipo.Text = eTipo
                Me.TextBoxTipo.Visible = True
                Me.ReferenciaTextBox.Text = eTipo
            Else
                LlenaComboTipo()
                Me.ComboBoxTipo.Visible = True
            End If

        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub LlenaComboTipo()
        Me.ComboBoxTipo.Items.Insert(0, "De Apertura")
        Me.ComboBoxTipo.Items.Insert(1, "De Cierre")
    End Sub


    Private Sub ComboBoxTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipo.SelectedIndexChanged
        If Me.ComboBoxTipo.Text.Length > 0 Then Me.ReferenciaTextBox.Text = Me.ComboBoxTipo.Text
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub TextBoxB1000_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxB1000.TextChanged

    End Sub

    Private Sub SUMATOTALMONEDAS()
        Dim M100 As Double = 0
        Dim M50 As Double = 0
        Dim M20 As Double = 0
        Dim M10 As Double = 0
        Dim M5 As Double = 0
        Dim M2 As Double = 0
        Dim M1 As Double = 0
        Dim M050 As Double = 0
        Dim M020 As Double = 0
        Dim M010 As Double = 0
        Dim M005 As Double = 0

        If IsNumeric(Me.TextBoxM100.Text) = True Then M100 = Me.TextBoxM100.Text Else M100 = 0
        If IsNumeric(Me.TextBoxM50.Text) = True Then M50 = Me.TextBoxM50.Text Else M50 = 0
        If IsNumeric(Me.TextBoxM20.Text) = True Then M20 = Me.TextBoxM20.Text Else M20 = 0
        If IsNumeric(Me.TextBoxM10.Text) = True Then M10 = Me.TextBoxM10.Text Else M10 = 0
        If IsNumeric(Me.TextBoxM5.Text) = True Then M5 = Me.TextBoxM5.Text Else M5 = 0
        If IsNumeric(Me.TextBoxM2.Text) = True Then M2 = Me.TextBoxM2.Text Else M2 = 0
        If IsNumeric(Me.TextBoxM1.Text) = True Then M1 = Me.TextBoxM1.Text Else M1 = 0
        If IsNumeric(Me.TextBoxM050.Text) = True Then M050 = Me.TextBoxM050.Text Else M050 = 0
        If IsNumeric(Me.TextBoxM020.Text) = True Then M020 = Me.TextBoxM020.Text Else M020 = 0
        If IsNumeric(Me.TextBoxM010.Text) = True Then M010 = Me.TextBoxM010.Text Else M010 = 0
        If IsNumeric(Me.TextBoxM005.Text) = True Then M005 = Me.TextBoxM005.Text Else M005 = 0

        
        Me.TextBoxTotalMonedas.Text = M100 + M50 + M20 + M10 + M5 + M2 + M1 + M050 + M020 + M010 + M005



    End Sub

    Private Sub SUMATOTALBILLETES()
        Dim b1000 As Double = 0
        Dim b500 As Double = 0
        Dim b200 As Double = 0
        Dim b100 As Double = 0
        Dim b20 As Double = 0
        Dim b50 As Double = 0
        If IsNumeric(Me.TextBoxB1000.Text) = True Then b1000 = Me.TextBoxB1000.Text Else b1000 = 0
        If IsNumeric(Me.TextBoxB500.Text) = True Then b500 = Me.TextBoxB500.Text Else b500 = 0
        If IsNumeric(Me.TextBoxB200.Text) = True Then b200 = Me.TextBoxB200.Text Else b200 = 0
        If IsNumeric(Me.TextBoxB100.Text) = True Then b100 = Me.TextBoxB100.Text Else b100 = 0
        If IsNumeric(Me.TextBoxB20.Text) = True Then b20 = Me.TextBoxB20.Text Else b20 = 0
        If IsNumeric(Me.TextBoxB50.Text) = True Then b50 = Me.TextBoxB50.Text Else b50 = 0
        Me.TextBoxTotalBilletes.Text = b1000 + b500 + b200 + b100 + b50 + b20


    End Sub


    Private Sub TextBoxTotalBilletes_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBoxTotalBilletes.TextChanged

    End Sub

    Private Sub TransTextbox_TextChanged(sender As System.Object, e As System.EventArgs) Handles TransTextbox.TextChanged
        If BND1 = False Then
            Suma_Detalle()
        End If
    End Sub

    Private Sub NUEVODESGLOSEMONEDA(ByVal prmFecha As Date, ByVal prmCajera As String, ByVal prmTotal As Decimal, ByVal prmB1000 As Integer, _
                                  ByVal prmB500 As Integer, ByVal prmB200 As Integer, ByVal prmB100 As Integer, ByVal prmB50 As Integer, _
                                  ByVal prmB20 As Integer, ByVal prmM100 As Integer, ByVal prmM50 As Integer, ByVal prmM20 As Integer, _
                                  ByVal prmM10 As Integer, ByVal prmM5 As Integer, ByVal prmM2 As Integer, ByVal prmM1 As Integer, _
                                  ByVal prmM050 As Integer, ByVal prmM020 As Integer, ByVal prmM010 As Integer, ByVal prmM005 As Integer, _
                                  ByVal prmCheques As Decimal, ByVal prmTarjeta As Decimal, ByVal prmReferencia As String, _
                                  ByVal prmTransferencia As Decimal, ByVal prmTarjetaDebito As Decimal)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("NUEVODESGLOSEMONEDA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Fecha", prmFecha)
        CMD.Parameters.AddWithValue("@Cajera", prmCajera)
        CMD.Parameters.AddWithValue("@Total", prmTotal)
        CMD.Parameters.AddWithValue("@B1000", prmB1000)
        CMD.Parameters.AddWithValue("@B500", prmB500)
        CMD.Parameters.AddWithValue("@B200", prmB200)
        CMD.Parameters.AddWithValue("@B100", prmB100)
        CMD.Parameters.AddWithValue("@B50", prmB50)
        CMD.Parameters.AddWithValue("@B20", prmB20)
        CMD.Parameters.AddWithValue("@M100", prmM100)
        CMD.Parameters.AddWithValue("@M50", prmM50)
        CMD.Parameters.AddWithValue("@M20", prmM20)
        CMD.Parameters.AddWithValue("@M10", prmM10)
        CMD.Parameters.AddWithValue("@M5", prmM5)
        CMD.Parameters.AddWithValue("@M2", prmM2)
        CMD.Parameters.AddWithValue("@M1", prmM1)
        CMD.Parameters.AddWithValue("@M050", prmM050)
        CMD.Parameters.AddWithValue("@M020", prmM020)
        CMD.Parameters.AddWithValue("@M010", prmM010)
        CMD.Parameters.AddWithValue("@M005", prmM005)
        CMD.Parameters.AddWithValue("@Cheques", prmCheques)
        CMD.Parameters.AddWithValue("@Tarjeta", prmTarjeta)
        CMD.Parameters.AddWithValue("@Referencia", prmReferencia)
        CMD.Parameters.AddWithValue("@Transferencia", prmTransferencia)
        CMD.Parameters.AddWithValue("@TarjetaDebito", prmTarjetaDebito)
        Dim parout As New SqlParameter("@Consecutivo", SqlDbType.BigInt)
        parout.Direction = ParameterDirection.Output
        parout.Value = 0
        CMD.Parameters.Add(parout)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            GloConsecutivo = CMD.Parameters("@Consecutivo").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Private Sub MODIFICADESGLOSEMONEDA(ByVal prmFecha As Date, ByVal prmCajera As String, ByVal prmTotal As Decimal, ByVal prmB1000 As Integer, _
                                  ByVal prmB500 As Integer, ByVal prmB200 As Integer, ByVal prmB100 As Integer, ByVal prmB50 As Integer, _
                                  ByVal prmB20 As Integer, ByVal prmM100 As Integer, ByVal prmM50 As Integer, ByVal prmM20 As Integer, _
                                  ByVal prmM10 As Integer, ByVal prmM5 As Integer, ByVal prmM2 As Integer, ByVal prmM1 As Integer, _
                                  ByVal prmM050 As Integer, ByVal prmM020 As Integer, ByVal prmM010 As Integer, ByVal prmM005 As Integer, _
                                  ByVal prmCheques As Decimal, ByVal prmTarjeta As Decimal, ByVal prmConsecutivo As Long, ByVal prmReferencia As String, _
                                  ByVal prmTransferencia As Decimal, ByVal prmTarjetaDebito As Decimal)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MODIFICADESGLOSEMONEDA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Fecha", prmFecha)
        CMD.Parameters.AddWithValue("@Cajera", prmCajera)
        CMD.Parameters.AddWithValue("@Total", prmTotal)
        CMD.Parameters.AddWithValue("@B1000", prmB1000)
        CMD.Parameters.AddWithValue("@B500", prmB500)
        CMD.Parameters.AddWithValue("@B200", prmB200)
        CMD.Parameters.AddWithValue("@B100", prmB100)
        CMD.Parameters.AddWithValue("@B50", prmB50)
        CMD.Parameters.AddWithValue("@B20", prmB20)
        CMD.Parameters.AddWithValue("@M100", prmM100)
        CMD.Parameters.AddWithValue("@M50", prmM50)
        CMD.Parameters.AddWithValue("@M20", prmM20)
        CMD.Parameters.AddWithValue("@M10", prmM10)
        CMD.Parameters.AddWithValue("@M5", prmM5)
        CMD.Parameters.AddWithValue("@M2", prmM2)
        CMD.Parameters.AddWithValue("@M1", prmM1)
        CMD.Parameters.AddWithValue("@M050", prmM050)
        CMD.Parameters.AddWithValue("@M020", prmM020)
        CMD.Parameters.AddWithValue("@M010", prmM010)
        CMD.Parameters.AddWithValue("@M005", prmM005)
        CMD.Parameters.AddWithValue("@Cheques", prmCheques)
        CMD.Parameters.AddWithValue("@Tarjeta", prmTarjeta)
        CMD.Parameters.AddWithValue("@Consecutivo", prmConsecutivo)
        CMD.Parameters.AddWithValue("@Referencia", prmReferencia)
        CMD.Parameters.AddWithValue("@Transferencia", prmTransferencia)
        CMD.Parameters.AddWithValue("@TarjetaDebito", prmTarjetaDebito)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

    Public Sub CONDESGLOSEMONEDA(ByVal prmConsecutivo As Integer)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("CONDESGLOSEMONEDA", CON)
        CMD.CommandType = CommandType.StoredProcedure
        CMD.Parameters.AddWithValue("@Consecutivo", prmConsecutivo)

        Dim reader As SqlDataReader

        Try
            CON.Open()
            reader = CMD.ExecuteReader()

            While reader.Read
                Me.TransTextbox.Text = FormatCurrency(reader(24).ToString, 2)
                Me.TarjetaDebitoTextBox.Text = FormatCurrency(reader(25).ToString, 2)
            End While

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub

End Class