Imports System.Data.SqlClient
Public Class VisorHistorialQuejas
    Private Sub consultar()
        If IsNumeric(Me.Clv_calleLabel2.Text) = True Then
            If gloClave > 0 And Me.Clv_calleLabel2.Text > 0 Then
                OPCION = "C"
                gloClave = Me.Clv_calleLabel2.Text
                'GloClv_TipSer = Me.Label9.Text
                '                FrmQueja.Show()
                LiTipo = 5
                FrmImprimir.Show()
            Else
                MsgBox("Seleccione la fila que desea Consultar")
            End If
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        consultar()
    End Sub

    Private Sub Busca(ByVal op As Integer)
        Dim sTATUS As String = "P"
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Try
            If IsNumeric(Me.ComboBox2.SelectedValue) = True Then
                If op = 0 Then 'contrato
                    'If Me.RadioButton1.Checked = True Then
                    '    sTATUS = "P"
                    'ElseIf Me.RadioButton2.Checked = True Then
                    '    sTATUS = "E"
                    'ElseIf Me.RadioButton3.Checked = True Then
                    '    sTATUS = "V"
                    'End If
                    If IsNumeric(GloContrato) = True Then
                        Me.BUSCAQUEJASTableAdapter.Connection = CON
                        Me.BUSCAQUEJASTableAdapter.Fill(Me.LydiaDataSet2.BUSCAQUEJAS, CLng(Me.ComboBox2.SelectedValue), 0, GloContrato, sTATUS, "", "", New System.Nullable(Of Integer)(CType(0, Integer)))
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                ElseIf op = 1 Then
                    If Len(Trim(Me.TextBox2.Text)) > 0 Then
                        Me.BUSCAQUEJASTableAdapter.Connection = CON
                        Me.BUSCAQUEJASTableAdapter.Fill(Me.LydiaDataSet2.BUSCAQUEJAS, CLng(Me.ComboBox2.SelectedValue), 0, 0, Me.TextBox2.Text, "", "", New System.Nullable(Of Integer)(CType(1, Integer)))
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If
                    'ElseIf op = 2 Then 'Calle y numero
                    '    Me.BUSCAQUEJASTableAdapter.Connection = CON
                    '    Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, 0, 0, "", Me.BCALLE.Text, Me.BNUMERO.Text, New System.Nullable(Of Integer)(CType(2, Integer)))
                    'ElseIf op = 3 Then 'clv_Orden
                    '    If IsNumeric(Me.TextBox3.Text) = True Then
                    '        Me.BUSCAQUEJASTableAdapter.Connection = CON
                    '        Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, 0, Me.TextBox3.Text, 0, "", "", "", New System.Nullable(Of Integer)(CType(3, Integer)))
                    '    Else
                    '        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    '    End If
                ElseIf op = 99 Then

                    If Me.RadioButton1.Checked = True Then
                        sTATUS = "P"
                    ElseIf Me.RadioButton2.Checked = True Then
                        sTATUS = "E"
                    ElseIf Me.RadioButton3.Checked = True Then
                        sTATUS = "V"
                    End If
                    If IsNumeric(GloContrato) = True Then
                        Me.BUSCAQUEJASTableAdapter.Connection = CON
                        'Me.BUSCAQUEJASTableAdapter.Fill(Me.NewSofTvDataSet.BUSCAQUEJAS, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Integer)), 0, GloContratoVer, sTATUS, "", "", New System.Nullable(Of Integer)(CType(99, Integer)))
                        Me.BUSCAQUEJASTableAdapter.Fill(Me.LydiaDataSet2.BUSCAQUEJAS, CLng(Me.ComboBox2.SelectedValue), 0, GloContrato, sTATUS, "", "", New System.Nullable(Of Integer)(CType(99, Integer)))
                    Else
                        MsgBox("No se puede realizar la busqueda con datos en blanco", MsgBoxStyle.Information)
                    End If

                Else
                    Me.BUSCAQUEJASTableAdapter.Connection = CON
                    Me.BUSCAQUEJASTableAdapter.Fill(Me.LydiaDataSet2.BUSCAQUEJAS, CLng(Me.ComboBox2.SelectedValue), 0, 0, "", "", "", New System.Nullable(Of Integer)(CType(4, Integer)))
                End If
                Me.TextBox1.Clear()
                Me.TextBox2.Clear()
                Me.TextBox3.Clear()
                Me.BNUMERO.Clear()
                Me.BCALLE.Clear()
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
        CON.Close()
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        Busca(0)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Busca(1)
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(0)
        End If
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(1)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(3)
        End If
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Busca(3)
    End Sub

    Private Sub Clv_calleLabel2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Clv_calleLabel2.TextChanged
        gloClave = Me.Clv_calleLabel2.Text
    End Sub

    Private Sub BRWQUEJAS_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If GloBnd = True Then
            GloBnd = False
            Busca(4)
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Busca(2)
    End Sub

    Private Sub BCALLE_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BCALLE.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub

    Private Sub BNUMERO_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles BNUMERO.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Busca(2)
        End If
    End Sub
    Private Sub VisorHistorialQuejas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Busca(0)
        GloBnd = False
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraTipSerPrincipal2TableAdapter.Connection = CON
        Me.MuestraTipSerPrincipal2TableAdapter.Fill(Me.ProcedimientosArnoldo3.MuestraTipSerPrincipal2)
        CON.Close()

    End Sub
    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        Busca(99)
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        Busca(99)
    End Sub

    Private Sub RadioButton3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton3.CheckedChanged
        Busca(99)
    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBox2.SelectedIndexChanged
        If Len(Me.ComboBox2.Text) > 0 Then
            Glo_tipSer = Me.ComboBox2.SelectedValue
            Busca(99)
        Else
            Glo_tipSer = 0
            Busca(99)
        End If
    End Sub
End Class