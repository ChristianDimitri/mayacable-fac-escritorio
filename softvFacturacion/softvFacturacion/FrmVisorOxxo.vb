Imports System.Data.SqlClient
Public Class FrmVisorOxxo

    Private Sub llena_grid()
        Dim CON As New SqlConnection(MiConexion)
        Try
            CON.Open()
            Me.MUESTRAVERDETALLE_OXXOTableAdapter.Connection = CON
            Me.MUESTRAVERDETALLE_OXXOTableAdapter.Fill(Me.DataSetEdgar.MUESTRAVERDETALLE_OXXO, GloClv_Recibo)
            CON.Close()
            Me.ContratoTextBox.Text = GloContrato
            BUSCACLIENTES(0)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub


    Private Sub BUSCACLIENTES(ByVal OP As Integer)
        Dim clv_Session1 As Long = 0
        Try
            Me.Panel5.Visible = False
            'If IsNumeric(Me.Clv_Session.Text) = True Then
            'Me.BorraClv_SessionTableAdapter.Fill(Me.NewsoftvDataSet.BorraClv_Session, New System.Nullable(Of Long)(CType(Me.Clv_Session.Text, Long)))
            'End If
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                GloContrato = Me.ContratoTextBox.Text
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Connection = CON
                Me.BUSCLIPORCONTRATO_FACTableAdapter.Fill(Me.NewsoftvDataSet.BUSCLIPORCONTRATO_FAC, Me.ContratoTextBox.Text, 0)
                Me.Button8.Enabled = True
                CON.Close()
                Suma_Detalle()
                If num = 0 Or num2 = 0 Then
                    CREAARBOL()
                Else


                    MsgBox("El Cliente " + Me.ContratoTextBox.Text + " Ha Sido Bloqueado por lo que no se Podr� Llevar a cabo la Queja ", MsgBoxStyle.Exclamation)
                    Me.ContratoTextBox.Text = 0

                    'Glocontratosel = 0
                End If
            End If
            Me.Clv_Session.Text = Glo_Clv_SessionVer
            If IsNumeric(Glo_BndErrorVer) = False Then Glo_BndErrorVer = 1
            If Glo_BndErrorVer = 1 Then
                Me.LABEL19.Text = Glo_MsgVer
                Me.Panel5.Visible = True
            
            End If

            DAMETIPOSCLIENTEDAME()

        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub DAMETIPOSCLIENTEDAME()
        Try
            Me.CLV_TIPOCLIENTELabel1.Text = ""
            Me.DESCRIPCIONLabel1.Text = ""
            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                If Me.ContratoTextBox.Text > 0 Then
                    Dim CON As New SqlConnection(MiConexion)
                    CON.Open()
                    Me.DAMETIPOSCLIENTESTableAdapter.Connection = CON
                    Me.DAMETIPOSCLIENTESTableAdapter.Fill(Me.DataSetEdgar.DAMETIPOSCLIENTES, Me.ContratoTextBox.Text)
                    CON.Close()
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CREAARBOL()

        Try
            Dim I As Integer = 0
            Dim X As Integer = 0
            Dim Y As Integer = 0
            Dim epasa As Boolean = True
            ' Assumes that customerConnection is a valid SqlConnection object.
            ' Assumes that orderConnection is a valid OleDbConnection object.
            'Dim custAdapter As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter( _
            '  "SELECT * FROM dbo.Customers", customerConnection)''

            'Dim customerOrders As DataSet = New DataSet()
            'custAdapter.Fill(customerOrders, "Customers")
            ' 
            'Dim pRow, cRow As DataRow
            'For Each pRow In customerOrders.Tables("Customers").Rows
            ' msgbox(pRow("CustomerID").ToString())
            'Next
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()

            If IsNumeric(Me.ContratoTextBox.Text) = True Then
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(Me.ContratoTextBox.Text, Long)))
            Else
                Me.DameSerDELCliFACTableAdapter.Connection = CON
                Me.DameSerDELCliFACTableAdapter.Fill(Me.NewsoftvDataSet.DameSerDELCliFAC, New System.Nullable(Of Long)(CType(0, Long)))
            End If
            Dim pasa As Boolean = False
            Dim Net As Boolean = False
            Dim dig As Boolean = False
            Dim jNet As Integer = -1
            Dim PasaJNet As Boolean = False
            Dim jDig As Integer = -1
            Dim FilaRow As DataRow
            'Me.TextBox1.Text = ""
            Me.TreeView1.Nodes.Clear()
            For Each FilaRow In Me.NewsoftvDataSet.DameSerDELCliFAC.Rows

                'MsgBox(Trim(FilaRow(1).ToString()) & " " & Trim(FilaRow(0).ToString()))
                X = 0
                'If Len(Trim(Me.TextBox1.Text)) = 0 Then
                'Me.TextBox1.Text = Trim(FilaRow("Servicio").ToString())
                'Else
                'Me.TextBox1.Text = Me.TextBox1.Text & " , " & Trim(FilaRow("Servicio").ToString())
                'End If
                'MsgBox(Mid(FilaRow("Servicio").ToString(), 1, 19))
                If Mid(FilaRow("Servicio").ToString(), 1, 3) = "---" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                    Net = False
                    dig = False
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Servicio Basico" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 31) = "Servicios de Televisi�n Digital" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)
                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 21) = "Servicios de Internet" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                ElseIf Mid(FilaRow("Servicio").ToString(), 1, 22) = "Servicios de Tel�fonia" Then
                    Me.TreeView1.Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = System.Drawing.Color.FromArgb(ColorBwr)

                    pasa = True
                Else
                    If Mid(FilaRow("Servicio").ToString(), 1, 14) = "Mac Cablemodem" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jNet = jNet + 1
                        pasa = False
                        Net = True
                    ElseIf Mid(FilaRow("Servicio").ToString(), 1, 15) = "Aparato Digital" Then
                        Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                        jDig = jDig + 1
                        pasa = False
                        dig = True
                    Else
                        If Net = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jNet).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        ElseIf dig = True Then
                            Me.TreeView1.Nodes(I - 1).Nodes(jDig).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                        Else
                            If epasa = True Then
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Black
                                pasa = False
                                epasa = False
                            Else
                                Me.TreeView1.Nodes(I - 1).Nodes.Add(FilaRow("Servicio").ToString()).ForeColor = Color.Gray
                                epasa = False
                                pasa = False
                            End If

                        End If
                    End If
                End If
                If pasa = True Then I = I + 1
            Next
            CON.Close()
            'Me.TreeView1.Nodes(0).ExpandAll()
            For Y = 0 To (I - 1)
                Me.TreeView1.Nodes(Y).ExpandAll()
            Next
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Suma_Detalle()
        Dim I As Integer = 0
        '
        Dim Importe As Double = 0
        Dim Adicional As Double = 0
        Dim TotalPuntos_1 As Double = 0
        '
        Dim Importe_T As Double = 0
        Dim Adicional_T As Double = 0
        Dim TotalPuntos_1_T As Double = 0
        '

        For I = 0 To Me.MUESTRAVERDETALLE_OXXODataGridView.RowCount - 1
            If IsNumeric(Me.MUESTRAVERDETALLE_OXXODataGridView.Item(5, I).Value) = True Then Importe = Me.MUESTRAVERDETALLE_OXXODataGridView.Item(5, I).Value Else Importe = 0
            If IsNumeric(Me.MUESTRAVERDETALLE_OXXODataGridView.Item(6, I).Value) = True Then Adicional = Me.MUESTRAVERDETALLE_OXXODataGridView.Item(6, I).Value Else Adicional = 0
            If IsNumeric(Me.MUESTRAVERDETALLE_OXXODataGridView.Item(7, I).Value) = True Then TotalPuntos_1 = Me.MUESTRAVERDETALLE_OXXODataGridView.Item(7, I).Value Else TotalPuntos_1 = 0
            Importe_T = Importe_T + Importe
            Adicional_T = Adicional_T + Adicional
            TotalPuntos_1_T = TotalPuntos_1_T + TotalPuntos_1
        Next I
        CMBImporte.Text = Format(CDec(Importe_T - TotalPuntos_1_T), "##,##0.00")
        Me.CMBSubtotal.Text = Format(CDec(Importe_T), "##,##0.00")
        Me.CMBPuntos.Text = Format(CDec(TotalPuntos_1_T), "##,##0.00")

    End Sub



    Private Sub FrmVisorOxxo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        llena_grid()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
   
    Private Sub ContratoTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        If IsNumeric(Me.ContratoTextBox.Text) = False Then GloContrato = 0 Else GloContrato = Me.ContratoTextBox.Text
        If IsNumeric(GloContrato) = True And GloContrato > 0 Then
            GloOpFacturas = 3
            eBotonGuardar = False
            BrwFacturas_Cancelar.Show()
        Else
            MsgBox("Seleccione un Cliente por favor", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CMBLabel4.Click

    End Sub

    Private Sub ContratoTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ContratoTextBox.TextChanged

    End Sub
End Class