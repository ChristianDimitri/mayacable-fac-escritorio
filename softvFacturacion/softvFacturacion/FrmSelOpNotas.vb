Imports System.Data.SqlClient
Public Class FrmSelOpNotas

    Private Sub FrmSelOpNotas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'TODO: esta l�nea de c�digo carga datos en la tabla 'Procedimientos_arnoldo.Muestra_Usuario_Notas' Puede moverla o quitarla seg�n sea necesario.
        Dim CON As New SqlConnection(MiConexion)
        If locbndrepnotas2 = True Then
            locbndrepnotas2 = False
            LocActiva = False
            LocCancelada = False
            LocSaldada = False
            If Locbndrepnotas = 0 Then
                Me.CMBLabel3.Text = "Usuario(a): "
                Me.ComboBox1.Visible = False
                Me.ComboVendedores.Visible = True
                CON.Open()
                Me.Muestra_Usuario_NotasTableAdapter.Connection = CON
                Me.Muestra_Usuario_NotasTableAdapter.Fill(Me.Procedimientos_arnoldo.Muestra_Usuario_Notas)
                CON.Close()
            ElseIf Locbndrepnotas = 1 Then
                Me.CMBLabel3.Text = "Sucursal(es):"
                Me.ComboVendedores.Visible = False
                Me.ComboBox1.Visible = True
                CON.Open()
                Me.Muestra_sucursales_notasTableAdapter.Connection = CON
                Me.Muestra_sucursales_notasTableAdapter.Fill(Me.ProcedimientosArnoldo2.Muestra_sucursales_notas)
                CON.Close()
            End If
        End If
    End Sub

    Private Sub FrmSelOpNotas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        SelTipoRepNotas.Show()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Me.DateTimePicker2.MinDate = Me.DateTimePicker1.Value
    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter
        Me.DateTimePicker1.MaxDate = Me.DateTimePicker2.Value
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If Me.CheckBox1.CheckState = CheckState.Checked Then
            LocActiva = True
        End If
    End Sub

    Private Sub CheckBox2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox2.CheckedChanged
        If Me.CheckBox2.CheckState = CheckState.Checked Then
            LocCancelada = True
        End If
    End Sub


    Private Sub CheckBox3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox3.CheckedChanged
        If Me.CheckBox3.CheckState = CheckState.Checked Then
            LocSaldada = True
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        If Me.CheckBox1.CheckState = CheckState.Unchecked And Me.CheckBox2.CheckState = CheckState.Unchecked And Me.CheckBox3.CheckState = CheckState.Unchecked Then
            MsgBox("Seleccione Al Menos Un Status De La Nota De Cr�dito", MsgBoxStyle.Information)
            Exit Sub
        Else
            LocFecha1 = Me.DateTimePicker1.Text
            LocFecha2 = Me.DateTimePicker2.Text
            LocbndNotas = True
            If Locbndrepnotas = 0 Then
                LocUsuariosNotas = Me.ComboVendedores.SelectedValue
            ElseIf Locbndrepnotas = 1 Then
                Locclv_sucursalnotas = CInt(Me.ComboBox1.SelectedValue)
            End If
            FrmImprimirRepGral.Show()
            Me.Close()
            End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub ComboVendedores_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboVendedores.SelectedIndexChanged

    End Sub
End Class