﻿Imports System.Data.SqlClient
Imports System.Text
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Public Class BrwEstadoCuenta

    Private Sub MuestraEstadoCuentaPeriodo()
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuentaPeriodo")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgPeriodo.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub


    Private Sub MuestraEstadoCuenta(ByVal Id As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraEstadoCuenta " + Id.ToString())
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()
        Dim bindingSource As New BindingSource()
        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            dgEstado.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub ReporteEstadoCuenta(ByVal Id As Integer, ByVal Contrato As Integer, ByVal Op As Integer)
        'Dim conexion As New SqlConnection(MiConexion)
        '' conexion.ConnectionTimeout = 0
        'Dim stringBuilder As New StringBuilder("EXEC [ReporteEstadoCuenta2] " + Id.ToString() + ", " + Contrato.ToString() + ", " + Op.ToString())
        'Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        'Dim dataSet As New DataSet()
        'Dim reportDocument As New ReportDocument
        'Dim dataTable As New DataTable

        'Try

        '    dataAdapter.Fill(dataSet)
        '    dataTable = MuestraGeneral()

        '    dataSet.Tables(0).TableName = "EstadoCuenta"
        '    dataSet.Tables(1).TableName = "DetEstadoCuenta"
        '    dataSet.Tables(2).TableName = "DetEstadoCuenta2"
        '    dataSet.Tables(3).TableName = "DetEstadoCuenta3"
        '    dataSet.Tables(4).TableName = "ClientesApellidos"
        '    dataSet.Tables.Add(dataTable)
        '    dataSet.Tables(5).TableName = "General"
        '    'Edgar 09/Enero/2011 Se Comento el Codigo de Barras
        '    'For Each dr As DataRow In dataSet.Tables(0).Rows
        '    '    dr("CodigoDeBarrasOxxo") = GeneraCodeBar128((dr("oxxo").ToString()))
        '    'Next

        'ReportDocument.Load(RutaReportes + "\ReporteEstadoCuenta_separadoFinal.rpt")
        'ReportDocument.SetDataSource(DataSet)

        'LiTipo = 0
        ''reportDocument.PrintOptions.PaperOrientation = PaperOrientation.Landscape
        'FrmImprimir.CrystalReportViewer1.ReportSource = ReportDocument
        'FrmImprimir.Show()

        'Catch ex As Exception
        '    MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        'End Try
        Try
            'Dim Y As Integer
            'Dim CONTRATOSAUX As Long
            'If Op = 0 Then
            '    For Y = 0 To dgEstado.Rows.Count - 1
            '        CONTRATOSAUX = CLng(dgEstado.Rows(Y).Cells(1).Value.ToString)
            '        Dim DT As New DataTable
            '        Dim TXT As String
            '        BaseII.limpiaParametros()
            '        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CONTRATOSAUX)
            '        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
            '        DT = BaseII.ConsultaDT("uspGeneraGodigoOxxo")
            '        If DT.Rows.Count > 0 Then
            '            TXT = DT.Rows(0)(0).ToString
            '            Dim imgBarCode As Byte()
            '            imgBarCode = GeneraCodeBar128(TXT)
            '            BaseII.limpiaParametros()
            '            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, CONTRATOSAUX)
            '            BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
            '            BaseII.CreateMyParameter("@imgBarCode", SqlDbType.Image, imgBarCode)
            '            BaseII.Inserta("UspGuardaCodigoOXXO")
            '        End If
            '    Next
            'Else
            '    Dim DT As New DataTable
            '    Dim TXT As String
            '    BaseII.limpiaParametros()
            '    BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            '    BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
            '    DT = BaseII.ConsultaDT("uspGeneraGodigoOxxo")
            '    If DT.Rows.Count > 0 Then
            '        TXT = DT.Rows(0)(0).ToString
            '        Dim imgBarCode As Byte()
            '        imgBarCode = GeneraCodeBar128(TXT)
            '        BaseII.limpiaParametros()
            '        BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            '        BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
            '        BaseII.CreateMyParameter("@imgBarCode", SqlDbType.Image, imgBarCode)
            '        BaseII.Inserta("UspGuardaCodigoOXXO")
            '    End If
            'End If


            Dim ds As New DataSet()
            Dim report As New ReportDocument()
            BaseII.limpiaParametros()
            BaseII.CreateMyParameter("@ID", SqlDbType.Int, Id)
            BaseII.CreateMyParameter("@CONTRATO", SqlDbType.BigInt, Contrato)
            BaseII.CreateMyParameter("@OP", SqlDbType.Int, Op)
            Dim lt As New List(Of String)
            lt.Add("EstadoCuenta")
            lt.Add("DetEstadoCuenta")
            lt.Add("DetEstadoCuenta2")
            lt.Add("DetEstadoCuenta3")
            lt.Add("ClientesApellidos")
            lt.Add("General")
            ds = BaseII.ConsultaDS("ReporteEstadoCuenta2", lt)
            report.Load(RutaReportes + "\ReporteEstadoCuenta_separadoFinal.rpt")
            report.SetDataSource(ds)
            LiTipo = 0
            FrmImprimir.CrystalReportViewer1.ReportSource = report
            FrmImprimir.Show()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Function MuestraGeneral() As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim stringBuilder As New StringBuilder("EXEC MuestraGeneral")
        Dim dataAdapter As New SqlDataAdapter(stringBuilder.ToString(), conexion)
        Dim dataTable As New DataTable()

        Try

            dataAdapter.Fill(dataTable)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dataTable

    End Function

    Private Sub bnVerTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVerTodos.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un periodo.")
            Exit Sub
        End If
        ReporteEstadoCuenta(dgPeriodo.SelectedCells(0).Value, 0, 0)
    End Sub



    Private Sub bnVer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnVer.Click
        If dgPeriodo.Rows.Count = 0 Then
            MsgBox("Selecciona un estado de cuenta.")
            Exit Sub
        End If
        ReporteEstadoCuenta(dgPeriodo.SelectedCells(0).Value, dgEstado.SelectedCells(1).Value, 1)
    End Sub


    Private Sub BrwEstadoCuenta_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        MuestraEstadoCuentaPeriodo()
    End Sub




    Private Sub dgPeriodo_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgPeriodo.SelectionChanged
        Try
            If dgPeriodo.Rows.Count = 0 Then Exit Sub
            MuestraEstadoCuenta(dgPeriodo.SelectedCells(0).Value)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub
End Class