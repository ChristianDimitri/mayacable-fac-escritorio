Imports System.Data.SqlClient
Imports System.Text

Public Class FrmSelUsuario

    Private Sub ConSelUsuariosGloPro(ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC ConSelUsuariosGloPro ")
        strSQL.Append(CStr(eClv_Session) & ", ")
        strSQL.Append(CStr(Op))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            conexion.Dispose()
            Me.DataGridPro.DataSource = BindingSource
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub
    
    Private Sub ConSelUsuariosGloTmp(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder

        strSQL.Append("EXEC ConSelUsuariosGloTmp ")
        strSQL.Append(CStr(eClv_Session))

        Try
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource
            conexion.Open()
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            conexion.Close()
            conexion.Dispose()
            Me.DataGridTmp.DataSource = bindingSource
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub InsertarSelUsuariosGloTmp(ByVal Clv_Id As Integer, ByVal Clave As Integer, ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertarSelUsuariosGloTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Id", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Id
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clave", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clave
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Session
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Op", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Op
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub BorrarSelUsuariosGloTmp(ByVal Clv_Id As Integer, ByVal Clave As Integer, ByVal Clv_Session As Long, ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorrarSelUsuariosGloTmp", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Id", SqlDbType.Int)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Id
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Clave", SqlDbType.Int)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Clave
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_Session
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Op", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = op
        comando.Parameters.Add(parametro4)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
            conexion.Dispose()
        Catch ex As Exception
            conexion.Close()
            conexion.Dispose()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Refrescar()
        ConSelUsuariosGloPro(eClv_Session, 1)
        ConSelUsuariosGloTmp(eClv_Session)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.DataGridPro.RowCount = 0 Then
            Exit Sub
        End If
        InsertarSelUsuariosGloTmp(CInt(Me.DataGridPro.SelectedCells(1).Value), CInt(Me.DataGridPro.SelectedCells(2).Value), eClv_Session, 0)
        Refrescar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If Me.DataGridPro.RowCount = 0 Then
            Exit Sub
        End If
        InsertarSelUsuariosGloTmp(0, 0, eClv_Session, 1)
        Refrescar()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        If Me.DataGridTmp.RowCount = 0 Then
            Exit Sub
        End If
        BorrarSelUsuariosGloTmp(CInt(Me.DataGridTmp.SelectedCells(1).Value), CInt(Me.DataGridTmp.SelectedCells(2).Value), eClv_Session, 0)
        Refrescar()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        If Me.DataGridTmp.RowCount = 0 Then
            Exit Sub
        End If
        BorrarSelUsuariosGloTmp(0, 0, eClv_Session, 1)
        Refrescar()
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        GloReporte = 10
        FrmImprimirRepGral.Show()
        Me.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub FrmSelUsuario_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        ConSelUsuariosGloPro(eClv_Session, 0)
    End Sub
End Class