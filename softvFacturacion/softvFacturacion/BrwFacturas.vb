Imports System.Data.SqlClient
Imports System.Text
Public Class BrwFacturas

    Private Sub MuestraTipoFactura(ByVal Op As Integer)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MUESTRATIPOFACTURA " & CStr(Op))

        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            ComboBoxTipoFac.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub MuestraFacturas(ByVal Op As Integer, ByVal Tipo As Char, ByVal Serie As String, ByVal Folio As Long, ByVal Fecha As Date, ByVal Contrato As Long, ByVal Nombre As String)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder("EXEC MuestraFacturas " & CStr(Op) & ", '" & Tipo & "', '" & Serie & "', " & CStr(Folio) & ", '" & CStr(Fecha) & "', " & CStr(Contrato) & ", '" & Nombre & "'")


        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable
        Dim bindingSource As New BindingSource

        Try
            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            DataGridViewFacturas.DataSource = bindingSource
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Limpiar()
        TextBoxBusSerie.Clear()
        TextBoxBusFolio.Clear()
        DateTimePicker1.Value = Today
        TextBoxBusContrato.Clear()
        TextBoxBusNombre.Clear()
    End Sub

    Private Sub BrwFacturas_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        If eNota = True Then
            eNota = False
            LocBndNotasReporteTick = True
            locoprepnotas = 1
            FrmImprimirRepGral.Show()
        End If
    End Sub

    Private Sub BrwFacturas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        DateTimePicker1.Value = Today
        MuestraTipoFactura(0)
        MuestraFacturas(0, ComboBoxTipoFac.SelectedValue, "", 0, Today, 0, "")
    End Sub

    Private Sub ComboBoxTipoFac_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ComboBoxTipoFac.SelectedIndexChanged
        Limpiar()
        MuestraFacturas(0, ComboBoxTipoFac.SelectedValue, "", 0, Today, 0, "")
    End Sub

    Private Sub TextBoxBusSerie_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusSerie.KeyPress
        If Asc(e.KeyChar) <> 13 Or TextBoxBusSerie.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraFacturas(1, ComboBoxTipoFac.SelectedValue, TextBoxBusSerie.Text, 0, Today, 0, "")
    End Sub

    Private Sub ButtonBusSerie_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusSerie.Click
        If TextBoxBusSerie.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraFacturas(1, ComboBoxTipoFac.SelectedValue, TextBoxBusSerie.Text, 0, Today, 0, "")
    End Sub

    Private Sub TextBoxBusFolio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusFolio.KeyPress
        If Asc(e.KeyChar) <> 13 Or TextBoxBusFolio.Text.Length = 0 Then
            Exit Sub
        End If
        If IsNumeric(TextBoxBusFolio.Text) = False Then
            Exit Sub
        End If
        MuestraFacturas(2, ComboBoxTipoFac.SelectedValue, "", TextBoxBusFolio.Text, Today, 0, "")
    End Sub

    Private Sub ButtonBusFolio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusFolio.Click
        If TextBoxBusFolio.Text.Length = 0 Then
            Exit Sub
        End If
        If IsNumeric(TextBoxBusFolio.Text) = False Then
            Exit Sub
        End If
        MuestraFacturas(2, ComboBoxTipoFac.SelectedValue, "", TextBoxBusFolio.Text, Today, 0, "")
    End Sub

    Private Sub ButtonBusFecha_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusFecha.Click
        MuestraFacturas(3, ComboBoxTipoFac.SelectedValue, "", 0, DateTimePicker1.Value, 0, "")
    End Sub

    Private Sub TextBoxBusContrato_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusContrato.KeyPress
        If Asc(e.KeyChar) <> 13 Or TextBoxBusContrato.Text.Length = 0 Then
            Exit Sub
        End If
        If IsNumeric(TextBoxBusContrato.Text) = False Then
            Exit Sub
        End If
        MuestraFacturas(4, ComboBoxTipoFac.SelectedValue, "", 0, Today, TextBoxBusContrato.Text, "")
    End Sub

    Private Sub ButtonBusContrato_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusContrato.Click
        If TextBoxBusContrato.Text.Length = 0 Then
            Exit Sub
        End If
        If IsNumeric(TextBoxBusContrato.Text) = False Then
            Exit Sub
        End If
        MuestraFacturas(4, ComboBoxTipoFac.SelectedValue, "", 0, Today, TextBoxBusContrato.Text, "")
    End Sub

    Private Sub TextBoxBusNombre_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBoxBusNombre.KeyPress
        If Asc(e.KeyChar) <> 13 Or TextBoxBusNombre.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraFacturas(5, ComboBoxTipoFac.SelectedValue, "", 0, Today, 0, TextBoxBusNombre.Text)
    End Sub

    Private Sub ButtonBusNombre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonBusNombre.Click
        If TextBoxBusNombre.Text.Length = 0 Then
            Exit Sub
        End If
        MuestraFacturas(5, ComboBoxTipoFac.SelectedValue, "", 0, Today, 0, TextBoxBusNombre.Text)
    End Sub

    Private Sub ButtonAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAceptar.Click
        If DataGridViewFacturas.RowCount = 0 Then
            MsgBox("Selecciona una Factura.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eContrato = DataGridViewFacturas.SelectedCells.Item(7).Value
        eClv_Factura = DataGridViewFacturas.SelectedCells.Item(0).Value
        eFactura = DataGridViewFacturas.SelectedCells.Item(3).Value
        FrmCobroErroneo.Show()
    End Sub

    Private Sub ButtonSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSalir.Click
        Me.Close()
    End Sub

    Private Sub DataGridViewFacturas_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridViewFacturas.CellDoubleClick
        If DataGridViewFacturas.RowCount = 0 Then
            MsgBox("Selecciona una Factura.", MsgBoxStyle.Information)
            Exit Sub
        End If
        eClv_Factura = DataGridViewFacturas.SelectedCells.Item(0).Value
        eFactura = DataGridViewFacturas.SelectedCells.Item(3).Value
        Me.Close()
    End Sub


End Class