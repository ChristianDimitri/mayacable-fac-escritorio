<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPoliza
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CMBClv_Llave_PolizaLabel As System.Windows.Forms.Label
        Dim CMBFechaLabel As System.Windows.Forms.Label
        Dim CMBClv_UsuarioLabel As System.Windows.Forms.Label
        Dim CMBStatusLabel As System.Windows.Forms.Label
        Dim CMBConceptoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPoliza))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Consulta_Genera_PolizaBindingNavigator = New System.Windows.Forms.BindingNavigator(Me.components)
        Me.Consulta_Genera_PolizaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetEdgar3 = New softvFacturacion.DataSetEdgar3()
        Me.BindingNavigatorCountItem = New System.Windows.Forms.ToolStripLabel()
        Me.BindingNavigatorMoveFirstItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMovePreviousItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorPositionItem = New System.Windows.Forms.ToolStripTextBox()
        Me.BindingNavigatorSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.BindingNavigatorMoveNextItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorMoveLastItem = New System.Windows.Forms.ToolStripButton()
        Me.BindingNavigatorSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem = New System.Windows.Forms.ToolStripButton()
        Me.Consulta_Genera_PolizaDataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolStripContainer1 = New System.Windows.Forms.ToolStripContainer()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Clv_Llave_PolizaTextBox = New System.Windows.Forms.TextBox()
        Me.Dame_Tabla_PolizaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FechaDateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.NombreTextBox = New System.Windows.Forms.TextBox()
        Me.StatusTextBox = New System.Windows.Forms.TextBox()
        Me.ConceptoTextBox = New System.Windows.Forms.TextBox()
        Me.Consulta_Genera_PolizaTableAdapter = New softvFacturacion.DataSetEdgar3TableAdapters.Consulta_Genera_PolizaTableAdapter()
        Me.Dame_Tabla_PolizaTableAdapter = New softvFacturacion.DataSetEdgar3TableAdapters.Dame_Tabla_PolizaTableAdapter()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.GeneraLayout = New System.Windows.Forms.Button()
        CMBClv_Llave_PolizaLabel = New System.Windows.Forms.Label()
        CMBFechaLabel = New System.Windows.Forms.Label()
        CMBClv_UsuarioLabel = New System.Windows.Forms.Label()
        CMBStatusLabel = New System.Windows.Forms.Label()
        CMBConceptoLabel = New System.Windows.Forms.Label()
        CType(Me.Consulta_Genera_PolizaBindingNavigator, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Consulta_Genera_PolizaBindingNavigator.SuspendLayout()
        CType(Me.Consulta_Genera_PolizaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStripContainer1.TopToolStripPanel.SuspendLayout()
        Me.ToolStripContainer1.SuspendLayout()
        CType(Me.Dame_Tabla_PolizaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_Llave_PolizaLabel
        '
        CMBClv_Llave_PolizaLabel.AutoSize = True
        CMBClv_Llave_PolizaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_Llave_PolizaLabel.Location = New System.Drawing.Point(92, 24)
        CMBClv_Llave_PolizaLabel.Name = "CMBClv_Llave_PolizaLabel"
        CMBClv_Llave_PolizaLabel.Size = New System.Drawing.Size(95, 16)
        CMBClv_Llave_PolizaLabel.TabIndex = 7
        CMBClv_Llave_PolizaLabel.Text = "Clave Póliza"
        CMBClv_Llave_PolizaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBFechaLabel
        '
        CMBFechaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBFechaLabel.Location = New System.Drawing.Point(595, -3)
        CMBFechaLabel.Name = "CMBFechaLabel"
        CMBFechaLabel.Size = New System.Drawing.Size(123, 46)
        CMBFechaLabel.TabIndex = 9
        CMBFechaLabel.Text = "Fecha Generación"
        CMBFechaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBClv_UsuarioLabel
        '
        CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBClv_UsuarioLabel.Location = New System.Drawing.Point(219, 22)
        CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        CMBClv_UsuarioLabel.Size = New System.Drawing.Size(360, 21)
        CMBClv_UsuarioLabel.TabIndex = 11
        CMBClv_UsuarioLabel.Text = "Usuario"
        CMBClv_UsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBStatusLabel
        '
        CMBStatusLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBStatusLabel.Location = New System.Drawing.Point(734, 27)
        CMBStatusLabel.Name = "CMBStatusLabel"
        CMBStatusLabel.Size = New System.Drawing.Size(143, 16)
        CMBStatusLabel.TabIndex = 15
        CMBStatusLabel.Text = "Status"
        CMBStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'CMBConceptoLabel
        '
        CMBConceptoLabel.AutoSize = True
        CMBConceptoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        CMBConceptoLabel.Location = New System.Drawing.Point(81, 85)
        CMBConceptoLabel.Name = "CMBConceptoLabel"
        CMBConceptoLabel.Size = New System.Drawing.Size(78, 16)
        CMBConceptoLabel.TabIndex = 17
        CMBConceptoLabel.Text = "Concepto:"
        '
        'Consulta_Genera_PolizaBindingNavigator
        '
        Me.Consulta_Genera_PolizaBindingNavigator.AddNewItem = Nothing
        Me.Consulta_Genera_PolizaBindingNavigator.BindingSource = Me.Consulta_Genera_PolizaBindingSource
        Me.Consulta_Genera_PolizaBindingNavigator.CountItem = Me.BindingNavigatorCountItem
        Me.Consulta_Genera_PolizaBindingNavigator.DeleteItem = Nothing
        Me.Consulta_Genera_PolizaBindingNavigator.Dock = System.Windows.Forms.DockStyle.None
        Me.Consulta_Genera_PolizaBindingNavigator.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BindingNavigatorMoveFirstItem, Me.BindingNavigatorMovePreviousItem, Me.BindingNavigatorSeparator, Me.BindingNavigatorPositionItem, Me.BindingNavigatorCountItem, Me.BindingNavigatorSeparator1, Me.BindingNavigatorMoveNextItem, Me.BindingNavigatorMoveLastItem, Me.BindingNavigatorSeparator2, Me.Consulta_Genera_PolizaBindingNavigatorSaveItem})
        Me.Consulta_Genera_PolizaBindingNavigator.Location = New System.Drawing.Point(3, 0)
        Me.Consulta_Genera_PolizaBindingNavigator.MoveFirstItem = Me.BindingNavigatorMoveFirstItem
        Me.Consulta_Genera_PolizaBindingNavigator.MoveLastItem = Me.BindingNavigatorMoveLastItem
        Me.Consulta_Genera_PolizaBindingNavigator.MoveNextItem = Me.BindingNavigatorMoveNextItem
        Me.Consulta_Genera_PolizaBindingNavigator.MovePreviousItem = Me.BindingNavigatorMovePreviousItem
        Me.Consulta_Genera_PolizaBindingNavigator.Name = "Consulta_Genera_PolizaBindingNavigator"
        Me.Consulta_Genera_PolizaBindingNavigator.PositionItem = Me.BindingNavigatorPositionItem
        Me.Consulta_Genera_PolizaBindingNavigator.Size = New System.Drawing.Size(330, 25)
        Me.Consulta_Genera_PolizaBindingNavigator.TabIndex = 0
        Me.Consulta_Genera_PolizaBindingNavigator.Text = "BindingNavigator1"
        '
        'Consulta_Genera_PolizaBindingSource
        '
        Me.Consulta_Genera_PolizaBindingSource.DataMember = "Consulta_Genera_Poliza"
        Me.Consulta_Genera_PolizaBindingSource.DataSource = Me.DataSetEdgar3
        '
        'DataSetEdgar3
        '
        Me.DataSetEdgar3.DataSetName = "DataSetEdgar3"
        Me.DataSetEdgar3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingNavigatorCountItem
        '
        Me.BindingNavigatorCountItem.Name = "BindingNavigatorCountItem"
        Me.BindingNavigatorCountItem.Size = New System.Drawing.Size(35, 22)
        Me.BindingNavigatorCountItem.Text = "of {0}"
        Me.BindingNavigatorCountItem.ToolTipText = "Número total de elementos"
        '
        'BindingNavigatorMoveFirstItem
        '
        Me.BindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveFirstItem.Image = CType(resources.GetObject("BindingNavigatorMoveFirstItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveFirstItem.Name = "BindingNavigatorMoveFirstItem"
        Me.BindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveFirstItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveFirstItem.Text = "Mover primero"
        '
        'BindingNavigatorMovePreviousItem
        '
        Me.BindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMovePreviousItem.Image = CType(resources.GetObject("BindingNavigatorMovePreviousItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMovePreviousItem.Name = "BindingNavigatorMovePreviousItem"
        Me.BindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMovePreviousItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMovePreviousItem.Text = "Mover anterior"
        '
        'BindingNavigatorSeparator
        '
        Me.BindingNavigatorSeparator.Name = "BindingNavigatorSeparator"
        Me.BindingNavigatorSeparator.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorPositionItem
        '
        Me.BindingNavigatorPositionItem.AccessibleName = "Posición"
        Me.BindingNavigatorPositionItem.AutoSize = False
        Me.BindingNavigatorPositionItem.Name = "BindingNavigatorPositionItem"
        Me.BindingNavigatorPositionItem.Size = New System.Drawing.Size(50, 21)
        Me.BindingNavigatorPositionItem.Text = "0"
        Me.BindingNavigatorPositionItem.ToolTipText = "Posición actual"
        '
        'BindingNavigatorSeparator1
        '
        Me.BindingNavigatorSeparator1.Name = "BindingNavigatorSeparator1"
        Me.BindingNavigatorSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'BindingNavigatorMoveNextItem
        '
        Me.BindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveNextItem.Image = CType(resources.GetObject("BindingNavigatorMoveNextItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveNextItem.Name = "BindingNavigatorMoveNextItem"
        Me.BindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveNextItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveNextItem.Text = "Mover siguiente"
        '
        'BindingNavigatorMoveLastItem
        '
        Me.BindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.BindingNavigatorMoveLastItem.Image = CType(resources.GetObject("BindingNavigatorMoveLastItem.Image"), System.Drawing.Image)
        Me.BindingNavigatorMoveLastItem.Name = "BindingNavigatorMoveLastItem"
        Me.BindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = True
        Me.BindingNavigatorMoveLastItem.Size = New System.Drawing.Size(23, 22)
        Me.BindingNavigatorMoveLastItem.Text = "Mover último"
        '
        'BindingNavigatorSeparator2
        '
        Me.BindingNavigatorSeparator2.Name = "BindingNavigatorSeparator2"
        Me.BindingNavigatorSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'Consulta_Genera_PolizaBindingNavigatorSaveItem
        '
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Image = CType(resources.GetObject("Consulta_Genera_PolizaBindingNavigatorSaveItem.Image"), System.Drawing.Image)
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Name = "Consulta_Genera_PolizaBindingNavigatorSaveItem"
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Size = New System.Drawing.Size(121, 22)
        Me.Consulta_Genera_PolizaBindingNavigatorSaveItem.Text = "Guardar datos"
        '
        'Consulta_Genera_PolizaDataGridView
        '
        Me.Consulta_Genera_PolizaDataGridView.AllowUserToAddRows = False
        Me.Consulta_Genera_PolizaDataGridView.AllowUserToDeleteRows = False
        Me.Consulta_Genera_PolizaDataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Consulta_Genera_PolizaDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.Consulta_Genera_PolizaDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7})
        Me.Consulta_Genera_PolizaDataGridView.DataSource = Me.Consulta_Genera_PolizaBindingSource
        Me.Consulta_Genera_PolizaDataGridView.Location = New System.Drawing.Point(22, 193)
        Me.Consulta_Genera_PolizaDataGridView.Name = "Consulta_Genera_PolizaDataGridView"
        Me.Consulta_Genera_PolizaDataGridView.Size = New System.Drawing.Size(935, 421)
        Me.Consulta_Genera_PolizaDataGridView.TabIndex = 2
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Clv_llave"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn1.Width = 5
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Cuenta"
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn2.DefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridViewTextBoxColumn2.HeaderText = "Cuenta"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn2.Width = 200
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Descripcion"
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DataGridViewTextBoxColumn3.DefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridViewTextBoxColumn3.HeaderText = "Descripcion"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn3.Width = 300
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Parcial"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.Format = "C2"
        DataGridViewCellStyle4.NullValue = Nothing
        Me.DataGridViewTextBoxColumn4.DefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridViewTextBoxColumn4.HeaderText = "Parcial"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn4.Width = 125
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Debe"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.Format = "C2"
        DataGridViewCellStyle5.NullValue = Nothing
        Me.DataGridViewTextBoxColumn5.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridViewTextBoxColumn5.HeaderText = "Debe"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn5.Width = 125
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Haber"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.Format = "C2"
        DataGridViewCellStyle6.NullValue = Nothing
        Me.DataGridViewTextBoxColumn6.DefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridViewTextBoxColumn6.HeaderText = "Haber"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn6.Width = 125
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Clv_llave_Poliza"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Clv_llave_Poliza"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.DataGridViewTextBoxColumn7.Width = 5
        '
        'ToolStripContainer1
        '
        '
        'ToolStripContainer1.ContentPanel
        '
        Me.ToolStripContainer1.ContentPanel.Size = New System.Drawing.Size(935, 5)
        Me.ToolStripContainer1.Location = New System.Drawing.Point(22, 160)
        Me.ToolStripContainer1.Name = "ToolStripContainer1"
        Me.ToolStripContainer1.Size = New System.Drawing.Size(935, 30)
        Me.ToolStripContainer1.TabIndex = 3
        Me.ToolStripContainer1.Text = "ToolStripContainer1"
        '
        'ToolStripContainer1.TopToolStripPanel
        '
        Me.ToolStripContainer1.TopToolStripPanel.Controls.Add(Me.Consulta_Genera_PolizaBindingNavigator)
        Me.ToolStripContainer1.TopToolStripPanel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button5.Location = New System.Drawing.Point(821, 674)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 5
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button1.Location = New System.Drawing.Point(679, 674)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&IMPRIMIR"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Clv_Llave_PolizaTextBox
        '
        Me.Clv_Llave_PolizaTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_Llave_PolizaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Tabla_PolizaBindingSource, "Clv_Llave_Poliza", True))
        Me.Clv_Llave_PolizaTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_Llave_PolizaTextBox.Location = New System.Drawing.Point(84, 790)
        Me.Clv_Llave_PolizaTextBox.Name = "Clv_Llave_PolizaTextBox"
        Me.Clv_Llave_PolizaTextBox.ReadOnly = True
        Me.Clv_Llave_PolizaTextBox.Size = New System.Drawing.Size(92, 22)
        Me.Clv_Llave_PolizaTextBox.TabIndex = 8
        '
        'Dame_Tabla_PolizaBindingSource
        '
        Me.Dame_Tabla_PolizaBindingSource.DataMember = "Dame_Tabla_Poliza"
        Me.Dame_Tabla_PolizaBindingSource.DataSource = Me.DataSetEdgar3
        '
        'FechaDateTimePicker
        '
        Me.FechaDateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.Dame_Tabla_PolizaBindingSource, "Fecha", True))
        Me.FechaDateTimePicker.Enabled = False
        Me.FechaDateTimePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FechaDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FechaDateTimePicker.Location = New System.Drawing.Point(598, 46)
        Me.FechaDateTimePicker.Name = "FechaDateTimePicker"
        Me.FechaDateTimePicker.Size = New System.Drawing.Size(120, 22)
        Me.FechaDateTimePicker.TabIndex = 10
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BackColor = System.Drawing.Color.White
        Me.Clv_UsuarioTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Tabla_PolizaBindingSource, "Clv_Usuario", True))
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(219, 46)
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.ReadOnly = True
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(86, 22)
        Me.Clv_UsuarioTextBox.TabIndex = 12
        '
        'NombreTextBox
        '
        Me.NombreTextBox.BackColor = System.Drawing.Color.White
        Me.NombreTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Tabla_PolizaBindingSource, "Nombre", True))
        Me.NombreTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NombreTextBox.Location = New System.Drawing.Point(311, 46)
        Me.NombreTextBox.Name = "NombreTextBox"
        Me.NombreTextBox.ReadOnly = True
        Me.NombreTextBox.Size = New System.Drawing.Size(268, 22)
        Me.NombreTextBox.TabIndex = 14
        '
        'StatusTextBox
        '
        Me.StatusTextBox.BackColor = System.Drawing.Color.White
        Me.StatusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Tabla_PolizaBindingSource, "Status", True))
        Me.StatusTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusTextBox.Location = New System.Drawing.Point(734, 46)
        Me.StatusTextBox.Name = "StatusTextBox"
        Me.StatusTextBox.ReadOnly = True
        Me.StatusTextBox.Size = New System.Drawing.Size(143, 22)
        Me.StatusTextBox.TabIndex = 16
        Me.StatusTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'ConceptoTextBox
        '
        Me.ConceptoTextBox.BackColor = System.Drawing.Color.White
        Me.ConceptoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.Dame_Tabla_PolizaBindingSource, "Concepto", True))
        Me.ConceptoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConceptoTextBox.Location = New System.Drawing.Point(165, 85)
        Me.ConceptoTextBox.Multiline = True
        Me.ConceptoTextBox.Name = "ConceptoTextBox"
        Me.ConceptoTextBox.Size = New System.Drawing.Size(553, 54)
        Me.ConceptoTextBox.TabIndex = 18
        '
        'Consulta_Genera_PolizaTableAdapter
        '
        Me.Consulta_Genera_PolizaTableAdapter.ClearBeforeFill = True
        '
        'Dame_Tabla_PolizaTableAdapter
        '
        Me.Dame_Tabla_PolizaTableAdapter.ClearBeforeFill = True
        '
        'TextBox1
        '
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.ForeColor = System.Drawing.Color.Red
        Me.TextBox1.Location = New System.Drawing.Point(706, 620)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(116, 24)
        Me.TextBox1.TabIndex = 19
        '
        'TextBox2
        '
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.ForeColor = System.Drawing.Color.Red
        Me.TextBox2.Location = New System.Drawing.Point(828, 620)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(116, 24)
        Me.TextBox2.TabIndex = 20
        '
        'CMBLabel1
        '
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.Location = New System.Drawing.Point(556, 620)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(144, 26)
        Me.CMBLabel1.TabIndex = 21
        Me.CMBLabel1.Text = "SUMAS IGUALES : "
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.White
        Me.TextBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox3.Location = New System.Drawing.Point(95, 46)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(92, 22)
        Me.TextBox3.TabIndex = 22
        '
        'GeneraLayout
        '
        Me.GeneraLayout.BackColor = System.Drawing.Color.DarkOrange
        Me.GeneraLayout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GeneraLayout.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GeneraLayout.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.GeneraLayout.Location = New System.Drawing.Point(454, 674)
        Me.GeneraLayout.Name = "GeneraLayout"
        Me.GeneraLayout.Size = New System.Drawing.Size(209, 36)
        Me.GeneraLayout.TabIndex = 24
        Me.GeneraLayout.Text = "&GENERAR LAYOUT"
        Me.GeneraLayout.UseVisualStyleBackColor = False
        '
        'FrmPoliza
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(977, 741)
        Me.Controls.Add(Me.GeneraLayout)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.CMBLabel1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(CMBClv_Llave_PolizaLabel)
        Me.Controls.Add(Me.Clv_Llave_PolizaTextBox)
        Me.Controls.Add(CMBFechaLabel)
        Me.Controls.Add(Me.FechaDateTimePicker)
        Me.Controls.Add(CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Me.NombreTextBox)
        Me.Controls.Add(CMBStatusLabel)
        Me.Controls.Add(Me.StatusTextBox)
        Me.Controls.Add(CMBConceptoLabel)
        Me.Controls.Add(Me.ConceptoTextBox)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.ToolStripContainer1)
        Me.Controls.Add(Me.Consulta_Genera_PolizaDataGridView)
        Me.MaximizeBox = False
        Me.Name = "FrmPoliza"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Captura de la Póliza"
        CType(Me.Consulta_Genera_PolizaBindingNavigator, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Consulta_Genera_PolizaBindingNavigator.ResumeLayout(False)
        Me.Consulta_Genera_PolizaBindingNavigator.PerformLayout()
        CType(Me.Consulta_Genera_PolizaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Consulta_Genera_PolizaDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStripContainer1.TopToolStripPanel.ResumeLayout(False)
        Me.ToolStripContainer1.TopToolStripPanel.PerformLayout()
        Me.ToolStripContainer1.ResumeLayout(False)
        Me.ToolStripContainer1.PerformLayout()
        CType(Me.Dame_Tabla_PolizaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataSetEdgar3 As softvFacturacion.DataSetEdgar3
    Friend WithEvents Consulta_Genera_PolizaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Consulta_Genera_PolizaTableAdapter As softvFacturacion.DataSetEdgar3TableAdapters.Consulta_Genera_PolizaTableAdapter
    Friend WithEvents Consulta_Genera_PolizaBindingNavigator As System.Windows.Forms.BindingNavigator
    Friend WithEvents BindingNavigatorCountItem As System.Windows.Forms.ToolStripLabel
    Friend WithEvents BindingNavigatorMoveFirstItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMovePreviousItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorPositionItem As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents BindingNavigatorSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents BindingNavigatorMoveNextItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorMoveLastItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents BindingNavigatorSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents Consulta_Genera_PolizaBindingNavigatorSaveItem As System.Windows.Forms.ToolStripButton
    Friend WithEvents Consulta_Genera_PolizaDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents ToolStripContainer1 As System.Windows.Forms.ToolStripContainer
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Dame_Tabla_PolizaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dame_Tabla_PolizaTableAdapter As softvFacturacion.DataSetEdgar3TableAdapters.Dame_Tabla_PolizaTableAdapter
    Friend WithEvents Clv_Llave_PolizaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents FechaDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NombreTextBox As System.Windows.Forms.TextBox
    Friend WithEvents StatusTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ConceptoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents GeneraLayout As System.Windows.Forms.Button
End Class
