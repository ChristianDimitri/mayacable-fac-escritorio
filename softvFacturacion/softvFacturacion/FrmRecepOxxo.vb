Imports System.Net.NetworkInformation
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Sockets
Imports System.IO.StreamReader
Imports System.IO.File
Imports System.IO
Imports System
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
Imports Microsoft.VisualBasic


Public Class FrmRecepOxxo
    Dim ClvFacturaIni As Long = 0
    Dim LocNomarchivo As String = Nothing
    Dim ClvFacturaFin As Long = 0
    Dim Clv_Session_Oxxo As Long = 0
    Dim ARCHIVO As String = Nothing
    Private customersByCityReport As ReportDocument

    Private Sub SetDBLogonForReport(ByVal myConnectionInfo As ConnectionInfo, ByVal myReportDocument As ReportDocument)
        customersByCityReport.DataSourceConnections(0).SetConnection(GloServerName, GloDatabaseName, GloUserID, GloPassword)
        'customersByCityReport.SetDatabaseLogon(GloUserID, GloPassword, GloServerName, GloDatabaseName)
        Dim myTables As Tables = myReportDocument.Database.Tables
        Dim myTable As CrystalDecisions.CrystalReports.Engine.Table
        For Each myTable In myTables
            Dim myTableLogonInfo As TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
            myTable.Location = GloDatabaseName & ".dbo." & myTable.Location.Substring(myTable.Location.LastIndexOf(".") + 1)
        Next
    End Sub

    Private Sub ConfigureCrystalReports(ByVal rangoini As Long, ByVal rangofin As Long)
        customersByCityReport = New ReportDocument
        Dim connectionInfo As New ConnectionInfo
        '"Data Source=SERVER-2003\SQLEXPRESS;Initial Catalog=Newsoftv;Persist Security Info"& _ 
        '    "=True;User ID=DeSistema;Password=1975huli")
        connectionInfo.ServerName = GloServerName
        connectionInfo.DatabaseName = GloDatabaseName
        connectionInfo.UserID = GloUserID
        connectionInfo.Password = GloPassword

        Dim reportPath As String = Nothing

        '        If GloImprimeTickets = False Then
        'reportPath = Application.StartupPath + "\Reportes\" + "ReporteCajas.rpt"
        'Else
        reportPath = RutaReportes + "\ReporteCajasTickets_2.rpt"
        'End If

        customersByCityReport.Load(reportPath)
        'If GloImprimeTickets = False Then
        '    SetDBLogonForSubReport(connectionInfo, customersByCityReport)
        'End If
        SetDBLogonForReport(connectionInfo, customersByCityReport)


        '@Clv_Factura 
        customersByCityReport.SetParameterValue(0, "0")
        '@Clv_Factura_Ini
        customersByCityReport.SetParameterValue(1, CStr(rangoini))
        '@Clv_Factura_Fin
        customersByCityReport.SetParameterValue(2, CStr(rangofin))
        '@Fecha_Ini
        customersByCityReport.SetParameterValue(3, "01/01/1900")
        '@Fecha_Fin
        customersByCityReport.SetParameterValue(4, "01/01/1900")
        '@op
        customersByCityReport.SetParameterValue(5, 1)

        ' If GloImprimeTickets = True Then
        customersByCityReport.DataDefinition.FormulaFields("Empresa").Text = "'" & GloEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("DireccionEmpresa").Text = "'" & GloDireccionEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("Colonia_CpEmpresa").Text = "'" & GloColonia_CpEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("CiudadEmpresa").Text = "'" & GloCiudadEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("RfcEmpresa").Text = "'" & GloRfcEmpresa & "'"
        customersByCityReport.DataDefinition.FormulaFields("TelefonoEmpresa").Text = "'" & GloTelefonoEmpresa & "'"
        ' End If

        customersByCityReport.PrintToPrinter(1, True, 0, 0)

        customersByCityReport.PrintOptions.PrinterName = LocImpresoraTickets

        bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Imprimieron las Facturas del Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)

        'CrystalReportViewer1.ReportSource = customersByCityReport

        'If GloOpFacturas = 3 Then
        'CrystalReportViewer1.ShowExportButton = False
        'CrystalReportViewer1.ShowPrintButton = False
        'CrystalReportViewer1.ShowRefreshButton = False
        'End If
        'SetDBLogonForReport2(connectionInfo)
        customersByCityReport = Nothing
    End Sub


    Private Sub DameClv_Session_Oxxo()
        Try
            Dim CON As New SqlConnection(MiConexion)
            Clv_Session_Oxxo = 0
            CON.Open()
            Dim LocClv_SessionBancos As Long = 0
            Dim comando As SqlClient.SqlCommand
            comando = New SqlClient.SqlCommand
            With comando
                .Connection = CON
                .CommandText = "DameClv_Session_Oxxo"
                .CommandType = CommandType.StoredProcedure
                .CommandTimeout = 0
                Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                prm.Direction = ParameterDirection.Output
                prm.Value = 0
                .Parameters.Add(prm)
                Dim prm1 As New SqlParameter("@Clv_Usuario", SqlDbType.VarChar)
                prm1.Direction = ParameterDirection.Input
                prm1.Value = GloUsuario
                .Parameters.Add(prm1)
                Dim prm2 As New SqlParameter("@ARCHIVO", SqlDbType.VarChar)
                prm2.Direction = ParameterDirection.Input
                prm2.Value = ARCHIVO
                .Parameters.Add(prm2)

                Dim i As Integer = comando.ExecuteNonQuery()
                Clv_Session_Oxxo = prm.Value
                CON.Close()
            End With
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try

            Dim CON As New SqlConnection(MiConexion)
            'CON.Open()
            Dim prueba As String = Nothing
            Dim locerrorsantander As Integer = 0
            Dim proceso As Integer = 0
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim z As Integer = 0
            Dim clv_aceptado As String = Nothing
            Dim clv_id As String = Nothing
            Dim contrato As String = Nothing
            Dim Fila_1 As String = Nothing



            Me.OpenFileDialog1.FileName = ""
            Me.OpenFileDialog1.Filter = "Archivo Resultados *.txt|*.txt"
            Me.OpenFileDialog1.ShowDialog()



            If Me.OpenFileDialog1.FileName = "" Or Me.OpenFileDialog1.FileName = "OpenFileDialog1" Then
                MsgBox("No Selecciono el Archivo", MsgBoxStyle.Information)
            Else
                ARCHIVO = Me.OpenFileDialog1.FileName
                DameClv_Session_Oxxo()
                If Clv_Session_Oxxo = 0 Then
                    MsgBox("Este archivo ya fue cargado anteriormente", MsgBoxStyle.Information)
                    Exit Sub
                End If
                LocNomarchivo = Me.OpenFileDialog1.FileName
                Dim POSICION_1 As Integer = 0
                Dim Consecutivo As String = Nothing
                Dim POSICION_2 As Integer = 0
                Dim Tienda As String = Nothing
                Dim POSICION_3 As Integer = 0
                Dim Fecha As String = Nothing
                Dim POSICION_4 As Integer = 0
                Dim Hora As String = Nothing
                Dim POSICION_5 As Integer = 0
                Dim Contratos As String = Nothing
                Dim POSICION_6 As Integer = 0
                Dim TEMP As String = Nothing
                Dim POSICION_7 As Integer = 0
                Dim MONTO As String = Nothing

                Dim archivo2 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                'Dim archivo3 As TextReader = New StreamReader(Me.OpenFileDialog1.FileName)
                While archivo2.Peek() <> -1
                    Fila_1 = archivo2.ReadLine()
                    POSICION_1 = Fila_1.IndexOfAny(",", 0)
                    Consecutivo = Mid(Fila_1, 1, POSICION_1)
                    POSICION_2 = Fila_1.IndexOfAny(",", POSICION_1 + 1)
                    Tienda = Mid(Fila_1, POSICION_1 + 2, POSICION_2 - (POSICION_1 + 1))
                    POSICION_3 = Fila_1.IndexOfAny(",", POSICION_2 + 1)
                    Fecha = Mid(Fila_1, POSICION_2 + 2, POSICION_3 - (POSICION_2 + 1))
                    POSICION_4 = Fila_1.IndexOfAny(",", POSICION_3 + 1)
                    Hora = Mid(Fila_1, POSICION_3 + 2, POSICION_4 - (POSICION_3 + 1))
                    POSICION_5 = Fila_1.IndexOfAny(",", POSICION_4 + 1)
                    Contratos = Mid(Fila_1, POSICION_4 + 4, POSICION_5 - (POSICION_4 + 19))
                    POSICION_6 = Fila_1.IndexOfAny(",", POSICION_5 + 1)
                    TEMP = Mid(Fila_1, POSICION_5 + 2, POSICION_6 - (POSICION_5 + 1))
                    POSICION_7 = Len(Fila_1)
                    MONTO = Mid(Fila_1, POSICION_6 + 2, POSICION_7 - (POSICION_6 + 1))
                    ''
                    CON.Open()
                    Dim comando As SqlClient.SqlCommand
                    comando = New SqlClient.SqlCommand
                    With comando
                        .Connection = CON
                        .CommandText = "EXEC GUARDA_Resultado_Oxxo " & Clv_Session_Oxxo & ",'" & Consecutivo & "','" & Tienda & "','" & Fecha & "','" & Hora & "','" & Contratos & "','" & TEMP & "','" & MONTO & "'"
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON.Close()
                    y += 1
                End While
                archivo2.Close()


                'archivo3.Close()
            End If
            CON.Open()
            Me.CONSULTA_Resultado_OxxoTableAdapter.Connection = CON
            Me.CONSULTA_Resultado_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTA_Resultado_Oxxo, Clv_Session_Oxxo)
            CON.Close()
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Abrio Un Archivo De Proceso de Oxxo", "", "Nombre Del Archivo:" + LocNomarchivo, SubCiudad)

            LlenaCONSULTATablaClv_Session_Oxxo()

            '========Afecto Clientes que Pasaron el el Archivo========00
            'If IsNumeric(Me.Clv_SessionBancosTextBox.Text) = True Then
            ' proceso = CLng(Me.Clv_SessionBancosTextBox.Text)
            'Me.Procesa_Arhivo_santaderTableAdapter.Connection = CON
            'Me.Procesa_Arhivo_santaderTableAdapter.Fill(Me.Procedimientos_arnoldo.Procesa_Arhivo_santader, proceso, locerrorsantander)
            'CON.Close()
            'If locerrorsantander = 0 Then
            ' MsgBox("Archivo Procesado Exitosamente", MsgBoxStyle.Information)
            ' DameDetalle()
            ' ElseIf locerrorsantander = 1 Then
            ' MsgBox("El N�mero De Proceso No Corresponde Al Archivo Seleccionado", MsgBoxStyle.Information)
            ' End If
            'End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Sub FrmRecepOxxo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        LlenaCONSULTATablaClv_Session_Oxxo()
    End Sub




    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONSULTA_Resultado_OxxoDataGridView.CellClick
        If e.ColumnIndex = 9 Then
            If Me.CONSULTA_Resultado_OxxoDataGridView.RowCount > 0 Then
                If IsNumeric(Me.ReciboTextBox.Text) = True Then
                    GloClv_Recibo = Me.ReciboTextBox.Text
                    FrmVisorOxxo.Show()
                Else
                    MsgBox("El No. Recibo no es valido ", MsgBoxStyle.Information)
                End If
            End If
        End If
    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONSULTA_Resultado_OxxoDataGridView.CellContentClick

    End Sub

    Private Sub CONSULTA_Resultado_OxxoDataGridView_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONSULTA_Resultado_OxxoDataGridView.CellDoubleClick

    End Sub

    Private Sub CLIENTELabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub CLIENTETextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CLIENTETextBox.TextChanged
        If IsNumeric(Me.CLIENTETextBox.Text) = True Then
            GloContrato = Me.CLIENTETextBox.Text
            Glocontratosel = Me.CLIENTETextBox.Text
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Try
            Dim BndError As Integer = 0
            Dim Msg As String = Nothing
            Dim CON As New SqlConnection(MiConexion)
            Dim I As Integer = 0
            Dim x As Long = 0
            If Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
                If Me.CONSULTA_Resultado_OxxoDataGridView.RowCount > 0 Then
                    For I = 0 To Me.CONSULTA_Resultado_OxxoDataGridView.RowCount - 1
                        If IsNumeric((Me.CONSULTA_Resultado_OxxoDataGridView.Item(0, I).Value)) = True Then
                            CON.Open()
                            Dim comando As SqlClient.SqlCommand
                            comando = New SqlClient.SqlCommand
                            With comando
                                .Connection = CON
                                .CommandText = "GuardaPagoOXXO "
                                .CommandType = CommandType.StoredProcedure
                                .CommandTimeout = 0
                                Dim prm As New SqlParameter("@Clv_Recibo", SqlDbType.BigInt)
                                prm.Direction = ParameterDirection.Input
                                prm.Value = Me.CONSULTA_Resultado_OxxoDataGridView.Item(4, I).Value
                                .Parameters.Add(prm)

                                Dim prm1 As New SqlParameter("@BndError_1", SqlDbType.Int)
                                prm1.Direction = ParameterDirection.Output
                                prm1.Value = 0
                                .Parameters.Add(prm1)

                                Dim prm2 As New SqlParameter("@Msg_1", SqlDbType.VarChar)
                                prm2.Direction = ParameterDirection.Output
                                prm2.Value = ""
                                .Parameters.Add(prm2)

                                Dim j As Integer = comando.ExecuteNonQuery()
                                BndError = prm1.Value
                                Msg = prm2.Value
                                If BndError > 0 Then
                                    MsgBox(Msg)
                                End If

                            End With
                            CON.Close()
                        End If
                    Next
                    CON.Open()
                    Dim comando1 As SqlClient.SqlCommand
                    comando1 = New SqlClient.SqlCommand
                    With comando1
                        .Connection = CON
                        .CommandText = "Update TablaClv_Session_Oxxo Set Status='P' Where  Clv_Session= " & Clv_Session_Oxxo
                        .CommandType = CommandType.Text
                        .CommandTimeout = 0
                        .ExecuteReader()
                    End With
                    CON.Close()
                    LlenaCONSULTATablaClv_Session_Oxxo()
                    bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Afectaron los Clientes del Archivo Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)
                    MsgBox("Se han procesado con �xito todos los cobros ", MsgBoxStyle.Information)
                Else
                    MsgBox("No ahi datos para procesarlos ", MsgBoxStyle.Information)
                End If
            ElseIf Mid(Me.StatusTextBox.Text, 1, 1) = "C" Then
                MsgBox("No se puede afectar por que esta cancelado este proceso ", MsgBoxStyle.Information)
            Else
                MsgBox("No se puede afectar por que ya fue afectado anteriormente ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LlenaCONSULTATablaClv_Session_Oxxo()
        Try
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Me.CONSULTATablaClv_Session_OxxoTableAdapter.Connection = Con
            Me.CONSULTATablaClv_Session_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTATablaClv_Session_Oxxo, 0)
            Con.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub Clv_SessionLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Clv_SessionTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_SessionTextBox.TextChanged
        If IsNumeric(Clv_SessionTextBox.Text) = True Then
            Dim Con As New SqlConnection(MiConexion)
            Con.Open()
            Clv_Session_Oxxo = Clv_SessionTextBox.Text
            Me.CONSULTA_Resultado_OxxoTableAdapter.Connection = Con
            Me.CONSULTA_Resultado_OxxoTableAdapter.Fill(Me.DataSetEdgar.CONSULTA_Resultado_Oxxo, Clv_Session_Oxxo)
            Con.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Con As New SqlConnection(MiConexion)
        If Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
            Con.Open()
            Dim comando1 As SqlClient.SqlCommand
            comando1 = New SqlClient.SqlCommand
            With comando1
                .Connection = Con
                .CommandText = "Update TablaClv_Session_Oxxo Set Status='C' Where  Clv_Session= " & Clv_Session_Oxxo
                .CommandType = CommandType.Text
                .CommandTimeout = 0
                .ExecuteReader()
            End With
            Con.Close()
            LlenaCONSULTATablaClv_Session_Oxxo()
            bitsist(GloUsuario, 0, GloSistema, Me.Name, "Se Cancelo el Proceso del Oxxo", "", "Nombre del Archivo:" + LocNomarchivo, SubCiudad)
        ElseIf Mid(Me.StatusTextBox.Text, 1, 1) = "A" Then
            MsgBox("Ya esta cancelado este proceso ", MsgBoxStyle.Information)
        Else
            MsgBox("Solo se pueden cancelar procesos con status Activo", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Try
            If Mid(Me.StatusTextBox.Text, 1, 1) = "P" Then
                Dim CON As New SqlConnection(MiConexion)
                CON.Open()
                Dim comando As SqlClient.SqlCommand
                comando = New SqlClient.SqlCommand
                With comando
                    .Connection = CON
                    .CommandText = "DAMEFACTURASOXXO"
                    .CommandType = CommandType.StoredProcedure
                    .CommandTimeout = 0
                    Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
                    prm.Direction = ParameterDirection.Input
                    prm.Value = Clv_SessionTextBox.Text
                    .Parameters.Add(prm)

                    Dim prm1 As New SqlParameter("@INI", SqlDbType.BigInt)
                    prm1.Direction = ParameterDirection.Output
                    prm1.Value = 0
                    .Parameters.Add(prm1)

                    Dim prm2 As New SqlParameter("@FIN", SqlDbType.BigInt)
                    prm2.Direction = ParameterDirection.Output
                    prm2.Value = 0
                    .Parameters.Add(prm2)
                    Dim i As Integer = comando.ExecuteNonQuery()
                    ClvFacturaIni = prm1.Value
                    ClvFacturaFin = prm2.Value
                    CON.Close()
                End With

                ConfigureCrystalReports(ClvFacturaIni, ClvFacturaFin)

            Else
                MsgBox("Solo se pueden imprimir las facturas de los proceso que tiene Status de Procesado ", MsgBoxStyle.Information)
            End If
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub CONSULTATablaClv_Session_OxxoDataGridView_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles CONSULTATablaClv_Session_OxxoDataGridView.CellContentClick

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        If IsNumeric(Me.Clv_SessionTextBox.Text) = True Then
            GloClv_SessionBancos = Me.Clv_SessionTextBox.Text
            GloReporte = 9
            My.Forms.FrmImprimirRepGral.Show()
        End If
    End Sub
End Class