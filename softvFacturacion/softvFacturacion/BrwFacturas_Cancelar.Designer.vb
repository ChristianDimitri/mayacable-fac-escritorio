<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwFacturas_Cancelar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim FacturaLabel As System.Windows.Forms.Label
        Dim SerieLabel As System.Windows.Forms.Label
        Dim FECHALabel As System.Windows.Forms.Label
        Dim ClienteLabel As System.Windows.Forms.Label
        Dim NOMBRELabel As System.Windows.Forms.Label
        Dim ImporteLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Dim Label15 As System.Windows.Forms.Label
        Dim Label17 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CONTRATOTextBox = New System.Windows.Forms.TextBox()
        Me.NOMBRETextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.BUSCAFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NewsoftvDataSet1 = New softvFacturacion.NewsoftvDataSet1()
        Me.CMBNOMBRETextBox1 = New System.Windows.Forms.TextBox()
        Me.ImporteLabel1 = New System.Windows.Forms.Label()
        Me.ClienteLabel1 = New System.Windows.Forms.Label()
        Me.FECHALabel1 = New System.Windows.Forms.Label()
        Me.FacturaLabel1 = New System.Windows.Forms.Label()
        Me.SerieLabel1 = New System.Windows.Forms.Label()
        Me.Clv_FacturaLabel1 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.FOLIOTextBox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ComboBox4 = New System.Windows.Forms.ComboBox()
        Me.MUESTRATIPOFACTURABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataSetLydia = New softvFacturacion.DataSetLydia()
        Me.CMBLabel5 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.FECHATextBox = New System.Windows.Forms.TextBox()
        Me.SERIETextBox = New System.Windows.Forms.TextBox()
        Me.CMBLabel1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BUSCANOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.BUSCANOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvFacturaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SerieDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FacturaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClienteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NOMBREDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ImporteDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.BUSCAFACTURASTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter()
        Me.CANCELACIONFACTURASBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CANCELACIONFACTURASTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter()
        Me.CMBPanel3 = New System.Windows.Forms.Panel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.CMBPanel4 = New System.Windows.Forms.Panel()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.DAMEFECHADELSERVIDORBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMEFECHADELSERVIDORTableAdapter = New softvFacturacion.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter()
        Me.FECHADateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.CMBPanel2 = New System.Windows.Forms.Panel()
        Me.EricDataSet = New softvFacturacion.EricDataSet()
        Me.CancelaCNRPPEBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CancelaCNRPPETableAdapter = New softvFacturacion.EricDataSetTableAdapters.CancelaCNRPPETableAdapter()
        Me.DataSetEdgar = New softvFacturacion.DataSetEdgar()
        Me.CancelaCambioServClienteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CancelaCambioServClienteTableAdapter = New softvFacturacion.DataSetEdgarTableAdapters.CancelaCambioServClienteTableAdapter()
        Me.Procedimientos_arnoldo = New softvFacturacion.Procedimientos_arnoldo()
        Me.Selecciona_impresoraticketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Selecciona_impresoraticketsTableAdapter = New softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter()
        Me.BUSCANOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter()
        Me.MUESTRATIPOFACTURATableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.MUESTRATIPOFACTURATableAdapter()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.DetalleNOTASDECREDITODataGridView = New System.Windows.Forms.DataGridView()
        Me.DetalleNOTASDECREDITOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.CMBTextBox1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.DetalleNOTASDECREDITOTableAdapter = New softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter()
        Me.EricDataSet2 = New softvFacturacion.EricDataSet2()
        Me.DameGeneralMsjTicketsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DameGeneralMsjTicketsTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter()
        Me.ValidaCancelacionFacturaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ValidaCancelacionFacturaTableAdapter = New softvFacturacion.EricDataSet2TableAdapters.ValidaCancelacionFacturaTableAdapter()
        Me.Column1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.MontoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FECHAdegeneracionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ClvNotadecreditoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        FacturaLabel = New System.Windows.Forms.Label()
        SerieLabel = New System.Windows.Forms.Label()
        FECHALabel = New System.Windows.Forms.Label()
        ClienteLabel = New System.Windows.Forms.Label()
        NOMBRELabel = New System.Windows.Forms.Label()
        ImporteLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label12 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        Label15 = New System.Windows.Forms.Label()
        Label17 = New System.Windows.Forms.Label()
        Label19 = New System.Windows.Forms.Label()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel3.SuspendLayout()
        Me.CMBPanel4.SuspendLayout()
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.CMBPanel2.SuspendLayout()
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CancelaCNRPPEBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CancelaCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ValidaCancelacionFacturaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FacturaLabel
        '
        FacturaLabel.AutoSize = True
        FacturaLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FacturaLabel.ForeColor = System.Drawing.Color.White
        FacturaLabel.Location = New System.Drawing.Point(28, 59)
        FacturaLabel.Name = "FacturaLabel"
        FacturaLabel.Size = New System.Drawing.Size(47, 15)
        FacturaLabel.TabIndex = 30
        FacturaLabel.Text = "Folio :"
        '
        'SerieLabel
        '
        SerieLabel.AutoSize = True
        SerieLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        SerieLabel.ForeColor = System.Drawing.Color.White
        SerieLabel.Location = New System.Drawing.Point(28, 36)
        SerieLabel.Name = "SerieLabel"
        SerieLabel.Size = New System.Drawing.Size(49, 15)
        SerieLabel.TabIndex = 29
        SerieLabel.Text = "Serie :"
        '
        'FECHALabel
        '
        FECHALabel.AutoSize = True
        FECHALabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        FECHALabel.ForeColor = System.Drawing.Color.White
        FECHALabel.Location = New System.Drawing.Point(23, 82)
        FECHALabel.Name = "FECHALabel"
        FECHALabel.Size = New System.Drawing.Size(54, 15)
        FECHALabel.TabIndex = 32
        FECHALabel.Text = "Fecha :"
        '
        'ClienteLabel
        '
        ClienteLabel.AutoSize = True
        ClienteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ClienteLabel.ForeColor = System.Drawing.Color.White
        ClienteLabel.Location = New System.Drawing.Point(8, 105)
        ClienteLabel.Name = "ClienteLabel"
        ClienteLabel.Size = New System.Drawing.Size(69, 15)
        ClienteLabel.TabIndex = 33
        ClienteLabel.Text = "Contrato :"
        '
        'NOMBRELabel
        '
        NOMBRELabel.AutoSize = True
        NOMBRELabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NOMBRELabel.ForeColor = System.Drawing.Color.White
        NOMBRELabel.Location = New System.Drawing.Point(11, 128)
        NOMBRELabel.Name = "NOMBRELabel"
        NOMBRELabel.Size = New System.Drawing.Size(66, 15)
        NOMBRELabel.TabIndex = 34
        NOMBRELabel.Text = "Nombre :"
        '
        'ImporteLabel
        '
        ImporteLabel.AutoSize = True
        ImporteLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ImporteLabel.ForeColor = System.Drawing.Color.White
        ImporteLabel.Location = New System.Drawing.Point(13, 178)
        ImporteLabel.Name = "ImporteLabel"
        ImporteLabel.Size = New System.Drawing.Size(64, 15)
        ImporteLabel.TabIndex = 35
        ImporteLabel.Text = "Importe :"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label5.ForeColor = System.Drawing.Color.White
        Label5.Location = New System.Drawing.Point(20, 199)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(55, 15)
        Label5.TabIndex = 101
        Label5.Text = "Status :"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label10.ForeColor = System.Drawing.Color.White
        Label10.Location = New System.Drawing.Point(67, 123)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(52, 15)
        Label10.TabIndex = 101
        Label10.Text = "Saldo :"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label12.ForeColor = System.Drawing.Color.White
        Label12.Location = New System.Drawing.Point(64, 100)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(55, 15)
        Label12.TabIndex = 35
        Label12.Text = "Monto :"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label14.ForeColor = System.Drawing.Color.White
        Label14.Location = New System.Drawing.Point(56, 74)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(53, 15)
        Label14.TabIndex = 34
        Label14.Text = "Ticket :"
        '
        'Label15
        '
        Label15.AutoSize = True
        Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label15.ForeColor = System.Drawing.Color.White
        Label15.Location = New System.Drawing.Point(50, 48)
        Label15.Name = "Label15"
        Label15.Size = New System.Drawing.Size(69, 15)
        Label15.TabIndex = 33
        Label15.Text = "Contrato :"
        '
        'Label17
        '
        Label17.AutoSize = True
        Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label17.ForeColor = System.Drawing.Color.White
        Label17.Location = New System.Drawing.Point(65, 149)
        Label17.Name = "Label17"
        Label17.Size = New System.Drawing.Size(54, 15)
        Label17.TabIndex = 32
        Label17.Text = "Fecha :"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Label19.ForeColor = System.Drawing.Color.White
        Label19.Location = New System.Drawing.Point(4, 25)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(86, 15)
        Label19.TabIndex = 29
        Label19.Text = "Devolución :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.AutoScroll = True
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FOLIOTextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ComboBox4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel5)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Button7)
        Me.SplitContainer1.Panel1.Controls.Add(Me.FECHATextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SERIETextBox)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CMBLabel1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.BUSCANOTASDECREDITODataGridView)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DataGridView1)
        Me.SplitContainer1.Size = New System.Drawing.Size(836, 719)
        Me.SplitContainer1.SplitterDistance = 278
        Me.SplitContainer1.TabIndex = 23
        Me.SplitContainer1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(126, 243)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 17)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Ej. : dd/mm/aa"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Button3)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Button4)
        Me.Panel5.Controls.Add(Me.CONTRATOTextBox)
        Me.Panel5.Controls.Add(Me.NOMBRETextBox)
        Me.Panel5.Controls.Add(Me.Label7)
        Me.Panel5.Location = New System.Drawing.Point(3, 302)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(272, 169)
        Me.Panel5.TabIndex = 28
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrange
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.Black
        Me.Button3.Location = New System.Drawing.Point(14, 55)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(88, 23)
        Me.Button3.TabIndex = 8
        Me.Button3.Text = "&Buscar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 10)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(69, 15)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Contrato :"
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrange
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.Black
        Me.Button4.Location = New System.Drawing.Point(14, 143)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(88, 23)
        Me.Button4.TabIndex = 10
        Me.Button4.Text = "&Buscar"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'CONTRATOTextBox
        '
        Me.CONTRATOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CONTRATOTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.CONTRATOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CONTRATOTextBox.Location = New System.Drawing.Point(14, 28)
        Me.CONTRATOTextBox.Name = "CONTRATOTextBox"
        Me.CONTRATOTextBox.Size = New System.Drawing.Size(88, 21)
        Me.CONTRATOTextBox.TabIndex = 7
        '
        'NOMBRETextBox
        '
        Me.NOMBRETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NOMBRETextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.NOMBRETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBRETextBox.Location = New System.Drawing.Point(14, 116)
        Me.NOMBRETextBox.Name = "NOMBRETextBox"
        Me.NOMBRETextBox.Size = New System.Drawing.Size(249, 21)
        Me.NOMBRETextBox.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(11, 98)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 15)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Nombre :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel1.Controls.Add(Label5)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.CMBNOMBRETextBox1)
        Me.Panel1.Controls.Add(ImporteLabel)
        Me.Panel1.Controls.Add(Me.ImporteLabel1)
        Me.Panel1.Controls.Add(NOMBRELabel)
        Me.Panel1.Controls.Add(ClienteLabel)
        Me.Panel1.Controls.Add(Me.ClienteLabel1)
        Me.Panel1.Controls.Add(FECHALabel)
        Me.Panel1.Controls.Add(Me.FECHALabel1)
        Me.Panel1.Controls.Add(FacturaLabel)
        Me.Panel1.Controls.Add(Me.FacturaLabel1)
        Me.Panel1.Controls.Add(SerieLabel)
        Me.Panel1.Controls.Add(Me.SerieLabel1)
        Me.Panel1.Controls.Add(Me.Clv_FacturaLabel1)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel1.Location = New System.Drawing.Point(3, 477)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(272, 238)
        Me.Panel1.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Status", True))
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(80, 199)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(180, 15)
        Me.Label9.TabIndex = 102
        '
        'BUSCAFACTURASBindingSource
        '
        Me.BUSCAFACTURASBindingSource.DataMember = "BUSCAFACTURAS"
        Me.BUSCAFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'NewsoftvDataSet1
        '
        Me.NewsoftvDataSet1.DataSetName = "NewsoftvDataSet1"
        Me.NewsoftvDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBNOMBRETextBox1
        '
        Me.CMBNOMBRETextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBNOMBRETextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBNOMBRETextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "NOMBRE", True))
        Me.CMBNOMBRETextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBNOMBRETextBox1.Location = New System.Drawing.Point(83, 131)
        Me.CMBNOMBRETextBox1.Multiline = True
        Me.CMBNOMBRETextBox1.Name = "CMBNOMBRETextBox1"
        Me.CMBNOMBRETextBox1.ReadOnly = True
        Me.CMBNOMBRETextBox1.Size = New System.Drawing.Size(180, 44)
        Me.CMBNOMBRETextBox1.TabIndex = 100
        Me.CMBNOMBRETextBox1.TabStop = False
        '
        'ImporteLabel1
        '
        Me.ImporteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "importe", True))
        Me.ImporteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteLabel1.ForeColor = System.Drawing.Color.White
        Me.ImporteLabel1.Location = New System.Drawing.Point(83, 178)
        Me.ImporteLabel1.Name = "ImporteLabel1"
        Me.ImporteLabel1.Size = New System.Drawing.Size(180, 15)
        Me.ImporteLabel1.TabIndex = 36
        '
        'ClienteLabel1
        '
        Me.ClienteLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "cliente", True))
        Me.ClienteLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClienteLabel1.ForeColor = System.Drawing.Color.White
        Me.ClienteLabel1.Location = New System.Drawing.Point(83, 105)
        Me.ClienteLabel1.Name = "ClienteLabel1"
        Me.ClienteLabel1.Size = New System.Drawing.Size(100, 23)
        Me.ClienteLabel1.TabIndex = 34
        '
        'FECHALabel1
        '
        Me.FECHALabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "FECHA", True))
        Me.FECHALabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHALabel1.ForeColor = System.Drawing.Color.White
        Me.FECHALabel1.Location = New System.Drawing.Point(83, 82)
        Me.FECHALabel1.Name = "FECHALabel1"
        Me.FECHALabel1.Size = New System.Drawing.Size(100, 23)
        Me.FECHALabel1.TabIndex = 33
        '
        'FacturaLabel1
        '
        Me.FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Factura", True))
        Me.FacturaLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturaLabel1.ForeColor = System.Drawing.Color.White
        Me.FacturaLabel1.Location = New System.Drawing.Point(83, 59)
        Me.FacturaLabel1.Name = "FacturaLabel1"
        Me.FacturaLabel1.Size = New System.Drawing.Size(100, 23)
        Me.FacturaLabel1.TabIndex = 32
        '
        'SerieLabel1
        '
        Me.SerieLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "Serie", True))
        Me.SerieLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieLabel1.ForeColor = System.Drawing.Color.White
        Me.SerieLabel1.Location = New System.Drawing.Point(83, 36)
        Me.SerieLabel1.Name = "SerieLabel1"
        Me.SerieLabel1.Size = New System.Drawing.Size(100, 23)
        Me.SerieLabel1.TabIndex = 31
        '
        'Clv_FacturaLabel1
        '
        Me.Clv_FacturaLabel1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCAFACTURASBindingSource, "clv_Factura", True))
        Me.Clv_FacturaLabel1.ForeColor = System.Drawing.Color.DarkOrange
        Me.Clv_FacturaLabel1.Location = New System.Drawing.Point(188, 8)
        Me.Clv_FacturaLabel1.Name = "Clv_FacturaLabel1"
        Me.Clv_FacturaLabel1.Size = New System.Drawing.Size(58, 23)
        Me.Clv_FacturaLabel1.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(4, 4)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(131, 18)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Datos del Ticket"
        '
        'FOLIOTextBox
        '
        Me.FOLIOTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FOLIOTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FOLIOTextBox.Location = New System.Drawing.Point(69, 159)
        Me.FOLIOTextBox.Name = "FOLIOTextBox"
        Me.FOLIOTextBox.Size = New System.Drawing.Size(88, 21)
        Me.FOLIOTextBox.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(14, 161)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 15)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "Folio :"
        '
        'ComboBox4
        '
        Me.ComboBox4.DataSource = Me.MUESTRATIPOFACTURABindingSource
        Me.ComboBox4.DisplayMember = "CONCEPTO"
        Me.ComboBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox4.FormattingEnabled = True
        Me.ComboBox4.Location = New System.Drawing.Point(17, 51)
        Me.ComboBox4.Name = "ComboBox4"
        Me.ComboBox4.Size = New System.Drawing.Size(236, 24)
        Me.ComboBox4.TabIndex = 1
        Me.ComboBox4.ValueMember = "CLAVE"
        '
        'MUESTRATIPOFACTURABindingSource
        '
        Me.MUESTRATIPOFACTURABindingSource.DataMember = "MUESTRATIPOFACTURA"
        Me.MUESTRATIPOFACTURABindingSource.DataSource = Me.DataSetLydia
        '
        'DataSetLydia
        '
        Me.DataSetLydia.DataSetName = "DataSetLydia"
        Me.DataSetLydia.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CMBLabel5
        '
        Me.CMBLabel5.AutoSize = True
        Me.CMBLabel5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel5.Location = New System.Drawing.Point(13, 12)
        Me.CMBLabel5.Name = "CMBLabel5"
        Me.CMBLabel5.Size = New System.Drawing.Size(162, 24)
        Me.CMBLabel5.TabIndex = 0
        Me.CMBLabel5.Text = "Tipo de Ticket  :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrange
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(17, 270)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(88, 23)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "&Buscar"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrange
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.Black
        Me.Button7.Location = New System.Drawing.Point(17, 186)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(88, 23)
        Me.Button7.TabIndex = 4
        Me.Button7.Text = "&Buscar"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'FECHATextBox
        '
        Me.FECHATextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.FECHATextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.FECHATextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHATextBox.Location = New System.Drawing.Point(17, 243)
        Me.FECHATextBox.Name = "FECHATextBox"
        Me.FECHATextBox.Size = New System.Drawing.Size(88, 21)
        Me.FECHATextBox.TabIndex = 5
        '
        'SERIETextBox
        '
        Me.SERIETextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.SERIETextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SERIETextBox.Location = New System.Drawing.Point(69, 132)
        Me.SERIETextBox.Name = "SERIETextBox"
        Me.SERIETextBox.Size = New System.Drawing.Size(88, 21)
        Me.SERIETextBox.TabIndex = 2
        '
        'CMBLabel1
        '
        Me.CMBLabel1.AutoSize = True
        Me.CMBLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBLabel1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.CMBLabel1.Location = New System.Drawing.Point(13, 92)
        Me.CMBLabel1.Name = "CMBLabel1"
        Me.CMBLabel1.Size = New System.Drawing.Size(186, 24)
        Me.CMBLabel1.TabIndex = 0
        Me.CMBLabel1.Text = "Buscar Ticket Por :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 134)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Serie :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(14, 225)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 15)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Fecha :"
        '
        'BUSCANOTASDECREDITODataGridView
        '
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.BUSCANOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.BUSCANOTASDECREDITODataGridView.AutoGenerateColumns = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.BUSCANOTASDECREDITODataGridView.ColumnHeadersHeight = 35
        Me.BUSCANOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn13, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn10, Me.DataGridViewTextBoxColumn7, Me.DataGridViewTextBoxColumn8, Me.DataGridViewTextBoxColumn9, Me.DataGridViewTextBoxColumn11, Me.DataGridViewTextBoxColumn12})
        Me.BUSCANOTASDECREDITODataGridView.DataSource = Me.BUSCANOTASDECREDITOBindingSource
        Me.BUSCANOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 0)
        Me.BUSCANOTASDECREDITODataGridView.Name = "BUSCANOTASDECREDITODataGridView"
        Me.BUSCANOTASDECREDITODataGridView.ReadOnly = True
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.BUSCANOTASDECREDITODataGridView.RowHeadersDefaultCellStyle = DataGridViewCellStyle2
        Me.BUSCANOTASDECREDITODataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.BUSCANOTASDECREDITODataGridView.Size = New System.Drawing.Size(554, 719)
        Me.BUSCANOTASDECREDITODataGridView.TabIndex = 29
        Me.BUSCANOTASDECREDITODataGridView.Visible = False
        '
        'BUSCANOTASDECREDITOBindingSource
        '
        Me.BUSCANOTASDECREDITOBindingSource.DataMember = "BUSCANOTASDECREDITO"
        Me.BUSCANOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.Chocolate
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Status, Me.ClvFacturaDataGridViewTextBoxColumn, Me.SerieDataGridViewTextBoxColumn, Me.FacturaDataGridViewTextBoxColumn, Me.FECHADataGridViewTextBoxColumn, Me.ClienteDataGridViewTextBoxColumn, Me.NOMBREDataGridViewTextBoxColumn, Me.ImporteDataGridViewTextBoxColumn, Me.TipoDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.BUSCAFACTURASBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(554, 719)
        Me.DataGridView1.StandardTab = True
        Me.DataGridView1.TabIndex = 0
        Me.DataGridView1.TabStop = False
        '
        'Status
        '
        Me.Status.DataPropertyName = "Status"
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Status.DefaultCellStyle = DataGridViewCellStyle4
        Me.Status.HeaderText = "Status"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        '
        'ClvFacturaDataGridViewTextBoxColumn
        '
        Me.ClvFacturaDataGridViewTextBoxColumn.DataPropertyName = "clv_Factura"
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClvFacturaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle5
        Me.ClvFacturaDataGridViewTextBoxColumn.HeaderText = "clv_Factura"
        Me.ClvFacturaDataGridViewTextBoxColumn.Name = "ClvFacturaDataGridViewTextBoxColumn"
        Me.ClvFacturaDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClvFacturaDataGridViewTextBoxColumn.Visible = False
        '
        'SerieDataGridViewTextBoxColumn
        '
        Me.SerieDataGridViewTextBoxColumn.DataPropertyName = "Serie"
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SerieDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle6
        Me.SerieDataGridViewTextBoxColumn.HeaderText = "Serie"
        Me.SerieDataGridViewTextBoxColumn.Name = "SerieDataGridViewTextBoxColumn"
        Me.SerieDataGridViewTextBoxColumn.ReadOnly = True
        Me.SerieDataGridViewTextBoxColumn.Width = 70
        '
        'FacturaDataGridViewTextBoxColumn
        '
        Me.FacturaDataGridViewTextBoxColumn.DataPropertyName = "Factura"
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FacturaDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle7
        Me.FacturaDataGridViewTextBoxColumn.HeaderText = "Folio"
        Me.FacturaDataGridViewTextBoxColumn.Name = "FacturaDataGridViewTextBoxColumn"
        Me.FacturaDataGridViewTextBoxColumn.ReadOnly = True
        Me.FacturaDataGridViewTextBoxColumn.Width = 70
        '
        'FECHADataGridViewTextBoxColumn
        '
        Me.FECHADataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.FECHADataGridViewTextBoxColumn.DataPropertyName = "FECHA"
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FECHADataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle8
        Me.FECHADataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FECHADataGridViewTextBoxColumn.Name = "FECHADataGridViewTextBoxColumn"
        Me.FECHADataGridViewTextBoxColumn.ReadOnly = True
        Me.FECHADataGridViewTextBoxColumn.Width = 79
        '
        'ClienteDataGridViewTextBoxColumn
        '
        Me.ClienteDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.ClienteDataGridViewTextBoxColumn.DataPropertyName = "cliente"
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ClienteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle9
        Me.ClienteDataGridViewTextBoxColumn.HeaderText = "Contrato"
        Me.ClienteDataGridViewTextBoxColumn.Name = "ClienteDataGridViewTextBoxColumn"
        Me.ClienteDataGridViewTextBoxColumn.ReadOnly = True
        Me.ClienteDataGridViewTextBoxColumn.Width = 99
        '
        'NOMBREDataGridViewTextBoxColumn
        '
        Me.NOMBREDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        Me.NOMBREDataGridViewTextBoxColumn.DataPropertyName = "NOMBRE"
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NOMBREDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle10
        Me.NOMBREDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NOMBREDataGridViewTextBoxColumn.Name = "NOMBREDataGridViewTextBoxColumn"
        Me.NOMBREDataGridViewTextBoxColumn.ReadOnly = True
        Me.NOMBREDataGridViewTextBoxColumn.Width = 93
        '
        'ImporteDataGridViewTextBoxColumn
        '
        Me.ImporteDataGridViewTextBoxColumn.DataPropertyName = "importe"
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImporteDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle11
        Me.ImporteDataGridViewTextBoxColumn.HeaderText = "Importe"
        Me.ImporteDataGridViewTextBoxColumn.Name = "ImporteDataGridViewTextBoxColumn"
        Me.ImporteDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TipoDataGridViewTextBoxColumn
        '
        Me.TipoDataGridViewTextBoxColumn.DataPropertyName = "tipo"
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TipoDataGridViewTextBoxColumn.DefaultCellStyle = DataGridViewCellStyle12
        Me.TipoDataGridViewTextBoxColumn.HeaderText = "tipo"
        Me.TipoDataGridViewTextBoxColumn.Name = "TipoDataGridViewTextBoxColumn"
        Me.TipoDataGridViewTextBoxColumn.ReadOnly = True
        Me.TipoDataGridViewTextBoxColumn.Visible = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrange
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.Black
        Me.Button5.Location = New System.Drawing.Point(868, 667)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(136, 36)
        Me.Button5.TabIndex = 12
        Me.Button5.Text = "&SALIR"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.Orange
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(3, 11)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 83)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "&Cancelar Ticket"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'BUSCAFACTURASTableAdapter
        '
        Me.BUSCAFACTURASTableAdapter.ClearBeforeFill = True
        '
        'CANCELACIONFACTURASBindingSource
        '
        Me.CANCELACIONFACTURASBindingSource.DataMember = "CANCELACIONFACTURAS"
        Me.CANCELACIONFACTURASBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'CANCELACIONFACTURASTableAdapter
        '
        Me.CANCELACIONFACTURASTableAdapter.ClearBeforeFill = True
        '
        'CMBPanel3
        '
        Me.CMBPanel3.Controls.Add(Me.Button6)
        Me.CMBPanel3.Location = New System.Drawing.Point(854, 12)
        Me.CMBPanel3.Name = "CMBPanel3"
        Me.CMBPanel3.Size = New System.Drawing.Size(144, 90)
        Me.CMBPanel3.TabIndex = 25
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.Orange
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.Black
        Me.Button6.Location = New System.Drawing.Point(3, 3)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(136, 83)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "&ReImprimir Ticket"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'CMBPanel4
        '
        Me.CMBPanel4.Controls.Add(Me.Button8)
        Me.CMBPanel4.Location = New System.Drawing.Point(847, 9)
        Me.CMBPanel4.Name = "CMBPanel4"
        Me.CMBPanel4.Size = New System.Drawing.Size(157, 93)
        Me.CMBPanel4.TabIndex = 26
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.Orange
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.Black
        Me.Button8.Location = New System.Drawing.Point(9, 3)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(136, 83)
        Me.Button8.TabIndex = 11
        Me.Button8.Text = "&Ver Ticket"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'DAMEFECHADELSERVIDORBindingSource
        '
        Me.DAMEFECHADELSERVIDORBindingSource.DataMember = "DAMEFECHADELSERVIDOR"
        Me.DAMEFECHADELSERVIDORBindingSource.DataSource = Me.NewsoftvDataSet1
        '
        'DAMEFECHADELSERVIDORTableAdapter
        '
        Me.DAMEFECHADELSERVIDORTableAdapter.ClearBeforeFill = True
        '
        'FECHADateTimePicker
        '
        Me.FECHADateTimePicker.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.DAMEFECHADELSERVIDORBindingSource, "FECHA", True))
        Me.FECHADateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.FECHADateTimePicker.Location = New System.Drawing.Point(22, 683)
        Me.FECHADateTimePicker.Name = "FECHADateTimePicker"
        Me.FECHADateTimePicker.Size = New System.Drawing.Size(102, 20)
        Me.FECHADateTimePicker.TabIndex = 28
        Me.FECHADateTimePicker.TabStop = False
        '
        'CMBPanel2
        '
        Me.CMBPanel2.Controls.Add(Me.Button2)
        Me.CMBPanel2.Location = New System.Drawing.Point(855, -4)
        Me.CMBPanel2.Name = "CMBPanel2"
        Me.CMBPanel2.Size = New System.Drawing.Size(143, 106)
        Me.CMBPanel2.TabIndex = 24
        '
        'EricDataSet
        '
        Me.EricDataSet.DataSetName = "EricDataSet"
        Me.EricDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CancelaCNRPPEBindingSource
        '
        Me.CancelaCNRPPEBindingSource.DataMember = "CancelaCNRPPE"
        Me.CancelaCNRPPEBindingSource.DataSource = Me.EricDataSet
        '
        'CancelaCNRPPETableAdapter
        '
        Me.CancelaCNRPPETableAdapter.ClearBeforeFill = True
        '
        'DataSetEdgar
        '
        Me.DataSetEdgar.DataSetName = "DataSetEdgar"
        Me.DataSetEdgar.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CancelaCambioServClienteBindingSource
        '
        Me.CancelaCambioServClienteBindingSource.DataMember = "CancelaCambioServCliente"
        Me.CancelaCambioServClienteBindingSource.DataSource = Me.DataSetEdgar
        '
        'CancelaCambioServClienteTableAdapter
        '
        Me.CancelaCambioServClienteTableAdapter.ClearBeforeFill = True
        '
        'Procedimientos_arnoldo
        '
        Me.Procedimientos_arnoldo.DataSetName = "Procedimientos_arnoldo"
        Me.Procedimientos_arnoldo.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Selecciona_impresoraticketsBindingSource
        '
        Me.Selecciona_impresoraticketsBindingSource.DataMember = "Selecciona_impresoratickets"
        Me.Selecciona_impresoraticketsBindingSource.DataSource = Me.Procedimientos_arnoldo
        '
        'Selecciona_impresoraticketsTableAdapter
        '
        Me.Selecciona_impresoraticketsTableAdapter.ClearBeforeFill = True
        '
        'BUSCANOTASDECREDITOTableAdapter
        '
        Me.BUSCANOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'MUESTRATIPOFACTURATableAdapter
        '
        Me.MUESTRATIPOFACTURATableAdapter.ClearBeforeFill = True
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.DarkOrange
        Me.Panel2.Controls.Add(Label10)
        Me.Panel2.Controls.Add(Me.Label11)
        Me.Panel2.Controls.Add(Me.DetalleNOTASDECREDITODataGridView)
        Me.Panel2.Controls.Add(Me.CMBTextBox1)
        Me.Panel2.Controls.Add(Label12)
        Me.Panel2.Controls.Add(Me.Label13)
        Me.Panel2.Controls.Add(Label14)
        Me.Panel2.Controls.Add(Label15)
        Me.Panel2.Controls.Add(Me.Label16)
        Me.Panel2.Controls.Add(Label17)
        Me.Panel2.Controls.Add(Me.Label18)
        Me.Panel2.Controls.Add(Label19)
        Me.Panel2.Controls.Add(Me.Label20)
        Me.Panel2.Controls.Add(Me.Label21)
        Me.Panel2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Panel2.Location = New System.Drawing.Point(12, 486)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(272, 245)
        Me.Panel2.TabIndex = 29
        Me.Panel2.Visible = False
        '
        'Label11
        '
        Me.Label11.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Saldo", True))
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(137, 123)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(107, 23)
        Me.Label11.TabIndex = 102
        '
        'DetalleNOTASDECREDITODataGridView
        '
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToAddRows = False
        Me.DetalleNOTASDECREDITODataGridView.AllowUserToDeleteRows = False
        Me.DetalleNOTASDECREDITODataGridView.AutoGenerateColumns = False
        Me.DetalleNOTASDECREDITODataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1DataGridViewTextBoxColumn, Me.MontoDataGridViewTextBoxColumn, Me.FECHAdegeneracionDataGridViewTextBoxColumn, Me.ClvNotadecreditoDataGridViewTextBoxColumn})
        Me.DetalleNOTASDECREDITODataGridView.DataSource = Me.DetalleNOTASDECREDITOBindingSource
        Me.DetalleNOTASDECREDITODataGridView.Location = New System.Drawing.Point(0, 172)
        Me.DetalleNOTASDECREDITODataGridView.Name = "DetalleNOTASDECREDITODataGridView"
        Me.DetalleNOTASDECREDITODataGridView.ReadOnly = True
        Me.DetalleNOTASDECREDITODataGridView.Size = New System.Drawing.Size(273, 126)
        Me.DetalleNOTASDECREDITODataGridView.TabIndex = 47
        '
        'DetalleNOTASDECREDITOBindingSource
        '
        Me.DetalleNOTASDECREDITOBindingSource.DataMember = "DetalleNOTASDECREDITO"
        Me.DetalleNOTASDECREDITOBindingSource.DataSource = Me.DataSetLydia
        '
        'CMBTextBox1
        '
        Me.CMBTextBox1.BackColor = System.Drawing.Color.DarkOrange
        Me.CMBTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CMBTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "Column1", True))
        Me.CMBTextBox1.ForeColor = System.Drawing.Color.White
        Me.CMBTextBox1.Location = New System.Drawing.Point(140, 74)
        Me.CMBTextBox1.Multiline = True
        Me.CMBTextBox1.Name = "CMBTextBox1"
        Me.CMBTextBox1.ReadOnly = True
        Me.CMBTextBox1.Size = New System.Drawing.Size(107, 23)
        Me.CMBTextBox1.TabIndex = 100
        Me.CMBTextBox1.TabStop = False
        '
        'Label13
        '
        Me.Label13.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "monto", True))
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(140, 100)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(107, 23)
        Me.Label13.TabIndex = 36
        '
        'Label16
        '
        Me.Label16.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "contrato", True))
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.White
        Me.Label16.Location = New System.Drawing.Point(140, 48)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(107, 23)
        Me.Label16.TabIndex = 34
        '
        'Label18
        '
        Me.Label18.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "FECHA_degeneracion", True))
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.Color.White
        Me.Label18.Location = New System.Drawing.Point(140, 146)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(127, 23)
        Me.Label18.TabIndex = 33
        '
        'Label20
        '
        Me.Label20.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.BUSCANOTASDECREDITOBindingSource, "clv_Notadecredito", True))
        Me.Label20.ForeColor = System.Drawing.Color.DarkOrange
        Me.Label20.Location = New System.Drawing.Point(137, 23)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(107, 23)
        Me.Label20.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.White
        Me.Label21.Location = New System.Drawing.Point(4, 3)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(183, 18)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Datos de la Devolución"
        '
        'DetalleNOTASDECREDITOTableAdapter
        '
        Me.DetalleNOTASDECREDITOTableAdapter.ClearBeforeFill = True
        '
        'EricDataSet2
        '
        Me.EricDataSet2.DataSetName = "EricDataSet2"
        Me.EricDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DameGeneralMsjTicketsBindingSource
        '
        Me.DameGeneralMsjTicketsBindingSource.DataMember = "DameGeneralMsjTickets"
        Me.DameGeneralMsjTicketsBindingSource.DataSource = Me.EricDataSet2
        '
        'DameGeneralMsjTicketsTableAdapter
        '
        Me.DameGeneralMsjTicketsTableAdapter.ClearBeforeFill = True
        '
        'ValidaCancelacionFacturaBindingSource
        '
        Me.ValidaCancelacionFacturaBindingSource.DataMember = "ValidaCancelacionFactura"
        Me.ValidaCancelacionFacturaBindingSource.DataSource = Me.EricDataSet2
        '
        'ValidaCancelacionFacturaTableAdapter
        '
        Me.ValidaCancelacionFacturaTableAdapter.ClearBeforeFill = True
        '
        'Column1DataGridViewTextBoxColumn
        '
        Me.Column1DataGridViewTextBoxColumn.DataPropertyName = "Column1"
        Me.Column1DataGridViewTextBoxColumn.HeaderText = "Ticket"
        Me.Column1DataGridViewTextBoxColumn.Name = "Column1DataGridViewTextBoxColumn"
        Me.Column1DataGridViewTextBoxColumn.ReadOnly = True
        '
        'MontoDataGridViewTextBoxColumn
        '
        Me.MontoDataGridViewTextBoxColumn.DataPropertyName = "monto"
        Me.MontoDataGridViewTextBoxColumn.HeaderText = "Monto"
        Me.MontoDataGridViewTextBoxColumn.Name = "MontoDataGridViewTextBoxColumn"
        Me.MontoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FECHAdegeneracionDataGridViewTextBoxColumn
        '
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.DataPropertyName = "FECHA_degeneracion"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.HeaderText = "Fecha"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.Name = "FECHAdegeneracionDataGridViewTextBoxColumn"
        Me.FECHAdegeneracionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ClvNotadecreditoDataGridViewTextBoxColumn
        '
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.DataPropertyName = "clv_Notadecredito"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.HeaderText = "Folio de la nota"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.Name = "ClvNotadecreditoDataGridViewTextBoxColumn"
        Me.ClvNotadecreditoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "clv_Notadecredito"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Devolución"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "contrato"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Contrato"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Column1"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Factura"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "monto"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Monto"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Saldo"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Saldo"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "FECHA_degeneracion"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Fecha"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Status"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Status"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "observaciones"
        Me.DataGridViewTextBoxColumn10.HeaderText = "observaciones"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Visible = False
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Usuario_captura"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Usuario_captura"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Visible = False
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Usuario_autorizo"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Usuario_autorizo"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Visible = False
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "fecha_caducidad"
        Me.DataGridViewTextBoxColumn9.HeaderText = "fecha_caducidad"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Visible = False
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "MotCan"
        Me.DataGridViewTextBoxColumn11.HeaderText = "MotCan"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Visible = False
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Clv_Factura_Aplicada"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Clv_Factura_Aplicada"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Visible = False
        '
        'BrwFacturas_Cancelar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1016, 734)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.CMBPanel2)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.FECHADateTimePicker)
        Me.Controls.Add(Me.CMBPanel4)
        Me.Controls.Add(Me.CMBPanel3)
        Me.MaximizeBox = False
        Me.Name = "BrwFacturas_Cancelar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cancelación de Facturas"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.BUSCAFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NewsoftvDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MUESTRATIPOFACTURABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetLydia, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCANOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BUSCANOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CANCELACIONFACTURASBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel3.ResumeLayout(False)
        Me.CMBPanel4.ResumeLayout(False)
        CType(Me.DAMEFECHADELSERVIDORBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.CMBPanel2.ResumeLayout(False)
        CType(Me.EricDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CancelaCNRPPEBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEdgar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CancelaCambioServClienteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Procedimientos_arnoldo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Selecciona_impresoraticketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.DetalleNOTASDECREDITODataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DetalleNOTASDECREDITOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EricDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DameGeneralMsjTicketsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ValidaCancelacionFacturaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents ComboBox4 As System.Windows.Forms.ComboBox
    Friend WithEvents CMBLabel5 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents FECHATextBox As System.Windows.Forms.TextBox
    Friend WithEvents SERIETextBox As System.Windows.Forms.TextBox
    Friend WithEvents CMBLabel1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents FOLIOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents NOMBRETextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CONTRATOTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents NewsoftvDataSet1 As softvFacturacion.NewsoftvDataSet1
    Friend WithEvents BUSCAFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCAFACTURASTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.BUSCAFACTURASTableAdapter
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ClienteLabel1 As System.Windows.Forms.Label
    Friend WithEvents FECHALabel1 As System.Windows.Forms.Label
    Friend WithEvents FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents SerieLabel1 As System.Windows.Forms.Label
    Friend WithEvents Clv_FacturaLabel1 As System.Windows.Forms.Label
    Friend WithEvents CMBNOMBRETextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ImporteLabel1 As System.Windows.Forms.Label
    Friend WithEvents CANCELACIONFACTURASBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CANCELACIONFACTURASTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.CANCELACIONFACTURASTableAdapter
    Friend WithEvents CMBPanel3 As System.Windows.Forms.Panel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CMBPanel4 As System.Windows.Forms.Panel
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents DAMEFECHADELSERVIDORBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMEFECHADELSERVIDORTableAdapter As softvFacturacion.NewsoftvDataSet1TableAdapters.DAMEFECHADELSERVIDORTableAdapter
    Friend WithEvents FECHADateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents CMBPanel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents EricDataSet As softvFacturacion.EricDataSet
    Friend WithEvents CancelaCNRPPEBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CancelaCNRPPETableAdapter As softvFacturacion.EricDataSetTableAdapters.CancelaCNRPPETableAdapter
    Friend WithEvents DataSetEdgar As softvFacturacion.DataSetEdgar
    Friend WithEvents CancelaCambioServClienteBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CancelaCambioServClienteTableAdapter As softvFacturacion.DataSetEdgarTableAdapters.CancelaCambioServClienteTableAdapter
    Friend WithEvents Procedimientos_arnoldo As softvFacturacion.Procedimientos_arnoldo
    Friend WithEvents Selecciona_impresoraticketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Selecciona_impresoraticketsTableAdapter As softvFacturacion.Procedimientos_arnoldoTableAdapters.Selecciona_impresoraticketsTableAdapter
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvFacturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SerieDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FacturaDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClienteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NOMBREDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ImporteDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataSetLydia As softvFacturacion.DataSetLydia
    Friend WithEvents BUSCANOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BUSCANOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.BUSCANOTASDECREDITOTableAdapter
    Friend WithEvents BUSCANOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents MUESTRATIPOFACTURABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MUESTRATIPOFACTURATableAdapter As softvFacturacion.DataSetLydiaTableAdapters.MUESTRATIPOFACTURATableAdapter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITODataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CMBTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents DetalleNOTASDECREDITOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DetalleNOTASDECREDITOTableAdapter As softvFacturacion.DataSetLydiaTableAdapters.DetalleNOTASDECREDITOTableAdapter
    Friend WithEvents EricDataSet2 As softvFacturacion.EricDataSet2
    Friend WithEvents DameGeneralMsjTicketsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DameGeneralMsjTicketsTableAdapter As softvFacturacion.EricDataSet2TableAdapters.DameGeneralMsjTicketsTableAdapter
    Friend WithEvents ValidaCancelacionFacturaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ValidaCancelacionFacturaTableAdapter As softvFacturacion.EricDataSet2TableAdapters.ValidaCancelacionFacturaTableAdapter
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents MontoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FECHAdegeneracionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ClvNotadecreditoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
