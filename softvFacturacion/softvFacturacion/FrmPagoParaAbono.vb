Imports System.Data.SqlClient
Public Class FrmPagoParaAbono

    Private Sub InsertaPagoParaAbonoACuenta(ByVal Clv_Session As Long, ByVal Importe As Double)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPagoParaAbonarACuenta", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Importe", SqlDbType.Decimal)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Importe
        comando.Parameters.Add(parametro2)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conexion.Close()
            conexion.Dispose()
        End Try

    End Sub
    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        e.KeyChar = Chr((ValidaKey(Me.TextBox1, Asc(LCase(e.KeyChar)), "L")))
        If Asc(e.KeyChar) = 13 Then
            Boton_Aceptar()
        End If
    End Sub

    Private Sub Boton_Aceptar()
        If IsNumeric(Me.TextBox1.Text) = False Then
            MsgBox("Captura el Importe.", MsgBoxStyle.Exclamation)
            Exit Sub
        End If
        InsertaPagoParaAbonoACuenta(GloTelClv_Session, Me.TextBox1.Text)
        GloAbonoACuenta = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Boton_Aceptar()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub FrmPagoParaAbono_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class