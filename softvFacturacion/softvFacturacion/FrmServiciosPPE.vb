Imports System.Data.SqlClient

Public Class FrmServiciosPPE


    Private Sub FrmServiciosPPE_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Today, 0)
        eFechaServidor = Today
        Me.DAMEFECHADELSERVIDORTableAdapter.Connection = CON
        Me.DAMEFECHADELSERVIDORTableAdapter.Fill(Me.EricDataSet.DAMEFECHADELSERVIDOR, eFechaServidor)
        CON.Close()
        Me.DateTimePicker1.Value = eFechaServidor
        Me.DateTimePicker1.MinDate = eFechaServidor
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, Me.TextBox1.Text, "", "", Today, 1)
        CON.Close()
    End Sub

    Private Sub TextBox1_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox1.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, Me.TextBox1.Text, "", "", Today, 1)
            CON.Close()
        End If
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", Me.TextBox2.Text, "", Today, 2)
        CON.Close()
    End Sub

    Private Sub TextBox2_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox2.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", Me.TextBox2.Text, "", Today, 2)
            CON.Close()
        End If
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", Me.TextBox3.Text, Today, 3)
        CON.Close()
    End Sub

    Private Sub TextBox3_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TextBox3.KeyPress
        If Asc(e.KeyChar) = 13 Then
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", Me.TextBox3.Text, Today, 3)
            CON.Close()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim Res As Integer = 0
        If Me.MuestraProgramacionPPEDataGridView.RowCount > 0 Then
            Res = MsgBox("Haz Seleccionado " & Me.DescripcionLabel1.Text & " a transmitirse el d�a " & Me.FechaLabel1.Text & ". ��sto es correcto?", MsgBoxStyle.YesNo, "Atenci�n")
            If Res = 6 Then
                eClv_Progra = Me.Clv_PrograTextBox.Text
                eClv_Txt = Me.Clv_TxtLabel1.Text
                GloBndExt = True
                eBndPPE = True
                GloClv_Txt = Me.Clv_TxtTextBox.Text
                Me.Close()
            End If
        Else
            MsgBox("No hay una Programaci�n para �ste PPE.", , "Atenci�n")
        End If
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Close()
    End Sub

    Private Sub Clv_TxtTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Clv_TxtTextBox.TextChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraProgramacionPPETableAdapter.Connection = CON
        Me.MuestraProgramacionPPETableAdapter.Fill(Me.EricDataSet.MuestraProgramacionPPE, 0, Me.Clv_TxtTextBox.Text, Today, 1)
        CON.Close()
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosPPETableAdapter.Connection = CON
            Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Me.DateTimePicker1.Value, 4)
            CON.Close()
        Catch
            MsgBox("Teclea una Fecha V�lida.", , "Error")
        End Try
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MuestraServiciosPPETableAdapter.Connection = CON
        Me.MuestraServiciosPPETableAdapter.Fill(Me.EricDataSet.MuestraServiciosPPE, "", "", "", Me.DateTimePicker1.Value, 4)
        CON.Close()
    End Sub
End Class